export const SAVE_LOGIN_INFO = 'save_login_info';
export const LOGIN = 'login';
export const SAVE_INITIAL_PICK_UP_INFO = 'save_initial_pick_up_info';
export const INITIAL_PICK_UP_INFO = 'initial_pick_up_info';
export const SAVE_ORIGIN = 'save_origin';
export const ORIGIN_INFO = 'origin_info';
export const SAVE_DESTINATION = 'save_destination';
export const DESTINATION_INFO = 'destination_info';
export const SAVE_DATE_TIME = 'save_date_time';
export const DATE_TIME_INFO = 'date_time_info';
export const SAVE_VACANT_SEATS = 'save_vacant_seats';
export const VACANT_SEATS_INFO = 'vacant_seats_info';

export const SAVE_INITIAL_SEARCH_INFO = 'save_initial_search_info';
export const INITIAL_SEARCH_INFO = 'initial_search_info';
export const SAVE_SEARCH_ORIGIN = 'save_search_origin';
export const SEARCH_ORIGIN_INFO = 'search_origin_info';
export const SAVE_SEARCH_DESTINATION = 'save_search_destination';
export const SEARCH_DESTINATION_INFO = 'search_destination_info';
export const SAVE_SEARCH_DATE_TIME = 'save_search_date_time';
export const SEARCH_DATE_TIME_INFO = 'search_date_time_info';
export const SAVE_NEAR_BY = 'save_near_by';
export const NEAR_BY_INFO = 'near_by_info';
export const SAVE_FILTER = 'save_filter';
export const FILTER_INFO = 'filter_info';

export const SAVE_VEHICLE_INFO = 'save_vehcile_info';
export const VEHCILE = 'vehcile';

export const SAVE_BANK_INFO = 'save_bank_info';
export const BANK = 'bank';

export const SAVE_RIDE_PAYMENT = 'save_ride_payment';
export const RIDE_INFO = 'ride_info';

export const SAVE_NOTIFCATIONS = 'save_notifications';
export const NOTIFCATIONS_INFO = 'notificatiosn_info';