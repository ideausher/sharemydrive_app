import { SAVE_LOGIN_INFO, SAVE_DATE_TIME, SAVE_ORIGIN, SAVE_DESTINATION, SAVE_VACANT_SEATS, SAVE_VEHICLE_INFO, SAVE_SEARCH_ORIGIN, SAVE_SEARCH_DESTINATION, SAVE_SEARCH_DATE_TIME, SAVE_INITIAL_PICK_UP_INFO, SAVE_INITIAL_SEARCH_INFO, SAVE_NEAR_BY, SAVE_FILTER, SAVE_BANK_INFO, SAVE_RIDE_PAYMENT, SAVE_NOTIFCATIONS} from './Constants';

export const saveLoginInfo = (data: string) => ({
  type: SAVE_LOGIN_INFO,
  payload: data,
});

export const saveVehcileInfo = (data: string) => ({
  type: SAVE_VEHICLE_INFO,
  payload: data,
});

export const saveBankInfo = (data: string) => ({
  type: SAVE_BANK_INFO,
  payload: data,
});

export const saveInitialPickupInfo = (data: any) => ({
  type: SAVE_INITIAL_PICK_UP_INFO,
  payload: data,
});

export const saveOriginInfo = (data: any) => ({
  type: SAVE_ORIGIN,
  payload: data,
});

export const saveDestinationInfo = (data: any) => ({
  type: SAVE_DESTINATION,
  payload: data,
});

export const saveBookingDateTimeInfo = (data: string) => ({
  type: SAVE_DATE_TIME,
  payload: data,
});

export const saveVacantInfo = (data: string) => ({
  type: SAVE_VACANT_SEATS,
  payload: data,
});

export const saveInitialSearchInfo = (data: any) => ({
  type: SAVE_INITIAL_SEARCH_INFO,
  payload: data,
});

export const saveSearchOriginInfo = (data: any) => ({
  type: SAVE_SEARCH_ORIGIN,
  payload: data,
});

export const saveSearchDestinationInfo = (data: any) => ({
  type: SAVE_SEARCH_DESTINATION,
  payload: data,
});

export const saveSearchDateTimeInfo = (data: string) => ({
  type: SAVE_SEARCH_DATE_TIME,
  payload: data,
});

export const saveNearByInfo = (data: string) => ({
  type: SAVE_NEAR_BY,
  payload: data,
});

export const saveFilterInfo = (data: string) => ({
  type: SAVE_FILTER,
  payload: data,
});


export const saveRidePaymentInfo = (data: string) => ({
  type: SAVE_RIDE_PAYMENT,
  payload: data,
});

export const saveNotificationsInfo = (data: string) => ({
  type: SAVE_NOTIFCATIONS,
  payload: data,
});