import { createStore, applyMiddleware } from 'redux';
import combineReducer from './Reducers'
import rootSaga from './Saga';
import createSagaMiddleware from 'redux-saga'

const sagaMiddleWare = createSagaMiddleware();
const store = createStore(combineReducer, applyMiddleware(sagaMiddleWare));
sagaMiddleWare.run(rootSaga);

export default store;