import * as Actions from './Constants';
import { combineReducers } from 'redux';

const INITIAL_STATE = {
    id: '',
    token: '',
    firstName: '',
    lastName: '',
    email: '',
    phoneNumber: '',
    countryCode: '',
    gender: -1,
    image: '',
    dob: '',
    referralCode: '',
    referralText: '',
    isNotification: '',
    bio: '',
    suggestedPrice: 0.40,
    isVehicleInfoAdded: 0,
    isBankDetailsAdded: 0,
    address: []
}

const saveLoginInfo = (state = INITIAL_STATE, action: any) => {

    switch (action.type) {
        case Actions.SAVE_LOGIN_INFO:

            console.log('Login Info Saved >>>', action.payload)
            return {
                ...state,
                id: action.payload.id,
                token: action.payload.token,
                firstName: action.payload.firstname,
                lastName: action.payload.lastname,
                email: action.payload.email,
                phoneNumber: action.payload.phone_number,
                countryCode: action.payload.phone_country_code,
                gender: (action.payload.gender != undefined ? action.payload.gender : -1),
                image: (action.payload.image != undefined ? action.payload.image : ''),
                dob: (action.payload.date_of_birth != undefined ? action.payload.date_of_birth : ''),
                referralCode: action.payload.referral_code,
                referralText: action.payload.referral_code_text,
                isNotification: action.payload.is_notification,
                bio: (action.payload.bio != undefined ? action.payload.bio : ''),
                suggestedPrice: (action.payload.sug_price_value != undefined ? action.payload.sug_price_value : 0.40),
                isVehicleInfoAdded: (action.payload.is_vehicle_added != undefined ? action.payload.is_vehicle_added : 0),
                isBankDetailsAdded: (action.payload.is_bank_detail_added != undefined ? action.payload.is_bank_detail_added : false),
                address: action.payload.address
            }
        default:
            return state;
    }
}

const VEHICLE_INITIAL_STATE = {
    //country: '',
    rego: '',
    carManufacture: '',
    carModel: '',
    carType: '',
    carColor: '',
    registerYear: '',
    registerState: '',
    userId: '',
    updatedAt: '',
    createdAt: '',
}

const saveVehicleInfo = (state = VEHICLE_INITIAL_STATE, action: any) => {

    switch (action.type) {
        case Actions.SAVE_VEHICLE_INFO:

            console.log('Vehicle Info Saved >>>', action.payload)
            return {
                ...state,
               // country: action.payload.country,
                rego: action.payload.rego,
                carManufacture: action.payload.car_manufacture,
                carModel: action.payload.car_model,
                carType: action.payload.car_type,
                carColor: action.payload.car_color,
                registerYear: action.payload.register_year,
                registerState: action.payload.register_state,
                userId: action.payload.user_id,
                updatedAt: action.payload.updated_at,
                createdAt: action.payload.created_at,
            }
        default:
            return state;
    }
}

const BANK_INITIAL_STATE = {
    accountHolderName: '',
    accountNum: '',
   // bankName: '',
   bsb_no: '',
}

const saveBankInfo = (state = BANK_INITIAL_STATE, action: any) => {

    switch (action.type) {
        case Actions.SAVE_BANK_INFO:

            console.log('Bank Info Saved >>>', action.payload)
            return {
                ...state,
                accountHolderName: action.payload.details.account_holder_name,
                accountNum: action.payload.details.account_number,
               // bankName: action.payload.details.bank_name,
               bsb_no: action.payload.details.bsb_no,
            }
        default:
            return state;
    }
}

const PICK_UP_INITIAL_STATE = {
    origin: '',
    destination: '',
    date: '',
    time: '',
    vacantSeats: ''
}

const savePickUpInfo = (state = PICK_UP_INITIAL_STATE, action: any) => {
    switch (action.type) {

        case Actions.SAVE_INITIAL_PICK_UP_INFO:
            console.log('saveInitialPickUpInfo func clled with payload >>', action.payload)
            return {
                origin: '',
                destination: '',
                date: '',
                time: '',
                vacantSeats: ''
            }
        case Actions.SAVE_ORIGIN:
            console.log('savePickUpInfo func clled with payload >>', action.payload)
            return {
                ...state,
                origin: action.payload,
            }
        case Actions.SAVE_DESTINATION:
            return {
                ...state,
                destination: action.payload,
            }
        case Actions.SAVE_DATE_TIME:
            console.log('SAVE_DATE_TIME func clled with payload >>', action.payload)
            return {
                ...state,
                date: action.payload.date,
                time: action.payload.time,
            }
        case Actions.SAVE_VACANT_SEATS:
            console.log('SAVE_VACANT_SEATS func clled with payload >>', action.payload)
            return {
                ...state,
                vacantSeats: action.payload,
            }
        default:
            return state;
    }
}

const SEARCH_INITIAL_STATE = {
    origin: '',
    destination: '',
    date: '',
    time: '',
    vacantSeats: '',
    isNearByEnabled: false,
    bestReviews: false,
    availabilitySeats: false,
    priceMin: 0,
    priceMax: 1000,
    gender: ''
}

const saveSearchInfo = (state = SEARCH_INITIAL_STATE, action: any) => {
    switch (action.type) {

        case Actions.SAVE_INITIAL_SEARCH_INFO:
            console.log('saveInitialSearchInfo func clled with payload >>', action.payload)
            return {
                origin: '',
                destination: '',
                date: '',
                time: '',
                vacantSeats: '',
                isNearByEnabled: false,
                bestReviews: false,
                availabilitySeats: false,
                priceMin: 0,
                priceMax: 1000,
                gender: ''
            }
        case Actions.SAVE_SEARCH_ORIGIN:
            console.log('saveSearchInfo func clled with payload >>', action.payload)
            return {
                ...state,
                origin: action.payload,
            }
        case Actions.SAVE_SEARCH_DESTINATION:
            return {
                ...state,
                destination: action.payload,
            }
        case Actions.SAVE_SEARCH_DATE_TIME:
            console.log('SAVE_DATE_TIME func clled with payload >>', action.payload)
            return {
                ...state,
                date: action.payload.date,
                time: action.payload.time,
            }
        case Actions.SAVE_NEAR_BY:
            console.log('SAVE_NEAR_BY func clled with payload >>', action.payload)
            return {
                ...state,
                isNearByEnabled: action.payload,
            }
        case Actions.SAVE_FILTER:
            console.log('SAVE_FILTER func clled with payload >>', action.payload)
            return {
                ...state,
                bestReviews: action.payload.bestReviews,
                availabilitySeats: action.payload.availabilitySeats,
                priceMin: action.payload.priceMin,
                priceMax: action.payload.priceMax,
                gender: action.payload.gender
            }
        default:
            return state;
    }
}

const RIDE_PAYMENT_INITIAL_STATE = {
    rideId: 0,
    amount: 0,
    showPaymentPopup: false
}

const saveRidePaymentInfo = (state = RIDE_PAYMENT_INITIAL_STATE, action: any) => {

    switch (action.type) {
        case Actions.SAVE_RIDE_PAYMENT:

            console.log('Ride payment  Info Saved >>>', action.payload)
            return {
                ...state,
                rideId: action.payload.rideId,
                amount: action.payload.amount,
                showPaymentPopup: action.payload.showPaymentPopup,
            }
        default:
            return state;
    }
}

const Notifications_INITIAL_STATE = {
    highlight: false,
}

const saveNotificationsInfo = (state = Notifications_INITIAL_STATE, action: any) => {

    switch (action.type) {
        case Actions.SAVE_NOTIFCATIONS:

            console.log('NOTIFICATIONS  Info Saved >>>', action.payload)
            return {
                ...state,
                highlight: action.payload
            }
        default:
            return state;
    }
}


export default combineReducers({
    saveLoginInfo: saveLoginInfo,
    savePickUpInfo: savePickUpInfo,
    saveVehicleInfo: saveVehicleInfo,
    saveSearchInfo: saveSearchInfo,
    saveBankInfo: saveBankInfo,
    saveRidePaymentInfo: saveRidePaymentInfo,
    saveNotificationsInfo: saveNotificationsInfo
});
