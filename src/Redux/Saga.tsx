
import { put, takeLatest } from 'redux-saga/effects'
import { LOGIN, DATE_TIME_INFO, ORIGIN_INFO, DESTINATION_INFO, VACANT_SEATS_INFO, VEHCILE, SEARCH_ORIGIN_INFO, SEARCH_DESTINATION_INFO, SEARCH_DATE_TIME_INFO, INITIAL_PICK_UP_INFO, INITIAL_SEARCH_INFO, NEAR_BY_INFO, FILTER_INFO, BANK, RIDE_INFO, NOTIFCATIONS_INFO } from './Constants';
import { saveLoginInfo, saveVehcileInfo, saveBookingDateTimeInfo, saveOriginInfo, saveDestinationInfo, saveVacantInfo, saveSearchOriginInfo, saveSearchDestinationInfo, saveSearchDateTimeInfo, saveInitialPickupInfo, saveInitialSearchInfo, saveNearByInfo, saveFilterInfo, saveBankInfo, saveRidePaymentInfo, saveNotificationsInfo } from './UsersAction';

function* loginUserToApp(data: any) {
  yield put(saveLoginInfo(data.payload));
}

function* setVehcileInfo(data: any) {
  console.log('RESPONSE SAGA setVehcileInfo: ', data.payload)
  yield put(saveVehcileInfo(data.payload));
}

function* setBankInfo(data: any) {
  console.log('RESPONSE SAGA setBankInfo: ', data.payload)
  yield put(saveBankInfo(data.payload));
}

function* setInitialPickupInfo(data: any) {
  console.log('RESPONSE SAGA setInitialPickupInfo: ', data.payload)
  yield put(saveInitialPickupInfo(data.payload));
}

function* setOriginInfo(data: any) {
  console.log('RESPONSE SAGA setOriginInfo: ', data.payload)
  yield put(saveOriginInfo(data.payload));
}

function* setDestinationInfo(data: any) {
  console.log('RESPONSE SAGA setDestinationInfo: ', data.payload)
  yield put(saveDestinationInfo(data.payload));
}

function* setBookingDateTimeInfo(data: any) {
  console.log('RESPONSE SAGA setBookingDateTimeInfo: ', data.payload)
  yield put(saveBookingDateTimeInfo(data.payload));
}

function* setVacantSeatsInfo(data: any) {
  console.log('RESPONSE SAGA setVacantSeatsInfo: ', data.payload)
  yield put(saveVacantInfo(data.payload));
}

function* setInitialSearchInfo(data: any) {
  console.log('RESPONSE SAGA setInitialSearchInfo: ', data.payload)
  yield put(saveInitialSearchInfo(data.payload));
}

function* setSearchOriginInfo(data: any) {
  console.log('RESPONSE SAGA setSearchOriginInfo: ', data.payload)
  yield put(saveSearchOriginInfo(data.payload));
}

function* setSearchDestinationInfo(data: any) {
  console.log('RESPONSE SAGA setSearchDestinationInfo: ', data.payload)
  yield put(saveSearchDestinationInfo(data.payload));
}

function* setSearchDateTimeInfo(data: any) {
  console.log('RESPONSE SAGA setSearchDateTimeInfo: ', data.payload)
  yield put(saveSearchDateTimeInfo(data.payload));
}

function* setNearByInfo(data: any) {
  console.log('RESPONSE SAGA setNearByInfo: ', data.payload)
  yield put(saveNearByInfo(data.payload));
}

function* setFilterInfo(data: any) {
  console.log('RESPONSE SAGA setFilterInfo: ', data.payload)
  yield put(saveFilterInfo(data.payload));
}

function* setRidePaymentInfo(data: any) {
  console.log('RESPONSE SAGA setFilterInfo: ', data.payload)
  yield put(saveRidePaymentInfo(data.payload));
}

function* setNotificationsInfo(data: any) {
  console.log('RESPONSE SAGA setNotificationsInfo: ', data.payload)
  yield put(saveNotificationsInfo(data.payload));
}

export default function* rootSaga(data: any) {
  yield takeLatest(LOGIN, loginUserToApp);
  yield takeLatest(VEHCILE, setVehcileInfo);
  yield takeLatest(BANK, setBankInfo);
  yield takeLatest(INITIAL_PICK_UP_INFO, setInitialPickupInfo);
  yield takeLatest(ORIGIN_INFO, setOriginInfo);
  yield takeLatest(DESTINATION_INFO, setDestinationInfo);
  yield takeLatest(DATE_TIME_INFO, setBookingDateTimeInfo);
  yield takeLatest(VACANT_SEATS_INFO, setVacantSeatsInfo);
  yield takeLatest(INITIAL_SEARCH_INFO, setInitialSearchInfo);
  yield takeLatest(SEARCH_ORIGIN_INFO, setSearchOriginInfo);
  yield takeLatest(SEARCH_DESTINATION_INFO, setSearchDestinationInfo);
  yield takeLatest(SEARCH_DATE_TIME_INFO, setSearchDateTimeInfo);
  yield takeLatest(NEAR_BY_INFO, setNearByInfo);
  yield takeLatest(FILTER_INFO, setFilterInfo);
  yield takeLatest(RIDE_INFO, setRidePaymentInfo);
  yield takeLatest(NOTIFCATIONS_INFO, setNotificationsInfo);
}
