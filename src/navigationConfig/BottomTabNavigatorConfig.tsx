import { createBottomTabNavigator } from 'react-navigation-tabs';
import { SearchStack, PickUpStack, HomeStack, ChatsStack, ProfileStack } from './StackConfig'
import React from 'react';
import { Image, View, Platform } from 'react-native';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_SHADOW_COLOR } from '../constants/colors';

export const AppBottomTabNavigator = createBottomTabNavigator({
  Search: {
    screen: SearchStack,

    navigationOptions: {
      tabBarLabel: <View />,
      tabBarIcon: ({ tintColor, focused }) => (<View style={styles.tabStyle}>
        <Image
          source={focused ? require('../assets/search_blue.png') : require('../assets/search_grey.png')}
          style={styles.tabImage}
        />
      </View>)
    },
  },
  PickUp: {
    screen: PickUpStack,
    navigationOptions: {
      tabBarLabel: <View />,
      tabBarIcon: ({ tintColor, focused }) => (<View style={styles.tabStyle}>
        <Image
          source={focused ? require('../assets/pickup_blue.png') : require('../assets/pickup_grey.png')}
          style={styles.tabImage}
        />
      </View>
      )
    },
  },
  Home: {
    screen: HomeStack,
    navigationOptions: {
      tabBarLabel: <View />,
      tabBarIcon: ({ tintColor, focused }) => (<View style={styles.tabStyle}>
        <Image
          source={focused ? require('../assets/car_blue.png') : require('../assets/car_grey.png')}
          style={styles.carImage}
        />
      </View>)
    },
  },
  Chats: {
    screen: ChatsStack,
    navigationOptions: {
      tabBarLabel: <View />,
      tabBarIcon: ({ tintColor, focused }) => (<View style={styles.tabStyle}>
        <Image
          source={focused ? require('../assets/chat_blue.png') : require('../assets/chat_grey.png')}
          style={styles.tabImage}
        />
      </View>)
    },
  },
  Profile: {
    screen: ProfileStack,
    navigationOptions: {
      tabBarLabel: <View />,
      tabBarIcon: ({ tintColor, focused }) => (<View style={styles.tabStyle}>
        <Image
          source={focused ? require('../assets/profile_blue.png') : require('../assets/profile_grey.png')}
          style={styles.tabImage}
        />
      </View>)
    },
  }
}, {
  order: ['Search', 'PickUp', 'Home', 'Chats', 'Profile'],
  tabBarOptions: {
    style: {
      backgroundColor: 'white',
      height: 50,
      borderWidth: 0,
      borderTopColor: "transparent",
      ...Platform.select({
        ios: {
          shadowColor: APP_SHADOW_COLOR,
          shadowOffset: { height: 0, width: 7 },
          shadowOpacity: 1.0,
          shadowRadius: 7,
        },
        android: {
          elevation: 20,
        },
      }),
    }
  },
},)

// export const tabStyle = {
//   width: 40,
//   height: 40,
//   alignItems: 'center',
//   justifyContent: 'center',
//   // backgroundColor:'red'
// }

export const styles = ScaleSheet.create({
  tabImage: {
    height: 22,
    width: 22,
    resizeMode: 'contain',
    // backgroundColor:'green'
  },
  carImage: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
    // backgroundColor:'green'
  },
  tabStyle:{
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
  }
});