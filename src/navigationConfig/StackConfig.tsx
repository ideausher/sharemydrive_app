import { createStackNavigator } from 'react-navigation-stack';
// Bottom tabs 
import Home from '../containers/Home';
import Chats from '../containers/Chats';
import Search from '../containers/Search';
import PickUp from '../containers/PickUp';
import Profile from '../containers/Profile';

// Drawer Menu
import ChangePswd from '../containers/ChangePswd';
import History from '../containers/History';
import TermsAndPP from '../containers/TermsAndPP';
import ContactUs from '../containers/ContactUs';
import InviteFriends from '../containers/InviteFriends';
import HelpSupport from '../containers/HelpSupport';

// PickUp Stack
import SelectLocation from '../containers/SelectLocation';

export const SearchStack = createStackNavigator({
  Search: Search,
})

export const PickUpStack = createStackNavigator({
  PickUp: PickUp,
})

export const HomeStack = createStackNavigator({
  Home: Home,
})

export const ChatsStack = createStackNavigator({
  Chats: Chats,
})

export const ProfileStack = createStackNavigator(
  {
    Profile: Profile,
  }
)

export const HistoryStack = createStackNavigator({
  History: History,
})

export const TermsAndPPStack = createStackNavigator({
  TermsAndPP: TermsAndPP,
})

export const InviteFriendsStack = createStackNavigator(
  {
    InviteFriends: InviteFriends,
  }
)

export const ChangePswdStack = createStackNavigator({
  ChangePswd: ChangePswd,
})

export const HelpSupportStack = createStackNavigator(
  {
    HelpSupport: HelpSupport,
  }
)