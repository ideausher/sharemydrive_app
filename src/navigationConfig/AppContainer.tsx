import { createSwitchNavigator, createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
//@ts-ignore
import { createDrawerNavigator } from 'react-navigation-drawer';
import Welcome from '../containers/Welcome';
import Login from '../containers/Login';
import ForgotPswd from '../containers/ForgotPswd';
import Verification from '../containers/Verification';
import ChangePswd from '../containers/ChangePswd';
import LocationAccess from '../containers/LocationAccess';
import LoginSuccess from '../containers/LoginSuccess';

import Drawer from '../containers/Drawer';

import React from 'react';
import { Image, Text } from "react-native";
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { HistoryStack, TermsAndPPStack, InviteFriendsStack, ChangePswdStack, HelpSupportStack } from './StackConfig'
import { AppBottomTabNavigator } from './BottomTabNavigatorConfig';


// PickUp Stack
import SelectLocation from '../containers/SelectLocation';
import SelectDateTime from '../containers/SelectDateTime';
import VacantSeats from '../containers/VacantSeats';
import SelectPrice from '../containers/SelectPrice';
import GreyPopup from '../containers/GreyPopup';

// Search Stack
import SearchResult from '../containers/SearchResult';
import ViewProfile from '../containers/ViewProfile';
import BookingDetails from '../containers/BookingDetails';
import RideBookedSuccess from '../containers/RideBookedSuccess';

// Home Stack
import TrackRide from '../containers/TrackRide';
import Notifications from '../containers/Notifications';
import Passengers from '../containers/Passengers';
import DriverRating from '../containers/DriverRating';
import PassengerRating from '../containers/PassengerRating';
import SavedCards from '../containers/SavedCards';
import PaymentMode from '../containers/PaymentMode';
import AddCard from '../containers/AddCard';
import CardDetails from '../containers/CardDetails';
import PaymentCompleted from '../containers/PaymentCompleted';

// Chats
import Messages from '../containers/Messages';

// Profile
import PersonalInfo from '../containers/PersonalInfo';
import VehicleInfo from '../containers/VehicleInfo';
import BankDetails from '../containers/BankDetails';

const LoginStackNavigator = createStackNavigator(
  {
    Welcome: Welcome,
    Login: Login,
    Verification: Verification,
    ForgotPswd: ForgotPswd,
    ChangePswd: ChangePswd,
    LocationAccess: LocationAccess,
    LoginSuccess: LoginSuccess
  }
)

const HomeStackNavigator = createStackNavigator(
  {
    Home: AppBottomTabNavigator,
    SelectLocation: SelectLocation,
    SelectDateTime: SelectDateTime,
    VacantSeats: VacantSeats,
    SelectPrice: SelectPrice,
    GreyPopup: GreyPopup,
    SearchResult: SearchResult,
    ViewProfile: ViewProfile,
    BookingDetails: BookingDetails,
    RideBookedSuccess: RideBookedSuccess,
    PersonalInfo: PersonalInfo,
    ForgotPswd: ForgotPswd,
    Verification: Verification,
    VehicleInfo: VehicleInfo,
    BankDetails: BankDetails,
    TrackRide: TrackRide,
    Notifications: Notifications,
    Passengers: Passengers,
    DriverRating: DriverRating,
    PassengerRating: PassengerRating,
    SavedCards: SavedCards,
    PaymentMode: PaymentMode,
    AddCard: AddCard,
    CardDetails: CardDetails,
    PaymentCompleted: PaymentCompleted,
    Messages: Messages
  },
  {
    defaultNavigationOptions: {
      headerStyle: { height: 0 },
      headerTitleStyle: { color: 'transparent' },
      headerForceInset: { top: 'never', bottom: 'never' },

    },
  },

)

const DrawerNavigator = createDrawerNavigator({
  Home: {
    screen: HomeStackNavigator,
    params: { image: require("../assets/car.png"), name: 'Home' },
    // navigationOptions: ({ navigation }) => ({
    //   drawerLabel: () => (
    //     <Text style={[styles.revealOptionText]}>Homes</Text>
    //   ),
    //   drawerIcon: () => (
    //     <Image
    //       style={styles.revealOptionImage}
    //       source={require("../assets/car.png")}
    //     />
    //   )
    // })
    
  },
  History: {
    screen: HistoryStack,
    params: { image: require("../assets/history.png"), name: 'History' },
    // navigationOptions: ({ navigation }) => ({
    //   drawerLabel: () => (
    //     <Text style={[styles.revealOptionText]}>History</Text>
    //   ),
    //   drawerIcon: () => (
    //     <Image
    //       style={styles.revealOptionImage}
    //       source={require("../assets/history.png")}
    //     />
    //   )
    // })
  },
  TermsAndPP: {
    screen: TermsAndPPStack,
    params: { image: require("../assets/terms.png"), name: 'Terms & Privacy Policy' },
    // navigationOptions: ({ navigation }) => ({
    //   drawerLabel: () => (
    //     <Text style={[styles.revealOptionText]}>Terms & Privacy Policy</Text>
    //   ),
    //   drawerIcon: () => (
    //     <Image
    //       style={styles.revealOptionImage}
    //       source={require("../assets/terms.png")}
    //     />
    //   )
    // })
  },
  InviteFriends: {
    screen: InviteFriendsStack,
    params: { image: require("../assets/invite_white.png"), name: 'Invite Friends' },
    // navigationOptions: ({ navigation }) => ({
    //   drawerLabel: () => (
    //     <Text style={[styles.revealOptionText]}>Invite Friends</Text>
    //   ),
    //   drawerIcon: () => (
    //     <Image
    //       style={styles.revealOptionImage}
    //       source={require("../assets/invite_white.png")}
    //     />
    //   )
    // })
  },
  ChangePswd: {
    screen: ChangePswdStack,
    params: { image: require("../assets/changePswd.png"), name: 'Change Password' },
    // navigationOptions: ({ navigation }) => ({
    //   drawerLabel: () => (
    //     <Text style={[styles.revealOptionText]}>Change Password</Text>
    //   ),
    //   drawerIcon: () => (
    //     <Image
    //       style={styles.revealOptionImage}
    //       source={require("../assets/changePswd.png")}
    //     />
    //   )
    // })
  },
  HelpSupport: {
    screen: HelpSupportStack,
    params: { image: require("../assets/contactUs.png"), name: 'Help & Support' },
    // navigationOptions: ({ navigation }) => ({
    //   drawerLabel: () => (
    //     <Text style={[styles.revealOptionText]}>Help & Support</Text>
    //   ),
    //   drawerIcon: () => (
    //     <Image
    //       style={styles.revealOptionImage}
    //       source={require("../assets/contactUs.png")}
    //     />
    //   )
    // })
  },
},
 {
  contentComponent: Drawer,
  drawerType:"front"
})

const AppSwitcher = createSwitchNavigator({
  LoginStack: LoginStackNavigator,
  navigator: DrawerNavigator,
},
)


const AppContainer = createAppContainer(AppSwitcher);
export default AppContainer;

export const styles = ScaleSheet.create({
  revealOptionImage: {
    height: 20,
    width: 30,
    resizeMode: 'contain',
  },
  revealOptionText: {
    //marginLeft: 8,
    fontWeight: '500',
    color: 'red',
    fontSize: 16,
    marginTop: 10,
    marginBottom: 10
  },
});
