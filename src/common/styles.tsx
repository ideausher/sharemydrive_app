import { Dimensions,Platform } from "react-native";
import { APP_SHADOW_COLOR } from '../constants/colors';
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD } from "../constants/fonts";
import { WINDOW_WIDTH } from '../constants/index';

export const commonShadowStyle = {
    shadowColor: APP_SHADOW_COLOR,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    elevation: 3,
}

export const navHdrTxtStyle = ScaleSheet.create({
    style: {
        textAlign: 'center',
        color: '#A2A2A2',
        fontFamily: APP_FONT_BOLD,
        fontSize: 18,
          width: Dimensions.get('screen').width - 140,
       // width: 40,
    }
})

export const headerTitleStyle = {
    alignSelf: 'center',
    textAlign: "center",
    justifyContent: 'center',
    flex: 1,
    fontFamily: APP_FONT_BOLD,
    textAlignVertical: 'center',
}

export const headerStyle = ScaleSheet.create({
    style: {
        ...Platform.select({
            ios: {
                height: 70,
          },
           android: {
            height: 50,
          },
        }),
       
        textAlign: 'center',
        justifyContent: 'center',
    },
})