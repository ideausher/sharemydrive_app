
export interface NotificationsModal {
    user_id_one: String,
    user_id_two: String,
    type: Number,
    title: String,
    message: String,
    is_read: Number
    created_at: String
    updated_at: String,
    user_two_details: UserTwoDetails
}

export interface UserTwoDetails {
    id: String,
    image: String,
    firstname: String,
    date_of_birth: String,
    userage: Number
}