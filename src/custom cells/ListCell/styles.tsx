//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import {  APP_GREY_TEXT_COLOR, App_LIGHT_BLUE_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingLeft:16,
        paddingRight:'16'
    },
    btn:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    darkGreyText: {
        fontSize: 16,
        textAlign: 'left',
        height: 30,
        color: APP_GREY_TEXT_COLOR
    },
    smallImg: {
        width: 16,
        height: 16,
        marginLeft: 8
    },
});