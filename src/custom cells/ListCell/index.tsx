import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { TextGrey } from '../../custom/CustomText';
import {  APP_GREY_TEXT_COLOR } from '../../constants/colors';

interface Props {
    onClickEvent: any,
    item: any,
    selecedItem: any,
}

class ListCell extends Component<Props> {
    render() {
        return (
            <View style={styles.container}>
            <TouchableOpacity style={styles.btn} onPress={() => { console.log('Func called'), this.props.onClickEvent(this.props.item) }}>
        <Text style={styles.darkGreyText}>{this.props.item}</Text>
                <Image
                    source={this.props.selecedItem == this.props.item ? require('../../assets/radio-checked.png') : require('../../assets/radio-unchecked.png')}
                    style={styles.smallImg}
                    resizeMode={'contain'}
                />
            </TouchableOpacity>
        </View >
        )
    }
}

export default ListCell;