//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 8,
        paddingBottom: 8
    },
    shadowVw: {
        flex: 1,
        width: '100%',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: APP_BORDER_GREY_COLOR,
        backgroundColor: 'white'
    },
    itemView: {
        flex: 1,
        padding: 8,
        flexDirection: 'row',
        alignItems: 'center',
       //  backgroundColor:'purple',
    },
    originDestinationVw: {
        flex: 1,
        height: 80,
        justifyContent: 'space-between',
        padding: 8,
         //backgroundColor:'orange'
    },
    image: {
        height: 80,
        marginleft: 8,
       // backgroundColor:'green',
    },
    priceContainer: {
        width: 105,
        height: '80%',
        alignItems: 'center',
        marginRight: 8,
        //backgroundColor:'green',
        justifyContent: 'center',
    },
    priceTxt: {
        fontSize: 18,
        fontFamily: APP_FONT_BOLD,
        color: '#2785FE',
        textAlign: 'center',
       // backgroundColor:'pink',
    },
});