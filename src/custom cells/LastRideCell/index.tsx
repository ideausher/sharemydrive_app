import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { commonShadowStyle } from '../../common/styles';
import { TextGreyBold } from '../../custom/CustomText';
import { CommonBtn } from '../../custom/CustomButton';

interface Props {
    onClickEvent: any,
    index: any,
    item: any,
    pickup: any,
}

class LastRideCell extends Component<Props> {

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.shadowVw, commonShadowStyle]}>
                    <TouchableOpacity
                        style={styles.itemView}
                        onPress={() => this.props.pickup ? null : this.props.onClickEvent(this.props.index)}>
                        <Image
                            source={require('../../assets/pickup.png')}
                            style={styles.image}
                            resizeMode='contain'
                        />
                            <View style={styles.originDestinationVw}>
                                <TextGreyBold marginTop={0} fontSize={14} text={this.props.item.start_location.city
                                } textAlign='left' />
                                <TextGreyBold marginTop={0} fontSize={14} text={this.props.item.end_location.city
                                } textAlign='left' />
                            </View>
                            <View style={styles.priceContainer}>
                                <Text style={styles.priceTxt}>${this.props.item.price_per_seat}</Text>
                                {this.props.pickup ? <CommonBtn title='Republish' func={() => this.props.onClickEvent(this.props.index)} width={105} height={30} marginTop={8} marginBottom={0} /> : null}
                            </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}

export default LastRideCell;