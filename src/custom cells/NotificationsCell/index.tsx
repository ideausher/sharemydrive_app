import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { TextGrey, TextGreyBold } from '../../custom/CustomText';
import { APP_GREY_BOLD_TEXT_COLOR } from '../../constants/colors';
import moment from 'moment';

interface Props {
    onClickEvent: any,
    item: any,
    index: any
}

class NotificationsCell extends Component<Props> {


    private getTime(createdAt: string) {

      //  console.log('CReated AT >>>', createdAt)

        // var gmtDateTime = moment.utc('2020-01-14 11:00:00', "YYYY-MM-DD HH:mm:ss")
        // console.log('gmtDateTimel >>>>>', gmtDateTime);
        // var local = gmtDateTime.local().format('YYYY-MM-DD HH:mm:ss');
        // console.log('local >>>>>', local);

        let bookingDate = moment(createdAt, 'YYYY-MM-DD HH:mm:ss')
        //console.log('bookingDate >>>>>', bookingDate);
        let diff = bookingDate.fromNow()
        //console.log('diff >>>>>', diff);

        return diff
    }


    render() {

        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.itemView}>
                    <Text style={styles.text}>
                        <TextGreyBold marginTop={0} fontSize={14} text={this.props.item.message} textAlign='left' marginLeft={16} />
                        {/* <TextGreyBold marginTop={0} fontSize={14} text='Parminder Kaur' textAlign='left' color='#4B4B4D'/> */}
                    </Text>
                    <TextGrey marginLeft={12} marginTop={0} fontSize={14} text={this.getTime(this.props.item.created_at)} textAlign='right' />
                </TouchableOpacity>
            </View>
        )
    }

}

export default NotificationsCell;