//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        paddingTop: 8,
        paddingBottom: 8,
       // backgroundColor:'pink'
    },
    itemView: {
        flex: 1,
       // alignItems: 'center',
        flexDirection: 'row',
       // backgroundColor: 'purple',
        padding: 16,
        justifyContent: 'space-between' 
    },
    text:{
        flex: 1,
    }
});