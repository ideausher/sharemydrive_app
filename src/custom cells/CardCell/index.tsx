import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { styles } from './styles';

interface Props {
    onClickEvent: any,
    item: any,
    index: any, 
    selectedCardIndx: any,
}

class CardCell extends Component<Props> {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.props.onClickEvent(this.props.index)} style={styles.cardVw}>
                                <Image
                                    source={this.props.selectedCardIndx == this.props.index ? require('../../assets/radio-checked.png') : require('../../assets/radio-unchecked.png')}
                                    style={styles.smallImg}
                                    resizeMode={'contain'}
                                />
                                <Text style={styles.cardText}>xxxx-xxxx-xxxx-{this.props.item.last4}</Text>
                                <Image
                                    source={this.props.item.brand == 'Visa' ? require('../../assets/visa2.png') : require('../../assets/mastercard.png')}
                                    style={styles.visaImg}
                                    resizeMode={'contain'}
                                />
                            </TouchableOpacity>
            </View>
        )
    }
}

export default CardCell;