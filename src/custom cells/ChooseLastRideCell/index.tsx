import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { commonShadowStyle } from '../../common/styles';
import { TextGrey } from '../../custom/CustomText';
import { CommonBtn } from '../../custom/CustomButton';
import { Rating } from 'react-native-ratings';
import { AVATAR_URL } from '../../constants/apis';
import FastImage from 'react-native-fast-image'

interface Props {
    onClickEvent: any,
    item: any,
    index: any
}

class ChooseLastRideCell extends Component<Props> {

    render() {

        let url = AVATAR_URL + this.props.item.user_details.image
        
        return (
            <View style={styles.container}>
                <View style={[styles.shadowVw, commonShadowStyle]}>
                    <TouchableOpacity style={styles.itemView}>
                        {this.props.item.image == '' ?
                            <Image
                                source={require('../../assets/avatar.png')}
                                style={styles.profileImg}
                                resizeMode='contain'
                            /> :
                            <FastImage style={styles.profileImg} source={{ uri: url}} />                        }
                        <TextGrey fontSize={12} text={this.props.item.user_details.firstname + ' ' +  this.props.item.user_details.lastname} color='#717173'/>
                        <TextGrey fontSize={8} text='Your last review' marginBottom={8}/>
                        <Rating
                                    startingValue={this.props.item.user_details.ratingasdriver}
                                    imageSize={16}
                                    ratingCount={5}
                                    isDisabled={true}
                                    readonly={true}
                                />
                        <CommonBtn title='Quick Add' func={() => this.props.onClickEvent(this.props.index)} width='100%' marginTop={16} marginBottom={0} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

}

export default ChooseLastRideCell;