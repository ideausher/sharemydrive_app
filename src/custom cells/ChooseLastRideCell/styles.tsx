//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 8,
        paddingRight: 8,
        width: 200
    },
    shadowVw: {
        flex: 1,
        width: '100%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: APP_BORDER_GREY_COLOR,
        backgroundColor: 'white'
    },
    itemView: {
        flex: 1,
        alignItems: 'center',
        // backgroundColor:'purple',
    },
    profileImg: {
        marginTop: 16,
        height:30,
        width: 30,
        borderRadius: 15,
        backgroudColor: 'red'
    },
});