import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { styles } from './styles';
import { TextGrey } from '../../custom/CustomText';
import { AVATAR_URL } from '../../constants/apis';
import firebase from 'react-native-firebase';
import { commonShadowStyle } from '../../common/styles';
import FastImage from 'react-native-fast-image'
import moment from 'moment';
import { TouchableOpacity } from 'react-native-gesture-handler';

interface Props {
    item: any,
    index: any,
    senderId: any,
    senderImage: any,
    receiverImage: any,
    imageClickEvent: any,
}

class MessagaCell extends Component<Props> {

    // state = {
    //     url: ''
    // }

    // getImageURl(imageName: string) {
    //     console.log('Firestore image name >>>>>', imageName)
    //     var firebaseStorageRef = firebase.storage().ref('chat/images');
    //     const imageRef = firebaseStorageRef.child(imageName);

    //     console.log('Firestore imageRef >>>>>', imageRef)

    //     imageRef.getDownloadURL().then((url: string) => { console.log('getImageURl Image url >>', url), this.setState({ url: url }) });

    //     //{console.log('getImageURl Image url >>', url), this.setState({url: url})}
    // }

    render() {

        //console.log('Sender Image url >>', this.props.senderImage)
       // console.log('Receiver Image url >>', this.props.receiverImage)


        let isSender = this.props.senderId == this.props.item.senderId

        // if (this.props.item.message == '') {
        //     console.log('Firestore Image func called >>', this.state.url)
        //     this.getImageURl(this.props.item.image)
        // }

        let msgDateTime = this.props.item.date
        let dateMoment = moment(msgDateTime, "DD MMM, hh:mm A")
        if (moment(dateMoment).isSame(moment(), 'day')) {
            msgDateTime = dateMoment.format(' hh:mm A')
        }

        return (
            isSender ?
                <View style={[styles.item, styles.itemOut]}>
                    <View style={styles.msgBallonTimeVwRight}>
                        {this.props.item.message != '' ?
                            <View style={[styles.balloonRight]}>
                                <TextGrey marginTop={0} fontSize={18} text={this.props.item.message} textAlign='left' color='#555555' />
                            </View> :
                            <TouchableOpacity
                                onPress={() => this.props.imageClickEvent(this.props.item.image)}>
                                <FastImage
                                    source={{ uri: this.props.item.image }}
                                    style={styles.image}
                                   // resizeMode={FastImage.resizeMode.contain}
                                // resizeMode='contain'
                                />
                            </TouchableOpacity>}
                        <TextGrey marginTop={8} fontSize={10} text={msgDateTime} textAlign='right' color='#AAAAAA' />
                    </View>
                    {this.props.senderImage == '' ?
                        <Image
                            source={require('../../assets/avatar.png')}
                            style={styles.profileImg}
                            resizeMode='contain'
                        /> :
                        <FastImage style={styles.profileImg} source={{ uri: this.props.senderImage }} />

                        //<Image style={styles.profileImg} source={{ uri: this.props.senderImage }} /> */}
                    }
                </View> :
                <View style={[styles.item, styles.itemIn]}>
                    {this.props.receiverImage == '' ?
                        <Image
                            source={require('../../assets/avatar.png')}
                            style={styles.profileImg}
                            resizeMode='contain'
                        /> :
                        <FastImage style={styles.profileImg} source={{ uri: this.props.receiverImage }} />
                        // <Image style={styles.profileImg} source={{ uri: this.props.receiverImage }} />
                    }

                    <View style={styles.msgBallonTimeVwLeft}>
                        {this.props.item.message != '' ? <View style={[commonShadowStyle, styles.balloonLeft]}>
                            <TextGrey marginTop={0} fontSize={18} text={this.props.item.message} textAlign='left' color='#555555' />
                        </View> :
                        <TouchableOpacity
                        onPress={() => this.props.imageClickEvent(this.props.item.image)}>
                            <FastImage
                                style={styles.image}
                                source={{ uri: this.props.item.image }}
                                //resizeMode={FastImage.resizeMode.contain}
                            />
                            </TouchableOpacity>

                            // <Image
                            //         source={{ uri: this.props.item.image }}
                            //         style={styles.image}
                            //     // resizeMode='contain'
                            //     />
                        }
                        <TextGrey marginTop={8} fontSize={10} text={msgDateTime} textAlign='left' color='#AAAAAA' />
                    </View>
                </View>
        )
    }
}

export default MessagaCell
