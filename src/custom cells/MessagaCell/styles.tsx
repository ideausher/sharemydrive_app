//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR, APP_GREEN_COLOR } from '../../constants/colors';
import { scale, verticalScale, moderateScale } from 'react-native-size-matters';

export const styles = ScaleSheet.create({
    item: {
        marginVertical: moderateScale(7, 2),
        flexDirection: 'row',
      //  backgroundColor:'green'
     },
     itemIn: {
         marginLeft: 20
     },
     itemOut: {
        alignSelf: 'flex-end',
        marginRight: 20
     },
     msgBallonTimeVwLeft:{
        marginLeft: 8
    },
    msgBallonTimeVwRight:{
        marginRight: 8
    },
     profileImg: {
        height: 40,
        width: 40,
        borderRadius: 20,
        backgroundColor: '#717173'
    },
     balloonLeft: {
        maxWidth: moderateScale(200, 2),
        paddingHorizontal: moderateScale(10, 2),
        paddingTop: moderateScale(5, 2),
        paddingBottom: moderateScale(7, 2),
        borderRadius: 20,
        borderTopLeftRadius: 0,     
        backgroundColor:'white'   
     },
     balloonRight: {
        maxWidth: moderateScale(200, 2),
        paddingHorizontal: moderateScale(10, 2),
        paddingTop: moderateScale(5, 2),
        paddingBottom: moderateScale(7, 2),
        borderRadius: 20,
        borderTopRightRadius: 0,   
        backgroundColor: APP_GREEN_COLOR     
     },
     image: {
        height: 100,
        width: 100,
        backgroundColor: '#717173'
    },
});