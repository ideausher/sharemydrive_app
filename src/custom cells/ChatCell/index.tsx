import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { commonShadowStyle } from '../../common/styles';
import { TextGrey } from '../../custom/CustomText';
import { CommonBtn } from '../../custom/CustomButton';
import { Rating } from 'react-native-ratings';
import { AVATAR_URL } from '../../constants/apis';
import FastImage from 'react-native-fast-image'
import moment from 'moment';

interface Props {
    onClickEvent: any,
    item: any,
    index: any,
    myId ? :number
}

class ChatCell extends Component<Props> {

    render() {

       // console.log('Image url in Chat Cell>>', this.props.item.image)

        let msgDateTime = this.props.item.sentDate
        let dateMoment = moment(msgDateTime, "DD MMM, hh:mm A")
        if (  moment(dateMoment).isSame(moment(), 'day')) {
            msgDateTime = dateMoment.format(' hh:mm A')
        }

        let url = AVATAR_URL + this.props.item.image
        console.log('start >> last msg >>', this.props.item.lastMsg)
        var status=(this.props.myId != this.props.item.senderID && this.props.item.status==0 ? 0 :1)
        
        return (
            <View style={styles.container}>
                <TouchableOpacity
                    style={styles.itemView}
                    onPress={() => this.props.onClickEvent(this.props.index)}>
                    {this.props.item.image == '' ?
                        <Image
                            source={require('../../assets/avatar.png')}
                            style={styles.profileImg}
                            resizeMode='contain'
                        /> :
                        <FastImage style={styles.profileImg} source={{ uri: url, }} />
                        //<CachedImage style={styles.profileImg}  source={{ uri: this.props.item.image }}/>
                    }
                    <View style={styles.nameLastMsgVw}>
                        <TextGrey fontSize={16} text={this.props.item.firstName + ' ' + this.props.item.lastName} color='#4E4E50' textAlign='left' />
                        {this.props.item.lastMsg != '' ?
                            <TextGrey fontSize={12} text={this.props.item.lastMsg} color= {status ==1 ?'rgba(78, 78, 80, 0.60)' : 'black'} textAlign='left' numberOfLines={2} /> :
                            <View style={styles.lastImageCntnr}>
                                <Image
                                    source={require('../../assets/camera-grey.png')}
                                    style={styles.cameraImg}
                                    resizeMode='contain'
                                />
                                <TextGrey fontSize={12} text='Image' color= {status ==1 ?'rgba(78, 78, 80, 0.60)' : 'black'} textAlign='left' numberOfLines={2} />
                            </View>}
                    </View>
                    <TextGrey fontSize={12} text={msgDateTime} color='rgba(78, 78, 80, 0.60)' textAlign='left' />
                </TouchableOpacity>
                {/* </View> */}
            </View>
        )
    }
}

export default ChatCell
