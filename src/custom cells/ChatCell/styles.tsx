//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 8,
        paddingBottom: 8
    },
    itemView: {
        flex: 1,
        flexDirection: 'row',
        //backgroundColor: 'purple',
        padding: 16, 
        marginBottom: 2
    },
    profileImg: {
        height: 60,
        width: 60,
        borderRadius: 30,
        backgroundColor: '#717173'
    },
    nameLastMsgVw:{
        flex: 1,
        marginLeft: 16,
        marginRight: 8,
        //backgroundColor: 'yellow',
    },
    locationIcon:{
        height: 26,
        marginRight: 14, 
    },
    lastImageCntnr:{
        flexDirection:'row'
    },
    cameraImg: {
        height: 30,
        width: 30,
    },
});