import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { commonShadowStyle } from '../../common/styles';
import { TextGrey } from '../../custom/CustomText';
import { CommonBtn } from '../../custom/CustomButton';
import { Rating } from 'react-native-ratings';
import {AVATAR_URL} from '../../constants/apis';
import moment from "moment";

interface Props {
    onClickEvent: any,
    item: any,
    index: any
}

class PassengerCell extends Component<Props> {

    calculateAge = (dob1: string) => {
        const age = moment().diff(dob1, 'years');

        return age
      }

    render() {

        let url = AVATAR_URL + this.props.item.image
        console.log('Image url >>', url)
        return (
            <View style={styles.container}>
                {/* <View style={[styles.shadowVw, commonShadowStyle]}> */}
                <TouchableOpacity style={styles.itemView}
                    onPress={() => this.props.onClickEvent(this.props.index)}>
                    <View style={styles.imgNameVw}>
                        {this.props.item.image == '' ?
                            <Image
                                source={require('../../assets/avatar.png')}
                                style={styles.profileImg}
                                resizeMode='contain'
                            /> :
                            <Image style={styles.profileImg} source={{ uri: url}} />
                            // <ImageLoad style={CommonStyles.doctorImg} placeholderSource={require('../../assets/avatar.png')} source={{ uri: this.props.doctorInfo.image }} />
                        }

                        <TextGrey fontSize={14} text={this.props.item.firstname + ' ' + this.props.item.lastname + ', ' + this.calculateAge(this.props.item.date_of_birth)} color='#717173' marginLeft={20} />
                    </View>
                    <Image
                        source={require('../../assets/location.png')}
                        style={styles.locationIcon}
                        resizeMode='contain'
                    />
                </TouchableOpacity>
                {/* </View> */}
            </View>
        )
    }

}

export default PassengerCell;