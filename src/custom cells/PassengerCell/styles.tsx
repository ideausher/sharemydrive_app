//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 8,
        paddingBottom: 8
    },
    itemView: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
       // backgroundColor: 'purple',
        padding: 16, 
        marginBottom: 2
    },
    imgNameVw:{
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        //backgroundColor: 'yellow',
    },
    profileImg: {
        height: 60,
        width: 60,
        borderRadius: 30,
        backgroudColor: '#717173'
    },
    locationIcon:{
        height: 26,
        marginRight: 14, 
    }
});