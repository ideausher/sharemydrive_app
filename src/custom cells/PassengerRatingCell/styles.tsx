//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';
import { Dimensions } from 'react-native';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
       // backgroundColor: 'red', 
        paddingTop: 8,
        paddingBottom: 8
    },
    itemView: {
        flex: 1,
        alignItems: 'center',
       // backgroundColor: 'purple',
    },
    imgNameRatingVw:{
        flex: 1, 
        flexDirection: 'row',
        justifyContent:'space-between',
       // backgroundColor: 'green',
    },
    imgNameVw:{
        flex: 1,
        //backgroundColor: 'yellow',
    },
    profileImg: {
        marginLeft: 20,
        height: 40,
        width: 40,
        borderRadius: 20,
        backgroudColor: '#717173'
    },
});