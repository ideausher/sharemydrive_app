import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { commonShadowStyle } from '../../common/styles';
import { TextGrey } from '../../custom/CustomText';
import { CommonBtn } from '../../custom/CustomButton';
import { Rating } from 'react-native-ratings';
import { AVATAR_URL } from '../../constants/apis';
import { TextInputMultiLine } from '../../custom/CustomTextInput';
import { AirbnbRating } from 'react-native-ratings';

interface Props {
    ratingChanged: any,
    reviewChanged: any
    item: any,
    index: any
}

class PassengerRatingCell extends Component<Props> {

    render() {

        console.log("Passenger info >>>>", this.props.item)

        let url = AVATAR_URL + this.props.item.image
        console.log('Image url >>', url)
        return (
            <View style={styles.container}>
                {/* <View style={[styles.shadowVw, commonShadowStyle]}> */}
                <TouchableOpacity style={styles.itemView}>
                    <View style={styles.imgNameRatingVw}>
                        <View style={styles.imgNameVw}>
                            {this.props.item.image == undefined ?
                                <Image
                                    source={require('../../assets/avatar.png')}
                                    style={styles.profileImg}
                                    resizeMode='contain'
                                /> :
                                <Image style={styles.profileImg} source={{ uri: url }} />}
                            <TextGrey fontSize={14} text={this.props.item.firstname + ' ' + this.props.item.lastname} color='#717173' marginLeft={0} textAlign='left'/>
                        </View>
                        <AirbnbRating
                                        showRating={false}
                                        onFinishRating={(rating: any) => this.props.ratingChanged(rating, this.props.index)}
                                        size={24} />
                    </View>
                    <TextInputMultiLine placeholder='Write a review....' onChange={(text: any) => this.props.reviewChanged(text, this.props.index)} value={this.props.item.review} fontSize={12} width='100%' height={80} />
                </TouchableOpacity>
                {/* </View> */}
            </View>
        )
    }
}

export default PassengerRatingCell;