import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { commonShadowStyle } from '../../common/styles';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import { CommonBtn } from '../../custom/CustomButton';
import { Rating } from 'react-native-ratings';
import moment from 'moment';
import { APP_SKY_BLUE_COLOR, APP_GREEN_COLOR } from '../../constants/colors';
import General from '../../Utils/General';
import { AVATAR_URL } from '../../constants/apis';

interface Props {
    item: any,
    index: any,
    leftBtnAction: any,
    rightBtnAction: any,
    onClickEvent: any,
}

class RideRequestsCell extends Component<Props> {

    render() {

       // console.log('Item >>>>', this.props.item)
        let item = this.props.item
        let rideDetails = item.ride_details[0]
   
        let dateTimeMoment = moment(rideDetails.ride_date_time, 'YYYY-MM-DD HH:mm:ss')
        let startTime = dateTimeMoment.format('YYYY-MM-DD, hh:mm A')

        let endTime = General.getEndTime(item.start_location.latitude, item.start_location.longitude, item.end_location.latitude, item.end_location.longitude, rideDetails.ride_date_time)

        let url = AVATAR_URL + this.props.item.image
        return (
            <View style={styles.container}>
                <View style={[styles.shadowVw, commonShadowStyle]}>
                    <TouchableOpacity
                        style={styles.itemView}
                        onPress={() => this.props.onClickEvent(this.props.index)}>
                        <View style={styles.userDetailsVw}>
                        {item.image == '' ?
                            <Image
                                source={require('../../assets/avatar.png')}
                                style={styles.profileImg}
                                resizeMode='contain'
                            /> :
                            <Image style={styles.profileImg} source={{ uri: url }} />
                        }
                            <TextGrey marginLeft={8}fontSize={12} text={item.firstname + ' ' + item.lastname} textAlign='left'  color='#717173' />
                        </View>
                        <View style={styles.locationContainer}>
                            <Image
                                source={require('../../assets/pickup.png')}
                                style={styles.image}
                                resizeMode='contain'
                            />
                            <View style={styles.originDestinationContainer}>
                                <View style={styles.originDestinationVw}>
                                    <TextGreyBold marginTop={0} fontSize={14} text={item.start_location.city} textAlign='left' />
                                    <View style={styles.dateTimeContainer}>
                                        <Image
                                            source={require('../../assets/calIcon.png')}
                                            style={styles.calIconImg}
                                            resizeMode='contain'
                                        />
                                        <TextGrey fontSize={10} marginTop={0} text={startTime} textAlign='left' color='#717173' />
                                    </View>
                                </View>
                                <View style={[styles.originDestinationVw, { justifyContent: 'flex-end' }]}>
                                    <TextGreyBold marginTop={0} fontSize={14} text={item.end_location.city} textAlign='left' />
                                    <View style={styles.dateTimeContainer}>
                                        <Image
                                            source={require('../../assets/calIcon.png')}
                                            style={styles.calIconImg}
                                            resizeMode='contain'
                                        />
                                        <TextGrey fontSize={10} marginTop={0} text={endTime} textAlign='left' color='#717173' />
                                    </View>
                                </View>
                            </View>
                        </View>
                        {this.props.isUpcoming == false ? <View style={styles.rideStatusVw}>
                            <TextGrey marginLeft={28} marginTop={0} fontSize={10} text={'Ride requested'} textAlign='left' color={APP_SKY_BLUE_COLOR} />
                            <TextGrey marginTop={0} fontSize={10} text={'Rejected'} textAlign='left' color='red' />
                        </View> :
                            <View style={styles.acceptRejectBtnsVw}>
                                <TouchableOpacity
                                    style={[styles.borderBtn, { borderColor: APP_GREEN_COLOR, marginLeft: 8 }]}
                                    onPress={() => this.props.leftBtnAction(this.props.index)}>
                                    <TextGrey marginTop={0} fontSize={12} text={'Accept'} textAlign='center' color={APP_GREEN_COLOR} />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={[styles.borderBtn, { borderColor: 'red', marginLeft: 8 }]}
                                    onPress={() => this.props.rightBtnAction(this.props.index)}>
                                    <TextGrey marginTop={0} fontSize={12} text={'Reject'} textAlign='center' color='red' />
                                </TouchableOpacity>
                            </View>
                        }

                    </TouchableOpacity>
                </View>
            </View >
        )
    }
}

export default RideRequestsCell;