//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 8,
        paddingBottom: 8
    },
    shadowVw: {
        flex: 1,
        width: '90%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: APP_BORDER_GREY_COLOR,
        backgroundColor: 'white'
    },
    itemView: {
        flex: 1,
        padding: 16,
        // backgroundColor:'purple',
    },
    userDetailsVw: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    profileImg: {
        height: 42,
        width: 42,
        borderRadius: 21,
        backgroudColor: '#717173'
    },
    locationContainer: {
        flex: 1,
        flexDirection: 'row',
        marginTop: 8,
        marginLeft: 21 // Half of Profile image to keep it in center of Image
        // backgroundColor:'red',
    },
    image: {
        height: 70,
        width: 20,
        //backgroundColor:'pink',
    },
    originDestinationContainer: {
        flex: 1,
        justifyContent: 'space-between',
        marginLeft: 21,
        marginRight: 8,
        // backgroundColor:'orange'
    },
    originDestinationVw: {
        flex: 1,
        // backgroundColor:'yellow'
    },
    dateTimeContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor:'green'
    },
    calIconImg: {
        width: 10,
        height: 10,
        marginRight: 4
    },
    acceptRejectBtnsVw: {
        marginTop: 8,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    borderBtn: {
        marginTop: 8,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'red',
        width: 80,
        height: 20,
        justifyContent: 'center'
    }
});