import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { commonShadowStyle } from '../../common/styles';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import { CommonBtn, BorderBtn, GreenTextBtn } from '../../custom/CustomButton';
import { Rating } from 'react-native-ratings';
import moment from 'moment';
import { APP_SKY_BLUE_COLOR, APP_GREEN_COLOR } from '../../constants/colors';
import General from '../../Utils/General';

interface Props {
    onClickEvent: any,
    item: any,
    index: any,
    isUpcoming: any,
    leftBtnAction: any,
    rightBtnAction: any,
    loginUserId: any
}

class HistoryRidesCell extends Component<Props> {

    render() {

        //console.log('Item >>>>', this.props.item)
        let item = this.props.item

        let dateTimeMoment = moment(item.ride_date_time, 'YYYY-MM-DD HH:mm:ss')
        let startTime = dateTimeMoment.format('YYYY-MM-DD, hh:mm A')

        let endTime = General.getEndTime(item.start_location.latitude, item.start_location.longitude, item.end_location.latitude, item.end_location.longitude, item.ride_date_time)


        // Check if curremt time passed
        let isCurrentTimePassed = dateTimeMoment.subtract(10, 'minutes').isBefore()

        //console.log('dateTimeMoment >>>>', dateTimeMoment)
        //console.log('Subtract 10 mins dateTimeMoment >>>>', dateTimeMoment.subtract(10, 'minutes'))
        /// console.log('isCurrentTimePassed >>>', isCurrentTimePassed)
        let acceptedUsers = item.ride_accepted

        let isAcceptedByDriver = false
        for (let i = 0; i < acceptedUsers.length; i++) {

            let user = acceptedUsers[i]

            if (user.user_id == this.props.loginUserId) {
                if (user.is_accepted_by_driver == 1) {
                    isAcceptedByDriver = true
                }

                break
            }
        }

        return (
            <View style={styles.container}>
                <View style={[styles.shadowVw, commonShadowStyle]}>
                    <TouchableOpacity
                        style={styles.itemView}
                        onPress={() => this.props.onClickEvent(this.props.index)}>

                        <View style={styles.locationPriceContainer}>
                            <View style={styles.locationContainer}>
                                <Image
                                    source={require('../../assets/pickup.png')}
                                    style={styles.image}
                                    resizeMode='contain'
                                />
                                <View style={styles.originDestinationContainer}>
                                    <View style={styles.originDestinationVw}>
                                        <TextGreyBold marginTop={0} fontSize={14} text={item.start_location.city} textAlign='left' />
                                        <View style={styles.dateTimeContainer}>
                                            <Image
                                                source={require('../../assets/calIcon.png')}
                                                style={styles.calIconImg}
                                                resizeMode='contain'
                                            />
                                            <TextGrey fontSize={10} marginTop={0} text={startTime} textAlign='left' color='#717173' />
                                        </View>
                                    </View>
                                    <View style={[styles.originDestinationVw, { justifyContent: 'flex-end' }]}>
                                        <TextGreyBold marginTop={0} fontSize={14} text={item.end_location.city} textAlign='left' />
                                        <View style={styles.dateTimeContainer}>
                                            <Image
                                                source={require('../../assets/calIcon.png')}
                                                style={styles.calIconImg}
                                                resizeMode='contain'
                                            />
                                            <TextGrey fontSize={10} marginTop={0} text={endTime} textAlign='left' color='#717173' />
                                        </View>
                                    </View>
                                </View>
                            </View>
                            <TextGreyBold marginTop={0} text={'$' + this.props.item.price_per_seat} textAlign='right' color={APP_SKY_BLUE_COLOR} fontSize={20} />
                        </View>
                        <TextGrey fontSize={10} text={'Ref ID: ' + item.id} textAlign='left' color='#717173' />
                        {this.props.isUpcoming == false ? <View style={styles.rideStatusVw}>
                            <TextGrey marginLeft={28} marginTop={0} fontSize={10} text={item.AccOrOff == 'Ride Requested' ?'Ride requested' : 'Ride offered'} textAlign='left' color={APP_SKY_BLUE_COLOR} />
                            <TextGrey marginTop={0} fontSize={10} text={item.status == '2' ? 'Completed' : item.status == '3' ? 'Cancelled' : 'Rejected'} textAlign='left' color='red' />
                        </View> :

                            (item.AccOrOff == 'Ride Requested' ?
                                <View style={styles.rideStatusVw}>
                                    {isAcceptedByDriver ?
                                        <BorderBtn title='Seat booked' titleColor={APP_GREEN_COLOR} borderColor={APP_GREEN_COLOR} func={(index: any) => this.props.leftBtnAction(index)} index={this.props.index} /> :
                                        <BorderBtn title='Pending' titleColor='red' func={(index: any) => this.props.leftBtnAction(index)} index={this.props.index} />}

                                    <BorderBtn title='Track ride' borderColor={item.status == '0' ? APP_GREEN_COLOR : '#F6F5F6'} titleColor={item.status == '0' ? APP_GREEN_COLOR : '#B6B6B6'} func={(index: any) => this.props.rightBtnAction(index)} index={this.props.index} />
                                </View> :
                                <View>
                                    {item.status == '1' ? <TextGrey fontSize={12} text={'Seat booked: ' + item.accepted_seats} textAlign='left' color={APP_GREEN_COLOR} /> : null}
                                    <View style={styles.rideStatusVw}>
                                        {item.status == '1' ? <BorderBtn title='Cancel Ride' titleColor='red' func={(index: any) => this.props.leftBtnAction(index)} index={this.props.index} /> :
                                            <TextGrey fontSize={12} text={'Seat booked: ' + item.accepted_seats} textAlign='left' color={APP_GREEN_COLOR} />}
                                        <BorderBtn title={item.status == '0' ? 'Track ride' : 'Start ride'} borderColor={item.status == '0' ? APP_GREEN_COLOR : (item.status == '1' && isCurrentTimePassed ? APP_GREEN_COLOR : '#F6F5F6')} titleColor={item.status == '0' ? APP_GREEN_COLOR : (item.status == '1' && isCurrentTimePassed ? APP_GREEN_COLOR : '#B6B6B6')} func={(index: any) => this.props.rightBtnAction(index)} index={this.props.index} />
                                    </View>
                                </View>)
                        }
                    </TouchableOpacity>
                </View>
            </View >
        )
    }
}

export default HistoryRidesCell;