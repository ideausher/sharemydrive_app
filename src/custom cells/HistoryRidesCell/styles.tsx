//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 8,
        paddingBottom: 8
    },
    shadowVw: {
        flex: 1,
        width: '90%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: APP_BORDER_GREY_COLOR,
        backgroundColor: 'white'
    },
    itemView: {
        flex: 1,
        paddingLeft: 16,
        paddingTop: 16,
        paddingBottom: 16,
        paddingRight: 16,
       // backgroundColor:'purple',
    },
    locationPriceContainer:{
        flexDirection: 'row',
        flex: 1,
       // backgroundColor:'Yellow',
    },
    locationContainer:{
        flex: 1,
        flexDirection: 'row',
       // backgroundColor:'red',
    }, 
    image: {
        height: 80,
        width: 20,
        //backgroundColor:'pink',
    },
    originDestinationContainer: {
        flex: 1,
        justifyContent: 'space-between',
        marginLeft: 8,
        marginRight: 8,
       // backgroundColor:'orange'
    },
    originDestinationVw: {
        flex: 1,
       // backgroundColor:'yellow'
    },
    dateTimeContainer:{
        flexDirection: 'row',
        alignItems:'center',
       // backgroundColor:'green'
    },
    calIconImg:{
        width: 10,
        height: 10,
        marginRight: 4
    },
    rideStatusVw:{
        marginTop: 8,
        flexDirection:'row',
        justifyContent: 'space-between'
    },
    borderBtn:{
        marginTop: 8,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'red',
        width: 80,
        height: 20,
        justifyContent:'center'
    }
});