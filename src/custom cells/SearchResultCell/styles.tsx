//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 8,
        paddingBottom: 8
    },
    shadowVw: {
        flex: 1,
        width: '100%',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: APP_BORDER_GREY_COLOR,
        backgroundColor: 'white'
    },
    itemView: {
        flex: 1,
        paddingLeft: 16,
        paddingTop: 16,
        paddingBottom: 8,
        paddingRight: 8,
        flexDirection: 'row',
        //backgroundColor:'purple',
    },
    profileImg: {
        height: 60,
        width: 60,
        borderRadius: 30,
        backgroudColor: '#717173'
    },
    rightContainer:{
        flex: 1,
        marginLeft: 8,
        //backgroundColor:'yellow'
    },
    nameRatingContainer:{
       //backgroundColor:'red',
        alignItems:'flex-start'
    },
    bottomVw:{
        flexDirection:'row',
        marginTop:16
    },
    timeAndPlaceVw:{
        height: 40,
        justifyContent: 'space-between',
    },
    image: {
        height: 40,
        marginLeft: 8,
        marginRight: 8,
        //backgroundColor:'orange'
    },
    priceVw:{
        marginTop: 8,
        backgroundColor:'#2785FE',
        borderRadius: 15,
        alignSelf:'flex-end', 
        height: 30,
        justifyContent:'center',
        paddingLeft:15,
        paddingRight:15
    },
    priceTxt: {
        fontSize: 12,
        fontFamily: APP_FONT_BOLD,
        color: 'white',
        textAlign: 'center',
        
    },
});