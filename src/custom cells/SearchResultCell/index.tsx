import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { commonShadowStyle } from '../../common/styles';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import { CommonBtn } from '../../custom/CustomButton';
import { Rating } from 'react-native-ratings';
import moment from 'moment';
import { AVATAR_URL } from '../../constants/apis';

interface Props {
    onClickEvent: any,
    item: any,
    index: any
}

class SearchResultCell extends Component<Props> {

    render() {

        //let date = this.props.item.ride_date_time.split(' ')[0]
        let time = this.props.item.ride_date_time.split(' ')[1]
        let formattedTime = moment(time, 'HH:mm:ss').format("hh:mm A")

        console.log('Item in Search Results >>>>', this.props.item.user_details.image)
        let url = AVATAR_URL + this.props.item.user_details.image

        return (
            <View style={styles.container}>
                <View style={[styles.shadowVw, commonShadowStyle]}>
                    <TouchableOpacity
                        style={styles.itemView}
                        onPress={() => this.props.onClickEvent(this.props.index)}>
                        {this.props.item.user_details.image == '' ?
                            <Image
                                source={require('../../assets/avatar.png')}
                                style={styles.profileImg}
                                resizeMode='contain'
                            /> :
                            <Image style={styles.profileImg} source={{ uri: url }} />
                        }
                        <View style={styles.rightContainer}>
                            <View style={styles.nameRatingContainer}>
                                <TextGrey fontSize={12} text={this.props.item.user_details.firstname + ' ' + this.props.item.user_details.lastname} textAlign='left' marginBottom={8} color='#717173' />
                                <Rating
                                    startingValue={this.props.item.user_details.ratingasdriver}
                                    imageSize={16}
                                    ratingCount={5}
                                    isDisabled={true}
                                    readonly={true}
                                />
                            </View>
                            <View style={styles.bottomVw}>
                                <View style={styles.timeAndPlaceVw}>
                                    <TextGrey fontSize={12} marginTop={0} text={formattedTime} textAlign='left' color='#717173' />
                                    <TextGrey fontSize={12} marginTop={0} text='' textAlign='left' color='#717173' />
                                </View>
                                <Image
                                    source={require('../../assets/pickup.png')}
                                    style={styles.image}
                                    resizeMode='contain'
                                />
                                <View style={styles.timeAndPlaceVw}>
                                    <TextGrey fontSize={12} marginTop={0} text={this.props.item.start_location.city
                                    } textAlign='left' color='#717173' flexWrap={true} width='100%' />
                                    <TextGrey fontSize={12} marginTop={0} text={this.props.item.end_location.city
                                    } textAlign='left' color='#717173' />
                                </View>
                            </View>
                            <View style={styles.priceVw}>
                                <Text style={styles.priceTxt}>${this.props.item.price_per_seat}</Text>
                            </View>
                        </View>

                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default SearchResultCell;