export const BASE_URL = 'http://iphoneapps.co.in/carpoolingapp/public/api/v1';
export const AVATAR_URL = 'http://iphoneapps.co.in/carpoolingapp/public/images/avatars/';

//export const BASE_URL = 'http://iphoneapps.co.in/testcarpoolingapp/public/api/v1';
//export const AVATAR_URL = 'http://iphoneapps.co.in/testcarpoolingapp/public/images/avatars/';


export const API_SENDOTP = BASE_URL +  '/sendotp';
export const API_SIGNUP = BASE_URL + '/user';
export const API_SIGNIN = BASE_URL + '/login';
export const API_SOCIALLOGIN = BASE_URL + '/sociallogin';
export const API_LOGOUT = BASE_URL + '/logout';
export const API_FORGOT_PSWD = BASE_URL + '/forgetpassword';
export const API_CHECK_OTP = BASE_URL + '/checkotp';
export const API_CHECK_PHONE_OTP = BASE_URL + '/checkPhoneOtp';


export const API_UPDATE_FORGOT_PASSWORD = BASE_URL + '/updateforgetpassword';
export const API_UPDATE_PASSWORD = BASE_URL + '/updatepassword';
export const API_VEHICLE_INFO = BASE_URL + '/vehicle';
export const API_OFFER_RIDE = BASE_URL + '/offerRide'; 
export const API_GET_PUBLISHED_RIDES = BASE_URL + '/getUserRides';
export const API_GET_RIDES = BASE_URL + '/getRides';
export const API_GET_RIDE_BY_ID = BASE_URL + '/getRideById';
export const API_USER_DETAILS_BY_ID = BASE_URL + '/getUserDetailsById';
export const API_GET_BOOKING_DETAILS = BASE_URL + '/bookingDetails';
export const API_BOOK_RIDE = BASE_URL + '/ride/accepted';
export const API_UPLOAD_IMAGE = BASE_URL + '/uploadImage';
export const API_SEND_BOOKING_REQUEST = BASE_URL + '/sendBookingRequest';
export const API_GET_RIDE_REQUESTS = BASE_URL + '/getRideRequest';
export const API_ACCEPT_REJECT_API = BASE_URL  + '/acceptOrRejectRideRequest';
export const API_DRIVER_CHANGE_RIDE_STATUS = BASE_URL + '/ride/driver/changeStatus';
export const API_PASSENGER_CHANGE_RIDE_STATUS = BASE_URL + '/ride/passenger/changeStatus';
export const API_GET_PASSENGERS = BASE_URL + '/ride/passengers';
export const API_BANK = BASE_URL +  '/bank';
export const API_REVIEW = BASE_URL + '/review';
export const API_COMPLETE_RIDE = BASE_URL + '/ride/completeRide';
export const API_GET_IN_TOUCH = BASE_URL + '/getintouch';
export const API_GET_CARDS = BASE_URL + '/card';
export const API_PAYMENT = BASE_URL + '/ride/payment';
export const API_NOTIFICATIONS_LIST = BASE_URL + '/notification';

export const API_GET_COUPENS = BASE_URL + '/coupons';
export const API_VALIDATE_COUPENS = BASE_URL + '/validate_coupon';
export const API_TERMS_COND = BASE_URL + '/term_condition';
export const API_SEND_MESSAGE_NOTIFICATION=BASE_URL+"/hitCustomNotification"

export const API_GET_CHATS = 'https://pt.iphoneapps.co.in:3002/api/v1.0/chat/getchat?user_id=';

