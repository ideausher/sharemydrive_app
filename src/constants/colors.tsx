import { Dimensions } from "react-native";
import ScaleSheet from 'react-native-scalesheet';

export const APP_GREEN_COLOR = '#4CE5B1';
export const APP_SKY_BLUE_COLOR = '#2785FE';
export const APP_NAVY_BLUE_COLOR = '#4A7DBF';
export const APP_BORDER_GREY_COLOR = '#EFEFF4';
export const APP_GREY_PLACEHOLDER_COLOR = '#C8C7CC';
export const APP_GREY_TEXT_COLOR = '#A2A2A2';
export const APP_GREY_BOLD_TEXT_COLOR = '#717173';

export const APP_GREY_BACK = '#FAFAFA';
export const APP_MEDIUM_GREY_COLOR = "#808080";
export const APP_LIGHT_GREY_COLOR = "#d9d9d9";
export const App_MIDIUM_GREY_COLOR = '#9B9B9B';
export const APP_BORDER_COLOR = '#C9D9E4';
export const App_LIGHT_BLUE_COLOR = '#E8F0F5';
export const APP_DARK_BLUE_COLOR = '#3D576F';
export const APP_THEME_LIGHT_COLOR = '#a9d3ef';

export const APP_SHADOW_COLOR = '#cecece';
export const APP_WELCOME_DARK_GREY = '#3B566E';
export const APP_WELCOME_LIGHT_GREY = '#8199AF';
