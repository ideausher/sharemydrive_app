import * as Modl from '../Modals/NotificationsModal';
import { Modal } from 'react-native';
import moment from 'moment';

export const notifiationsArr = (data: any) => {
    return Notifications(data);
}

function getLocalTime(timeUTC: any) {

    console.log('timeUTC >>>', timeUTC)

    var timeUTCMoment = moment.utc(timeUTC, "YYYY-MM-DD HH:mm:ss")
    console.log('timeUTCMoment >>>>>', timeUTCMoment);
    var timeLocal = timeUTCMoment.local().format('YYYY-MM-DD HH:mm:ss');
    console.log('timeLocal >>>>>', timeLocal);

    return timeLocal
}

async function Notifications(responeData: any) {
    return await new Promise(function (resolve, reject) {

        let notificationsArr = responeData.map((data: any, index: any) => {

            // Notification message Object
            let userTwoDetails: Modl.UserTwoDetails = {
                id: data.id,
                image: data.image,
                firstname: data.firstname,
                date_of_birth: data.date_of_birth,
                userage: data.userage
            }

            // NotificatiosnArr
            let notification: Modl.NotificationsModal = {
                user_id_one: data.user_id_one,
                user_id_two: data.user_id_two,
                type: data.type,
                title: data.title,
                message: data.message,
                is_read: data.is_raed,
                created_at: data.created_at, //getLocalTime(data.created_at),
                updated_at: data.updated_at, //getLocalTime(data.updated_at),
                user_two_details: userTwoDetails
            }
            return notification
        })

        resolve(notificationsArr)
    })
}