
import React, { useRef } from 'react';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import {  Text } from 'react-native';
import { APP_FONT_REGULAR, APP_FONT_BOLD } from '../constants/fonts';

export const TextGreyBold = (props: any) => {
  return (
    <Text style={[styles.greyBoldText, (props.color != null ? {color: props.color} : null), (props.fontSize != null ? {fontSize: props.fontSize} : null), (props.marginTop != null ? {marginTop: props.marginTop} : null), (props.textAlign != null ? {textAlign: props.textAlign} : null), (props.width != null ? {width: props.width} : null), (props.marginLeft != null ? {marginLeft: props.marginLeft} : null), (props.marginBottom != null ? {marginBottom: props.marginBottom} : null)]}>{props.text}</Text>
  )
}

export const TextGrey = (props: any) => {

    return (
      <Text numberOfLines={props.numberOfLines != null ? props.numberOfLines : 0} ellipsizeMode='tail' style={[styles.greyText, (props.color != null ? {color: props.color} : null), (props.fontSize != null ? {fontSize: props.fontSize} : null), (props.marginTop != null ? {marginTop: props.marginTop} : null), (props.marginBottom != null ? {marginBottom: props.marginBottom} : null), (props.marginLeft != null ? {marginLeft: props.marginLeft} : null), (props.marginRight != null ? {marginRight: props.marginRight} : null), (props.width != null ? {width: props.width} : null), (props.textAlign != null ? {textAlign: props.textAlign} : null)]}>{props.text}</Text>
    )
  }

const styles = ScaleSheet.create({

    greyBoldText: {
        fontSize: 25,
        fontFamily: APP_FONT_BOLD,
        color: '#717173',
        textAlign: 'center',
        marginTop: 8,
       // backgroundColor: 'red'
    },
    greyText: {
        fontSize: 14,
        fontFamily: APP_FONT_REGULAR,
        color: '#A2A2A2',
        textAlign: 'center',
        marginTop: 8,
       // backgroundColor:'red'
    },
})
