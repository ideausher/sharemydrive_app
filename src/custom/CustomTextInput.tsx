
import React, { useRef } from 'react';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { View, TextInput, TouchableOpacity, Image, Text } from 'react-native';
import { APP_BORDER_GREY_COLOR, APP_GREY_TEXT_COLOR, APP_LIGHT_GREY_COLOR, APP_GREY_PLACEHOLDER_COLOR, APP_THEME_LIGHT_COLOR, APP_GREY_BOLD_TEXT_COLOR } from '../constants/colors';
import { APP_FONT_REGULAR } from '../constants/fonts';
//import { CustomPickerAndroid } from './CustomPicker';

export const TextInputWithoutImage = (props: any) => {

  //console.log('Value in  TextInputWithoutImage>>>>>>', props.value)
  return (
    <View ref={props.ref != null ? props.ref : null} style={[styles.inputContainer, (props.height != null ? { height: props.height } : null), (props.marginTop != null ? { marginTop: props.marginTop } : null), (props.marginRight != null ? { marginRight: props.marginRight } : null), (props.width != null ? { width: props.width } : null)]}>
      <TextInput style={[styles.txtFld12, (props.height != null ? { height: props.height } : null), (props.fontSize != null ? { fontSize: props.fontSize } : null), (props.textAlign != null ? { textAlign: props.textAlign } : null),]} 
      placeholder={props.placeholder} 
      placeholderTextColor={APP_GREY_PLACEHOLDER_COLOR} 
      keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} value={props.value} 
      onChangeText={(text: any) => props.onChange(text)} 
      secureTextEntry={props.secureTextEntry != null ? props.secureTextEntry : false} 
      autoCapitalize={props.autoCapitalize != null ? props.autoCapitalize : "words"} 
      returnKeyType={props.returnKeyType != null ? props.returnKeyType : 'default'} 
      maxLength={props.maxLength != null ? props.maxLength : null} 
      editable={props.editable != null ? props.editable : true} />
    </View>
  )
}

export const TextInputWithoutImageFunc = (props: any) => {

  let textColor = APP_GREY_PLACEHOLDER_COLOR
  let text = props.placeholder
  if (props.value.length > 0) {
    textColor = APP_GREY_BOLD_TEXT_COLOR
    text = props.value
  }

  return (

    <TouchableOpacity onPress={() => props.func()} style={[styles.inputContainer, (props.height != null ? { height: props.height } : null), (props.marginTop != null ? { marginTop: props.marginTop } : null), (props.marginRight != null ? { marginRight: props.marginRight } : null), (props.width != null ? { width: props.width } : null)]} disabled={props.disabled != null ? props.disabled : false}>
      <Text style={[styles.txtFld12, (props.height != null ? { height: props.height } : null), (props.fontSize != null ? { fontSize: props.fontSize } : null), (props.textAlign != null ? { textAlign: props.textAlign } : null), { color: textColor, lineHeight: 40 }]}>{text}</Text>
    </TouchableOpacity >
  )
}


export const TextInputMultiLine = (props: any) => {
  return (
    <View style={[styles.inputContainer, { width: props.width, height: props.height }]}>
      <TextInput style={[styles.txtFldMultiLine, { minHeight: props.height - 20 }]} placeholder={props.placeholder} placeholderTextColor={APP_GREY_PLACEHOLDER_COLOR} keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} value={props.value} onChangeText={(text) => props.onChange(text)} multiline={true} blurOnSubmit={true} />
    </View>
  )
}

export const TextInputPhoneNum = (props: any) => {

  return (
    <View style={[styles.inputContainer, (props.marginTop != null ? { marginTop: props.marginTop } : null), (props.width != null ? { width: props.width } : null)]}>
      <TouchableOpacity style={styles.dropdownBtn} onPress={() => props.func()} disabled={props.disabled != null ? props.disabled : false}>
        {props.img != null ?
          <Image
            style={styles.imgVw}
            source={props.img}
          /> : null
        }
        <Image
          style={{
            height: 10,
            width: 10,
            marginLeft: 4,
            marginRight: 4,
            resizeMode: 'contain',
          }}
          source={require('../assets/arrow-down.png')}
        />
      </TouchableOpacity>
      <Text style={[styles.countryCodeTxt, (props.textAlign != null ? { textAlign: props.textAlign } : null), (props.countryCode == '' ? { color: APP_GREY_PLACEHOLDER_COLOR } : { color: APP_GREY_BOLD_TEXT_COLOR })]}>{props.countryCode != '' ? '+' + props.countryCode : props.countryPlaceholder}</Text>
      <TextInput style={[styles.txtFld12, (props.fontSize != null ? { fontSize: props.fontSize } : null)]} placeholder={props.placeholder} placeholderTextColor={APP_GREY_PLACEHOLDER_COLOR} keyboardType='phone-pad' value={props.value} onChangeText={(text) => props.onChange(text)} returnKeyType="done" maxLength={12}/>
    </View>
  )
}

export const TextInputWithImage = (props: any) => {

  return (
    <View style={[styles.inputContainer, (props.backgroundColor != null ? { backgroundColor: props.backgroundColor } : null), (props.width != null ? { width: props.width } : null), (props.marginTop != null ? { marginTop: props.marginTop } : null)]}>
      {props.func != null && props.rightImage == null ?
        <TouchableOpacity onPress={() => props.func()}>
          <Image
            style={styles.imgVw}
            source={props.icon}
          />
        </TouchableOpacity> :
        (props.rightImage == null ?
          <Image
            style={styles.imgVw}
            source={props.icon}
          /> : null)
      }

      <TextInput ref={props.ref != null ? props.ref : null} style={[styles.txtFld, {
        adjustsFontSizeToFitWidth: true,
        flexWrap: 'wrap'
      }, { width: '90%', fontSize: 13 },
      ]} placeholder={props.placeholder} placeholderTextColor={props.placeholderColor} keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} onChangeText={(text) => props.onChange(text)} value={props.value} autoCapitalize={props.autoCapitalize != null ? props.autoCapitalize : 'words'} secureTextEntry={props.secureTextEntry != null ? props.secureTextEntry : false} editable={props.editable != null ? props.editable : true} returnKeyType={props.returnKeyType != null ? props.returnKeyType : 'default'} maxLength={props.maxLength != null ? props.maxLength : null} />

      {props.func != null && props.rightImage != null ?
        <TouchableOpacity onPress={() => props.func()}>
          <Image
            style={styles.imgVw}
            source={props.icon}
          />
        </TouchableOpacity> :
        (props.rightImage != null ?
          <Image
            style={styles.imgVw}
            source={props.icon}
          /> : null)
      }
    </View>
  )
}

export const TextInputWithBtn = (props: any) => {
  return (
    <TouchableOpacity style={[styles.inputContainer, (props.backgroundColor != null ? { backgroundColor: props.backgroundColor } : null), (props.alignSelf != null ? { alignSelf: props.alignSelf } : null), (props.marginTop != null ? { marginTop: props.marginTop } : null), (props.width != null ? { width: props.width } : null), (props.height != null ? { height: props.height } : null)]} onPress={() => props.func()} disabled={props.disabled != null ? props.disabled : false}>
      {props.rightImage == null  && props.icon != null?
        <Image
          style={[styles.imgVw, (props.imgSizeWidth != null ? {width: props.imgSizeWidth, height: props.imgSizeHeight} : null)]}
          source={props.icon}
        /> : null}
      <Text style={[styles.txtInputWithBtnText, (props.textAlign != null ? { textAlign: props.textAlign } : null), (props.value == '' ? { color: APP_GREY_PLACEHOLDER_COLOR } : null), (props.fontSize != null ? { fontSize: props.fontSize } : null), (props.height != null ? { height: props.height, lineHeight: props.height } : null), (props.color != null ? { color: props.color } : null),]}>{props.value != '' ? props.value : props.placeholder}</Text>
      {props.rightImage != null ?
        <Image
          style={[styles.imgVw, (props.imgSizeWidth != null ? {width: props.imgSizeWidth, height: props.imgSizeHeight} : null)]}
          source={props.icon}
        /> : null}
    </TouchableOpacity>
  )
}

export const TextInputWithImageFoucsEdit = (props: any) => {

  return (
    <View style={[styles.inputContainer, (props.backgroundColor != null ? { backgroundColor: props.backgroundColor } : null), (props.alignSelf != null ? { alignSelf: props.alignSelf } : null)]}>
      {props.rightImage == null ?
        <Image
          style={styles.imgVw}
          source={props.icon}
        /> : null}
      <TextInput style={styles.txtFld} onFocus={() => props.focus()} placeholder={props.placeholder} placeholderTextColor={APP_GREY_PLACEHOLDER_COLOR} keyboardType={props.keyboardType != null ? props.keyboardType : 'default'} value={props.value} onChangeText={(text) => props.onChange(text)} />
      {props.rightImage != null ?
        <Image
          style={styles.imgVw}
          source={props.icon}
        /> : null}
    </View>
  )
}

export const TextInputPhone = (props: any) => {
  return (
    <View style={[styles.inputContainer, { width: '60%', alignSelf: 'flex-end' }]}>
      <TextInput style={styles.txtFld} placeholder={props.placeholder} placeholderTextColor='grey' keyboardType='phone-pad' value={props.value} onChangeText={(text) => props.onChange(text)} returnKeyType="done" />
    </View>
  )
}

const styles = ScaleSheet.create({

  //View Components
  imgVw: {
    height: 16,
    width: 16,
    marginRight: 8,
    marginLeft: 8,
    resizeMode: 'contain',
  },
  inputContainer: {
    flexDirection: 'row',
    marginTop: 16,
    alignItems: 'center',
    height: 40,
    padding: 8,
    borderWidth: 0.2,
    borderColor: APP_BORDER_GREY_COLOR,
    width: '90%',
    borderRadius: 10,
    backgroundColor: 'white',
  },
  // Text Input Components
  txtFld: {
    textAlign: 'left',
    flex: 1,
    height: 40,
    fontSize: 16,
    fontFamily: APP_FONT_REGULAR,
    color: APP_GREY_TEXT_COLOR,
  },
  txtFld12: {
    textAlign: 'left',
    flex: 1,
    height: 40,
    fontSize: 12,
    fontFamily: APP_FONT_REGULAR,
    color: APP_GREY_BOLD_TEXT_COLOR,
  },
  txtFldMultiLine: {
    flex: 1,
    textAlign: 'left',
    height: 'auto',
    fontSize: 12,
    fontFamily: APP_FONT_REGULAR,
    color: APP_GREY_BOLD_TEXT_COLOR,
    // Android only Property
    textAlignVertical: 'top'
  },
  countryCodeTxt: {
    textAlign: 'left',
    height: 40,
    lineHeight: 40,
    fontSize: 14,
    fontFamily: APP_FONT_REGULAR,
    color: APP_GREY_BOLD_TEXT_COLOR,
    marginRight: 16,
    marginLeft: 16
  },
  txtInputWithBtnText: {
    flex: 1,
    textAlign: 'left',
    height: 40,
    lineHeight: 40,
    fontSize: 14,
    fontFamily: APP_FONT_REGULAR,
    color: APP_GREY_BOLD_TEXT_COLOR,
  },
  dropdownBtn: {
    //flex: 1,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
    //justifyContent: 'center', 
  },
  verificationTxtFld: {
    textAlign: 'center',
    width: '100%',
    height: 100,
    fontSize: 30,
    fontFamily: APP_FONT_REGULAR,
    color: APP_GREY_TEXT_COLOR,
  },
  txtFldWithoutBorder: {
    textAlign: 'left',
    width: '80%',
    height: 50,
    fontSize: 14,
    fontFamily: APP_FONT_REGULAR,
    color: APP_GREY_TEXT_COLOR,
  },
})
