import React from 'react';
import { View, StyleSheet, Text, FlatList, Dimensions, SafeAreaView, TouchableOpacity } from 'react-native';
import ListCell from '../custom cells/ListCell';
import { CommonBtn } from '../custom/CustomButton';

export const DropdownList = (props: any) => {

  console.log('Props data in Dropdownlist', props)
  return (

    <View style={styles.container}>
      <View style={[styles.popupVw]}>
        <FlatList
          style={styles.list}
          data={props.data}
          keyExtractor={(item: any, index: any) => index.toString()}
          renderItem={({ item, index }) => <ListCell
          onClickEvent={(item: any) => props.func(item)}
          item={item} selecedItem={props.selecedItem} />}
        />
        <CommonBtn title={'Cancel'} func={() => props.removeList()} width='60%' height={40} marginBottom={8}/>
      </View>
    </View>
    // <SafeAreaView style={{ backgroundColor: 'rgba(0, 0, 0, 0.5)', height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center' }}>
    //   <View style={[styles.listVw, shadowStyle, { marginTop: props.topMargin }]}>

    //     <CommonBtn title={'Cancel'} func={() => props.removeList()} width='60%' height={40} />
    //   </View>
    // </SafeAreaView>
  )
}

export const OptionsMenu = (props: any) => {

  return (
    <View style={[styles.optionsMenu, shadowStyle]}>
      <FlatList
        data={props.data}
        keyExtractor={(item: any, index: any) => index.toString()}
        renderItem={({ item, index }) => <ListCell cellData={item} type={props.type} index={index} func={(props.func)} />}
      /></View>
  )
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#140F2693',
    alignItems: 'center',
    justifyContent: 'center'
  },
  popupVw: {
    margin: 16,
    padding: 8,
    borderRadius: 10,
    backgroundColor: 'white',
    width: '80%',
  },
  list: {
    marginTop: 8,
    borderRadius: 10,
  },
  //Button
  listVw: {
    width: Dimensions.get('window').width - 40,
    padding: 10,
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
  },
  optionsMenu: {
    height: 80,
    width: 200,
    padding: 10,
    marginRight: 20,
    marginTop: 10,
    backgroundColor: 'white',
    position: 'absolute',
  },
  btnStyle: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  }
})

export const shadowStyle = {
  shadowColor: 'black',
  shadowRadius: 2,
  shadowOpacity: 0.5,
  shadowOffset: { width: 0, height: 2 },
  elevation: 2,
}
