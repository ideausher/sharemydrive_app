import React from 'react';
import { Text, TouchableOpacity, View, Image } from 'react-native';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREEN_COLOR } from '../constants/colors';
import { TextGrey } from './CustomText';

export const CommonAlert = (props: any) => {

  return (
    <View style={styles.conatiner}>
      <Image style={styles.image} source={require('../assets/tick.png')}></Image>
      <Text style={styles.title}>{props.title}</Text>
      <TouchableOpacity
        style={styles.btn}
        onPress={() => props.func()}>
        <Text style={styles.text}>Ok</Text>
      </TouchableOpacity>
    </View>

  )
}


const styles = ScaleSheet.create({
  conatiner: {
    width: '70%',
    height: 300,
    flexDirection: 'column'
  },
  image: {
    width: 70,
    height: 70,
  },
  title: {
    fontSize: 22,
    color: APP_GREEN_COLOR,
    marginTop: 20,
  },
  //Button
  btn: {
    width: '80%',
    height: 40,
    backgroundColor: APP_GREEN_COLOR,
    marginTop: 20,
    justifyContent: 'center',
    color: 'white',
    borderRadius: 10,
    alignSelf: 'center',
    marginBottom: 16
  },

})
