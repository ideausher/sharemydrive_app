import React, { useRef } from 'react';
import { View, TouchableOpacity, Image, Text, I18nManager } from 'react-native';
import { APP_LIGHT_GREY_COLOR, APP_GREY_TEXT_COLOR, App_MIDIUM_GREY_COLOR, APP_SHADOW_COLOR, APP_GREEN_COLOR } from '../constants/colors';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { CommonBtn } from './CustomButton';
import LinearGradient from 'react-native-linear-gradient';
import { TextGrey } from './CustomText';

export const LineVw = () => {
  return (
    <View style={{ height: 1.0, alignSelf: 'center', backgroundColor: APP_LIGHT_GREY_COLOR, width: '100%', marginTop: 10 }}></View>
  )
}

export const BackHeaderBtn = (props: any) => {
  return (
    <TouchableOpacity style={styles.headerBtn} activeOpacity={0.9} onPress={props.func}>
      <Image style={styles.headerImg} source={require('../assets/back-arrow.png')} />
    </TouchableOpacity>
  )
}

export const OpenDrawerHeaderBtn = (props: any) => {
  return (
    <TouchableOpacity style={styles.headerBtn} activeOpacity={0.9} onPress={props.func}>
      <Image style={styles.headerImg} source={props.isWhite ? require('../assets/menuWhite.png') : require('../assets/menu.png')} />
    </TouchableOpacity>
  )
}

export const NotificationsHeaderBtn = (props: any) => {
  return (
    <TouchableOpacity style={styles.headerBtn} activeOpacity={0.9} onPress={props.func}>
      <Image style={styles.headerImg} source={props.isHighlighted ? require('../assets/notiBellYellow.png') : (props.isWhite ? require('../assets/notiBellWhite.png') : require('../assets/notiBell.png'))} />
    </TouchableOpacity>
  )
}

export const NoDataFoundView = (props: any) => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      {props.message != '' ? <Text style={styles.text}>{props.message}</Text> : null}
      <Image style={styles.noDataImage} source={require('../assets/cartoonWithPaper.png')} />
      <TouchableOpacity style={{ padding: 30 }} onPress={props.func}>
        <Text style={{ color: APP_GREEN_COLOR, textDecorationLine: 'underline', fontWeight: "bold" }}>Try Again</Text>
      </TouchableOpacity>
    </View>
  )
}

export const NoInternetFoundView = (props: any) => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Image style={styles.noInternetImage} source={require('../assets/internet_error.png')} />
      <Text style={styles.light_text}>Please check your internet connection</Text>
      <TouchableOpacity style={{ padding: 30 }} onPress={props.func}>
        <Text style={{ color: APP_GREEN_COLOR, textDecorationLine: 'underline', fontWeight: "bold" }}>Try Again</Text>
      </TouchableOpacity>
    </View>
  )
}

export const NotificationsNoDataFoundView = (props: any) => {
  return (
      <View style={{ flex: 1, alignItems: 'center', marginTop: 50 }}>
          <Image style={[styles.notificatiosnNoDataImage, (props.marginTop!= null ? {marginTop: props.marginTop} : null)]} source={props.img} />
          <TextGrey marginLeft={16} marginTop={32} fontSize={14} text={props.title} />
          {props.message != '' ? <TextGrey marginLeft={12} marginTop={16} fontSize={12} text={props.message} /> : null}
         {props.func != null ? <TouchableOpacity style={{ width: '100%', alignItems: 'center', padding: 30 }} onPress={props.func}>
              <Text style={{ textAlign: 'center', width: '100%', color: APP_GREEN_COLOR, textDecorationLine: 'underline', fontWeight: "bold" }}>Try Again</Text>
          </TouchableOpacity> : null }
      </View>
  )
}

export const BottomShadowView = (props: any) => {
  return (
    //<View />
    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} colors={['transparent', '#eee',]} style={styles.gradient} />
  )
}

const styles = ScaleSheet.create({

  headerBtn:
  {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerImg: {
    height: 16,
    width: 16,
    resizeMode: 'contain'
  },
  text: {
    fontSize: 18,
    fontWeight: '400',
    color: APP_GREY_TEXT_COLOR,
    textAlign: 'center',
  },
  light_text: {
    fontSize: 15,
    fontWeight: '400',
    color: App_MIDIUM_GREY_COLOR,
    textAlign: 'center',
    marginTop: 8
  },
  gradient: {
    bottom: 0,
    position: 'absolute',
    height: 5,
    backgroundColor: 'red',
  },
  noDataImage: {
    height: 150,
    width: 180,
    resizeMode: 'contain',
    marginTop: 60
  },
  noInternetImage: {
    height: 70,
    width: 70,
    resizeMode: 'contain'
  },
  notificatiosnNoDataImage: {
    height: 140,
    width: 150,
    resizeMode: 'contain',
    marginTop: 0
  },
})

export const bottomShadowVwStyle = {
  position: 'absolute',
  bottom: 5,
  backgroundColor: 'green',
  height: 0,
  width: '100%',
  shadowColor: APP_SHADOW_COLOR,
  shadowOffset: { width: 0, height: 7 },
  shadowRadius: 7,
  shadowOpacity: 1.0,
  elevation: 7,
}
