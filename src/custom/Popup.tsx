import React from 'react';
import { View, Image } from 'react-native'
import { CommonBtn, GreenTextBtn } from '../custom/CustomButton';
import { TextGreyBold, TextGrey } from '../custom/CustomText';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREY_TEXT_COLOR } from '../constants/colors';

export const Popup = (props: any) => {
    return (
        <View style={styles.container}>
            <View style={[styles.popupVw]}>
                {props.img != null ? <Image source={props.img} style={styles.image} /> : null}
                {props.title != null ? <TextGreyBold marginTop={36} fontSize={14} text={props.title} width='70%' /> : null}
                {props.msg != null ? <TextGrey marginTop={16} text={props.msg} width='70%' /> : null}
                <CommonBtn title={props.btnText} func={() => props.func()} width='70%' marginTop={40} marginBottom={16} fontSize={15}/>
                {props.greenBtnText != null ? <GreenTextBtn title={props.greenBtnText} func={() => props.greenBtnAction()} fontSize={15} marginTop={0} marginBottom={16}/> : null}
            </View>
        </View>
    )
  }

  const styles = ScaleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#140F2693', 
        alignItems:'center',
        justifyContent:'center'
    },  
    popupVw: {
        margin: 16,
        borderRadius: 10,
        backgroundColor: 'white',
        width: '80%',
        alignItems:'center'
    },
    image: {
        width: 120,
        height:120,
        marginTop: 60
    },
    text: {
        marginTop: 80,
        marginLeft: 50,
        marginRight: 50,
        marginBottom: 40,
        fontSize: 18,
        fontWeight: '400',
        color: APP_GREY_TEXT_COLOR,
        textAlign: 'center',
        
      },  
})
