import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREEN_COLOR, App_MIDIUM_GREY_COLOR, APP_NAVY_BLUE_COLOR } from '../constants/colors';
import { APP_FONT_REGULAR, APP_FONT_BOLD } from '../constants/fonts';
import { TextGrey } from './CustomText';

export const CommonBtn = (props: any) => {

  return (
    <TouchableOpacity
      style={[styles.btn, (props.width != null ? { width: props.width } : null), (props.height != null ? { height: props.height } : null), (props.marginTop != null ? { marginTop: props.marginTop } : null), (props.height != null ? { height: props.height } : null), (props.alignSelf != null ? { alignSelf: props.alignSelf } : null), (props.marginRight != null ? { marginRight: props.marginRight } : null), (props.position != null ? { position: props.position } : null), (props.marginBottom != null ? { marginBottom: props.marginBottom } : null), (props.bottom != null ? { bottom: props.bottom } : null)]}
      onPress={() => props.func()}>
      <Text style={styles.text}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export const BlueTextBtn = (props: any) => {

  return (
    <TouchableOpacity onPress={() => props.func()} style={[styles.blueTextBtn, (props.width != null ? { width: props.width } : null), (props.alignSelf != null ? { alignSelf: props.alignSelf } : null), (props.marginLeft != null ? { marginLeft: props.marginLeft } : null), (props.marginRight != null ? { marginRight: props.marginRight } : null), (props.marginTop != null ? { marginTop: props.marginTop } : null), (props.textAlign != null ? { textAlign: props.textAlign } : null)]}>
      <Text style={[styles.blueText, (props.textAlign != null ? { textAlign: props.textAlign } : 'center'), (props.width != null ? { width: props.width } : null), (props.fontSize != null ? { fontSize: props.fontSize } : null), (props.bold != null ? { fontFamily: APP_FONT_BOLD } : null)]}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export const GreenTextBtn = (props: any) => {

  return (
    <TouchableOpacity onPress={() => props.func()} style={[styles.blueTextBtn, (props.width != null ? { width: props.width } : null), (props.alignSelf != null ? { alignSelf: props.alignSelf } : null), (props.marginLeft != null ? { marginLeft: props.marginLeft } : null), (props.marginTop != null ? { marginTop: props.marginTop } : null), (props.marginBottom != null ? { marginBottom: props.marginBottom } : null), (props.textAlign != null ? { textAlign: props.textAlign } : null)]}>
      <Text style={[styles.blueText, { color: APP_GREEN_COLOR }, (props.textAlign != null ? { textAlign: props.textAlign } : 'center'), (props.width != null ? { width: props.width } : null), (props.fontSize != null ? { fontSize: props.fontSize } : null), (props.bold != null ? { fontFamily: APP_FONT_BOLD } : null)]}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export const CommonBtnWhite = (props: any) => {

  return (
    <TouchableOpacity
      style={[styles.btnWhite, (props.marginTop != null ? { marginTop: props.marginTop } : null)]}
      onPress={() => props.func()}>
      <Text style={[styles.text, { color: App_MIDIUM_GREY_COLOR, fontSize: 14 }]}>{props.title}</Text>
    </TouchableOpacity>
  )
}

export const BorderBtn = (props: any) => {

  return (
    <TouchableOpacity
      style={[styles.borderBtn, (props.marginLeft != null ? { marginLeft: props.marginLeft } : null), (props.marginTop != null ? { marginTop: props.marginTop } : null), (props.borderColor != null ? { borderColor: props.borderColor } : null), (props.width != null ? { width: props.width } : null), (props.height != null ? { height: props.height } : null), (props.backgroundColor != null ? { backgroundColor: props.backgroundColor } : null)]}
      onPress={() => props.func(props.index)}>
      <TextGrey marginTop={0} fontSize={12} text={props.title} textAlign='center' color={props.titleColor} />
    </TouchableOpacity>
  )
}

const styles = ScaleSheet.create({
  //Button
  btn: {
    width: '80%',
    height: 40,
    backgroundColor: APP_GREEN_COLOR,
    marginTop: 20,
    justifyContent: 'center',
    color: 'white',
    borderRadius: 10,
    alignSelf: 'center',
    marginBottom: 16
  },
  btnWhite: {
    width: '80%',
    height: 50,
    backgroundColor: 'white',
    marginTop: 20,
    justifyContent: 'center',
    borderRadius: 25,
    borderWidth: 2,
    borderColor: APP_GREEN_COLOR,
    alignSelf: 'center',
    marginBottom: 80
  },
  text: {
    textAlign: 'center',
    color: 'white',
    fontSize: 15,
    fontFamily: APP_FONT_REGULAR,
  },
  blueTextBtn: {
    marginTop: 8,
    justifyContent: 'center',
    width: 120,
    height: 30,
  },
  blueText: {
    textAlign: 'center',
    color: APP_NAVY_BLUE_COLOR,
    fontSize: 12,
    fontFamily: APP_FONT_REGULAR,
  },
  borderBtn: {
    marginTop: 8,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'red',
    width: 90,
    height: 20,
    justifyContent: 'center'
  }
})
