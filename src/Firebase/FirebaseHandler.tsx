import firebase, { Firebase } from 'react-native-firebase'
import { Alert, Platform } from 'react-native';

// Change Password
export const reauthenticate = (oldPswd: string, newPswd: string) => {
    return new Promise((resolve, reject) => {
        var user = firebase.auth().currentUser;
        var cred = firebase.auth.EmailAuthProvider.credential(
            user.email, oldPswd);
        user.reauthenticateWithCredential(cred).then((data) => {

            user.updatePassword(newPswd).then(() => {
                Alert.alert('Password updated successfully')
                resolve()
            }, (error) => {
                Alert.alert(error.message)
            });
        }, (error) => {
            Alert.alert(error.message)
        }).catch((error) => {
            console.log('Error while reauthenticate>>>', error)
        });
    })
}

export const getLoginUserInfo = (uid: string) => {
    return new Promise((resolve, reject) => {
        let path = '/organization_user_ids/' + uid

        firebase.database().ref(path).once('value', (snapshot) => {
            if (snapshot.val()) {
                let data = snapshot.val()
                resolve(data)
            }
            else {
                Alert.alert('Data not Found')
            }
        }, (error) => {
            Alert.alert(error.message)
        }).catch((error) => {
            console.log('Error while getLoginUserInfo>>>', error)
        });
    })
}

export const updateProfile = (organizationId: string, uid: String, details: any, userType: Number) => {
    return new Promise((resolve, reject) => {
        var path = ''
        if (userType == 1) {
            path = '/organization_details/' + organizationId + '/employee_details/admins/' + uid
        }
        else if (userType == 2) {
            path = '/organization_details/' + organizationId + '/employee_details/sub_admins/' + uid
        }
        else if (userType == 3) {
            path = '/organization_details/' + organizationId + '/employee_details/drivers/' + uid
        }
        firebase.database().ref(path).update(details).then((success) => {
            resolve(success);
        }, (error) => {
            Alert.alert(error.message)
        }).catch((error) => {
            reject(error);
            console.log('Error while updateProfile>>>', error)
        });
    })
}

export const createAM = (organizationId: string, uid: String, details: any) => {
    return new Promise((resolve, reject) => {
        let path = '/organization_details/' + organizationId + '/employee_details/sub_admins/' + uid

        firebase.database().ref(path).set(details).then((success) => {
            Alert.alert('Ambulance Manager created successfully')
        }, (error) => {
            Alert.alert(error.message)
        }).catch((error) => {
            console.log('Error while createAM>>>', error)
        });
    })
}

export const createVehicle = (organizationId: string, pushKey: string, details: any) => {
    return new Promise((resolve, reject) => {

        let path = '/organization_details/' + organizationId + '/vehicle_details/' + pushKey

        firebase.database().ref(path).set(details).then((success) => {
            Alert.alert('Vehicle created successfully')
            resolve()
        }, (error) => {
            Alert.alert(error.message)
        }).catch((error) => {
            console.log('Error while createVehicle>>>', error)
        });
    })
}

export const createUser = (email: string) => {

    return new Promise((resolve, reject) => {

        var config = {
            apiKey: "AIzaSyCMk0Ylv0c5WKotmdhXpkU3-pXN2DkKVKI",
            authDomain: "ambotracker.firebaseapp.com",
            databaseURL: "https://ambotracker.firebaseio.com",
            projectId: "ambotracker",
            storageBucket: "ambotracker.appspot.com",
            messagingSenderId: "sender-id",
            appId: Platform.OS === 'ios' ? '1:419273788852:ios:d4fe60f88342a9a1' : '1:419273788852:android:a90c46f3fe768f14',
        };
        var secondaryApp = firebase.initializeApp(config, "Secondary");

        secondaryApp.auth().createUserWithEmailAndPassword(email, '123456').then(function (credential) {

            resolve(credential)
            secondaryApp.auth().signOut();
        }, (error) => {
            Alert.alert(error.message)
        }).catch((error) => {
            console.log('Error while signupDriver>>>', error)
        });
    })
}

export const addDriverDetails = (uid: string, details: any) => {
    return new Promise((resolve, reject) => {

        let path = '/organization_user_ids/' + uid

        firebase.database().ref(path).set(details).then((success) => {

            resolve()
        }, (error) => {
            Alert.alert(error.message)
        }).catch((error) => {
            console.log('Error while addDriverDetails>>>', error)
        });
    })
}

export const createDriver = (organizationId: string, uid: string, details: any) => {
    return new Promise((resolve, reject) => {

        let path = '/organization_details/' + organizationId + '/employee_details/drivers/' + uid

        firebase.database().ref(path).set(details).then((success) => {
            resolve(success)
        }, (error) => {
            Alert.alert(error.message)
        }).catch((error) => {
            console.log('Error while createDriver>>>', error)
        });
    })
}

export const fetchProfileDetails = (organizationId: string, uid: string, userType: Number) => {

    return new Promise((resolve, reject) => {
        var path = ''
        if (userType == 1) {
            path = '/organization_details/' + organizationId + '/employee_details/admins/' + uid
        }
        else if (userType == 2) {
            path = '/organization_details/' + organizationId + '/employee_details/sub_admins/' + uid
        }
        else if (userType == 3) {
            path = '/organization_details/' + organizationId + '/employee_details/drivers/' + uid
        }

        firebase.database().ref(path).once('value', (snapshot) => {

            if (snapshot.val()) {
                let data = snapshot.val()
                resolve(data)
            }
            else {
                reject('Data not Found')
            }
        }, (error) => {
            Alert.alert(error.message)
        }).catch((error) => {
            console.log('Error while fetchProfileDetails>>>', error)
        });
    })
}

export const registerAMInOrg = (uid: String, details: any) => {

    return new Promise((resolve, reject) => {
        let path = '/organization_user_ids/' + uid
        firebase.database().ref(path).set(details).then((success) => {
            resolve()
        }, (error) => {
            Alert.alert(error.message)
        }).catch((error) => {
            console.log('Error while registerAMInOrg>>>', error)
        });
    })
}

export const createRoute = (organizationId: string, details: any) => {

    return new Promise((resolve, reject) => {
        let ref = firebase.database().ref('organization_details').child(organizationId).child('route_details').child(firebase.auth().currentUser!.uid).push();
        details['mkey'] = ref.key;
        ref.set(details).then((success) => {
            Alert.alert('Route created successfully')
            resolve()
        }, (error) => {
            Alert.alert(error.message);
        }).catch((error) => {
            console.log('Error while createRoute>>>', error)
        });
    })
}

export const fetchVehicles = (organizationId: string, subAdminId: any) => {
    return new Promise((resolve, reject) => {
        let path = '/organization_details/' + organizationId + '/vehicle_details'

        firebase.database().ref(path).orderByChild('subAdminId').equalTo(subAdminId).on('value', (snapshot) => {
            if (snapshot.val()) {
                let data = snapshot.val()
                resolve(data)
            }
            else {
                //reject('Data not Found')
            }
        }, (error) => {
            Alert.alert(error.message);
        });
    })
}

export const forgotPassword = (email: string) => {
    return new Promise((resolve, reject) => {
        firebase.auth().sendPasswordResetEmail(email)
            .then(function (user) {
                resolve(user);
            }, (error) => {
                Alert.alert(error.message);
            }).catch((error) => {
                console.log('Error while forgotPassword>>>', error)
            });
    })
}

export const fetchRoutes = (organizationId: string, subAdminId: string) => {
    return new Promise((resolve, reject) => {
        let path = '/organization_details/' + organizationId + '/route_details/' + subAdminId

        firebase.database().ref(path).on('value', (snapshot) => {

            if (snapshot.val()) {
                let data = snapshot.val()
                resolve(data)
            }
            else {
                //reject('Data not Found')
            }
        }, (error) => {
            Alert.alert(error.message);
        });
    })
}

export const fetchDrivers = (organizationId: string) => {
    return new Promise((resolve, reject) => {
        let path = '/organization_details/' + organizationId + '/employee_details/drivers/'

        firebase.database().ref(path).on('value', (snapshot) => {

            if (snapshot.val()) {
                let data = snapshot.val()
                resolve(data)
            }
            else {
                //reject('Data not Found')
            }
        }, (error) => {
            Alert.alert(error.message);
        });
    })
}

export const sendBroadcast = (organizationId: string, uid: string, details: any) => {

    return new Promise((resolve, reject) => {
        let ref = firebase.database().ref('organization_details').child(organizationId).child('notification_details').child(uid).push();
        details['mkey'] = ref.key;
        ref.set(details).then((success) => {
            resolve(success);
        }, (error) => {
            Alert.alert(error);
        }).catch((error) => {
            console.log('Erro while sendBroadcast>>>', error)
        });
    })
}

export const updateMessage = (organizationId: string, details, key: string) => {

    return new Promise((resolve, reject) => {
        let ref = firebase.database().ref('organization_details').child(organizationId).child('notification_details').child(firebase.auth().currentUser!.uid).child(key);
        ref.update(details).then((success) => {
            resolve(success);
        }, (error) => {
            Alert.alert(error);
        }).catch((error) => {
            console.log('Error while updateMessage>>>', error)
        });
    })
}

export const deleteNotification = (organizationId: string, key: string) => {

    return new Promise((resolve, reject) => {
        let ref = firebase.database().ref('organization_details').child(organizationId).child('notification_details').child(firebase.auth().currentUser!.uid).child(key);
        ref.remove().then((success) => {
            resolve(success);
        }, (error) => {
            Alert.alert(error.message);
        }).catch((error) => {
            console.log('Error while deleteNotification>>>', error)
        });
    })
}

export const activateDrivers = (organizationId: string, driverId: string, details: any) => {
    return new Promise((resolve, reject) => {
        let path = '/organization_details/' + organizationId + '/employee_details/drivers/' + driverId

        firebase.database().ref(path).update(details).then((success) => {
            Alert.alert('Driver activated successfully');

        }, (error) => {
            Alert.alert(error.message);
        }).catch((error) => {
            console.log('Error while activateDrivers>>>', error)
        });
    })
}

export const updateDriver = (organizationId: string, driverId: string, details: any) => {
    return new Promise((resolve, reject) => {
        let path = '/organization_details/' + organizationId + '/employee_details/drivers/' + driverId

        firebase.database().ref(path).update(details).then((success) => {
           // Alert.alert('Driver updated successfully');

        }, (error) => {
            Alert.alert(error.message);
        }).catch((error) => {
            console.log('Error while updateDriver>>>', error)
        });
    })
}

export const updateVehicle = (organizationId: string, vehicleId: string, details: any) => {
    return new Promise((resolve, reject) => {
        let path = '/organization_details/' + organizationId + '/vehicle_details/' + vehicleId

        firebase.database().ref(path).update(details).then((success) => {
            resolve(success);
        }, (error) => {
            Alert.alert(error.message);
        }).catch((error) => {
            console.log('Error while updateVehicle>>>', error)
        });
    })
}

export const updateRoute = (organizationId: string, routeId: string, subAdminId: string, details: any) => {
    return new Promise((resolve, reject) => {
        let path = '/organization_details/' + organizationId + '/route_details/' + subAdminId + '/' + routeId

        firebase.database().ref(path).update(details).then((success) => {
            resolve(success)
        }, (error) => {
            Alert.alert(error.message);
        }).catch((error) => {
            console.log('Error while updateRoute>>>', error)
        });
    })
}

export const startRoute = (organizationId: string, vehicleId: string, routeId: string, year: string, month: string, date: string, key, details: any) => {

    return new Promise((resolve, reject) => {
        var path = '/organization_details/' + organizationId + '/tracking/' + vehicleId + '/' + routeId + '/' + year + '/' + month + '/' + date + '/' + key + '/location_details'
        let ref = firebase.database().ref(path).push()
        details['mkey'] = ref.key;

        ref.set(details).then((success) => {
            resolve(success)
        }, (error) => {
            Alert.alert(error.message);
        }).catch((error) => {
            console.log('Error while startRoute>>>', error)
        });

        // Updtae Current Location
        var currentLocationPath = '/organization_details/' + organizationId + '/tracking/' + vehicleId + '/' + routeId + '/current_location'
        details['mkey'] = key
        firebase.database().ref(currentLocationPath).update(details).then((success) => {

        }, (error) => {
            Alert.alert(error.message);
        }).catch((error) => {
            console.log('Error while startRoute>>>', error)
        });
    })
}

export const updateTRackingHistory = (organizationId: string, vehicleId: string, routeId: string, year: string, month: string, date: string, details: any) => {

    return new Promise((resolve, reject) => {

        var ref: any

        if (details['mkey'] == '') {
            var path = '/organization_details/' + organizationId + '/tracking_history/' + vehicleId + '/' + routeId + '/' + year + '/' + month + '/' + date
            ref = firebase.database().ref(path).push()
            details['mkey'] = ref.key;
        }
        else {
            var path = '/organization_details/' + organizationId + '/tracking_history/' + vehicleId + '/' + routeId + '/' + year + '/' + month + '/' + date + '/' + details['mkey']
            ref = firebase.database().ref(path)
        }

        ref.set(details).then((success) => {
            resolve(ref.key)
        }, (error) => {
            Alert.alert(error.message);
        }).catch((error) => {
            console.log('Error while updateTRackingHistory>>>', error)
        });
    })

}


export const stopRoute = (organizationId: string, vehicleId: string, routeId: string, year: string, month: string, date: string, key: string, details: any) => {

    return new Promise((resolve, reject) => {
        var path = '/organization_details/' + organizationId + '/tracking_history/' + vehicleId + '/' + routeId + '/' + year + '/' + month + '/' + date + '/' + key
        firebase.database().ref(path).update(details).then((success) => {
            resolve(success)
        }, (error) => {
            Alert.alert(error.message);
        }).catch((error) => {
            console.log('Error while updateRstopRouteoute>>>', error)
        });
    })

}
