//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD, APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
    },
    linearGradient: {
        flex: 1, 
        justifyContent:'center'
    },
    popupVw: {
        borderRadius: 10,
        backgroundColor: 'white',
        padding: 16,
       // height: '40%',
        margin: 16,
        alignItems: 'center'
    },
    greyBoldText: {
        fontSize: 20,
        fontFamily: APP_FONT_BOLD,
        color: '#717173',
        textAlign: 'center',
        marginTop: 8,
    },
    image: {
        marginTop: 16, 
        width: 50,
        height: 50
    },
    greyText: {
        fontSize: 16,
        fontFamily: APP_FONT_REGULAR,
        color: '#4B4B4D',
        textAlign: 'center',
        marginTop: 16
    },
});