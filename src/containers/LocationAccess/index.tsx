import React, { Component } from 'react';
import {View, Text, Image} from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import LinearGradient from 'react-native-linear-gradient';
import { styles } from './styles';
import {commonShadowStyle} from '../../common/styles';
import { CommonBtn } from '../../custom/CustomButton';
import { GreenTextBtn } from '../../custom/CustomButton';

export interface props {
    navigation: NavigationScreenProp<any, any>
}
export default class LocationAccess extends Component<props, object> {

    static navigationOptions = () => {
        return {
            header: null
        }
    }

    allowBtnAction() {

    }

    skipBtnAction() {

    }

    render() {
        return (
            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} colors={['#4CE5B1', '#0CB8CB']} style={styles.linearGradient}>
                <View style={[styles.popupVw, commonShadowStyle]}>
                <Text style={styles.greyBoldText}>Use your Location</Text>
                <Image source={require('../../assets/locationCircle.png')} style={styles.image} />
                <Text style={styles.greyText}>Give access to your Location</Text>
                <CommonBtn title='Allow' func={() => this.allowBtnAction()} marginTop={20} width='90%' marginBottom={0}/>
                <GreenTextBtn title='Skip' func={() => this.skipBtnAction()} fontSize={16} marginTop={0}/>
                </View>
            </LinearGradient>
        )
    }

}