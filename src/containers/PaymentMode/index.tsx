import React, { Component } from 'react';
import { View, Text, Modal, Image, TouchableOpacity } from 'react-native';
import { headerStyle, headerTitleStyle, navHdrTxtStyle } from '../../common/styles';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager';
import { API_GET_CARDS, } from '../../constants/apis';
import { Loader } from '../../Utils/Loader';
import General from '../../Utils/General';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextGrey } from '../../custom/CustomText';
import CardCell from '../../custom cells/CardCell';

export interface props {
    navigation: NavigationScreenProp<any, any>,
};

const PaymentOptionsVw = (props: any) => {
    return (
        <TouchableOpacity onPress={() => props.func(props.index)} style={styles.cardVw}>
            <View style={{ flexDirection: 'row' }}>
                <Image
                    source={props.image}
                    style={styles.visaImg}
                    resizeMode={'contain'}
                />
                <TextGrey fontSize={18} text={props.text} textAlign='left' color='#4E4E50' marginTop={0} marginLeft={30} />
            </View>
            {props.selectedIndex == props.index ? <Image
                source={require('../../assets/greenTick.png')}
                style={styles.tickImg}
                resizeMode={'contain'}
            /> : null}
        </TouchableOpacity>
    )
}

export default class AddCard extends Component<props, object> {

    state = {
        showLoader: false,
        selectedIndex: 0
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Payment mode</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    paymentModeSelected(index: any) {
        this.setState({ selectedIndex: index })
        setTimeout(() => {

            this.props.navigation.navigate('AddCard', {
                selectedCard: this.state.selectedIndex
            });
        }, 200)
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    <TextGrey fontSize={18} text='How would you prefer to pay?' textAlign='center' width='90%' color='#888892' marginTop={50} marginLeft={22} marginBottom={40}/>
                    <PaymentOptionsVw index={0} selectedIndex={this.state.selectedIndex} image={require('../../assets/mastercard.png')} func={(index: any) => this.paymentModeSelected(index)} text='Master Card' />
                    <PaymentOptionsVw index={1} selectedIndex={this.state.selectedIndex} image={require('../../assets/visa2.png')} func={(index: any) => this.paymentModeSelected(index)} text='Visa' />
                </View>
            </SafeAreaView>
        );
    }
}