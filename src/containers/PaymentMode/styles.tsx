//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREY_TEXT_COLOR } from '../../constants/colors'

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    cardVw: {
        height: 70,
        flexDirection: 'row',
        padding: 32,
        alignItems: 'center',
        marginBottom: 8,
        justifyContent:'space-between',
      //  backgroundColor:'green'
    },
    visaImg: {
        width: 30,
        height: 30,
    },
    tickImg: {
        width: 26,
        marginRight: 0,
    }
});