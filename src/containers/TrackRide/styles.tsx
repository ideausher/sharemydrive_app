//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD, APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F7F7F7',
        alignItems: 'center'
    },
    headerVw: {
        width: '100%',
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        //backgroundColor:'red',
        position: 'absolute'
    },
    lowerVw: {
        backgroundColor: 'rgba(247, 247, 247, 0.95)',
        width: '90%',
        height: 100,
        marginTop: -20,
        borderRadius: 10
    },
    imgNameContainer: {
        flexDirection: 'row',
        margin: 16,
        alignItems: 'center',
        // backgroundColor:'red'
    },
    nameContainer: {
        marginLeft: 8
    },
    profileImg: {
        height: 50,
        width: 50,
        borderRadius: 25,
        backgroudColor: '#717173'
    },
    locationContainer: {
        flexDirection: 'row',
        marginTop: 8,
        backgroundColor: 'white',
        height: 80,
        alignItems: 'center',
    },
    image: {
        height: 60,
        marginLeft: 25
    },
    originDestinationVw: {
        flex: 1,
        justifyContent: 'space-between',
        marginLeft: 8,
        marginRight: 8,
        height: 60,
        // backgroundColor:'orange'
    },
    carVW:{
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    CarDetailsVw: {
        flex: 1,
        marginLeft: 8,
        marginRight: 32,
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    carImage: {
        height: 24
    }
});