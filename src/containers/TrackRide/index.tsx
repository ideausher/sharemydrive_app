import React, { Component } from 'react';
import { View, Text, Image, StatusBar, Platform, StatusBarIOS, NativeModules, SafeAreaView, Keyboard } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { headerStyle, headerTitleStyle } from '../../common/styles';
import { TextInputWithImageFoucsEdit, TextInputWithBtn } from '../../custom/CustomTextInput';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import Map from '../../Utils/Map';
import RNGooglePlaces from 'react-native-google-places';
import { ORIGIN_INFO, DESTINATION_INFO, SEARCH_DESTINATION_INFO, SEARCH_ORIGIN_INFO } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { firestore } from 'react-native-firebase';
import CustomMap from '../../Utils/CustomMap';
import RequestManager from '../../Utils/RequestManager';
import { API_GET_BOOKING_DETAILS, API_COMPLETE_RIDE, AVATAR_URL } from '../../constants/apis';
import General from '../../Utils/General';
import { getPreciseDistance } from 'geolib';
import moment from 'moment';

export interface props {
    navigation: NavigationScreenProp<any, any>
    pickupInfo: any,
    saveOriginInfo: any,
    saveDestinationInfo: any,
    saveSearchOriginInfo: any,
    saveSearchDestinationInfo: any
}

const { StatusBarManager } = NativeModules;

const CallMsgView = (props: any) => {
    return (
        <View style={styles.callMsgVw}>
            <TextGrey fontSize={13} marginTop={16} text={props.title} color='#919196' />
            <TextGrey fontSize={15} marginTop={16} text={props.value} color='#4E4E50' />
        </View>
    );
};

class TrackRide extends Component<props, object> {

    state = {
        statusBarHeight: 0,
        location: '' as any,
        markers: [] as any[],
        isOrigin: false,
        isSearch: false,
        currentPositionDriver: {} as any,
        bookingDetails: {} as any
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            header: null
        }
    }

    componentDidMount() {
        console.log('Props >>>>', this.props)
        StatusBar.setHidden(true)

        this.fetchRouteDetails()

        // Get Boooking details in case of Passenger for getting Price with Discount
        if (this.props.navigation.getParam('rideInfo').AccOrOff == 'Ride Requested') {
            this.apiGetBookingDetails()
        }
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    // Get paarmeters for Login API
    private getBooingDetailsAPIParams() {
        return {
            "ride_id": this.props.navigation.getParam('rideInfo').id,
            "previous": 0,
        }
    }

    async apiGetBookingDetails() {
        console.log('GET apiGetBookingDetails called with params >>>', this.getBooingDetailsAPIParams())
        this.setState({ showLoader: true, isAPICalledOnce: true, isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_BOOKING_DETAILS, this.getBooingDetailsAPIParams())
            .then((response: any) => {
                console.log('REsponse of apiGetBookingDetails API >>>>', response);
                weakSelf.setState({ showLoader: false, bookingDetails: response.data })

            }).catch((error: any) => {
                General.showErroMsgInternetNoData(this, error)
            })
    }

    fetchRouteDetails() {
        let rideInfo = this.props.navigation.getParam('rideInfo')
        let ref = firestore().collection(rideInfo.user_id.toString()).doc(rideInfo.id.toString());

        console.log('ref In Fetch route Details', ref)

        ref.onSnapshot((querySnapshot: any) => {
            console.log('Snapshot Data  >>>>>', querySnapshot._data)

            this.setState({ currentPositionDriver: querySnapshot._data })
        })
    }

    completeRideBtnAction() {

        if (this.props.navigation.getParam('rideInfo').AccOrOff == 'Ride Requested') {
            this.props.navigation.navigate('DriverRating', { rideInfo: this.props.navigation.getParam('rideInfo') })
        }
        else {
            // stop updating current Location
            let instance = this.props.navigation.getParam('instance')
            console.log('Stop Timer Called', instance.state.timerId)
            clearInterval(instance.state.timerId)
            instance.setState({ timerId: 0, isAnyRideStarted: false })
            
            this.apiCompleteRide()
        }
    }

    // Get parameters of Change Status of Ride Api (Cancel/Start Ride)
    getCompleteRideAPIParams() {

        let rideInfo = this.props.navigation.getParam('rideInfo')
        return {
            "rideid": rideInfo.id,
            "userid": rideInfo.user_id,
        }
    }

    // API tp send OTp on added Phone Number for verification
    apiCompleteRide() {

        console.log('apiCompleteRide called with params >>>>', this.getCompleteRideAPIParams())
        RequestManager.postRequestWithHeaders(API_COMPLETE_RIDE, this.getCompleteRideAPIParams()).then((response: any) => {
            console.log('REsponse of apiCompleteRide >>>>', response);
            this.setState({ showLoader: false })

            // Move to rating Screen after completing Ride
            console.log("accepted_seats >>>>", this.props.navigation.getParam('rideInfo').accepted_seats)
            if(this.props.navigation.getParam('rideInfo').accepted_seats > 0) {
                this.props.navigation.navigate('PassengerRating', { rideInfo: this.props.navigation.getParam('rideInfo') })
            }
            else {
                this.props.navigation.goBack();
            }

            // if (response.message == 'Ride Status Changed Successfully') {
            //     Toast.show(cancel ? 'Ride cancelled' : 'Ride started', Toast.SHORT);
            // }
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    render() {
        let rideInfo = this.props.navigation.getParam('rideInfo')
        console.log('Ride Info in Track Ride >>>>', rideInfo)
        console.log('currentPositionDriver >>>>', this.state.currentPositionDriver)
        let userData = rideInfo.user_details
        let url = AVATAR_URL + userData.image

        console.log('url in RENDER>>>>', url)

        let startPoint = rideInfo.start_location
        if (this.state.currentPositionDriver != undefined && this.state.currentPositionDriver.lat != undefined) {
            console.log('startPoint in If >>>>')
            startPoint = {
                latitude: this.state.currentPositionDriver.lat,
                longitude: this.state.currentPositionDriver.long,
                city: ''
            }
        }

        console.log('startPoint >>>>', startPoint)
        let endPoint = rideInfo.end_location
        let pricePerSeat = Number(rideInfo.price_per_seat) + Number(rideInfo.total_toll_tax)

        let endTime = General.getEndTime(startPoint.latitude, startPoint.longitude, endPoint.latitude, endPoint.longitude, rideInfo.ride_date_time)

        let endTimeMoment = moment(endTime, 'YYYY-MM-DD, hh:mm A')
        console.log('EndTiem Moment >>>>>>>>>>>>>>', endTimeMoment)
        let timeToReach = endTimeMoment.fromNow();


        console.log('Tiem to reachj >>>>>>', timeToReach)

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.container}>

                    {/* <Map markers={this.state.markers} func={(coordinate: any) => this.addMarkers(coordinate)} markerClick={this.markerClick} height='50%' marginTop={0} /> */}
                    <CustomMap
                        //trailArr={this.state.markers}
                        startPoint={startPoint}
                        endPoint={rideInfo.end_location}
                        markers={this.state.markers}
                        func={() => { }}
                        edit={false}
                        height='50%'
                        marginTop={0}
                    />

                    <View style={styles.headerVw}>
                        <BackHeaderBtn func={() => this.backBtnHandler()} />
                    </View>
                    <View style={styles.lowerVw}>
                        <View style={styles.imgNameContainer}>
                            {userData.image == '' ?
                            <Image
                                source={require('../../assets/avatar.png')}
                                style={styles.profileImg}
                                resizeMode='contain'
                            /> :
                            <Image style={styles.profileImg} source={{ uri: url }} />
                        }
                            <View style={styles.nameContainer}>
                                <TextGrey marginTop={0} text={userData.firstname + ' ' + userData.lastname} textAlign='left' color='#717173' fontSize={17} />
                                <TextGrey text={rideInfo.vehicleinformation[0].rego} textAlign='left' color='#717173' />
                            </View>
                        </View>
                        <View style={styles.locationContainer}>
                            <Image
                                source={require('../../assets/pickup.png')}
                                style={styles.image}
                                resizeMode='contain'
                            />
                            <View style={styles.originDestinationVw}>
                                <TextGreyBold marginTop={0} fontSize={16} text={rideInfo.start_location.city} textAlign='left' />
                                <TextGreyBold marginTop={0} fontSize={16} text={rideInfo.end_location.city} textAlign='left' />
                            </View>
                        </View>
                        <View style={[styles.locationContainer, styles.carVW]}>
                            <Image
                                source={require('../../assets/carBlack.png')}
                                style={styles.carImage}
                                resizeMode='contain'
                            />
                            <View style={styles.CarDetailsVw}>
                                <CallMsgView title='COLOR' value={rideInfo.vehicleinformation[0].car_color} />
                                <CallMsgView title='TIME' value={timeToReach} />
                                <CallMsgView title='PRICE' value={pricePerSeat} />
                            </View>
                        </View>
                    </View>
                    <CommonBtn title='Complete Ride' func={() => this.completeRideBtnAction()} position='absolute' bottom={8} />
                </View>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state: any) => ({
    pickupInfo: state.savePickUpInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
    saveOriginInfo: (info: string) => dispatch({ type: ORIGIN_INFO, payload: info }),
    saveDestio: (info: string) => dispatch({ type: DESTINATION_INFO, payload: info }),
    saveSearchOriginInationInfnfo: (info: string) => dispatch({ type: SEARCH_ORIGIN_INFO, payload: info }),
    saveSearchDestinationInfo: (info: string) => dispatch({ type: SEARCH_DESTINATION_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackRide);
