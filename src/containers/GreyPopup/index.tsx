import React, { Component } from 'react';
import { View, Text, Image, StatusBar, Platform, StatusBarIOS, NativeModules, KeyboardAvoidingView, Keyboard ,BackHandler} from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn, GreenTextBtn } from '../../custom/CustomButton';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { TextInputWithImageFoucsEdit, TextInputWithBtn } from '../../custom/CustomTextInput';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import Map from '../../Utils/Map';
import RNGooglePlaces from 'react-native-google-places';
import { ORIGIN_INFO, DESTINATION_INFO } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { commonShadowStyle } from '../../common/styles';
import { PopupScreenType } from '../../Utils/Enums';

export interface props {
    navigation: NavigationScreenProp<any, any>
}

export interface props {
    navigation: NavigationScreenProp<any, any>,
};

const { StatusBarManager } = NativeModules;

export default class GreyPopup extends Component<props, object> {

    state = {
        statusBarHeight: 0,
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            header: null
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);

        console.log("   ")
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

        
    }

  
    
    componentWillUnmount() {
        console.log("start >> component unmounted");
        
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        return true;
    }

    btnHandler() {

        let screenType = this.props.navigation.getParam('screenType')

        if (screenType == PopupScreenType.ridePublished) {
            this.props.navigation.navigate('Home')
        }
        else if (screenType == PopupScreenType.rideStarted) {
            this.props.navigation.navigate('SavedCards')
        }
    }

    render() {

        console.log("Render func called")

        let screenType = this.props.navigation.getParam('screenType')

        let img: any
        let title = ''
        let message = ''
        let btnText = ''

        if (screenType == PopupScreenType.ridePublished) {
            img = require('../../assets/blueCar.png')
            title = 'Ride Published'
            message = 'Your ride has been published successfully.'
            btnText = 'Okay'
        }
        else if (screenType == PopupScreenType.rideStarted) {
            img = require('../../assets/blueCar.png')
            title = 'Ride Started'
            message = 'Your ride has begun, Its time to pay for your ride!'
            btnText = 'Pay Now'
        }

        return (
            <View style={styles.container}>
                <View style={[styles.popupVw]}>
                    <Image source={img} style={styles.image} />
                    <TextGreyBold marginTop={36} fontSize={20} text={title} width='70%' />
                    <TextGrey marginTop={16} text={message} width='70%' />
                    <CommonBtn title={btnText} func={() => this.btnHandler()} width='70%' marginTop={40} />
                </View>
            </View>
        )
    }
}
