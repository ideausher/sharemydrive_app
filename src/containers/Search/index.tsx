import React, { Component } from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableOpacity, Image, Alert, Modal } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { OpenDrawerHeaderBtn, NotificationsHeaderBtn } from '../../custom/CustomComponents';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { TextInputWithBtn } from '../../custom/CustomTextInput';
import LastRideCell from '../../custom cells/LastRideCell';
//@ts-ignore
import { connect } from 'react-redux';
import { CommonBtn, GreenTextBtn } from '../../custom/CustomButton';
import { TextGrey, TextGreyBold } from '../../custom/CustomText';
import moment from 'moment';
import { SEARCH_ORIGIN_INFO, SEARCH_DESTINATION_INFO, SEARCH_DATE_TIME_INFO, NEAR_BY_INFO } from '../../Redux/Constants';
import RequestManager from '../../Utils/RequestManager';
import { API_GET_PUBLISHED_RIDES } from '../../constants/apis';
import General from '../../Utils/General';
import { internetConnectionError } from '../../constants/messages';
import Toast from 'react-native-simple-toast';
import { APP_SKY_BLUE_COLOR } from '../../constants/colors';
import { Popup } from '../../custom/Popup';
import LocalDataManager from '../../Utils/LocalDataManager';
import { EventRegister } from 'react-native-event-listeners'

export interface props {
    navigation: NavigationScreenProp<any, any>
    searchInfo: any,
    saveOriginInfo: any,
    saveDestinationInfo: any,
    saveBookingDateTimeInfo: any,
    saveNearByInfo: any,
    loginInfo: any,
    notificationsInfo: any
};

const NearByInfoView = (props: any) => {
    return (
        <View style={styles.nearByVwContainer}>
            <View style={[styles.popupVw]}>
                <TextGreyBold marginTop={16} fontSize={18} text='Nearby' />
                <TextGrey marginTop={16} fontSize={12} text='Enabling nearby will allow you to see all the rides that are within 1 km of your origin & destination' color='rgba(78, 78, 80, 0.6)' />
                <GreenTextBtn title='Got it' func={() => props.func()} fontSize={15} marginTop={20} marginBottom={8} />
            </View>
        </View>
    );
};

class Search extends Component<props, object> {

    notificationListener: any = undefined

    state = {
        lastRidesArr: [] as any[],
        nearByEnabled: false,
        showNearByVw: false,
        page: 0,
        isRefreshing: false,
        loadMore: false,
        isCalled: false,
        prevPage: -1,
        showLoader: false,
        showPopup: false,
        isHighlighted: false,
    }
    

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Search Ride</Text>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerRight: (
                <NotificationsHeaderBtn func={navigation.getParam('notificationsHandler')} isHighlighted={navigation.getParam('isHighlighted')}/>
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': this.state.isHighlighted });
        
        this.props.navigation.addListener('willFocus', (route) => this.refreshData());

        // this.notificationListener = EventRegister.addEventListener('notificationListener', (data) => {
        //     this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': data });
        // })

        console.log("Navigatiom insaerch >>>>>>", this.props.navigation)
    }

    componentWillUnmount() {
        //EventRegister.removeEventListener(this.notificationListener)
    }

    componentWillReceiveProps(newProps: any) {
        console.log('Props Notifications Info Search >>>', newProps.notificationsInfo)
        console.log('is Highlighted Search>>>', newProps.notificationsInfo.highlight)
        console.log('this.state.ishigh;ighted Search>>>', this.state.isHighlighted)

        if(this.state.isHighlighted != newProps.notificationsInfo.highlight)
        {
            this.props.navigation.setParams({ 'isHighlighted': newProps.notificationsInfo.highlight });
            this.setState({isHighlighted: newProps.notificationsInfo.highlight})
        }
    }

    revealBtnHandler() {
        //this.props.navigation.openDrawer();
        this.props.navigation.toggleDrawer()
    }

    notificationsHandler() {
        this.props.navigation.navigate('Notifications')
    }

    private refreshData() {
        console.log("Refresh data search called")
       /// this.checkForNewNotifications()

       this.setState({ page: 0, prevPage: -1, lastRidesArr: [], isCalled: true })
            setTimeout(() => {
                this.apiGetLastAcceptedRides(true)
            }, 200)
        
        // if (this.props.loginInfo.firstName == '' || this.props.loginInfo.isVehicleInfoAdded == 0 || this.props.loginInfo.isBankDetailsAdded == 0) {
        //     this.setState({ showPopup: true })
        // }
        // else {
        //     this.setState({ page: 0, prevPage: -1, lastRidesArr: [], isCalled: true })
        //     setTimeout(() => {
        //         this.apiGetLastAcceptedRides(true)
        //     }, 200)
        // }
    }

    checkForNewNotifications() {
        // Check if notificatiosj icon needs to be highlighted
        LocalDataManager.getDataAsyncStorage('newNotifications').then((data: any) => {
            console.log('Data Retrived from Storage >>>>', data)
            if (data != null) {
                this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': data });
                this.setState({isHighlighted: data})
            } else {
                this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': false });
                this.setState({isHighlighted: false})
            }
        })
    }

    private refreshList() {
        this.setState({ isRefreshing: true })
        setTimeout(() => {
            this.apiGetLastAcceptedRides(true)
        }, 200)
    }

    // Get paarmeters for Login API
    private getLastAcceptedRidesParams() {
        return {
            type: "2",
            fetch: 'a',
            page: this.state.isRefreshing ? 0 : this.state.page,
            limit: "5",
        }
    }

    async apiGetLastAcceptedRides(refresh: boolean) {
        console.log('GET apiGetLastAcceptedRides called with page index >>>', this.state.page)
        this.setState({ showLoader: true })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_PUBLISHED_RIDES, this.getLastAcceptedRidesParams())
            .then((response: any) => {
                console.log('REsponse of apiGetLastAcceptedRides API >>>>', response);
                weakSelf.setState({ showLoader: false })
                let ridesArrData: any[] = []

                if (refresh) {
                    console.log('data refershed')
                    weakSelf.setState({ page: 0, prevPage: -1, lastRidesArr: [] })
                    ridesArrData = response.data

                    console.log('Rides data in refresh >>>>', ridesArrData)
                }
                else {
                    ridesArrData = this.state.lastRidesArr.concat(response.data)
                }

                setTimeout(() => {

                    console.log('Prev and next page >>>', this.state.prevPage, this.state.page)

                    weakSelf.setState({ showLoader: false, lastRidesArr: ridesArrData, loadMore: false, prevPage: this.state.page, isRefreshing: false })

                    setTimeout(() => {

                        console.log('Again Prev and next page >>>', this.state.prevPage, this.state.page)
                        weakSelf.setState({ isCalled: false })
                    }, 200)

                    if (response.data.length > 0) {
                        let nextPage = this.state.page + 1
                        weakSelf.setState({ page: nextPage })
                    }
                }, 200)
            }).catch((error: any) => {
                this.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })
                console.log('Error >>>>', error)
                if (error == internetConnectionError) {

                }
                else {
                    if (error.message == null || error.message == undefined) {
                        Toast.show((error), Toast.SHORT);
                    }
                    else if (error.message != "No Record Found.") {
                        Toast.show((error.message), Toast.SHORT);
                    }
                }
            })
    }

    selectOrigin() {
        console.log('Seelct origin Func Called')
        this.props.navigation.navigate('SelectLocation', { isOrigin: true, isSearch: true,location:this.props.searchInfo.origin })
    }

    selectDestination() {

        this.props.navigation.navigate('SelectLocation', { isOrigin: false, isSearch: true ,location:this.props.searchInfo.destination})
    }

    selectDateTime() {
        this.props.navigation.navigate('SelectDateTime', { isSearch: true })
    }

    addNowBtnAction() {
        let today = moment().format("YYYY-MM-DD")
        console.log('Toay >>,', today)
        let currentTime = moment().format("hh:mm A")

        this.props.saveBookingDateTimeInfo({ date: today, time: currentTime })
    }

    cellSelected(index: any) {
        let originDict = this.getLocationDict(this.state.lastRidesArr[index].start_location)
        console.log('originDict >>>>', originDict)
        this.props.saveOriginInfo(originDict)

        let destinationDict = this.getLocationDict(this.state.lastRidesArr[index].end_location)
        console.log('destinationDict >>>>', destinationDict)
        this.props.saveDestinationInfo(destinationDict)

        console.log('ride_date_time >>>', this.state.lastRidesArr[index].ride_date_time)
        let lastDateTimeMoment = moment(this.state.lastRidesArr[index].ride_date_time, "yyyy-MM-dd HH:mm:ss")

        if (lastDateTimeMoment > moment().add(10, 'minutes')) {
            let date = this.state.lastRidesArr[index].ride_date_time.split(' ')[0]
            let time = this.state.lastRidesArr[index].ride_date_time.split(' ')[1]

            console.log('Date time >>>', date, time)


            let formattedTime = moment(time, 'HH:mm:ss').format("hh:mm A")
            console.log('formattedTime >>>', formattedTime)

            this.props.saveBookingDateTimeInfo({ date: date, time: formattedTime })
        }
    }

    private getLocationDict(location: any) {

        return {
            address: location.full_address,
            country: location.country,
            lat: location.latitude,
            long: location.longitude,
            state: location.state,
            city: location.city,
            placeId: location.place_id
        }
    }


    searchBtnAction() {
        this.props.navigation.navigate('SearchResult')
    }

    nearByBtnAction() {
        this.props.saveNearByInfo(!this.state.nearByEnabled)
        this.setState({ nearByEnabled: !this.state.nearByEnabled })
    }

    showHideNearByVw() {
        this.setState({ showNearByVw: !this.state.showNearByVw })
    }

    //Will call when reached last cell
    private loadMoreData = () => {

        console.log('Func Clledv >>>', this.state.prevPage, this.state.page)
        if (this.state.prevPage == this.state.page) {
            console.log('Func  return Clled')
            return
        }
        this.setState({ loadMore: true, isCalled: true })
        console.log('Func Clled2')
        setTimeout(() => {
            console.log('Func Clled3')
            this.apiGetLastAcceptedRides(false)
        }, 200)
    }

    goToProfile() {
        this.setState({ showPopup: false })
        this.props.navigation.navigate('Profile')
    }

    render() {

        console.log('pickupInfo >>>>', this.props.searchInfo)
        console.log('Last Ride Array >>>>', this.state.lastRidesArr)
        let origin = ''
        let destination = ''
        let date = ''
        let time = ''

        if (this.props.searchInfo.origin != '') {
            origin = this.props.searchInfo.origin.address
        }

        if (this.props.searchInfo.destination != '') {
            destination = this.props.searchInfo.destination.address
        }

        if (this.props.searchInfo.date != '') {
            date = moment(this.props.searchInfo.date, 'YYYY-MM-DD').format('DD-MM-YYYY')
        }

        if (this.props.searchInfo.time != '') {
            time = this.props.searchInfo.time
        }

        let popupMsg = ''

        console.log('LoginInfo isVehicleInfoAdded>> ', this.props.loginInfo.isVehicleInfoAdded, popupMsg)

        if (this.props.loginInfo.firstName == '') {
            popupMsg = 'Please update your profile before publishing a ride'
        }
        else if (this.props.loginInfo.isVehicleInfoAdded == 0) {
            console.log(' isVehicleInfoAdded condition true>> ', this.props.loginInfo.isVehicleInfoAdded)
            popupMsg = 'Please add your vehicle information before publishing a ride'
        }
        else if (this.props.loginInfo.isBankDetailsAdded == 0) {
            popupMsg = 'Please add your bank information before publishing a ride'
        }

        console.log('LoginInfo showPOpup, pop up msg search>> ', this.state.showPopup, popupMsg)

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showPopup}>
                    <Popup title={popupMsg} btnText='Go to Profile' func={() => this.goToProfile()} />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showNearByVw}>
                    <NearByInfoView func={() => this.showHideNearByVw()} />
                </Modal>
                <View style={styles.container}>
                    <TextInputWithBtn placeholder='Origin' value={origin} icon={require('../../assets/originIcon.png')} backgroundColor='#F6F6F6' func={() => this.selectOrigin()} marginTop={40} />
                    <TextInputWithBtn placeholder='Destination' value={destination} icon={require('../../assets/destinationIcon.png')} backgroundColor='#F6F6F6' func={() => this.selectDestination()} />
                    {date == '' ?
                        <View style={styles.addNowDateTimeVw}>
                            <TextInputWithBtn placeholder='Add Now' value='Add Now' backgroundColor='#F6F6F6' func={() => this.addNowBtnAction()} marginTop={0} width='40%' textAlign='center' height={35} color='red' />
                            <TextGrey marginTop={0} fontSize={14} text='OR' />
                            <TextInputWithBtn placeholder='Date & Time' value='Date & Time' backgroundColor='#F6F6F6' func={() => this.selectDateTime()} marginTop={0} width='40%' textAlign='center' height={35} color={APP_SKY_BLUE_COLOR} />
                        </View> :
                        <TextInputWithBtn placeholder='Date & Time' value={date == '' ? '' : (date + ', ' + time)} icon={require('../../assets/calendar.png')} backgroundColor='#F6F6F6' func={() => this.selectDateTime()} />}


                    {origin == '' || destination == '' || date == '' ?
                        (this.state.lastRidesArr.length > 0 ?
                            < View style={styles.listContainer}>
                                {/* // <View style={styles.line} /> */}
                                <TextGrey fontSize={16} text='Rebook your last rides' textAlign='left' width='90%' />
                                <FlatList
                                    onRefresh={() => this.refreshList()}
                                    refreshing={this.state.isRefreshing}
                                    style={{ marginTop: 24, width: '100%', marginBottom: 200 }}
                                    data={this.state.lastRidesArr}
                                    keyExtractor={(item: any, index: any) => index.toString()}
                                    renderItem={({ item, index }: any) => <LastRideCell
                                        onClickEvent={(index: any) => this.cellSelected(index)}
                                        item={item} index={index} pickup={false} />}
                                    onEndReachedThreshold={0.5}
                                    onScroll={({ nativeEvent }) => {
                                        if (this.state.isCalled == false && General.isCloseToBottom(nativeEvent) && this.state.isRefreshing == false) {
                                            this.loadMoreData()
                                        }
                                    }}
                                />
                            </View> :
                            <View style={[styles.listContainer, { marginTop: 30 }]}>
                                <TextGrey marginTop={0} fontSize={14} text={'Enter your details here if you\'d like a lift to your destination'} />
                                <View style={{ marginTop: 8, width: '90%', height: 2, backgroundColor: '#F6F6F6', alignSelf: 'center' }} />
                            </View>
                        )
                        :
                        <View style={{ width: '90%' }}>
                            <View style={styles.enableFeatureContainer}>
                                <TouchableOpacity style={styles.imgBtn} onPress={() => this.nearByBtnAction()}>
                                    <Image resizeMode='contain' source={this.state.nearByEnabled ? require('../../assets/checkBox.png') : require('../../assets/checkBoxBlank.png')} style={styles.checkImg} />
                                </TouchableOpacity>
                                <TextGrey fontSize={12} text='Enable near by feature' textAlign='left' marginLeft={-8} marginRight={8} marginTop={0} />
                                <TouchableOpacity style={styles.imgBtn} onPress={() => this.showHideNearByVw()}>
                                    <Image resizeMode='contain' source={require('../../assets/infoIcon.png')} style={styles.infoImg} />
                                </TouchableOpacity>
                            </View>

                            <CommonBtn title='Search' func={() => this.searchBtnAction()} marginTop={150} />
                        </View>}
                    {origin == '' || destination == '' || date == '' ?
                        (this.state.lastRidesArr.length > 0 ?
                            null : <Image
                                source={require('../../assets/searchImg.png')}
                                style={styles.homeImg}
                                resizeMode='contain' />) : null}

                </View>
            </SafeAreaView>
        );
    }
}


const mapStateToProps = (state: any) => ({
    searchInfo: state.saveSearchInfo,
    loginInfo: state.saveLoginInfo,
    notificationsInfo: state.saveNotificationsInfo
});

const mapDispatchToProps = (dispatch: any) => ({
    saveOriginInfo: (info: string) => dispatch({ type: SEARCH_ORIGIN_INFO, payload: info }),
    saveDestinationInfo: (info: string) => dispatch({ type: SEARCH_DESTINATION_INFO, payload: info }),
    saveBookingDateTimeInfo: (info: string) => dispatch({ type: SEARCH_DATE_TIME_INFO, payload: info }),
    saveNearByInfo: (info: string) => dispatch({ type: NEAR_BY_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
