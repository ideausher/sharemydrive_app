//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    listContainer: {
        width: '90%',
        marginTop: 80,
       // backgroundColor: 'red',
    },
    enableFeatureContainer: {
        marginTop: 20,
        flexDirection: 'row',
        height: 60,
        alignItems: 'center',
       // backgroundColor: 'green',
    },
    imgBtn: {
        width: 40,
        height: 60,
        //alignItems: 'center',
        justifyContent: 'center',
       // backgroundColor: 'red'
    },
    checkImg: {
        width: 20,
        height: 20
    },
    infoImg: {
        width: 12,
        height: 12
    },
    nearByVwContainer: {
        flex: 1,
        backgroundColor: '#140F2693', 
        alignItems:'center',
        justifyContent:'center'
    },  
    popupVw: {
        padding: 8,
        borderRadius: 10,
        backgroundColor: 'white',
        width: '80%',
        alignItems:'center'
    },
    homeImg: {
        position: 'absolute',
       // backgroundColor: 'green',
       // left: -17,
        bottom: 8,
       // width: '70%',
       // alignSelf:'center'
    },
    addNowDateTimeVw:{
        marginTop: 40,
        width: '90%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
       // backgroundColor:'red'
    }
});