import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image, Alert, FlatList, Modal, TouchableHighlightBase } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { headerStyle, headerTitleStyle ,navHdrTxtStyle} from '../../common/styles';
import { NoDataFoundView, NoInternetFoundView, BackHeaderBtn, NotificationsNoDataFoundView } from '../../custom/CustomComponents';
import { GreenTextBtn } from '../../custom/CustomButton';
import { CommonBtn } from '../../custom/CustomButton';
//@ts-ignore
import { connect } from 'react-redux';
import { Popup } from '../../custom/Popup';
import RequestManager from '../../Utils/RequestManager';
import { API_GET_RIDE_REQUESTS, API_ACCEPT_REJECT_API, API_NOTIFICATIONS_LIST } from '../../constants/apis';
import { internetConnectionError } from '../../constants/messages';
import Toast from 'react-native-simple-toast';
import { Loader } from '../../Utils/Loader';
import General from '../../Utils/General';
import RideRequestsCell from '../../custom cells/RideRequestsCell';
import NotificationsCell from '../../custom cells/NotificationsCell';
import { notifiationsArr } from '../../Handlers/NotificationsHandler';
import moment from 'moment';
import { TextGrey } from '../../custom/CustomText';
import { APP_GREEN_COLOR } from '../../constants/colors';
import LocalDataManager from '../../Utils/LocalDataManager';
import { EventRegister } from 'react-native-event-listeners';
import { NOTIFCATIONS_INFO } from '../../Redux/Constants';

export interface props {
    navigation: NavigationScreenProp<any, any>,
    loginInfo: any,
    saveNotificationsInfo: any
};

class Notifications extends Component<props, object> {

    _isMounted = false

    state = {
        isNotifications: true,
        notificationsArr: [] as any[],
        ridesrequestsArr: [] as any[],
        isRefreshing: false,
        loadMore: false,
        isCalled: false,
        page: 0,
        prevPage: -1,
        // rideRequestsPage: 0,
        //rideRequestsPrevPage: -1,
        showLoader: false,
        isInternetError: false,
        isDataNotFoundError: false,
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Notifications</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        console.log("Component did mount nitificatiosn called")

        this._isMounted = true

        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });



        if (this.props.navigation.getParam("showRideRequests") != undefined && this.props.navigation.getParam("showRideRequests") == true) {
            this.setState({ isNotifications: false })
            this.apiGetRideRequestsRides()
        } else {
            this.apiGetNotificationsList(true)
        }

    }

    componentWillUnmount() {
        this._isMounted = false
        console.log("Component wiill unmount nitificatiosn called")
    }

    backBtnHandler() {
        this.props.navigation.goBack()
    }

    notificationsRideRequestsBtnAction(notifications: boolean) {

        console.log('IS notifications >>>>', notifications)

        if (notifications) {
            this.setState({ isNotifications: true })

            setTimeout(() => {

                this.apiGetNotificationsList(true)
            }, 200)
        }
        else {
            this.setState({ isNotifications: false })

            setTimeout(() => {

                this.apiGetRideRequestsRides()
            }, 200)
        }
    }

    // Get paarmeters 
    private getParams(refresh: boolean) {

        return {
            "page": refresh ? 0 : this.state.page,
            "limit": 10,
            "is_read": 3
        }
    }

    async apiGetNotificationsList(refresh: boolean) {

        console.log('apiGetNotificationsList called >>>>');
        this.setState({ showLoader: true, isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this
        RequestManager.getRequest(API_NOTIFICATIONS_LIST, this.getParams(refresh)).then((response: any) => {
            console.log('REsponse of API_NOTIFICATIONS_LIST API >>>>', response);
            console.log('response.data.length) >>>>', response.data.length, this._isMounted);
            weakSelf.setState({ showLoader: false })
            if (this._isMounted) {


                if (response.data.length > 0) {

                    LocalDataManager.saveDataAsyncStorage('newNotifications', JSON.stringify(false))
                    weakSelf.props.saveNotificationsInfo(false)
                    //EventRegister.emit('notificationListener', false)

                    notifiationsArr(response.data).then((arr: any) => {

                        console.log('Notifications Arr>>>>', arr);
                        let notificationsArrData: any[] = []
                        if (refresh) {
                            weakSelf.setState({ page: 0, prevPage: -1, notificationsArr: [] })
                            notificationsArrData = arr
                        }
                        else {
                            notificationsArrData = this.state.notificationsArr.concat(arr)
                        }

                        setTimeout(() => {
                            let sortedNotifications = notificationsArrData.sort((a: any, b: any) => this.sortNotifications(a, b));
                            weakSelf.setState({ showLoader: false, notificationsArr: sortedNotifications, loadMore: false, prevPage: this.state.page, isRefreshing: false })

                            setTimeout(() => {

                                if (weakSelf.state.notificationsArr.length == 0) {
                                    weakSelf.setState({ isInternetError: false, isDataNotFoundError: true })
                                }
                                weakSelf.setState({ isCalled: false })
                            }, 200)

                            if (arr.length > 0) {
                                let nextPage = this.state.page + 1
                                weakSelf.setState({ page: nextPage })
                            }
                            else if (this.state.notificationsArr.length == 0) {
                                this.setState({ isDataNotFoundError: true })
                            }
                        }, 200)
                    })
                }
                else {
                    this.setState({ isDataNotFoundError: true })
                }
            }

            console.log('response data Arrr  >>>>', response.data)
            console.log('Notificatios Arr >>>>', this.state.notificationsArr)
        }).catch((error: any) => {
            this.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })
            General.showErroMsgInternetNoData(this, error)
        })
    }

    private sortNotifications(a: any, b: any) {
        let first: any = moment(a.created_at, "YYYY_MM_DD HH:mm:ss")
        let second: any = moment(b.created_at, "YYYY_MM_DD HH:mm:ss")

        return second - first
    }

    //Will call when scroll view pulled down to refresh
    private refreshList() {
        this.setState({ isRefreshing: true })
        setTimeout(() => {
            this.apiGetNotificationsList(true)
        }, 200)
    }

    //Will call when reached last cell
    private loadMoreData = () => {
        if (this.state.prevPage == this.state.page) {
            return
        }
        this.setState({ loadMore: true, isCalled: true })
        setTimeout(() => {
            this.apiGetNotificationsList(false)
        }, 200)
    }

    private refreshData() {

        if (this.state.isNotifications) {
            this.apiGetNotificationsList(true)
        }
        else {
            this.apiGetRideRequestsRides()
        }

    }

    // Get paarmeters for Login API
    private getRideRequestAPIParams() {
        return {
            type: 0
        }
    }

    async apiGetRideRequestsRides() {
        console.log('apiGetRideRequestsRides called with params >>>', this.getRideRequestAPIParams())
        this.setState({ showLoader: true, isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_RIDE_REQUESTS, this.getRideRequestAPIParams())
            .then((response: any) => {
                console.log('REsponse of apiGetRideRequestsRides API >>>>', response);

                let sortedRideRequests = response.data.sort((a: any, b: any) => this.sortNotifications(a, b));
                weakSelf.setState({ showLoader: false, ridesrequestsArr: sortedRideRequests })

                if (response.data.length == 0) {
                    this.setState({ isDataNotFoundError: true })
                }
            }).catch((error: any) => {
                General.showErroMsgInternetNoData(this, error)
            })
    }

    // Get paarmeters for Login API
    private getAcceptRejectRequestsAPIParams(index: any, accept: boolean) {

        let selectedRide = this.state.ridesrequestsArr[index]
        return {
            "rideid": selectedRide.ride_id,
            "userid": selectedRide.user_id,
            "status": accept ? 1 : 2
        }
    }

    async apiAcceptRejectRequests(index: any, accept: boolean) {
        console.log('apiAcceptRejectRequests called with params >>>', this.getAcceptRejectRequestsAPIParams(index, accept))
        this.setState({ showLoader: true })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.postRequestWithHeaders(API_ACCEPT_REJECT_API, this.getAcceptRejectRequestsAPIParams(index, accept))
            .then((response: any) => {
                console.log('REsponse of apiAcceptRejectRequests API >>>>', response);
                weakSelf.setState({ showLoader: false })

                // Updtae List -> Remove ride which is requested or accepted
                let rides = this.state.ridesrequestsArr
                rides.splice(index, 1)
                this.setState({ ridesrequestsArr: rides })
                if (response.message == 'Updated Successfully') {
                    console.log("In MEssage updtes uccess conditiopn >>>>", accept)
                    Toast.show(accept ? 'Ride accepted successfully' : 'Ride rejected successfully', Toast.SHORT);
                }

                if (rides.length == 0) {
                    this.setState({ isDataNotFoundError: true })
                }

            }).catch((error: any) => {
                General.showErroMsg(this, error)
            })
    }

    private cellSelected(index: any) {
        console.log('Cell Selected Func called >>>', this.state.ridesrequestsArr[index])
        this.props.navigation.navigate('ViewProfile', { rideInfo: this.state.ridesrequestsArr[index], showBookRideBtn: false })
    }

    render() {

        console.log('RideRequests ARr >>.', this.state.ridesrequestsArr)

        console.log('is data found  >>.', this.state.isDataNotFoundError)
        console.log('is internet error  >>.', this.state.isInternetError)

        let arrToShow: any[] = []
        if (this.state.isNotifications) {
            arrToShow = this.state.notificationsArr
        }
        else {
            arrToShow = this.state.ridesrequestsArr
        }

        console.log('arrToShow >>.', arrToShow, arrToShow.length)

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    <View style={{ flexDirection: 'row', marginTop: 24 }}>
                        <View style={styles.signBtnVw}>
                            <TouchableOpacity onPress={() => this.notificationsRideRequestsBtnAction(true)}>
                                <Text style={styles.upcomingHistoryTxt}>NOTIFICATIONS</Text>
                                <View style={this.state.isNotifications ? styles.activeBlueLine : styles.inactiveBlueLine} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.signBtnVw}>
                            <TouchableOpacity onPress={() => this.notificationsRideRequestsBtnAction(false)}>
                                <Text style={styles.upcomingHistoryTxt}>RIDE REQUESTS</Text>
                                <View style={this.state.isNotifications ? styles.inactiveBlueLine : styles.activeBlueLine} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {arrToShow.length > 0 ?
                        (this.state.isNotifications ? <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{ marginTop: 24, width: '100%', marginBottom: 8 }}
                            onRefresh={() => this.refreshList()}
                            refreshing={this.state.isRefreshing}
                            data={arrToShow}
                            keyExtractor={(item: any, index: any) => index.toString()}
                            renderItem={({ item, index }: any) => <NotificationsCell onClickEvent={(index: any) => this.cellSelected(index)} item={item} index={index} />}
                            onEndReachedThreshold={0.5}
                            onScroll={({ nativeEvent }) => {
                                if (this.state.isCalled == false && General.isCloseToBottom(nativeEvent) && this.state.isRefreshing == false) {
                                    this.loadMoreData()
                                }
                            }}
                        /> : <FlatList
                                showsVerticalScrollIndicator={false}
                                style={{ marginTop: 24, width: '100%', marginBottom: 8 }}
                                data={arrToShow}
                                keyExtractor={(item: any, index: any) => index.toString()}
                                renderItem={({ item, index }: any) => <RideRequestsCell leftBtnAction={(index: any) => this.apiAcceptRejectRequests(index, true)} rightBtnAction={(index: any) => this.apiAcceptRejectRequests(index, false)} onClickEvent={(index: any) => this.cellSelected(index)} item={item} index={index} />}
                            />) : (this.state.showLoader ? null :
                                (this.state.isDataNotFoundError ?
                                    <NotificationsNoDataFoundView func={() => this.refreshData()} title={this.state.isNotifications ? 'NO NOTIFICATIONS YET !' : 'NO REQUESTS YET !'} message={this.state.isNotifications ? '' : 'You will see notifications related to your ride requests'} img={this.state.isNotifications ? require('../../assets/bell.png') : require('../../assets/noRequests.png')} /> :
                                    (this.state.isInternetError ?
                                        <NoInternetFoundView func={() => this.refreshData()} /> : null)))}
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    loginInfo: state.saveLoginInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
    saveNotificationsInfo: (info: string) => dispatch({ type: NOTIFCATIONS_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);