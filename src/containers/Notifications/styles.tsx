//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_NAVY_BLUE_COLOR, APP_GREEN_COLOR } from '../../constants/colors';
import { APP_FONT_REGULAR, APP_FONT_BOLD } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    signBtnVw: {
        width: '50%',
        //height: 100,
        alignItems: 'center',
       // backgroundColor:'red',
        padding: 8
    },
    activeBlueLine: {
        marginTop: 8,
        width: 130,
        height: 2,
        backgroundColor: APP_GREEN_COLOR,
    },
    inactiveBlueLine: {
        marginTop: 8,
        width: 100,
        height: 2,
        backgroundColor: 'white',
    },
    upcomingHistoryTxt: {
        fontFamily: APP_FONT_REGULAR,
        fontSize: 16,
        textAlign: 'center',
        color: APP_NAVY_BLUE_COLOR,
       // backgroundColor:'green'
    },
    greyBoldText: {
        fontSize: 25,
        fontFamily: APP_FONT_REGULAR,
        color: '#4B4B4D',
        textAlign: 'center',
        marginTop: 24,
    },
    greyText: {
        fontSize: 14,
        fontFamily: APP_FONT_REGULAR,
        color: '#A2A2A2',
        textAlign: 'center',
        marginTop: 8
    },
    homeImg: {
        position: 'absolute',
        // backgroundColor: 'green',
       // left: -17,
        bottom: 0,
        width: '100%'
    },
});