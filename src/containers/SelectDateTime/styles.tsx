//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD, APP_FONT_REGULAR } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },   
    calendarStyle:{
        marginTop: 40,
    },
    dateText:{
        fontSize: 14,
        fontFamily: APP_FONT_REGULAR,
        color: '#666673',
        textAlign: 'center',
    },
    calendarContainer:{
        backgroundColor: 'white'
    },
    calendarControls:{
        fontSize: 20,
        fontFamily: APP_FONT_REGULAR,
        color: '#717173',
        textAlign: 'center',
       // backgroundColor: 'green'
    },
    dayHeading:{
        fontSize: 14,
        fontFamily: APP_FONT_REGULAR,
        color: '#454555',
        textAlign: 'center',
    },
    timeBtn: {
        width: '80%',
        height: 60,
        marginTop: 50,
        alignSelf: 'center', 
       // backgroundColor:'red',
        borderColor:'#C8C7CC', 
        borderWidth: 2, 
        borderRadius: 30, 
        alignItems: 'center',
        justifyContent: 'center'
    }, 
    timeText:{
        fontSize: 36,
        fontFamily: APP_FONT_BOLD,
        color: '#D8D8D8',
        textAlign: 'center',
    }
});