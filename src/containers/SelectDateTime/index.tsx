import React, { Component } from 'react';
import { View, Text, Image, StatusBar, TouchableOpacity, ScrollView, Alert } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { TextGreyBold } from '../../custom/CustomText';
import Map from '../../Utils/Map';
import RNGooglePlaces from 'react-native-google-places';
//@ts-ignore
import { connect } from 'react-redux';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import moment from 'moment';
//@ts-ignore
import { Calendar } from 'react-native-calendars';
import { APP_FONT_REGULAR } from '../../constants/fonts';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { DATE_TIME_INFO, SEARCH_DATE_TIME_INFO } from '../../Redux/Constants';
import General from 'src/Utils/General';

export interface props {
    navigation: NavigationScreenProp<any, any>
    saveBookingDateTimeInfo: any,
    saveSearchBookingDateTimeInfo: any,
    searchInfo: any,
    pickupInfo: any,
}

class SelectDateTime extends Component<props, object> {

    state = {
        selectedDate: [new Date()] as any,
        selectedTime: '',
        minDate: new Date() as any,
        markedDates: {} as any,
        showTimePicker: false,
        isSearch: false,
        isMultipleDays: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Select Date & Time</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        console.log('Props >>>>', this.props)
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() })

        let isSearch = this.props.navigation.getParam('isSearch')

        // Create Array with single value of Today
        let today = isSearch ? moment().format("YYYY-MM-DD") : [moment().format("YYYY-MM-DD")]
        console.log('Today before >>,', today)
        let currentTime = moment().format("hh:mm A")

        if (isSearch && this.props.searchInfo.date != '') {
            console.log('Search Info >>,', this.props.searchInfo)
            let lastSelectedDate = this.props.searchInfo.date
            let lastSelectedTime = this.props.searchInfo.time

            today = lastSelectedDate
            currentTime = lastSelectedTime
        }
        else if (!isSearch && this.props.pickupInfo.date != '') {
            let lastSelectedDate = this.props.pickupInfo.date
            let lastSelectedTime = this.props.pickupInfo.time

            today = lastSelectedDate
            currentTime = lastSelectedTime
        }

        if (this.props.navigation.getParam('isMultipleDays') != undefined) {
            this.setState({ isMultipleDays: this.props.navigation.getParam('isMultipleDays') })
        }

        console.log('Today After>>,', today)

        if(isSearch) {
            this.setState({ markedDates: this.getMarkedDate([today]), selectedTime: currentTime, selectedDate: [today], isSearch: this.props.navigation.getParam('isSearch') })
        }
        else {
            this.setState({ markedDates: this.getMarkedDate(today), selectedTime: currentTime, selectedDate: today, isSearch: this.props.navigation.getParam('isSearch') })
        }
       
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    private daySelected(date: any) {
        console.log('selected day', date.dateString)
        console.log('isMultipleDays', this.state.isMultipleDays)

        if (this.state.isMultipleDays) {
            const selectedDates: any[] = this.state.selectedDate
            console.log('selectedDates Dates before>>>', selectedDates)

            // if (selectedDates.every((date) => console.log(date), date != date.dateString)) {

            //     selectedDates.push(date.dateString)
            //     this.setState({ selectedDate: selectedDates, markedDates: this.getMarkedDate(selectedDates) })
            // }

            let isAddedBefore = false
            let position:any
            for (let i = 0; i < selectedDates.length; i++) {
                let oldDate = selectedDates[i]
                console.log("Old date >>>", oldDate)
                if (oldDate == date.dateString) {
                    isAddedBefore = true
                    position=i
                    break
                }
            }

            if (isAddedBefore == false) {
                selectedDates.push(date.dateString)
                this.setState({ selectedDate: selectedDates, markedDates: this.getMarkedDate(selectedDates) })
            }else{
                selectedDates.splice(position,1)
                this.setState({ selectedDate: selectedDates, markedDates: this.getMarkedDate(selectedDates) })
            }

            console.log('selectedDates Dates after>>>', selectedDates)
        }
        else {
            this.setState({ selectedDate: [date.dateString], markedDates: this.getMarkedDate([date.dateString]) })
        }
    }

    getMarkedDate(dates: any) {

        let markedDates = {} as any
        for (let i = 0; i < dates.length; i++) {
            let date = dates[i]
            markedDates[date] = { selected: true }
        }
        console.log('getMarkedDate >>>>', markedDates)
        return markedDates
    }

    timeBtnAction() {
        this.setState({ showTimePicker: true })
    }

    confirmBtnAction(value: any) {
        console.log('Time Selected Value >>>', value)
        if (this.state.isMultipleDays && this.checkIfAnyDateSelectedisToday() && moment(value) <= moment()) {
            Alert.alert('You have selected Today\'s date and time selected is passed away')
        }
        else {
            let formattedTime = moment(value).format("hh:mm A")
            console.log('formattedTime >>>', formattedTime)
            this.setState({ showTimePicker: false, selectedTime: formattedTime })
        }
    }

    cancelBtnAction(value: any) {
        console.log('Time Selcted Cancelled >>>', value)
        this.setState({ showTimePicker: false })
    }

    doneBtnAction() {
        console.log('Time Selcted and adte selected >>>', this.state.selectedTime, this.state.selectedDate)
        if(this.state.selectedDate.length==0){
            Alert.alert("Please select atleast 1 date")
            return;
        }
        if (this.state.isSearch) {
            this.props.saveSearchBookingDateTimeInfo({ date: this.state.selectedDate[0], time: this.state.selectedTime })
        }
        else {
            this.props.saveBookingDateTimeInfo({ date: this.state.selectedDate, time: this.state.selectedTime })
        }

        this.props.navigation.goBack()
    }

    checkIfAnyDateSelectedisToday() {

        let isAnyDateToday = false
        for (let i = 0; i < this.state.selectedDate.length; i++) {
            let date = this.state.selectedDate[i]
            let today = moment().format("YYYY-MM-DD")
            if (date == today) {
                isAnyDateToday = true
                break
            }
        }

        return isAnyDateToday
    }

    render() {

        let today = moment().format("YYYY-MM-DD")

        return (
            <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
                <View style={styles.container}>
                    <DateTimePickerModal
                        isVisible={this.state.showTimePicker}
                        mode="time"
                        headerTextIOS='Select time'
                        //date= {this.state.selectedTime}
                        minimumDate={(this.state.selectedDate.length == 1 && this.state.selectedDate[0] == today) ? new Date() : undefined}
                        onConfirm={(value: any) => this.confirmBtnAction(value)}
                        onCancel={(value: any) => this.cancelBtnAction(value)}
                    />
                    <Calendar
                        style={styles.calendarStyle}
                        // current={this.state.selectedDate}
                        minDate={this.state.minDate}
                        onDayPress={(day: any) => this.daySelected(day)}
                        monthFormat={"MMM yyyy"} //dd MMM yyyy
                        onMonthChange={(month: any) => { console.log('month changed', month) }}
                        hideExtraDays={true}
                        firstDay={1}
                        markedDates={this.state.markedDates}
                        theme={{
                            calendarBackground: 'white',
                            textSectionTitleColor: '#454555',
                            selectedDayBackgroundColor: '#2785FE',
                            selectedDayTextColor: 'white',
                            todayTextColor: 'rgba(102, 102, 103, 0.5)',
                            dayTextColor: 'rgba(102, 102, 103, 0.5)',
                            textDisabledColor: '#d9e1e8',
                            arrowColor: '#9C9C9D',
                            disabledArrowColor: '#d9e1e8',
                            monthTextColor: 'rgba(113, 113, 115, 0.7)',
                            textDayFontFamily: APP_FONT_REGULAR,
                            textMonthFontFamily: APP_FONT_REGULAR,
                            textDayHeaderFontFamily: APP_FONT_REGULAR,
                            textDayFontSize: 14,
                            textMonthFontSize: 20,
                            textDayHeaderFontSize: 14,
                        }}
                    />
                    <TouchableOpacity
                        style={styles.timeBtn}
                        onPress={() => this.timeBtnAction()}>
                        <Text style={styles.timeText}>{this.state.selectedTime}</Text>
                    </TouchableOpacity>
                    <CommonBtn title='Done' func={() => this.doneBtnAction()} marginTop={40} />
                </View>
            </ScrollView>

        )
    }
}

const mapStateToProps = (state: any) => ({
    searchInfo: state.saveSearchInfo,
    pickupInfo: state.savePickUpInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
    saveBookingDateTimeInfo: (info: string) => dispatch({ type: DATE_TIME_INFO, payload: info }),
    saveSearchBookingDateTimeInfo: (info: string) => dispatch({ type: SEARCH_DATE_TIME_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectDateTime);
