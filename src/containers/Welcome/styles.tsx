//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import {APP_FONT_REGULAR} from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        width: '100%',
        height: '70%',
        //resizeMode: 'contain'
    }, 
    linearGradient: {
        flex: 1,
    }, 
    welcomeText: {
        color: 'white',
        fontSize: 16,
        fontFamily: APP_FONT_REGULAR,
        textAlign: 'center',
        marginTop: 40, 
        alignSelf: 'center',
    }
});