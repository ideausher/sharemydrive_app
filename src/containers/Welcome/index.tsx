import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image, Platform, PermissionsAndroid, Modal, Alert } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
//@ts-ignore
import AppIntroSlider from 'react-native-app-intro-slider';
import LinearGradient from 'react-native-linear-gradient';
import LocalDataManager from '../../Utils/LocalDataManager';
import RequestManager from '../../Utils/RequestManager';
import { VerificationFor, PopupScreenType } from '../../Utils/Enums';
import { API_SIGNUP } from '../../constants/apis';
import General from '../../Utils/General';
//@ts-ignore
import { connect } from 'react-redux';
import { LOGIN, INITIAL_PICK_UP_INFO, INITIAL_SEARCH_INFO, RIDE_INFO, NOTIFCATIONS_INFO } from '../../Redux/Constants';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import { NoInternetFoundView } from '../../custom/CustomComponents';
import { Loader } from '../../Utils/Loader';
import firebase from 'react-native-firebase';
import { EventRegister } from 'react-native-event-listeners'
import { NotificationsType } from '../../Utils/Enums';
const slides = [
    {
        key: 'somethun',
        title: 'A more comfortable way to travel',
        image: require('../../assets/welcome1.png'),
    },
    {
        key: 'somethun-dos',
        title: 'A more comfortable way to travel',
        image: require('../../assets/welcome2.png'),
    },
    {
        key: 'somethun1',
        title: 'A more comfortable way to travel',
        image: require('../../assets/welcome3.png'),
    }
];

export interface props {
    navigation: NavigationScreenProp<any, any>
    saveLoginInfo: any,
    saveInitialPickupInfo: any,
    saveInitialSearchInfo: any,
    saveRidePaymentInfo: any
    saveNotificationsInfo: any
};

class Welcome extends Component<props, object> {

    notificationData: any = undefined
    
    state = {
        showWelcomeScreen: false,
        lastSlideIndex: -1,
        slideIndex: 0,
        onSlideFuncCalled: false,
        token: '',
        isInternetError: false,
        showLoader: false
    }

    static navigationOptions = () => {
        return {
            header: null
        }
    }

    componentDidMount() {
        if (Platform.OS === "ios") {
            this.checkForiOSLocationPermissions()
        }
        else {
            this.requestAndroidLocationPermission()
        }

        this.messageListener();
    }

    messageListener = async () => {

        firebase.notifications().onNotification((notification) => {
            notification.android.setChannelId('test-channel')
                .android.setSmallIcon('ic_launcher')
                .android.setPriority(firebase.notifications.Android.Priority.Max)
                .android.setAutoCancel(true) 
                .setSound('default');
            firebase.notifications().displayNotification(notification);

            LocalDataManager.saveDataAsyncStorage('newNotifications', JSON.stringify(true))
            this.props.saveNotificationsInfo(true)
            // EventRegister.emit('notificationListener', true)

            // console.log('onNotification func  called  with data >>>>', notification)
        });

        firebase.notifications().onNotificationOpened((notificationOpen) => {
            this.notificationData = notificationOpen.notification.data;
            // console.log('onNotificationOpened func  called  with data >>>>', this.notificationData)
            this.manageRedirection()
        });

        // console.log("Notificatiosn Badge >>>>", firebase.notifications().getBadge())
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            this.notificationData = notificationOpen.notification.data;
           // console.log('if of Notification openend called  with data >>>>', this.notificationData)
            this.checkUserSession()
        }
        else {
           // console.log('else of Notification openend called  with data >>>>', notificationOpen, this.notificationData)
            this.checkUserSession()
        }
      
    }
    
    manageRedirection() {
      
      //  console.log('manage Notificatiosn func called with data >>>>', this.notificationData)
       // console.log("Ride request received >>>>>", NotificationsType.rideRequestReceived)
        if (this.notificationData != undefined) {
            if (this.notificationData != undefined && this.notificationData.ride_id != undefined) {
                LocalDataManager.saveDataAsyncStorage('shouldShowPaymentPopup', JSON.stringify(true))
                LocalDataManager.saveDataAsyncStorage('paymentInfo', JSON.stringify(this.notificationData))
                this.props.navigation.navigate('GreyPopup', { screenType: PopupScreenType.rideStarted })
            } else {
                switch (this.notificationData.type) {
                    case NotificationsType.rideRequestReceived:
                        this.props.navigation.navigate('Notifications', { showRideRequests: true })
                        break;
                    case NotificationsType.rideStatusPending:
                        this.props.navigation.navigate('Notifications')
                        break;
                    case NotificationsType.rideAccepted:
                        this.props.navigation.navigate('Notifications')
                        break;
                    case NotificationsType.rideRejected:
                        this.props.navigation.navigate('Notifications')
                        break;
                    case NotificationsType.rideCompleted:
                        this.props.navigation.navigate('Notifications')
                        break;
                    case NotificationsType.rideCancelledByDriver:
                        this.props.navigation.navigate('Notifications')
                        break;
                    case NotificationsType.rideRequestForRide:
                        this.props.navigation.navigate('Notifications')
                        break;
                    case NotificationsType.rideCancelledByPassenger:
                        this.props.navigation.navigate('Notifications')
                        break;
                    case NotificationsType.offerNotification:
                        this.props.navigation.navigate('Notifications')
                        break;
                    default:
                        this.props.navigation.navigate('Messages', { isFromNotification: true, chatData: this.notificationData })
                        break;
                }
            }
        }
    }

    private checkForiOSLocationPermissions() {
        check(PERMISSIONS.IOS.LOCATION_ALWAYS)
            .then((result: any) => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log(
                            'This feature is not available (on this device / in this context)',
                        );
                        break;
                    case RESULTS.DENIED:
                        console.log(
                            'The permission has not been requested / is denied but requestable',
                            request(PERMISSIONS.IOS.LOCATION_ALWAYS).then((result: any) => {

                                console.log('Results >>>', result);
                            })
                        );
                        break;
                    case RESULTS.GRANTED:
                        console.log('The permission is granted');
                        break;
                    case RESULTS.BLOCKED:
                        console.log('The permission is denied and not requestable anymore');
                        break;
                }
            })
            .catch((error: any) => {
                // …
            });
    }
    async requestAndroidLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: 'Share My Drive',
                    message: String('Share My Drive App needs access to your location'),
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the location');
                // Toast.show('You can use the location', Toast.SHORT);
            } else {
                console.log('location permission denied');
                // Toast.show('location permission denied', Toast.SHORT);
            }
        } catch (err) {
            console.warn(err);
        }
    }

    private checkUserSession() {

        LocalDataManager.getDataAsyncStorage('isFirstTime').then((data: any) => {
            if (data == null) {
                LocalDataManager.saveDataAsyncStorage('isFirstTime', JSON.stringify(true))
                this.setState({ showWelcomeScreen: true })
            }
            else {
                // this.props.navigation.navigate('Login');
                this.checkIfUserAlreadyLoggedIn()
            }
        })
    }

    // When user comes to Login Screen first check if User alredy logged in. If yes then save his data in redux state and move Directly to home Screen
    private checkIfUserAlreadyLoggedIn() {
        let weakSelf = this
        LocalDataManager.getLoginInfo().then((data: any) => {
            console.log('Data Retrived from Storage >>>>', data)
            if (data != null && data != {}) {
                // Get Login Data Again to refresh Data
                this.setState({ token: data.token })
                weakSelf.apiGetLoginUserData()
            }
            else {
                this.props.navigation.navigate('Login')
                //this.checkIfUserSignupAndVerificationPending()
            }
        })
    }

    async apiGetLoginUserData() {

        this.setState({ showLoader: true, isInternetError: false })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_SIGNUP, {})
            .then((response: any) => {
                console.log('REsponse of Get Users Details API >>>>', response);
                weakSelf.setState({ showLoader: false })
                weakSelf.saveLoginUserDetailsAndMove(response.data)
            }).catch((error: any) => {
                General.showErroMsgInternetNoData(this, error)
            })
    }

    // // Save Login data in redux and Local Storage(for use after App kill) and Move to Home Screen
    private saveLoginUserDetailsAndMove(data: any) {

        data["token"] = this.state.token
        LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(data))
        console.log("Payment pending >>>>", data.payment_pending_ride)
        //Check if payment is Pending
        if (data.payment_pending_ride != undefined && data.payment_pending_ride.ride_id != undefined) {
            console.log("Payment pending >>>>", data.payment_pending_ride)
            LocalDataManager.saveDataAsyncStorage('shouldShowPaymentPopup', JSON.stringify(true))
            LocalDataManager.saveDataAsyncStorage('paymentInfo', JSON.stringify(data.payment_pending_ride))
        }
        // LocalDataManager.saveDataAsyncStorage('shouldShowPaymentPopup', JSON.stringify(false))

        LocalDataManager.getDataAsyncStorage('newNotifications').then((data: any) => {
            console.log('Data Retrived from Storage componentDidMount >>>>', data)
            if (data != null) {
                this.props.saveNotificationsInfo(data)
            }
        })

        this.props.saveLoginInfo(data);
        this.props.navigation.navigate('Home');
        this.manageRedirection()
    }

    // // Check if Signup form data exists Means user Signup but verification pending thenmove directly to Verification Screen
    private checkIfUserSignupAndVerificationPending() {
        LocalDataManager.getDataAsyncStorage('signupInfo').then((data: any) => {
            if (data != null && data != {}) {
                this.props.navigation.navigate('Verification', { verificationFor: VerificationFor.signup, phoneNumber: data['phone_country_code'] + ' ' + data['phone_number'] })
            } else {
                this.props.navigation.navigate('Login')
            }
        })
    }

    renderItem = (item: any) => {
        
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.container}>
                    <Image style={styles.image} source={item.item.image} />
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} colors={['#4CE5B1', '#0CB8CB']} style={styles.linearGradient} >
                        <Text style={styles.welcomeText}>{item.item.title}</Text>
                        <TouchableOpacity style={{ marginTop: 30, width: 100, alignSelf: 'center' }} onPress={() => this.skipBtnAction()}>
                            <Text style={[styles.welcomeText, { marginTop: 0, fontSize: 18 }]}>Skip</Text>
                        </TouchableOpacity>
                        <View style={{ width: 120, height: 4, alignSelf: 'center', backgroundColor: 'white', position: 'absolute', bottom: 8 }} />
                    </LinearGradient>
                </View>
            </SafeAreaView>
        );
    }

    skipBtnAction() {
        this.props.navigation.navigate('Login')
    }

    onSlideChange(index: number, lastIndex: number) {
        this.setState({ lastSlideIndex: lastIndex, slideIndex: index, onSlideFuncCalled: true })
    }

    onScrollEnd() {
        this.setState({ onSlideFuncCalled: false })
        setTimeout(() => {
            if (this.state.lastSlideIndex == 1 && this.state.slideIndex == 2 && !this.state.onSlideFuncCalled) {
                this.props.navigation.navigate('Login')
            }
        }, 400);
    }

    render() {
       
        if (!this.state.showWelcomeScreen) {
            return <View style={{ flex: 1 }}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                {
                    (this.state.isInternetError ?
                        <NoInternetFoundView func={() => this.apiGetLoginUserData()} /> : null)
                }
            </View>;
        }
        else {
            return <AppIntroSlider
                renderItem={this.renderItem}
                slides={slides}
                showNextButton={false}
                showDoneButton={false}
                dotStyle={{ backgroundColor: 'white', width: 30, height: 5, marginLeft: 0, marginRight: 0, borderRadius: 2, marginTop: 8 }}
                activeDotStyle={{ backgroundColor: '#5578BD', width: 30, height: 5, marginLeft: 0, marginRight: 0, borderRadius: 2, marginTop: 8 }}
                onSlideChange={(index: number, lastIndex: number) => this.onSlideChange(index, lastIndex)}
                onScrollEndDrag={(nativeEvent: any) => this.onScrollEnd()} />;
        }
    }
}

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveLoginInfo: (info: string) => dispatch({ type: LOGIN, payload: info }),
    saveInitialPickupInfo: (info: string) => dispatch({ type: INITIAL_PICK_UP_INFO, payload: info }),
    saveInitialSearchInfo: (info: string) => dispatch({ type: INITIAL_SEARCH_INFO, payload: info }),
    saveRidePaymentInfo: (info: string) => dispatch({ type: RIDE_INFO, payload: info }),
    saveNotificationsInfo: (info: string) => dispatch({ type: NOTIFCATIONS_INFO, payload: info }),
});

export default connect(null, mapDispatchToProps)(Welcome);
