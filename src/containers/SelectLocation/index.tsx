import React, { Component } from 'react';
import { View, Text, Image, StatusBar, Platform, StatusBarIOS, NativeModules, KeyboardAvoidingView, TouchableOpacity } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { headerStyle, headerTitleStyle, navHdrTxtStyle } from '../../common/styles';
import { TextInputWithImageFoucsEdit, TextInputWithBtn } from '../../custom/CustomTextInput';
import { TextGreyBold } from '../../custom/CustomText';
import Map from '../../Utils/Map';
import RNGooglePlaces from 'react-native-google-places';
import { ORIGIN_INFO, DESTINATION_INFO, SEARCH_DESTINATION_INFO, SEARCH_ORIGIN_INFO } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import GetLocation from 'react-native-get-location'


export interface props {
    navigation: NavigationScreenProp<any, any>
    pickupInfo: any,
    saveOriginInfo: any,
    saveDestinationInfo: any,
    saveSearchOriginInfo: any,
    saveSearchDestinationInfo: any,
}

const { StatusBarManager } = NativeModules;

class SelectLocation extends Component<props, object> {

    state = {
        statusBarHeight: 0,
        location: '' as any,
        markers: [] as any[],
        isOrigin: false,
        isSearch: false,
        isAddress: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Location</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        console.log('Select Location Props >>>>', this.props)
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        console.log('selected location >>>', this.props.navigation.getParam('location'))
        if (this.props.navigation.getParam('isAddress') != null) {
            this.setState({ isAddress: this.props.navigation.getParam('isAddress') })
        }
        else {
            let newMarkers = [];
            newMarkers[0] = this.props.navigation.getParam('location')
            console.log('props markers >>>>', this.props.navigation.getParam('location'))
            if (this.props.navigation.getParam('location') == "" || this.props.navigation.getParam('location') == undefined)
                this.setState({ isOrigin: this.props.navigation.getParam('isOrigin'), isSearch: this.props.navigation.getParam('isSearch') })
            else
                this.setState({ isOrigin: this.props.navigation.getParam('isOrigin'), isSearch: this.props.navigation.getParam('isSearch'), location: this.props.navigation.getParam('location'), markers: newMarkers })
        }
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    addMarkers(newMarker: any) {
        console.log('New marker >>>>', newMarker)
        this.fetchCoordDtls(newMarker);
    }

    getCurrentLocation = () => {
        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000,
        })
        .then((location:any) => {
            console.log(location);
            this.fetchCoordDtls(location);
        })
        .catch((error:any) => {
            const { code, message } = error;
            console.warn(code, message);
        })
    }

    fetchCoordDtls(coordinate: any) {
        fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + coordinate.latitude + ',' + coordinate.longitude + '&key=' + 'AIzaSyAYs5C1yyuhTqPrUftQLi15cOlUAOc16d4')
            .then((response) => response.json())
            .then((responseJson) => {
                let newMarkers = this.state.markers;
                console.log('Selectd location data>>>>', responseJson.results[0])
                this.placeInfoDict(responseJson.results[0]).then((data: any) => {
                    console.log('fetchCoordDtls data>>>>', data)
                    newMarkers[0] = data
                    this.setState({ markers: newMarkers, location: data });
                });
            })
    }

    placeInfoDict(dtls: any) {
        return new Promise((resolve, reject) => {
            let country = '';

            for (let i = 0; i < dtls['address_components'].length; i++) {
                for (let j = 0; j < dtls['address_components'][i]['types'].length; j++) {
                    if (dtls['address_components'][i]['types'][j] == 'country') {
                        country = dtls['address_components'][i]['long_name'];
                        break;
                    }
                }
            }

            let state = '';
            let type = ''
            for (let i = 0; i < dtls['address_components'].length; i++) {
                for (let j = 0; j < dtls['address_components'][i]['types'].length; j++) {
                    type = dtls['address_components'][i]['types'][j]
                    if (type == 'administrative_area_level_2' || type == 'administrative_area_level_1') {
                        state = dtls['address_components'][i]['long_name'];
                        break;
                    }
                }
            }

            let city = '';
            for (let i = 0; i < dtls['address_components'].length; i++) {
                for (let j = 0; j < dtls['address_components'][i]['types'].length; j++) {
                    type = dtls['address_components'][i]['types'][j]
                    if (type == 'locality' || type == 'sublocality' || type == 'sublocality_level_1') {
                        city = dtls['address_components'][i]['long_name'];
                        break;
                    }
                }
            }

            console.log('Selected Place Details >>>>>', dtls)

            resolve({
                address: dtls.formatted_address,
                country: country,
                index: this.state.markers.length,
                lat: dtls.geometry.location.lat,
                long: dtls.geometry.location.lng,
                state: state,
                city: city,
                placeId: dtls.place_id
            })
        })
    }

    openSearchModal(isStartLocation: Boolean) {
        RNGooglePlaces.openAutocompleteModal()
            .then((place) => {
                {
                    this.setState({ location: place })
                    console.log('Location', place)
                    // this.addMarkers(place.location)

                    this.fetchCoordDtlsMarkers(place.location);
                }
            })
            .catch(error => console.log(error.message));  // error is a Javascript Error object
    }

    fetchCoordDtlsMarkers(coordinate: any) {

        fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + coordinate.latitude + ',' + coordinate.longitude + '&key=' + 'AIzaSyAYs5C1yyuhTqPrUftQLi15cOlUAOc16d4')
            .then((response) => response.json())
            .then((responseJson) => {
                let newMarkers = this.state.markers
                console.log('Old', this.state.markers)
                console.log('After Slice Old', newMarkers)
                this.placeInfoDict(responseJson.results[0]).then((data) => {
                    newMarkers[0] = data;
                    console.log('New', newMarkers)
                    this.setState({ markers: newMarkers })
                });
            })
    }

    markerClick(index: any) {
        // if (index > 1) {
        //     this.setState({
        //         stoppageToDelete: index,
        //         showConfirmationMsg: true
        //     })
        // }
    }

    doneBtnAction() {

        if (this.state.markers.length > 0) {

            if (this.state.isAddress) {
                this.props.navigation.state.params.func(this.state.markers[0])
                this.props.navigation.goBack()
            }
            else {
                if (this.state.isOrigin) {
                    if (this.state.isSearch) {
                        this.props.saveSearchOriginInfo(this.state.markers[0])
                    }
                    else {
                        this.props.saveOriginInfo(this.state.markers[0])
                    }
                }
                else {
                    if (this.state.isSearch) {
                        this.props.saveSearchDestinationInfo(this.state.markers[0])
                    }
                    else {
                        this.props.saveDestinationInfo(this.state.markers[0])
                    }
                }
            }
        }

        this.props.navigation.goBack()
    }

    render() {
        console.log('State Markers >>>>', this.state.markers)
        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <View style={styles.container}>
                    {/* <TextInputWithBtn placeholder='Enter location' func={() => this.openSearchModal(true)} value={this.state.location.address != null ? this.state.location.address : ''} icon={require('../../assets/locationIcon.png')} rightImage={true} width='90%' alignSelf='center' marginTop={40} /> */}
                    <View style={styles.selectLocationView}>
                        <TextInputWithBtn placeholder='Enter location' func={() => this.openSearchModal(true)} value={this.state.location.address != null ? this.state.location.address : ''} width='90%' alignSelf='center' marginTop={0} />
                        <TouchableOpacity onPress={() => this.getCurrentLocation()}>
                            <Image style={styles.navigationImage} source={require('../../assets/locationIcon.png')} />
                        </TouchableOpacity>
                    </View>

                    <TextGreyBold text={(this.state.isOrigin ? 'Where will you be starting from?' : 'Where are you going?')} textAlign='left' fontSize={20} width='60%' marginLeft='5%' />
                    <Map markers={this.state.markers} func={(coordinate: any) => this.addMarkers(coordinate)} markerClick={this.markerClick} height={364} />
                    <CommonBtn title='Done' func={() => this.doneBtnAction()} position='absolute' bottom={8} />
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (state: any) => ({
    pickupInfo: state.savePickUpInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
    saveOriginInfo: (info: string) => dispatch({ type: ORIGIN_INFO, payload: info }),
    saveDestinationInfo: (info: string) => dispatch({ type: DESTINATION_INFO, payload: info }),
    saveSearchOriginInfo: (info: string) => dispatch({ type: SEARCH_ORIGIN_INFO, payload: info }),
    saveSearchDestinationInfo: (info: string) => dispatch({ type: SEARCH_DESTINATION_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectLocation);
