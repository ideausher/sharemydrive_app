//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD, APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    }, 
    selectLocationView:{flexDirection:'row',alignItems:'center',alignSelf:'center', width:'90%',marginTop:40},
    navigationImage:{width:19,height:19,marginLeft:8}   
});