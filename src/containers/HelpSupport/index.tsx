import React, { Component } from 'react';
import { View, Text, SafeAreaView, StatusBar, ScrollView, KeyboardAvoidingView, Platform, StatusBarIOS, NativeModules, Modal } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { headerStyle, headerTitleStyle,navHdrTxtStyle } from '../../common/styles';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import { CommonBtn } from '../../custom/CustomButton';
import { TextInputWithoutImage, TextInputMultiLine } from '../../custom/CustomTextInput';
import Toast from 'react-native-simple-toast';
import { API_GET_IN_TOUCH } from '../../constants/apis';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
//@ts-ignore
import validator from 'validator';
import { Loader } from '../../Utils/Loader';

export interface props {
    navigation: NavigationScreenProp<any, any>
};

const { StatusBarManager } = NativeModules;

export default class HelpSupport extends Component<props, object> {

    state = {
        statusBarHeight: 0,
        email: '',
        rideId: '',
        subject: '',
        description: '',
        showLoader: false
    }
    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle:  (
                <View >
                  <Text style={navHdrTxtStyle.style}>Help &amp; Support</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }
    }

    backBtnHandler() {
        this.props.navigation.navigate('Home');
    }

    // Called on Save  btn Click
    private submitBtnAction() {

        if (this.validateDetails()) {
            this.setState({ showLoader: true })
            this.callGetInTouchAPI()
        }
    }

    // Validate Login Credentials eneterd
    private validateDetails() {

        if (this.state.email.length <= 0) {
            // Alert.alert('Please enter Country Code.');
            Toast.show('Please enter email', Toast.SHORT);
            return false;
        }
        else if (!validator.isEmail(this.state.email)) {
            // Alert.alert('Please enter valid email.');
            Toast.show('Please enter valid email', Toast.SHORT);
            return false;
        }
        else if (this.state.rideId.length <= 0) {
            Toast.show('Please enter ride id', Toast.SHORT);
            return false;
        }
        else if (this.state.subject.length <= 0) {
            Toast.show('Please enter subject', Toast.SHORT);
            return false;
        }
        else if (this.state.description.length <= 0) {
            Toast.show('Please enter description', Toast.SHORT);
            return false;
        }

        return true;
    }


    // paarmeters for API
    private getParams() {

        var params: any = {
            "ride_id": this.state.rideId,
            "email": this.state.email,
            "subject": this.state.subject,
            "description": this.state.description,
        }

        return params
    }

    // Call API to update User Data
    private callGetInTouchAPI() {
        console.log('callGetInTouchAPI API called with params >>>', this.getParams())
        let weakSelf = this

        RequestManager.postRequestWithHeaders(API_GET_IN_TOUCH, this.getParams()).then((response: any) => {
            this.setState({ showLoader: false })
            General.showMsgWithDelay(response.message)
            weakSelf.props.navigation.navigate('Home');
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showLoader}>
                        <Loader />
                    </Modal>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                            <TextGreyBold marginTop={24} fontSize={25} text='Get in touch!' />
                            <TextGrey marginTop={8} fontSize={13} text='Tell us why do you need help.' color='rgba(78, 78, 80, 0.40)' />

                            <TextGrey marginTop={40} fontSize={14} text='Your Email' textAlign='left' color='#707070' width='90%' />
                            <TextInputWithoutImage placeholder='Enter your email' onChange={(text: any) => this.setState({ email: text })} value={this.state.email} fontSize={12} width='100%' />

                            <TextGrey marginTop={40} fontSize={14} text='Ride ID' textAlign='left' color='#707070' width='90%' />
                            <TextInputWithoutImage placeholder='abc123##' onChange={(text: any) => this.setState({ rideId: text })} value={this.state.rideId} fontSize={12} width='100%' />

                            <TextGrey marginTop={40} fontSize={14} text='Subject' textAlign='left' color='#707070' width='90%' />
                            <TextInputMultiLine placeholder='Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.' onChange={(text: any) => this.setState({ subject: text })} value={this.state.subject} fontSize={10} height={60} width='100%' />

                            <TextGrey marginTop={40} fontSize={14} text='Description' textAlign='left' color='#707070' width='90%' />
                            <TextInputMultiLine placeholder='Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.' onChange={(text: any) => this.setState({ description: text })} value={this.state.description} fontSize={10} height={80} width='100%' />

                            <CommonBtn title='Submit' func={() => this.submitBtnAction()} marginTop={60} />
                        </ScrollView>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}