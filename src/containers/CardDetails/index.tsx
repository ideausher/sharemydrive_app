import React, { Component } from 'react';
import { View, Text, Modal, Image, TouchableOpacity, Keyboard, StatusBar, Platform, StatusBarIOS, NativeModules, KeyboardAvoidingView, ScrollView, TextInput } from 'react-native';
import { headerStyle, headerTitleStyle, navHdrTxtStyle, commonShadowStyle } from '../../common/styles';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager';
import { API_GET_BOOKING_DETAILS, API_PAYMENT, } from '../../constants/apis';
import { Loader } from '../../Utils/Loader';
import General from '../../Utils/General';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextGrey } from '../../custom/CustomText';
import { TextInputWithoutImage } from '../../custom/CustomTextInput';
import { APP_GREY_BOLD_TEXT_COLOR, APP_GREEN_COLOR } from '../../constants/colors';
//@ts-ignore
import { CommonBtn } from '../../custom/CustomButton';
import Toast from 'react-native-simple-toast';
//@ts-ignore
import { connect } from 'react-redux';
import LocalDataManager from '../../Utils/LocalDataManager';

export interface props {
    navigation: NavigationScreenProp<any, any>,
    ridePaymentInfo: any
};

const { StatusBarManager } = NativeModules;

class CardDetails extends Component<props, object> {

    state = {
        expiryMonth: '',
        expiryYear: '',
        cvv: '',
        statusBarHeight: 0,
        showLoader: false,
        bookingDetails: {} as any,
        rideId: '',
        amount: 0,
        originalAmount: 0
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Add card details</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        LocalDataManager.getDataAsyncStorage('paymentInfo').then((data: any) => {
            console.log('Data Retrived from Storage paymentInfo >>>>', data)
            if (data != null) {
                this.setState({rideId: data.ride_id, amount: data.amount, originalAmount: data.original_amount})
                this.apiGetBookingDetails(data.ride_id)
            }
        })

        console.log('RIde Id in CArd details>>>>', this.props.ridePaymentInfo.rideId)
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    // Get paarmeters for Get Booking Details API
    private getBooingDetailsAPIParams(rideId: string) {
        return {
            "ride_id": rideId,
            "previous": 0,
        }
    }

    async apiGetBookingDetails(rideId: string) {
        console.log('GET apiGetBookingDetails called with params >>>', this.getBooingDetailsAPIParams(rideId))
        this.setState({ showLoader: true, isAPICalledOnce: true, isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_BOOKING_DETAILS, this.getBooingDetailsAPIParams(rideId))
            .then((response: any) => {
                console.log('REsponse of apiGetBookingDetails API >>>>', response);
                weakSelf.setState({ showLoader: false, bookingDetails: response.data[0] })

            }).catch((error: any) => {
                General.showErroMsgInternetNoData(this, error)
            })
    }

    validateData() {
        if (this.state.expiryMonth.length == 0) {
            Toast.show('Please enter expiry month', Toast.SHORT);
            return
        }
        else if (this.state.expiryYear.length == 0) {
            Toast.show('Please enter expiry year', Toast.SHORT);
            return
        }
        else if (this.state.cvv.length == 0) {
            Toast.show('Please enter expiry cvv/cvc', Toast.SHORT);
            return
        }

        return true
    }

    async continueBtnAction() {
        if (this.validateData()) {
            this.callPaymentAPI()
        }
    }

    // Get paarmeters for APPOINTMENT BOOK API
    private getParams() {

        let selectedCard = this.props.navigation.getParam('selectedCard')

        let dict: any = {
            "ride_id": this.state.rideId,
            "consume_seats": 1,
            "card_source_id": selectedCard.id,
            "card_save": 0,
            "currency": 'usd',
            "original_amount": this.state.originalAmount,    ///bookingDetails.without_Discount_Payable,
            "card_id": selectedCard.id,
            "price": this.state.amount
        }

        if(Number(this.state.bookingDetails.discount_Applied)  > 0)
        {
            dict["referral_code"] = this.state.bookingDetails.referral_Code_To_Use
            dict["discount_applied"] = this.state.bookingDetails.discount_Applied
        }

        return dict
    }

    private async callPaymentAPI() {
        console.log('PArams callPaymentAPI >>>>', this.getParams())
        this.setState({ showLoader: true })
        let weakSelf = this
        RequestManager.postRequestWithHeaders(API_PAYMENT, this.getParams()).then((response: any) => {

            console.log('REsponse of callPaymentAPI >>>>', response);
            LocalDataManager.saveDataAsyncStorage('shouldShowPaymentPopup', JSON.stringify(false))

            setTimeout(function () {
                weakSelf.setState({ showLoader: false })
            }, 200);
            Toast.show(response.message, Toast.SHORT);
            weakSelf.props.navigation.navigate('PaymentCompleted')
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    txtExpiryMonth: TextInput;
    txtExpiryYear: TextInput;
    txtCvv: TextInput;

    render() {

        let actualPrice = '$' + this.state.amount

        let selectedCard = this.props.navigation.getParam('selectedCard')

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showLoader}>
                        <Loader />
                    </Modal>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                            <View style={[styles.cardVw, commonShadowStyle]}>
                                <Image
                                    source={selectedCard.brand == 'Visa' ? require('../../assets/visa2.png') : require('../../assets/mastercard.png')}
                                    style={styles.img}
                                    resizeMode={'contain'}
                                />
                                <TextGrey fontSize={16} text='Your card number is' color='#4E4E50' />
                                <TextGrey fontSize={16} text={'XXXX - XXXX - XXXX -' + selectedCard.last4} color={APP_GREEN_COLOR} />
                            </View>
                            <TextGrey marginTop={30} fontSize={16} text='Enter amount' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImage placeholder='$0' onChange={(text: any) => this.setState({ a: text })} value={actualPrice} fontSize={14} width='100%' editable={false}/>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <TextGrey marginTop={30} fontSize={16} text='Expiration Date' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                                <TextGrey marginTop={30} fontSize={16} text='CVV / CVC' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View style={{ flexDirection: 'row', width: '50%', justifyContent: 'space-between' }}>
                                    <TextInputWithoutImage ref={(input: any) => { this.txtExpiryMonth = input as any }} placeholder='MM' onChange={(text: any) => { this.setState({ expiryMonth: text }), ((text.length == 2) ? Keyboard.dismiss() : null) }} value={this.state.expiryMonth} width='45%' keyboardType='numeric' returnKeyType='done' maxLength={2} fontSize={14} />
                                    <TextInputWithoutImage ref={(input: any) => { this.txtExpiryYear = input as any }} placeholder='YY' onChange={(text: any) => { this.setState({ expiryYear: text }), ((text.length == 2) ? Keyboard.dismiss() : null) }} value={this.state.expiryYear} width='45%' keyboardType='numeric' returnKeyType='done' maxLength={2} fontSize={14} />
                                </View>
                                <TextInputWithoutImage ref={(input: any) => { this.txtCvv = input as any }} placeholder='000' onChange={(text: any) => { this.setState({ cvv: text }), ((text.length == 3) ? Keyboard.dismiss() : null) }} value={this.state.cvv} width='22.5%' keyboardType='numeric' returnKeyType='done' maxLength={3} fontSize={14} />
                            </View>
                            <CommonBtn title='Continue' func={() => this.continueBtnAction()} marginTop={60} marginBottom={60}/>
                        </ScrollView>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    ridePaymentInfo: state.saveRidePaymentInfo,
});

export default connect(mapStateToProps)(CardDetails);
