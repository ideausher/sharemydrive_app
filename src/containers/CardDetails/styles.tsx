//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_GREY_TEXT_COLOR } from '../../constants/colors'

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 20
    },
    cardVw: {
        marginTop: 16,
        borderRadius: 10,
        backgroundColor: 'white',
        padding: 16,
        alignItems:'center'
    },
    img: {
        width: 60,
        height: 60,
    },
});