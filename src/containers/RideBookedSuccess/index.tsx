import React, { Component } from 'react';
import { View, Text, Image } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import LinearGradient from 'react-native-linear-gradient';
import { styles } from './styles';
import { commonShadowStyle } from '../../common/styles';
import { CommonBtn } from '../../custom/CustomButton';
import { GreenTextBtn } from '../../custom/CustomButton';
import { TextGreyBold } from '../../custom/CustomText';

export interface props {
    navigation: NavigationScreenProp<any, any>
}
export default class RideBookedSuccess extends Component<props, object> {

    static navigationOptions = () => {
        return {
            header: null
        }
    }

    doneBtnAction() {
        this.props.navigation.navigate('Home')
    }

    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../../assets/rideBookedSuccess.png')} style={styles.image} />
                <TextGreyBold marginTop={60} fontSize={20} text='Booking request sent Successfully!' />
                <CommonBtn title='Done' func={() => this.doneBtnAction()} marginTop={70} width='80%'/>
            </View>
        )
    }
}