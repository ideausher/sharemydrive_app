//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD, APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white', 
        alignItems: 'center'
    },  
    image: {
        marginTop: 40, 
        width: 180,
        height: 120
    },
    plusMinusContainer:{
        marginTop: 60,
        flexDirection: 'row', 
        width: '70%',
        justifyContent: 'space-between', 
        alignItems:'center'
    },
    plusMinusBtn:{
        width: 30,
        height: 30, 
    },
    plusMinusImg: {
        width: 30,
        height: 30
    },  
});