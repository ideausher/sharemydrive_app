import React, { Component } from 'react';
import { View, Image, StatusBar, Platform, StatusBarIOS, NativeModules, TouchableOpacity } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import {  VACANT_SEATS_INFO } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { headerStyle, headerTitleStyle } from '../../common/styles';

export interface props {
    navigation: NavigationScreenProp<any, any>,
    saveVacantSeatsInfo: any,
};

const { StatusBarManager } = NativeModules;

class VacantSeats extends Component<props, object> {

    state = {
        statusBarHeight: 0,
        vacantSeats: 1
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: null,
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    minusBtnAction() {

        if (this.state.vacantSeats == 1) {
            return
        }

        let vacantSeats = this.state.vacantSeats

        vacantSeats -= 1;
        this.setState({ vacantSeats: vacantSeats })

    }

    plusBtnAction() {

        if (this.state.vacantSeats == 3) {
            return
        }
        let vacantSeats = this.state.vacantSeats

        vacantSeats += 1;
        this.setState({ vacantSeats: vacantSeats })
    }

    nextBtnAction() {
        this.props.saveVacantSeatsInfo(this.state.vacantSeats)
        this.props.navigation.navigate('SelectPrice')
    }

    render() {
        return (
            <View style={styles.container}>
                <TextGreyBold marginTop={60} fontSize={20} text='How many vacant seats do you have?' textAlign='left' width='70%' />
                <Image source={require('../../assets/vacantSeats.png')} style={styles.image} />
                <View style={styles.plusMinusContainer}>
                    <TouchableOpacity
                        style={styles.plusMinusBtn}
                        disabled={this.state.vacantSeats == 0 ? true : false}
                        onPress={() => this.minusBtnAction()}>
                        <Image style={styles.plusMinusImg} source={require('../../assets/minus.png')} />
                    </TouchableOpacity>
                    <TextGrey marginTop={0} fontSize={44} text={this.state.vacantSeats} textAlign='center' />
                    <TouchableOpacity
                        style={styles.plusMinusBtn}
                        disabled={this.state.vacantSeats == 3 ? true : false}
                        onPress={() => this.plusBtnAction()}>
                        <Image style={styles.plusMinusImg} source={require('../../assets/plus.png')} />
                    </TouchableOpacity>
                </View>
                <CommonBtn title='Next' func={() => this.nextBtnAction()} marginTop={50} />
            </View>
        )
    }
}

const mapStateToProps = (state: any) => ({
    pickupInfo: state.savePickUpInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
    saveVacantSeatsInfo: (info: string) => dispatch({ type: VACANT_SEATS_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(VacantSeats);
