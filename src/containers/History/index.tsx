import React, { Component } from 'react';
import { View, Text, SafeAreaView, StatusBar, FlatList, Modal } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { headerStyle, headerTitleStyle, navHdrTxtStyle, commonShadowStyle } from '../../common/styles';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager';
import { API_GET_PUBLISHED_RIDES } from '../../constants/apis';
import { internetConnectionError } from '../../constants/messages';
import General from '../../Utils/General';
import HistoryRidesCell from '../../custom cells/HistoryRidesCell';
//@ts-ignore
import { connect } from 'react-redux';
import { Loader } from '../../Utils/Loader';
import { NoDataFoundView, NoInternetFoundView } from '../../custom/CustomComponents';

export interface props {
    navigation: NavigationScreenProp<any, any>
    loginInfo: any
};

class History extends Component<props, object> {
    state = {
        historyRidesArr: [] as any[],
        showLoader: false,
        page: 0,
        isRefreshing: false,
        loadMore: false,
        isCalled: false,
        prevPage: -1,
        isInternetError: false,
        isDataNotFoundError: false,
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>History</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        this.props.navigation.addListener('willFocus', (route) => this.refreshData());
    }

    backBtnHandler() {
        this.props.navigation.navigate('Home');
    }

    private refreshData() {

        this.setState({ page: 0, prevPage: -1, historyRidesArr: [] })

            setTimeout(() => {
                this.apiGetUpcomingHistoryRides(true)
            }, 200)
    }

    // Get paarmeters for Login API
    private getParams() {
        return {
            type: "2",
            fetch: 'b',
            page: this.state.isRefreshing ? 0 : this.state.page,
            limit: "5",
        }
    }

    async apiGetUpcomingHistoryRides(refresh: boolean) {
        console.log('apiGetUpcomingHistoryRides called with params >>>', this.getParams())
        this.setState({ showLoader: true, isCalled: true, isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_PUBLISHED_RIDES, this.getParams())
            .then((response: any) => {
                console.log('REsponse of apiGetUpcomingHistoryRides API >>>>', response);
                weakSelf.setState({ showLoader: false })
                let ridesArrData: any[] = []

                    if (refresh) {
                        weakSelf.setState({ page: 0, prevPage: -1, historyRidesArr: [] })
                        ridesArrData = response.data
                    }
                    else {
                        ridesArrData = this.state.historyRidesArr.concat(response.data)
                    }

                    setTimeout(() => {

                        console.log('Prev and next page >>>', this.state.prevPage, this.state.page)

                        weakSelf.setState({ showLoader: false, historyRidesArr: ridesArrData, loadMore: false, prevPage: this.state.page, isRefreshing: false })

                        setTimeout(() => {

                            console.log('Again Prev and next page >>>', this.state.prevPage, this.state.page)
                            if (weakSelf.state.historyRidesArr.length == 0) {
                                weakSelf.setState({ isInternetError: false, isDataNotFoundError: true })
                            }
                            weakSelf.setState({ isCalled: false })
                        }, 200)

                        if (response.data.length > 0) {
                            let nextPage = this.state.page + 1
                            weakSelf.setState({ page: nextPage })
                        }
                    }, 200)
            }).catch((error: any) => {
                this.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })

                if (error == internetConnectionError) {
                    this.setState({ isInternetError: true, isDataNotFoundError: false })
                }
                else if (error.message != null && error.message != undefined && error.message == "No Record Found.") {
                    this.setState({ isInternetError: false, isDataNotFoundError: true })
                }
                else {
                    General.showErroMsg(this, error)
                }
            })
    }

    private refreshList() {
        this.setState({ isRefreshing: true })
        setTimeout(() => {
            this.apiGetUpcomingHistoryRides(true)
        }, 200)
    }

    //Will call when reached last cell
    private loadMoreData = () => {

        if (this.state.prevPage == this.state.page) {
            console.log('Func Clledv >>>', this.state.prevPage, this.state.page)
            console.log('Func  return Clled')
            return
        }

        this.setState({ loadMore: true, isCalled: true })
        console.log('Func Clled2')
        setTimeout(() => {
            console.log('Func Clled3')
            this.apiGetUpcomingHistoryRides(false)
        }, 200)
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    {this.state.historyRidesArr.length > 0 ? <FlatList
                        showsVerticalScrollIndicator={false}
                        style={{ marginTop: 24, width: '100%', marginBottom: 8 }}
                        onRefresh={() => this.refreshList()}
                        refreshing={this.state.isRefreshing}
                        data={this.state.historyRidesArr}
                        // keyExtractor={(item: any, index: any) => index.toString()}
                        renderItem={({ item, index }: any) => <HistoryRidesCell
                            onClickEvent={(index: any) => null} leftBtnAction={(index: any) => null} rightBtnAction={(index: any) => null}
                            item={item} index={index} isUpcoming={false} loginUserId={this.props.loginInfo.id} />}
                        onEndReachedThreshold={0.5}
                        onScroll={({ nativeEvent }) => {
                            if (this.state.isCalled == false && General.isCloseToBottom(nativeEvent) && this.state.isRefreshing == false) {
                                this.loadMoreData()
                            }
                        }}
                    /> :(this.state.showLoader ? null :
                        (this.state.isDataNotFoundError ?
                            <NoDataFoundView func={() => this.refreshData()} message='You have no recent ride offers.' /> :
                            (this.state.isInternetError ?
                                <NoInternetFoundView func={() => this.refreshData()} /> : null))) }
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    loginInfo: state.saveLoginInfo,
});

export default connect(mapStateToProps)(History);
