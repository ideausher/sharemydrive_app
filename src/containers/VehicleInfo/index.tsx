import React, { Component } from 'react';
import { View, Text, Image, StatusBar, Platform, StatusBarIOS, NativeModules, KeyboardAvoidingView, Keyboard, TouchableOpacity, SafeAreaView, ScrollView, Modal } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { TextInputWithBtn, TextInputWithoutImage } from '../../custom/CustomTextInput';
import { TextGrey } from '../../custom/CustomText';
import { LOGIN, VEHCILE } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { APP_GREY_BOLD_TEXT_COLOR } from '../../constants/colors';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
import { API_SIGNUP, API_VEHICLE_INFO } from '../../constants/apis';
import { ForGotScreenType } from '../../Utils/Enums';
import LocalDataManager from '../../Utils/LocalDataManager';
import { DropdownList } from '../../custom/DropdownList';
import Toast from 'react-native-simple-toast';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment';
import SimpleToast from 'react-native-simple-toast';
import { MaterialDialog } from 'react-native-material-dialog';
import { APP_GREEN_COLOR } from '../../constants/colors';



export interface props {
    navigation: NavigationScreenProp<any, any>
    vehcileInfo: any,
    saveVehicleInfo: any,
    loginInfo: any,
    saveLoginInfo: any,
}

const { StatusBarManager } = NativeModules;

const carTypes = ['Hatchback', 'Sedan', 'MPV', 'SUV', 'Crossover', 'Coupe', 'Convertible']
const carColors = ['Black', 'White', 'Grey', 'Red', 'Yellow']
const list = ['Western Australia', 'Queensland', 'New South Wales', 'Victoria', 'South Australia', 'Tasmania', 'Northern Territory']

class VehicleInfo extends Component<props, object> {

    state = {
        showPopup: false,
        updationMessage: '',
        statusBarHeight: 0,
        // country: '',
        rego: '',
        carManufac: '',
        carModel: '',
        carType: '',
        carColor: '',
        regYear: '',
        regState: '',
        showCarTypesPicker: false,
        showcarColorsPicker: false,
        showCarModalPicker: false,
        showPicker: false,
        lastYears: []
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Vehicle Details</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        console.log('Props >>>>', this.props)
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        this.apiGetVehicleDetails()

        // Get List of years
        let currentYear = moment().format("YYYY")
        let years = Array.from(new Array(20), (val, index) => Number(currentYear) - index);

        this.setState({ lastYears: years })
    }

    async apiGetVehicleDetails() {

        this.setState({ showLoader: true })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_VEHICLE_INFO, {})
            .then((response: any) => {
                console.log('REsponse of apiGetVehicleDetails API >>>>', response);
                weakSelf.setState({ showLoader: false })
                weakSelf.props.saveVehicleInfo(response.data)
            }).catch((error: any) => {
                console.log('Error Mesage Shown From here 1')
                General.showErroMsg(this, error)
            })
    }

    componentWillReceiveProps(newProps: any) {

        console.log('Old Props: ' + JSON.stringify(newProps))

        this.setState({
            // country: newProps.vehcileInfo.country,
            rego: newProps.vehcileInfo.rego,
            carManufac: newProps.vehcileInfo.carManufacture,
            carModel: newProps.vehcileInfo.carModel,
            carType: newProps.vehcileInfo.carType,
            carColor: newProps.vehcileInfo.carColor,
            regYear: newProps.vehcileInfo.registerYear,
            regState: newProps.vehcileInfo.registerState,
        })
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    carTypeBtnAction() {
        this.setState({ showCarTypesPicker: true })
    }

    carColorBtnAction() {
        this.setState({ showcarColorsPicker: true })
    }

    regYearBtnAction() {
        this.setState({ showCarModalPicker: true })
    }


    regStateBtnAction() {
        this.setState({ showPicker: true })
    }

    valueSelected(item: any) {


        console.log('')
        if (this.state.showCarTypesPicker == true) {

            this.setState({ showCarTypesPicker: false, carType: item })
        }
        else if (this.state.showcarColorsPicker == true) {
            this.setState({ showcarColorsPicker: false, carColor: item })
        }
        else if (this.state.showCarModalPicker == true) {
            this.setState({ showCarModalPicker: false, regYear: item })
        }
        else {
            this.setState({ showPicker: false, regState: item })
        }

    }

    // Called on Save  btn Click
    private saveBtnAction() {
        if (this.validateDetails()) {
            this.setState({ showLoader: true })
            this.apiUpdateVehicleInfo()
        }
    }



    /**
    * @function validateDetails validate data of all fields
    */
    private validateDetails() {

        // if (this.state.country==undefined || this.state.country.length <= 0) {
        //     Toast.show('Please enter country', Toast.SHORT);
        //     return false;
        // }
        if (this.state.rego == undefined || this.state.rego.length <= 0) {
            Toast.show('Please enter REGO', Toast.SHORT);
            return false;
        }
        else if (this.state.carManufac == undefined || this.state.carManufac.length <= 0) {
            Toast.show('Please enter car manufacturers', Toast.SHORT);
            return false;
        }
        else if (this.state.carModel == undefined || this.state.carModel.length <= 0) {
            Toast.show('Please enter car model', Toast.SHORT);
            return false;
        }
        else if (this.state.carType == undefined || this.state.carType.length <= 0) {
            Toast.show('Please enter car type', Toast.SHORT);
            return false;
        }
        else if (this.state.carColor == undefined || this.state.carColor.length <= 0) {
            Toast.show('Please enter car color', Toast.SHORT);
            return false;
        }
        else if (this.state.regYear == undefined || this.state.regYear.length <= 0) {
            Toast.show('Please enter registered year', Toast.SHORT);
            return false;
        }
        else if (this.state.regState == undefined || this.state.regState.length <= 0) {
            Toast.show('Please enter registered state', Toast.SHORT);
            return false;
        }

        return true;
    }

    getParams() {
        return {
            // country: this.state.country,
            rego: this.state.rego,
            car_manufacture: this.state.carManufac,
            car_model: this.state.carModel,
            car_type: this.state.carType,
            car_color: this.state.carColor,
            register_year: this.state.regYear,
            register_state: this.state.regState
        }
    }

    async apiUpdateVehicleInfo() {

        console.log('apiUpdateVehicleInfo func called wirthparams >>>>', this.getParams())
        let weakSelf = this

        RequestManager.postRequestWithHeaders(API_VEHICLE_INFO, this.getParams()).then((response: any) => {

            console.log('REsponse of apiUpdateVehicleInfo >>>>', response);
            weakSelf.setState({ showLoader: false, showPopup: true, updationMessage: response.message })
            // General.showMsgWithDelay(response.message)

            weakSelf.props.saveVehicleInfo(response.data)
            this.updateLoginInfo()
            // weakSelf.props.navigation.goBack()
        }).catch((error: any) => {
            console.log('Error Mesage Shown From here 2')
            General.showErroMsg(this, error)
        })
    }

    goBack = () => {
        this.setState({ ...this.state, showPopup: false })
        this.props.navigation.goBack()
    }

    private updateLoginInfo() {

        let loginDict = this.getLoginInfoDict()
        console.log('loginDict >>>>', loginDict)
        LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(loginDict))
        this.props.saveLoginInfo(loginDict)

    }
    private getLoginInfoDict() {

        return {
            email: this.props.loginInfo.email,
            firstname: this.props.loginInfo.firstName,
            lastname: this.props.loginInfo.lastName,
            id: this.props.loginInfo.id,
            phone_country_code: this.props.loginInfo.countryCode,
            phone_number: this.props.loginInfo.phoneNumber,
            token: this.props.loginInfo.token,
            gender: this.props.loginInfo.gender,
            image: this.props.loginInfo.image,
            date_of_birth: this.props.loginInfo.dob,
            refferal_code: this.props.loginInfo.referralCode,
            referral_code_text: this.props.loginInfo.referralText,
            is_notification: this.props.loginInfo.isNotification,
            bio: this.props.loginInfo.bio,
            suggestedPrice: this.props.loginInfo.suggestedPrice,
            is_vehicle_added: 1,
            is_bank_detail_added: this.props.loginInfo.isBankDetailsAdded,
            address: this.props.loginInfo.address
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>

                <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                    <MaterialDialog
                        title="Vehicle info Updation!"
                        titleColor={APP_GREEN_COLOR}
                        visible={this.state.showPopup}
                        cancelLabel=""
                        onOk={() => this.goBack()}
                        colorAccent={APP_GREEN_COLOR}
                        onCancel={() => { }}>
                        <Text style={styles.dialogText}>
                            {this.state.updationMessage}
                        </Text>
                    </MaterialDialog>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                            {/* <TextGrey marginTop={30} fontSize={12} text='Country' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImage placeholder='Enter country' onChange={(text: any) => this.setState({ country: text })} value={this.state.country} width='100%' /> */}
                            <TextGrey marginTop={16} fontSize={12} text='REGO' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextGrey marginTop={10} fontSize={10} text={'We\'ll only share number plate with people who book ride with you'} color='#487CFD' width='100%' textAlign='left' />
                            <TextInputWithoutImage placeholder='Enter REGO' onChange={(text: any) => this.setState({ rego: text })} value={this.state.rego} width='100%' />
                            <TextGrey marginTop={16} fontSize={12} text='Car manufacturers' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextGrey marginTop={10} fontSize={10} text='Enter manufacturers like : Toyota, Mazda, BMW, Subaru, etc...' color='#487CFD' width='100%' textAlign='left' />
                            <TextInputWithoutImage placeholder='Enter car manufacturers' onChange={(text: any) => this.setState({ carManufac: text })} value={this.state.carManufac} width='100%' />
                            <TextGrey marginTop={16} fontSize={12} text='Car Model' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImage placeholder='Enter car model' onChange={(text: any) => this.setState({ carModel: text })} value={this.state.carModel} width='100%' />
                            <TextGrey marginTop={16} fontSize={12} text='Car Type' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithBtn placeholder='Car Type' func={() => this.carTypeBtnAction()} value={this.state.carType} icon={require('../../assets/blueArrow.png')} rightImage={true} width='100%' alignSelf='center' imgSizeWidth={14} imgSizeHeight={6} />
                            <TextGrey marginTop={16} fontSize={12} text='Car Color' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithBtn placeholder='Car Color' func={() => this.carColorBtnAction()} value={this.state.carColor} icon={require('../../assets/blueArrow.png')} rightImage={true} width='100%' alignSelf='center' imgSizeWidth={14} imgSizeHeight={6} />
                            <TextGrey marginTop={16} fontSize={12} text='Registered Year' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithBtn placeholder='Registered Year' func={() => this.regYearBtnAction()} value={this.state.regYear} icon={require('../../assets/blueArrow.png')} rightImage={true} width='100%' alignSelf='center' imgSizeWidth={14} imgSizeHeight={6} />
                            <TextGrey marginTop={16} fontSize={12} text='Registered State' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithBtn placeholder='Registered State' func={() => this.regStateBtnAction()} value={this.state.regState} icon={require('../../assets/blueArrow.png')} rightImage={true} width='100%' alignSelf='center' imgSizeWidth={14} imgSizeHeight={6} />
                            <CommonBtn title='Save' func={() => this.saveBtnAction()} marginTop={50} marginBottom={60} />
                        </ScrollView>
                    </View>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showCarTypesPicker}>
                        <DropdownList data={carTypes} topMargin={50} func={(item: any) => this.valueSelected(item)} removeList={() => { this.setState({ showCarTypesPicker: false }), console.log('Remove LIst func Called') }} selecedItem={this.state.carType} />
                    </Modal>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showcarColorsPicker}>
                        <DropdownList data={carColors} topMargin={50} func={(item: any) => this.valueSelected(item)} removeList={() => { this.setState({ showcarColorsPicker: false }), console.log('Remove LIst func Called') }} selecedItem={this.state.carColor} />
                    </Modal>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showCarModalPicker}>
                        <DropdownList data={this.state.lastYears} topMargin={50} func={(item: any) => this.valueSelected(item)} removeList={() => { this.setState({ showCarModalPicker: false }), console.log('Remove LIst func Called') }} selecedItem={this.state.regYear} />
                    </Modal>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showPicker}>
                        <DropdownList data={list} topMargin={50} func={(item: any) => this.valueSelected(item)} removeList={() => { this.setState({ showPicker: false }), console.log('Remove LIst func Called') }} selecedItem={this.state.regState} />
                    </Modal>
                </KeyboardAvoidingView>
            </SafeAreaView>

        )
    }
}

const mapStateToProps = (state: any) => ({
    vehcileInfo: state.saveVehicleInfo,
    loginInfo: state.saveLoginInfo,
});

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveVehicleInfo: (info: string) => dispatch({ type: VEHCILE, payload: info }),
    saveLoginInfo: (info: string) => dispatch({ type: LOGIN, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(VehicleInfo);
