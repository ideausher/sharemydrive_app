import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';

export interface props {
    navigation: NavigationScreenProp<any, any>
}
export default class LoginSuccess extends Component<props, object> {

    static navigationOptions = () => {
        return {
            header: null
        }
    }

    componentDidMount() {

        setTimeout(() => {
            this.props.navigation.navigate('Home')
        }, 1000);
    }

    allowBtnAction() {

    }

    moveToHome() {
        this.props.navigation.navigate('Home')
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <TouchableOpacity style={styles.container} onPress={() => this.moveToHome()}>
                    <Image source={require('../../assets/loginSuccess.png')} style={styles.image} />
                    <Text style={[styles.greyBoldText, { opacity: 0.8 }]}>Login Successful!</Text>
                </TouchableOpacity>
            </SafeAreaView>

        )
    }
}