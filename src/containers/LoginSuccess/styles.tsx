//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD, APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center', 
        alignItems: 'center'
    },    
    greyBoldText: {
        fontSize: 25,
        fontFamily: APP_FONT_BOLD,
        color: '#717173',
        textAlign: 'center',
        marginTop: 60,
    },
    image: {
        width: 170,
        height:170
    },
});