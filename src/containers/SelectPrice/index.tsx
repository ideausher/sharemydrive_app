import React, { Component } from 'react';
import { View, Text, Image, StatusBar, Platform, StatusBarIOS, NativeModules, KeyboardAvoidingView, Keyboard } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { TextInputWithoutImage } from '../../custom/CustomTextInput';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import { ORIGIN_INFO, DESTINATION_INFO, INITIAL_PICK_UP_INFO } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { headerStyle, headerTitleStyle } from '../../common/styles';
import RequestManager from '../../Utils/RequestManager';
import { API_OFFER_RIDE } from '../../constants/apis';
import General from '../../Utils/General';
import moment from 'moment';
import { getDistance, getPreciseDistance } from 'geolib';
import { PopupScreenType } from '../../Utils/Enums';

export interface props {
    navigation: NavigationScreenProp<any, any>
    pickupInfo: any,
    loginInfo: any,
    saveInitialPickupInfo: any,
}

const { StatusBarManager } = NativeModules;

class SelectPrice extends Component<props, object> {

    state = {
        statusBarHeight: 0,
        price: 0,
        toll: 0
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: null,
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {

        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        console.log('Props pickupInfo >>>>', this.props.pickupInfo)
        console.log('Props LoginInfo >>>>', this.props.loginInfo)

        let distance = getPreciseDistance(
            { latitude: this.props.pickupInfo.origin.lat, longitude: this.props.pickupInfo.origin.long },
            { latitude: this.props.pickupInfo.destination.lat, longitude: this.props.pickupInfo.destination.long },
            0.01
        );

       // console.log('Distance >>>>', distance)
        let suggestedPrice = ((distance / 1000) * this.props.loginInfo.suggestedPrice).toFixed(2)
        this.setState({ price: suggestedPrice.toString() })
       // console.log('suggestedPrice >>>>', suggestedPrice)
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    nextBtnAction() {

        // return if pickup info is blank
        if(this.props.pickupInfo.origin == "") {
            return
        }

        this.apiOfferRide()
    }

    getRideDtaeTiemArray() {
        const dateTimeArr: any[] = []
        for(let i=0; i<this.props.pickupInfo.date.length; i++) {
            let date = this.props.pickupInfo.date[i]
            dateTimeArr.push(date + ' ' + moment(this.props.pickupInfo.time, "hh:mm a").format('HH:mm:ss'))
        }

        console.log('this.getRideDtaeTiemArray >>>>', dateTimeArr)

        return dateTimeArr
    }

    // paarmeters for API
    private getParams() {

        console.log("Params Func called >>>>>", this.props.pickupInfo)

        var params: any = {
            ride_date_time: this.getRideDtaeTiemArray(),
            max_no_seats: this.props.pickupInfo.vacantSeats.toString(),
            gender: this.props.loginInfo.gender,
            price_per_seat: this.state.price,
            total_toll_tax: this.state.toll.toString(),
            start_latitude: this.props.pickupInfo.origin.lat.toString(),
            start_longitude: this.props.pickupInfo.origin.long.toString(),
            start_country: this.props.pickupInfo.origin.country,
            start_place_id: this.props.pickupInfo.origin.placeId,
            start_city: this.props.pickupInfo.origin.city,
            start_state: this.props.pickupInfo.origin.state,
            start_full_address: this.props.pickupInfo.origin.address,
            end_latitude: this.props.pickupInfo.destination.lat.toString(),
            end_longitude: this.props.pickupInfo.destination.long.toString(),
            end_country: this.props.pickupInfo.destination.country,
            end_place_id: this.props.pickupInfo.destination.placeId,
            end_city: this.props.pickupInfo.destination.city,
            end_state: this.props.pickupInfo.destination.state,
            end_full_address: this.props.pickupInfo.destination.address
        }

        console.log("Params Func after >>>>>", params)

        return params
    }

    // Call API to update User Data
    private apiOfferRide() {
        this.setState({ showLoader: true })
        console.log('apiOfferRide API called with params >>>', this.getParams())
        RequestManager.postRequestWithHeaders(API_OFFER_RIDE, this.getParams()).then((response: any) => {

            console.log('Success Response of api offer ride API >>>>', response);
            this.setState({ showLoader: false })
            this.props.saveInitialPickupInfo()
            this.props.navigation.navigate('GreyPopup', {screenType : PopupScreenType.ridePublished})
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    render() {

        console.log('State Price >>>', this.state.price)

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <View style={styles.container}>
                    <TextGreyBold marginTop={40} fontSize={20} text='Recommended price per seat.' textAlign='left' width='80%' />
                    <TextGreyBold marginTop={0} fontSize={20} text='Do you Agree?' textAlign='left' width='80%' />
                    <TextGrey marginTop={16} fontSize={13} text='Try suggested price, it gets faster reservations.' textAlign='left' width='80%' />
                    <TextInputWithoutImage placeholder='$0.0' onChange={(text: any) => this.setState({ price: text })} value={this.state.price} marginTop={60} keyboardType='decimal-pad' returnKeyType="done" width='80%' />
                    <Text style={styles.blueText}>Edit price as per your choice</Text>
                    <Text style={styles.plusText}>+</Text>
                    <TextInputWithoutImage placeholder='Add toll tax (if any)' onChange={(text: any) => this.setState({ toll: text })} value={this.state.toll} marginTop={16} keyboardType='decimal-pad' returnKeyType="done" width='80%' />
                    <CommonBtn title='Next' func={() => this.nextBtnAction()} marginTop={80} />
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (state: any) => ({
    pickupInfo: state.savePickUpInfo,
    loginInfo: state.saveLoginInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
    saveInitialPickupInfo: (info: string) => dispatch({ type: INITIAL_PICK_UP_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(SelectPrice);
