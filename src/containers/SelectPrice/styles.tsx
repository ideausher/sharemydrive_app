//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD, APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems:'center'
    },    
    blueText:{
        fontSize: 10,
        fontFamily: APP_FONT_REGULAR,
        color: '#2785FE',
        textAlign: 'left',
        width:'80%',
        marginTop: 8
    },
    plusText: {
        fontSize: 30,
        fontFamily: APP_FONT_REGULAR,
        color: '#717173',
        textAlign: 'center',
    },
});