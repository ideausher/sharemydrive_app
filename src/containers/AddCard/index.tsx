import React, { Component } from 'react';
import { View, Text, Modal, Image, TouchableOpacity, Keyboard, StatusBar, Platform, StatusBarIOS, NativeModules, KeyboardAvoidingView, TextInput } from 'react-native';
import { headerStyle, headerTitleStyle, navHdrTxtStyle } from '../../common/styles';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager';
import { API_PAYMENT, API_GET_BOOKING_DETAILS, } from '../../constants/apis';
import { Loader } from '../../Utils/Loader';
import General from '../../Utils/General';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextGrey } from '../../custom/CustomText';
import { TextInputWithoutImage } from '../../custom/CustomTextInput';
import { APP_GREY_BOLD_TEXT_COLOR, APP_GREEN_COLOR } from '../../constants/colors';
//@ts-ignore
import ToggleSwitch from 'toggle-switch-react-native';
import { CommonBtn } from '../../custom/CustomButton';
import Toast from 'react-native-simple-toast';
//@ts-ignore
import { connect } from 'react-redux';
import LocalDataManager from '../../Utils/LocalDataManager';

export interface props {
    navigation: NavigationScreenProp<any, any>,
    ridePaymentInfo: any
};

const { StatusBarManager } = NativeModules;

class AddCard extends Component<props, object> {

    state = {
        amount: '',
        cardHolderName: '',
        cardNo: '', //'4242 4242 4242 4242',
        expiryMonth: '', //'06',
        expiryYear: '', ///'20',
        cvv: '',    //'256',
        statusBarHeight: 0,
        isCardSaved: false,
        showLoader: false,
        coupenCode: '',
        discountedPrice: 0,
        stripeToken: '',
        bookingDetails: {} as any,
        rideId: '',
        originalAmount: 0
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Add card details</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        LocalDataManager.getDataAsyncStorage('paymentInfo').then((data: any) => {
            console.log('Data Retrived from Storage >>>>', data)
            if (data != null) {
                this.setState({ rideId: data.ride_id, amount: data.amount, originalAmount: data.original_amount })
                this.apiGetBookingDetails(data.ride_id)
            }
        })

        console.log('RIde Id in Add CArd >>>>', this.props.ridePaymentInfo.rideId)
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    // Get paarmeters for Get Booking Details API
    private getBooingDetailsAPIParams(rideId: string) {
        return {
            "ride_id": rideId,
            "previous": 0,
        }
    }

    async apiGetBookingDetails(rideId: string) {
        console.log('GET apiGetBookingDetails called with params >>>', this.getBooingDetailsAPIParams(rideId))
        this.setState({ showLoader: true, isAPICalledOnce: true, isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_BOOKING_DETAILS, this.getBooingDetailsAPIParams(rideId))
            .then((response: any) => {
                console.log('REsponse of apiGetBookingDetails API >>>>', response);
                weakSelf.setState({ showLoader: false, bookingDetails: response.data[0] })

            }).catch((error: any) => {
                General.showErroMsgInternetNoData(this, error)
            })
    }

    validateData() {
        // if (this.state.amount.length == 0) {
        //     Alert.alert('Please enter amount.');
        //     return false
        // }
        if (this.state.cardHolderName.length == 0) {
            Toast.show('Please enter card holder name', Toast.SHORT);
            return false
        }
        else if (this.state.cardNo.length == 0) {
            Toast.show('Please enter card number', Toast.SHORT);
            return
        }
        else if (this.state.expiryMonth.length == 0) {
            Toast.show('Please enter expiry month', Toast.SHORT);
            return
        }
        else if (this.state.expiryYear.length == 0) {
            Toast.show('Please enter expiry year', Toast.SHORT);
            return
        }
        else if (this.state.cvv.length == 0) {
            Toast.show('Please enter expiry cvv/cvc', Toast.SHORT);
            return
        }

        return true
    }

    async addCardBtnAction() {
        // this.props.navigation.navigate('Verification', { verificationFor: VerificationFor.payment })

        if (this.validateData()) {
            let cardNum = this.state.cardNo.replace(/ /g, '')

            var stripe = require('stripe-client')('pk_test_gVTWFUkRg5xoLaNQnCfgY3Zf00Sdn1jBwb'); //pk_test_pku2v8tppoSjIx8zYldEZalN

            console.log('Stripe >>>>', stripe)
            stripe.createToken({
                card: {
                    "number": cardNum,
                    "exp_month": this.state.expiryMonth,
                    "exp_year": this.state.expiryYear,
                    "cvc": this.state.cvv
                }
            }).then((sucess: any) => {
                //Toast.show("SUCCESS in Stripe token creation >>>>>" + sucess, Toast.SHORT);
                    console.log("start >>> card add Response >>",sucess,sucess.id);

                if (sucess == undefined || sucess.id==undefined) {
                    Toast.show("Please enter valid card information here");
                }
                else {
                    console.log('Stripe Token Genrated >>>>', sucess);
                    this.setState({ stripeToken: sucess })
                    this.callPaymentAPI()
                }

            }).catch((error: any) => {
                console.log("start >>> card add Error >>",error);
                Toast.show("Error in Stripe token creation >>>>>" + error, Toast.SHORT);
            })

            // const token = PaymentManager.setStripeKey(cardNum, this.state.expiryMonth, this.state.expiryYear, this.state.cvv)

        }
    }

    // Get paarmeters for APPOINTMENT BOOK API
    private getParams(token: any) {

        let dict: any = {
            "ride_id": this.state.rideId,
            "consume_seats": 1,
            "card_source_id": token.id,
            "card_save": this.state.isCardSaved ? 1 : 0,
            "currency": 'usd',
            "original_amount": this.state.originalAmount,  //bookingDetails.without_Discount_Payable,
            "card_id": token.card.id,
            "price": this.state.amount       ///bookingDetails.with_Discount_Payable
        }

        if (Number(this.state.bookingDetails.discount_Applied) > 0) {
            dict["referral_code"] = this.state.bookingDetails.referral_Code_To_Use
            dict["discount_applied"] = this.state.bookingDetails.discount_Applied
        }

        return dict
    }

    private async callPaymentAPI() {
        //Toast.show("payment APi called", Toast.SHORT);

        console.log('Stripe Token >>>>>', this.state.stripeToken)
        console.log('PArams callPaymentAPI >>>>', this.getParams(this.state.stripeToken))
        this.setState({ showLoader: true })
        let weakSelf = this
        RequestManager.postRequestWithHeaders(API_PAYMENT, this.getParams(this.state.stripeToken)).then((response: any) => {

            console.log('REsponse of callPaymentAPI >>>>', response);
            LocalDataManager.saveDataAsyncStorage('shouldShowPaymentPopup', JSON.stringify(false))
            setTimeout(function () {
                weakSelf.setState({ showLoader: false })
            }, 200);
            Toast.show(response.message, Toast.SHORT);
            weakSelf.props.navigation.navigate('PaymentCompleted')
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    // Instances of Textinput
    txtCardNum: TextInput;
    txtExpiryMonth: TextInput;
    txtExpiryYear: TextInput;
    txtCvv: TextInput;

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showLoader}>
                        <Loader />
                    </Modal>
                    <View style={styles.container}>
                        <TextGrey marginTop={30} fontSize={16} text='Card Holder Name' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                        <TextInputWithoutImage placeholder='Jackson Murinondo' onChange={(text: any) => this.setState({ cardHolderName: text })} value={this.state.cardHolderName} fontSize={14} width='100%' />
                        <TextGrey marginTop={30} fontSize={16} text='Card Number' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                        <TextInputWithoutImage ref={(input: any) => { this.txtCardNum = input as any }} placeholder='xxxx xxxx xxxx xxxx' onChange={(text: any) => { this.setState({ cardNo: text.replace(/\s?/g, '').replace(/(\d{4})/g, '$1 ').trim() }), ((text.length == 19) ? Keyboard.dismiss() : null) }} value={this.state.cardNo} keyboardType='numeric' returnKeyType='done' maxLength={19} fontSize={14} width='100%' />
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TextGrey marginTop={30} fontSize={16} text='Expiration Date' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextGrey marginTop={30} fontSize={16} text='CVV / CVC' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flexDirection: 'row', width: '50%', justifyContent: 'space-between' }}>
                                <TextInputWithoutImage ref={(input: any) => { this.txtExpiryMonth = input as any }} placeholder='MM' onChange={(text: any) => { this.setState({ expiryMonth: text }), ((text.length == 2) ? Keyboard.dismiss() : null) }} value={this.state.expiryMonth} width='45%' keyboardType='numeric' returnKeyType='done' maxLength={2} fontSize={14} />
                                <TextInputWithoutImage ref={(input: any) => { this.txtExpiryYear = input as any }} placeholder='YY' onChange={(text: any) => { this.setState({ expiryYear: text }), ((text.length == 2) ? Keyboard.dismiss() : null) }} value={this.state.expiryYear} width='45%' keyboardType='numeric' returnKeyType='done' maxLength={2} fontSize={14} />
                            </View>
                            <TextInputWithoutImage ref={(input: any) => { this.txtCvv = input as any }} placeholder='000' onChange={(text: any) => { this.setState({ cvv: text }), ((text.length == 3) ? Keyboard.dismiss() : null) }} value={this.state.cvv} width='22.5%' keyboardType='numeric' returnKeyType='done' maxLength={3} fontSize={14} />
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 24, alignItems: 'center' }}>
                            <ToggleSwitch
                                isOn={this.state.isCardSaved}
                                onColor={APP_GREEN_COLOR}
                                onToggle={(isCardSaved: any) =>
                                    this.setState({ isCardSaved: isCardSaved })}
                            />
                            <TextGrey marginTop={0} marginLeft={8} fontSize={12} text='Save Card for future' textAlign='left' color='#4E4E50' />
                        </View>
                        <CommonBtn title='Add card' func={() => this.addCardBtnAction()} marginTop={60} marginBottom={60} />
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    ridePaymentInfo: state.saveRidePaymentInfo,
});

export default connect(mapStateToProps)(AddCard);