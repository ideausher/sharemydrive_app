import React, { Component } from 'react';
import { View, Text, FlatList, SafeAreaView, TouchableOpacity, Image } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
//@ts-ignore
import RangeSlider from 'rn-range-slider';
import { APP_SKY_BLUE_COLOR } from '../../constants/colors';
import { BlurView } from 'react-native-blur';
//@ts-ignore
import { connect } from 'react-redux';
import { FILTER_INFO } from '../../Redux/Constants';

const AddRangeSlider = (props: any) => {

  console.log('AddRangeSlider >>>', props)
  return (
    <RangeSlider
      style={{ height: 100 }}
      gravity={'center'}
      min={props.min}
      max={props.max}
      step={props.step}
      initialLowValue={props.selectedMin}
      initialHighValue={props.selectedMax}
      thumbRadius={10}
      thumbBorderWidth={1}
      lineWidth={1}
      textSize={16}
      thumbBorderColor={APP_SKY_BLUE_COLOR}
      // labelStyle='none'
      labelTailHeight={0}
      labelBorderWidth={0}
      //labelBackgroundColor={'#FFFFFF'}
      selectionColor={APP_SKY_BLUE_COLOR}
      blankColor={'#FFFFFF'}
      onValueChanged={(low: any, high: any, fromUser: any) => {
        props.func(low, high)
      }} />
  );
};

export interface props {
  navigation: NavigationScreenProp<any, any>
  searchInfo: any,
  saveFilterInfo: any,
  removeFilters: any
};

class Filters extends Component<props, object> {

  state = {

  }

  bestReviews() {
    this.props.saveFilterInfo(this.getFiltersData(!this.props.searchInfo.bestReviews, this.props.searchInfo.availabilitySeats, this.props.searchInfo.priceMin, this.props.searchInfo.priceMax, this.props.searchInfo.gender))
  }

  seatsAvailability() {
    this.props.saveFilterInfo(this.getFiltersData(this.props.searchInfo.bestReviews, !this.props.searchInfo.availabilitySeats, this.props.searchInfo.priceMin, this.props.searchInfo.priceMax, this.props.searchInfo.gender))
  }

  setPrice(min: any, max: any) {
    this.props.saveFilterInfo(this.getFiltersData(this.props.searchInfo.bestReviews, this.props.searchInfo.availabilitySeats, min, max, this.props.searchInfo.gender))
  }

  setGender(value: any) {
    console.log('New gender Value >>>', value)
    console.log('Old gender Value >>>', this.props.searchInfo.gender)

    let genderValue = value
    if (this.props.searchInfo.gender == value) {
      genderValue = ''
    }

    this.props.saveFilterInfo(this.getFiltersData(this.props.searchInfo.bestReviews, this.props.searchInfo.availabilitySeats, this.props.searchInfo.priceMin, this.props.searchInfo.priceMax, genderValue))
  }

  getFiltersData(bestReviews: any, availabilitySeats: any, priceMin: any, priceMax: any, gender: any) {
    return {
      bestReviews: bestReviews,
      availabilitySeats: availabilitySeats,
      priceMin: priceMin,
      priceMax: priceMax,
      gender: gender
    }
  }

  render() {

    console.log('Gender val >>>>', this.props.searchInfo.gender)
    return (

      <SafeAreaView style={{ flex: 1, backgroundColor: 'rgba(255, 255, 255, 0.6)' }}>
        <BlurView
          style={styles.absolute}
          //viewRef={this.state.viewRef}
          blurType="light"
          blurAmount={10}
        />
        <View style={styles.container}>
          <TextGreyBold marginTop={20} fontSize={20} text='Filters' color='white' />
          <View style={styles.checkFiltersContainer}>
            <TouchableOpacity style={styles.imgBtn} onPress={() => this.bestReviews()}>
              <Image resizeMode='contain' source={this.props.searchInfo.bestReviews ? require('../../assets/blueCheck.png') : require('../../assets/blueBoxBlank.png')} style={styles.checkImg} />
            </TouchableOpacity>
            <TextGreyBold marginTop={0} fontSize={16} text='Best reviews' color='white' />
          </View>
          <View style={styles.checkFiltersContainer}>
            <TouchableOpacity style={styles.imgBtn} onPress={() => this.seatsAvailability()}>
              <Image resizeMode='contain' source={this.props.searchInfo.availabilitySeats ? require('../../assets/blueCheck.png') : require('../../assets/blueBoxBlank.png')} style={styles.checkImg} />
            </TouchableOpacity>
            <TextGreyBold marginTop={0} fontSize={16} text='Availability of seats' color='white' />
          </View>
          <TextGreyBold marginTop={30} fontSize={16} text='Price' color='white' textAlign='left' />
          <AddRangeSlider min={0} max={1000} selectedMin={this.props.searchInfo.priceMin} selectedMax={this.props.searchInfo.priceMax} step={100} func={(low: number, high: number) => this.setPrice(low, high)} />
          <TextGreyBold marginTop={20} fontSize={16} text='Gender' color='white' textAlign='left' />
          <View style={styles.genderDOBContainer}>
            <TouchableOpacity onPress={() => this.setGender('0')} style={[styles.genderBtn, (this.props.searchInfo.gender.toString() == '0' ? { backgroundColor: '#487CFD', } : { backgroundColor: '#D3D3D3' })]}>
              <Image
                source={require('../../assets/male.png')}
                style={styles.genderImg}
                resizeMode='contain' />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setGender('1')} style={[styles.genderBtn, (this.props.searchInfo.gender.toString() == '1' ? { backgroundColor: '#487CFD', } : { backgroundColor: '#D3D3D3' })]}>
              <Image
                source={require('../../assets/female.png')}
                style={styles.genderImg}
                resizeMode='contain' />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setGender('2')} style={[styles.genderBtn, styles.otherBtn, (this.props.searchInfo.gender.toString() == '2' ? { backgroundColor: '#487CFD', } : { backgroundColor: '#D3D3D3' })]}>
              <Image
                source={require('../../assets/other.png')}
                style={styles.otherImg}
                resizeMode='contain' />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={() => this.props.removeFilters()} style={styles.crossBtn}>
            <Text style={styles.crossText}>X</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    )
  }
}

const mapStateToProps = (state: any) => ({
  searchInfo: state.saveSearchInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
  saveFilterInfo: (info: string) => dispatch({ type: FILTER_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Filters);


