//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_REGULAR, APP_FONT_BOLD } from '../../constants/fonts';
import { APP_GREY_TEXT_COLOR, APP_GREEN_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#140F2693',
        padding: '10%'
    },
    absolute: {
        position: "absolute",
        top: 0, left: 0, bottom: 0, right: 0,
    },
    checkFiltersContainer: {
        marginTop: 20,
        flexDirection: 'row',
        height: 40,
        alignItems: 'center',
       // backgroundColor: 'green',
    },
    imgBtn: {
        width: 40,
        height: 60,
        //alignItems: 'center',
        justifyContent: 'center',
       // backgroundColor: 'red'
    },
    checkImg: {
        width: 20,
        height: 20
    },
    genderDOBContainer: {
        marginTop: 20,
        flexDirection: 'row',
        //backgroundColor: 'red'
    },
    genderBtn: {
        backgroundColor: '#D3D3D3',
        borderRadius: 5,
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 32
    },
    otherBtn: {
        width: 60,
    },
    genderImg: {
        width: 12,
        height: 18
    },
    otherImg: {
        width: 32,
        height: 18
    },
    crossBtn: {
        backgroundColor: 'white',
        position: "absolute",
        bottom: 32,
        width: 50,
        height: 50,
        borderRadius: 25, 
        justifyContent: 'center',
        alignItems: 'center', 
        alignSelf: 'center'
    },
    crossText: {
        fontSize: 20,
        alignText: 'center',
        fontFamily: APP_FONT_REGULAR,
        color: APP_GREEN_COLOR,
    }
});