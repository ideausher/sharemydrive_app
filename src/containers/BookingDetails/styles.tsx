//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD, APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 16
    },   
    locationContainer:{
        flexDirection: 'row',
        marginTop: 24,
       // backgroundColor:'red',
        height: 90
    }, 
    image: {
        height: 80,
    },
    originDestinationContainer: {
        flex: 1,
        justifyContent: 'space-between',
        marginLeft: 8,
        marginRight: 8,
       // backgroundColor:'orange'
    },
    originDestinationVw: {
        flex: 1,
       // backgroundColor:'yellow'
    },
    dateTimeContainer:{
        flexDirection: 'row',
        alignItems:'center',
       // backgroundColor:'green'
    },
    calIconImg:{
        width: 10,
        height: 10,
        marginRight: 4
    },
    vehicleInfoVw:{
        marginTop: 24,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        //backgroundColor:'red'
    },
    carImg:{
        width: 36,
        height: 12,
    },
    callIconImg:{
        width: 18,
        height: 18,
    },
    callMsgContainer:{
        marginTop: 32,
        marginLeft: 16,
        marginRight: 16,
        flexDirection:'row',
        justifyContent:'space-between',
    },
    callMsgVw:{
        alignItems:'center',
        //backgroundColor:'red'
    }
});