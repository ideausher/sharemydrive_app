import React, { Component } from 'react';
import { View, Text, Image, StatusBar, Platform, ScrollView, NativeModules, Modal, TouchableOpacity, Linking } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn, NoDataFoundView, NoInternetFoundView } from '../../custom/CustomComponents';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import { INITIAL_SEARCH_INFO } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';
import { Popup } from '../../custom/Popup';
import RequestManager from '../../Utils/RequestManager';
import { API_SEND_BOOKING_REQUEST, API_GET_BOOKING_DETAILS, API_PASSENGER_CHANGE_RIDE_STATUS, AVATAR_URL } from '../../constants/apis';
import General from '../../Utils/General';
import Home from '../Home';
import { getDistance, getPreciseDistance } from 'geolib';
import moment from 'moment';
import { Loader } from '../../Utils/Loader';
import Toast from 'react-native-simple-toast';
import { checkAndAddUser } from '../../Firebase/FirestoreHandler';

export interface props {
    navigation: NavigationScreenProp<any, any>
    searchInfo: any,
    saveInitialSearchInfo: any,
    loginInfo: any
}

const { StatusBarManager } = NativeModules;

const GreyTextView = (props: any) => {
    return (<View style={styles.vehicleInfoVw}>
        <TextGrey fontSize={14} marginTop={0} text={props.text} textAlign='left' color={props.color == null ? '#717173' : props.color} />
        <TextGrey fontSize={12} marginTop={0} text={props.value} textAlign='left' color={props.color == null ? '#717173' : props.color} />
    </View>
    );
};

const CallMsgView = (props: any) => {
    return (<TouchableOpacity style={styles.callMsgVw} onPress={() => props.func()}>
        <Image
            source={props.img}
            style={styles.callIconImg}
            resizeMode='contain'
        />
        <TextGrey fontSize={14} marginTop={16} text={props.title} color='#717173' />
    </TouchableOpacity>
    );
};

class BookingDetails extends Component<props, object> {

    state = {
        statusBarHeight: 0,
        showConfirmPopup: false,
        showCancelPopup: false,
        bookingDetails: {} as any,
        isAPICalledOnce: false,
        showLoader: false,
        isInternetError: false,
        isDataNotFoundError: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Booking details</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        this.apiGetBookingDetails()
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    callBtnAction() {

        let num = this.state.bookingDetails.user_details.phone_number
        Linking.openURL(`tel:${num}`)
    }

    messageBtnAction() {

        let chatId = this.props.loginInfo.id.toString() + this.state.bookingDetails.user_details.id.toString()

        // First Add CHat for Sender means Login User
        let receiverDict = {
            firstName: this.state.bookingDetails.user_details.firstname,
            lastName: this.state.bookingDetails.user_details.lastname,
            image: AVATAR_URL + this.state.bookingDetails.user_details.image,
            // email: this.props.loginInfo.email,
            createdOn: Date.now(),
            id: this.state.bookingDetails.user_details.id,
            chatId: chatId
        }

        console.log('receiverDict Sent >>>>>>', receiverDict)

        checkAndAddUser(this.props.loginInfo.id, this.state.bookingDetails.user_details.id, receiverDict).then((data: any) => {
            if (data.exists) {
                receiverDict["chatId"] = data.chatId
                this.props.navigation.navigate('Messages', { chatData: receiverDict })
            } else {
                // Now AddChat for Receiver means Driver

                let senderDict = {
                    firstName: this.props.loginInfo.firstName,
                    lastName: this.props.loginInfo.lastName,
                    image: this.props.loginInfo.image,
                    // email: this.props.loginInfo.email,
                    createdOn: Date.now(),
                    id: this.props.loginInfo.id,
                    chatId: chatId
                }

                console.log('senderDict Sent >>>>>>', senderDict)

                checkAndAddUser(this.state.bookingDetails.user_details.id, this.props.loginInfo.id, senderDict)

                this.props.navigation.navigate('Messages', { chatData: receiverDict })
            }

        })
    }

    cancelBtnACtion() {
        this.setState({ showCancelPopup: true })
    }

    confirmBtnAction() {
        this.setState({ showConfirmPopup: true })
    }

    confirmRideBtnAction() {
        this.setState({ showConfirmPopup: false })

        this.apiSendBookingRequest()
    }

    noThanksBtnAction() {
        this.setState({ showConfirmPopup: false, showCancelPopup: false })
    }

    cancelRideBtnAction() {

        this.setState({ showCancelPopup: false })
        if (this.props.navigation.getParam('afterBooking') == true) {
            this.apiChangeRideSttaus()
        }
        else {
            this.props.saveInitialSearchInfo()
            this.props.navigation.navigate('Home')
        }
    }

    // Get parameters of Change Status of Ride Api (Cancel/Start Ride)
    getChangeRideStatusAPIParams() {

        return {
            "rideid": this.props.navigation.getParam('rideId'),
            "ridestatus": 3  // 0 for start and 3 for Cancel
        }
    }

    // API tp send OTp on added Phone Number for verification
    apiChangeRideSttaus() {

        console.log('apiChangeRideStatus called with params >>>>', this.getChangeRideStatusAPIParams())
        RequestManager.postRequestWithHeaders(API_PASSENGER_CHANGE_RIDE_STATUS, this.getChangeRideStatusAPIParams()).then((response: any) => {
            console.log('REsponse of apiChangeRideSttaus >>>>', response);
            this.setState({ showLoader: false })

            Toast.show('Ride cancelled', Toast.SHORT);

            this.props.navigation.goBack()
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }


    // Get paarmeters for API_SEND_BOOKING_REQUEST API
    private getParams() {
        return {
            rideid: this.props.navigation.getParam('rideId'),
            consume_seats: 1
        }
    }

    async apiSendBookingRequest() {
        console.log('GET apiSendBookingRequest called with params >>>', this.getParams())
        this.setState({ showLoader: false, isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.postRequestWithHeaders(API_SEND_BOOKING_REQUEST, this.getParams())
            .then((response: any) => {
                console.log('REsponse of apiSendBookingRequest API >>>>', response);
                this.props.saveInitialSearchInfo()
                this.setState({ showLoader: false })

                setTimeout(() => {
                    this.props.navigation.navigate('RideBookedSuccess')
                }, 200);
            }).catch((error: any) => {
                General.showErroMsg(this, error)

                if (error.message == 'Your ride is still in Pending Status, You can\'t send again request.') {
                    this.props.navigation.navigate('Home')
                }
            })
    }

    // Get paarmeters for Login API
    private getBooingDetailsAPIParams() {
        return {
            "ride_id": this.props.navigation.getParam('rideId'),
            "previous": 0,
        }
    }

    async apiGetBookingDetails() {
        console.log('GET apiGetBookingDetails called with params >>>', this.getBooingDetailsAPIParams())
        this.setState({ showLoader: true, isAPICalledOnce: true, isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_BOOKING_DETAILS, this.getBooingDetailsAPIParams())
            .then((response: any) => {
                console.log('REsponse of apiGetBookingDetails API >>>>', response);
                weakSelf.setState({ showLoader: false, bookingDetails: response.data[0] })

            }).catch((error: any) => {
                General.showErroMsgInternetNoData(this, error)
            })
    }

    render() {
        console.log("bookingDetails in render>>>>>", this.state.bookingDetails)

        let startTime = ''
        let endTime = ''
        let rideInfo = {} as any
        let startLocation = {} as any
        let endLocation = {} as any
        let vehicleInfo = {} as any
        let pricePerSeat = ''
        let toll = ''
        let discount = ''
        let total = ''
        let maxSeats = 0
        let cosumedSeats = 0

        if (this.state.bookingDetails.id != null && this.state.bookingDetails.id != undefined) {

            let rideInfo = this.state.bookingDetails
            console.log("Ride Info render if>>>>>", rideInfo)

            pricePerSeat = rideInfo.price_per_seat != undefined ? rideInfo.price_per_seat : "0.00"
            toll = rideInfo.total_toll_tax != undefined ? rideInfo.total_toll_tax : "0.00"
            discount = rideInfo.discount_Applied != undefined ? rideInfo.discount_Applied : "0.00"
            total = rideInfo.with_Discount_Payable != undefined ? rideInfo.with_Discount_Payable : "0.00"
            maxSeats = rideInfo.max_no_seats
            cosumedSeats = rideInfo.consume_seat_sum

            startLocation = rideInfo.start_location
            endLocation = rideInfo.end_location
            vehicleInfo = rideInfo.vehicleinformation[0]

            let dateTimeMoment = moment(rideInfo.ride_date_time, 'YYYY-MM-DD HH:mm:ss')
            startTime = dateTimeMoment.format('YYYY-MM-DD, hh:mm A')
            endTime = General.getEndTime(startLocation.latitude, startLocation.longitude, endLocation.latitude, endLocation.longitude, rideInfo.ride_date_time)
        }

        console.log("Ride Info if>>>>>", rideInfo)

        return (
           // <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                startTime != '' ? <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                <View style={styles.container}>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showConfirmPopup}>
                        <Popup img={require('../../assets/cartoon.png')} title='Are you sure you want to confirm the ride?' btnText='Confirm ride' greenBtnText='No, thanks' func={() => this.confirmRideBtnAction()} greenBtnAction={() => this.noThanksBtnAction()} />
                    </Modal>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showCancelPopup}>
                        <Popup img={require('../../assets/cartoon.png')} title='Are you sure you want to cancel the ride?' btnText='Cancel ride' greenBtnText='No, thanks' func={() => this.cancelRideBtnAction()} greenBtnAction={() => this.noThanksBtnAction()} />
                    </Modal>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showLoader}>
                        <Loader />
                    </Modal>
                    <View style={styles.locationContainer}>
                        <Image
                            source={require('../../assets/pickup.png')}
                            style={styles.image}
                            resizeMode='contain'
                        />
                        <View style={styles.originDestinationContainer}>
                            <View style={styles.originDestinationVw}>
                                <TextGreyBold marginTop={0} fontSize={14} text={startLocation.city} textAlign='left' />
                                <View style={styles.dateTimeContainer}>
                                    <Image
                                        source={require('../../assets/calIcon.png')}
                                        style={styles.calIconImg}
                                        resizeMode='contain'
                                    />
                                    <TextGrey fontSize={10} marginTop={0} text={startTime} textAlign='left' color='#717173' />
                                </View>
                            </View>
                            <View style={[styles.originDestinationVw, { justifyContent: 'flex-end' }]}>
                                <TextGreyBold marginTop={0} fontSize={14} text={endLocation.city} textAlign='left' />
                                <View style={styles.dateTimeContainer}>
                                    <Image
                                        source={require('../../assets/calIcon.png')}
                                        style={styles.calIconImg}
                                        resizeMode='contain'
                                    />
                                    <TextGrey fontSize={10} marginTop={0} text={endTime} textAlign='left' color='#717173' />
                                </View>
                            </View>
                        </View>
                    </View>
                    <GreyTextView text='Available seats' value={maxSeats - cosumedSeats} />
                    <View style={styles.vehicleInfoVw}>
                        <TextGreyBold marginTop={0} fontSize={12} text={vehicleInfo.car_model} textAlign='left' />
                        <Image
                            source={require('../../assets/greyCar.png')}
                            style={styles.carImg}
                            resizeMode='contain'
                        />
                    </View>
                    <View style={styles.vehicleInfoVw}>
                        <TextGreyBold marginTop={0} fontSize={12} text="REGO" textAlign='left' />
                        <TextGreyBold marginTop={0} fontSize={12} text={vehicleInfo.rego} textAlign='left' />
                    </View>
                    <View style={styles.vehicleInfoVw}>
                        <TextGreyBold marginTop={0} fontSize={12} text="Manufacturer" textAlign='left' />
                        <TextGreyBold marginTop={0} fontSize={12} text={vehicleInfo.rego} textAlign='left' />
                    </View>
                    <View style={styles.vehicleInfoVw}>
                        <TextGreyBold marginTop={0} fontSize={12} text="Model" textAlign='left' />
                        <TextGreyBold marginTop={0} fontSize={12} text={vehicleInfo.car_model} textAlign='left' />
                    </View>
                    <View style={styles.vehicleInfoVw}>
                        <TextGreyBold marginTop={0} fontSize={12} text="Type" textAlign='left' />
                        <TextGreyBold marginTop={0} fontSize={12} text={vehicleInfo.car_type} textAlign='left' />
                    </View>
                    <View style={styles.vehicleInfoVw}>
                        <TextGreyBold marginTop={0} fontSize={12} text="Color" textAlign='left' />
                        <TextGreyBold marginTop={0} fontSize={12} text={vehicleInfo.car_color} textAlign='left' />
                    </View>
                    <View style={styles.vehicleInfoVw}>
                        <TextGreyBold marginTop={0} fontSize={12} text="Registered year" textAlign='left' />
                        <TextGreyBold marginTop={0} fontSize={12} text={vehicleInfo.register_year} textAlign='left' />
                    </View>
                    <View style={{ borderColor: APP_BORDER_GREY_COLOR, borderBottomWidth: 1, marginTop: 16 }} />
                    <GreyTextView text='Ride fare' value={'$ ' + pricePerSeat} />
                    <GreyTextView text='Toll tax' value={'$ ' + toll} />
                    <GreyTextView text='Invite code discount' value={'$ ' + discount} />
                    <GreyTextView text='Payable amount' value={'$ ' + total} color='#2785FE' />
                    <View style={styles.callMsgContainer}>
                        <CallMsgView title='Call' img={require('../../assets/phone.png')} func={() => this.callBtnAction()} />
                        <CallMsgView title='Message' img={require('../../assets/message.png')} func={() => this.messageBtnAction()} />
                        <CallMsgView title='Cancel' img={require('../../assets/cancel.png')} func={() => this.cancelBtnACtion()} />
                    </View>

                    {this.props.navigation.getParam('afterBooking') == false ? <CommonBtn title='Confirm' func={() => this.confirmBtnAction()} marginTop={40} width='90%' marginBottom={0} /> : null}
                </View>
                </ScrollView> :
                <View style={{ flex: 1 }}>{
                    (this.state.showLoader || this.state.isAPICalledOnce == false) ?
                        null :
                        (this.state.isInternetError ?
                            <NoInternetFoundView func={() => this.apiGetBookingDetails()} /> : <NoDataFoundView func={() => this.apiGetBookingDetails()} message='' />)
                }
                </View>
          //  </ScrollView>
        )
    }
}

const mapStateToProps = (state: any) => ({
    searchInfo: state.saveSearchInfo,
    loginInfo: state.saveLoginInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
    saveInitialSearchInfo: (info: any) => dispatch({ type: INITIAL_SEARCH_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(BookingDetails);