//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { WINDOW_WIDTH } from '../../constants';
import { APP_NAVY_BLUE_COLOR, APP_GREY_TEXT_COLOR } from '../../constants/colors';
import { APP_FONT_REGULAR, APP_FONT_BOLD } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    scrollContainer: {
        alignItems: 'center',
    },
    greyBoldText: {
        fontSize: 25,
        fontFamily: APP_FONT_BOLD,
        color: '#717173',
        textAlign: 'center',
        marginTop: 30,
    },
    greyText: {
        fontSize: 14,
        fontFamily: APP_FONT_REGULAR,
        color: '#707070',
        textAlign: 'left',
        width: '90%',
        marginTop: 60
    },
    codeInputContainer: {
        marginTop: 50,
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
      inputContainer: {
        alignItems: 'center',
        height: 70,
        width: '18%',
        padding: 10,
        borderBottomColor: '#D8D8D8',
        borderBottomWidth: 1,
        textAlign: 'center',
        fontSize: 27,
        fontFamily: APP_FONT_REGULAR,
        color: '#4E4E50',
    },
    didntReceiveCodeTxt: {
        marginTop: 50
    }
});