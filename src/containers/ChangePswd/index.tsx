import React, { Component } from 'react';
import { View, Text, TouchableOpacity, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules, TextInput, Keyboard, Modal, Alert, ScrollView, StatusBar } from 'react-native';
import { } from '../../constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { CommonBtn, BlueTextBtn } from '../../custom/CustomButton';
import { Loader } from '../../Utils/Loader';
// import RequestManager from '../../Utils/RequestManager';
// import { API_SIGNUP, API_SENDOTP, API_CHECK_OTP, API_CHECK_PHONE_OTP, API_FORGOT_PSWD } from '../../Utils/APIs'
// import AsyncStorage from '@react-native-community/async-storage';
// import { LOGIN } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { VerificationFor } from '../../Utils/Enums';
// import LocalDataManager from '../../Utils/LocalDataManager';
// import General from '../../Utils/General';
// import { BackHeaderBtn } from '../../Custom/CustomComponents';
import { NavigationWithBack } from '../../Utils/NavigationOptions';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { lessThan } from 'react-native-reanimated';
import { ChangePswdScreenType } from '../../Utils/Enums';
import { TextInputWithImage } from '../../custom/CustomTextInput';
import Toast from 'react-native-simple-toast';
import RequestManager from '../../Utils/RequestManager';
import { API_UPDATE_FORGOT_PASSWORD, API_UPDATE_PASSWORD } from '../../constants/apis';
import General from '../../Utils/General';

export interface props {
    navigation: NavigationScreenProp<any, any> 
};

const { StatusBarManager } = NativeModules;

const oldPswdIndex = 111
const newPswdIndex = 222
const cnfrmNewPswdIndex = 333

export default class ChangePswd extends Component<props, object> {

    state = {
        oldPswd: '',
        pswd: '',
        cnfrmPswd: '',
        statusBarHeight: 0,
        showLoader: false,
        isForgotPassword: false,
        showChangePSwdSuccessPopUp: false,
        changePswdScreenType: ChangePswdScreenType.updatePswd, 
        secureOldPswd: true,
        secureNewPswd: true,
        secureCnfrmNewPswd: true,
    };

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Change Password</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        console.log('Props >>>>', this.props)
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        var param = this.props.navigation.getParam('params');
        console.log('PArams >>>>>', param);
        if(param != null)
        {
            this.setState({ changePswdScreenType: param['changePswdScreenType'] })
        }
        else
        {
            this.setState({ changePswdScreenType: ChangePswdScreenType.changePswd })
        }
        
    }

    backBtnHandler() {
        this.props.navigation.navigate('Home');
    }

    changePswdBtnAction() {
        Keyboard.dismiss()
        if (this.isValidateDetail()) {
            if (this.state.changePswdScreenType == ChangePswdScreenType.updatePswd) {
                this.setState({ showLoader: true })
                this.makeForgotPasswordResetRequest()

            } else if (this.state.changePswdScreenType == ChangePswdScreenType.changePswd) {
                this.setState({ showLoader: true })
                this.makePasswordResetRequest()
            }
        }
    }

    isValidateDetail() {
        if (this.state.changePswdScreenType == ChangePswdScreenType.changePswd && this.state.oldPswd.length <= 0) {
            Toast.show('Please enter Old Password', Toast.SHORT);
            return false;
        }
        else if (this.state.pswd.length <= 0) {
            Toast.show('Please enter Password', Toast.SHORT);
            return false;
        }
        else if (this.state.pswd.length < 6) {
            Toast.show('Please enter Password of atleast 6 digits', Toast.SHORT);
            return false;
        }
        else if (this.state.cnfrmPswd.length <= 0) {
            Toast.show('Please enter confirm Password', Toast.SHORT);
            return false;
        }
        else if (this.state.cnfrmPswd != this.state.pswd) {
            Toast.show('Confirm password doesn\'t matched', Toast.SHORT);
            return false;
        }

        return true;
    }

    getForgotPasswordResetParams() {
        var param = this.props.navigation.getParam('params');
        const id = param['id'];
        return {
            password: this.state.pswd,
            user_id: id,
            otp: param['otp']
        }
    }

    getResetPasswordParams() {

        return {
            password: this.state.pswd,
            confirm_password: this.state.cnfrmPswd,
            old_password: this.state.oldPswd
        }
    }

    makeForgotPasswordResetRequest() {
        console.log('makeForgotPasswordResetRequest Func called with params >>>>>', this.getForgotPasswordResetParams());
        RequestManager.putRequest(API_UPDATE_FORGOT_PASSWORD, this.getForgotPasswordResetParams()).then((response: any) => {
            console.log('REsponse of Send OTP API >>>>', response);

            this.setState({ showLoader: false });

            setTimeout(() => {
                Alert.alert(response.message)
                this.props.navigation.navigate('Login')
            }, 200);

        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    makePasswordResetRequest() {
        RequestManager.putRequestWithHeaders(API_UPDATE_PASSWORD, this.getResetPasswordParams()).then((response: any) => {
            console.log('Response of makePasswordResetRequest >>>>', response);
            this.setState({ showLoader: false })

            setTimeout(() => {
                this.setState({ oldPswd: '', pswd: '', cnfrmPswd: '', secureOldPswd: true, secureNewPswd: true, secureCnfrmNewPswd: true})
                Toast.show(response.message, Toast.SHORT);
                this.props.navigation.navigate('Home');
            }, 800);

        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    showHidePswd(pswdFldIndex: number) {
        console.log('pswd Index>>>', pswdFldIndex)

        if(pswdFldIndex == oldPswdIndex)
        {
            this.setState({secureOldPswd: !this.state.secureOldPswd})
        }
        else if(pswdFldIndex == newPswdIndex)
        {
            this.setState({secureNewPswd: !this.state.secureNewPswd})
        }
        else if(pswdFldIndex == cnfrmNewPswdIndex)
        {
            this.setState({secureCnfrmNewPswd: !this.state.secureCnfrmNewPswd})
        }
    }

    render() {

        let title = 'Create new password'
        if (this.state.changePswdScreenType == ChangePswdScreenType.changePswd) {
            title = 'Change your password'
        }
        console.log('Title >>>', title);

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                        <View style={styles.scrollContainer}>
                            <Text style={styles.greyBoldText}>{title}</Text>
                            <Text style={styles.greyText}>Please enter a new password</Text>
                            {this.state.changePswdScreenType == ChangePswdScreenType.changePswd ? <TextInputWithImage placeholder='Enter old password' onChange={(text: any) => this.setState({ oldPswd: text })} value={this.state.oldPswd} icon={require('../../assets/hidePswd.png')} rightImage={true} autoCapitalize='none' secureTextEntry={this.state.secureOldPswd} func={() => this.showHidePswd(oldPswdIndex)} marginTop={50}/> : null}
                            <TextInputWithImage placeholder='Enter new password' onChange={(text: any) => this.setState({ pswd: text })} value={this.state.pswd} icon={require('../../assets/hidePswd.png')} rightImage={true} autoCapitalize='none' secureTextEntry={this.state.secureNewPswd} func={() => this.showHidePswd(newPswdIndex)} />
                            <TextInputWithImage placeholder='Re-enter new password' onChange={(text: any) => this.setState({ cnfrmPswd: text })} value={this.state.cnfrmPswd} icon={require('../../assets/hidePswd.png')} rightImage={true} autoCapitalize='none' secureTextEntry={this.state.secureCnfrmNewPswd} func={() => this.showHidePswd(cnfrmNewPswdIndex)} />

                            <CommonBtn title='Change password' func={() => this.changePswdBtnAction()} marginTop={70} />
                        </View>
                    </ScrollView>
                </View>
            </KeyboardAvoidingView>
        );
    }
}
