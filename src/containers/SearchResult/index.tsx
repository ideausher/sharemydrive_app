import React, { Component } from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableOpacity, Image, ScrollView, Modal } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { TextInputWithBtn } from '../../custom/CustomTextInput';
import LastRideCell from '../../custom cells/LastRideCell';
//@ts-ignore
import { connect } from 'react-redux';
import { CommonBtn } from '../../custom/CustomButton';
import { TextGrey, TextGreyBold } from '../../custom/CustomText';
import SearchResultCell from '../../custom cells/SearchResultCell';
import ChooseLastRideCell from '../../custom cells/ChooseLastRideCell';
import Filters from '../Filters';
import { API_GET_RIDES, API_GET_PUBLISHED_RIDES } from '../../constants/apis';
import RequestManager from '../../Utils/RequestManager';
import moment from 'moment';
import { internetConnectionError } from '../../constants/messages';
import Toast from 'react-native-simple-toast';
import General from '../../Utils/General';
import { FILTER_INFO } from '../../Redux/Constants';
import { Loader } from '../../Utils/Loader';
import { NoInternetFoundView } from '../../custom/CustomComponents';
import { not } from 'react-native-reanimated';

export interface props {
    navigation: NavigationScreenProp<any, any>
    searchInfo: any,
    saveFilterInfo: any
};

class SearchResult extends Component<props, object> {
    state = {
        searchResults: [] as any[],
        lastRidesArr: [] as any[],
        showFilters: false,
        page: 0,
        loadMore: false,
        isCalled: false,
        prevPage: -1,
        showLoader: false,
        isApiCalled: false,
        updateState: true,
        isInternetError: false,
        isDataNotFoundError: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: null,
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerRight: (
                <TouchableOpacity style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center' }} activeOpacity={0.9} onPress={navigation.getParam('filtersHandler')}>
                    <Image style={{ marginRight: 10, height: 20, width: 20, resizeMode: 'contain' }} source={require('../../assets/filterIcon.png')} />
                </TouchableOpacity>
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler(), 'filtersHandler': () => this.filtersHandler() });

        this.apiGetRides()
        /// this.apiGetLastAcceptedRides()
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    filtersHandler() {
        this.setState({ showFilters: true })
    }

    componentWillReceiveProps(newProps: any) {

        console.log('Old Props: >>', this.props.searchInfo)

        console.log('new Props: ' + JSON.stringify(newProps))


        setTimeout(() => {

            console.log('Old Props: >>', this.props.searchInfo)
        }, 200)

    }

    private refreshData() {
        this.apiGetRides()
    }

    // Get paarmeters for Login API
    private getLastAcceptedRidesParams() {
        return {
            type: "2",
            fetch: 'a',
            page: this.state.page,
            limit: "5",
        }
    }

    async apiGetLastAcceptedRides() {
        console.log('GET apiGetLastAcceptedRides called with page index >>>', this.state.page)
        this.setState({ showLoader: true })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_PUBLISHED_RIDES, this.getLastAcceptedRidesParams())
            .then((response: any) => {
                console.log('REsponse of apiGetLastAcceptedRides API >>>>', response);
                weakSelf.setState({ showLoader: false })
                let ridesArrData: any[] = []

                ridesArrData = this.state.lastRidesArr.concat(response.data)

                setTimeout(() => {

                    console.log('Prev and next page >>>', this.state.prevPage, this.state.page)

                    weakSelf.setState({ showLoader: false, lastRidesArr: ridesArrData, loadMore: false, prevPage: this.state.page })

                    setTimeout(() => {

                        console.log('Again Prev and next page >>>', this.state.prevPage, this.state.page)
                        weakSelf.setState({ isCalled: false })
                    }, 200)

                    if (response.data.length > 0) {
                        let nextPage = this.state.page + 1
                        weakSelf.setState({ page: nextPage })
                    }
                }, 200)
            }).catch((error: any) => {
                this.setState({ showLoader: false, loadMore: false, isCalled: false })
                console.log('Error >>>>', error)
                if (error == internetConnectionError) {

                }
                else {
                    if (error.message == null || error.message == undefined) {
                        Toast.show((error), Toast.SHORT);
                    }
                    else if (error.message != "No Record Found.") {
                        Toast.show((error.message), Toast.SHORT);
                    }
                }
            })
    }

    // Get paarmeters for Login API
    private getParams() {
        const dict: any = {
            start_latitude: this.props.searchInfo.origin.lat,
            start_longitude: this.props.searchInfo.origin.long,
            end_latitude: this.props.searchInfo.destination.lat,
            end_longitude: this.props.searchInfo.destination.long,
            date: this.props.searchInfo.date + ' ' + moment(this.props.searchInfo.time, "hh:mm a").format('HH:mm:ss'),
            page: this.state.page,
            limit: "10",
            distance: this.props.searchInfo.isNearByEnabled ? '1' : '5',
            min_price: this.props.searchInfo.priceMin,
            max_price: this.props.searchInfo.priceMax,
        }

        if (this.props.searchInfo.bestReviews) {
            dict['review_ratings'] = '3'
        }

        if (this.props.searchInfo.availabilitySeats) {
            dict['available_seats'] = '1'
        }

        if (this.props.searchInfo.gender != '') {
            dict['gender'] = this.props.searchInfo.gender
        }

        return dict
    }

    private filterSeacrhResults(searchResultsData:any) {

        let alreadyTravelledWithDriversRides = []
        let notTravelledWithDriversRides = []

        for (let searchResult of searchResultsData) {

            if (searchResult.is_travelled_already_with == 0) {
                notTravelledWithDriversRides.push(searchResult)
            }
            else
            {
                alreadyTravelledWithDriversRides.push(searchResult)
            }
        }

        console.log('alreadyTravelledWithDriversRides >>>>>', alreadyTravelledWithDriversRides)

        this.setState({lastRidesArr: alreadyTravelledWithDriversRides, searchResults: notTravelledWithDriversRides})
    }

    async apiGetRides() {
        console.log('GET apiGetRides called with page index and params >>>', this.state.page, this.getParams())
        this.setState({ showLoader: true, isApiCalled: true, isDataNotFoundError: false, isInternetError: false })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_RIDES, this.getParams())
            .then((response: any) => {
                console.log('REsponse of apiGetRides API >>>>', response);
                let searchResultsData: any[] = this.state.searchResults

                searchResultsData = searchResultsData.concat(response.data)

                setTimeout(() => {

                    weakSelf.setState({ showLoader: false, loadMore: false, prevPage: this.state.page, isApiCalled: true })

                    setTimeout(() => {
                        weakSelf.setState({ isCalled: false })
                    }, 200)

                    if (response.data.length > 0) {
                        let nextPage = this.state.page + 1
                        weakSelf.setState({ page: nextPage })

                        this.filterSeacrhResults(searchResultsData)
                    }
                    else {
                        if (this.state.searchResults.length == 0) {
                            this.setState({ isDataNotFoundError: true })
                        }
                    }
                }, 200)
            }).catch((error: any) => {
                General.showErroMsgInternetNoData(this, error)
            })
    }

    lastRidecellSelected(index: any) {

        console.log('Cell Selected Func called >>>', this.state.lastRidesArr[index])
        this.props.navigation.navigate('ViewProfile', { rideInfo: this.state.lastRidesArr[index], showBookRideBtn: true })

    }

    cellSelected(index: any) {
        console.log('Cell Selected Func called >>>', this.state.searchResults[index])
        this.props.navigation.navigate('ViewProfile', { rideInfo: this.state.searchResults[index], showBookRideBtn: true })
    }

    removeFilters() {
        this.setState({ showFilters: false, isApiCalled: false, prevPage: -1, page: 0, searchResults: [] })
        setTimeout(() => {
            this.apiGetRides()
        }, 200)
    }

    updateState(filtersData: any) {
        console.log('updateState Func called')
        this.props.saveFilterInfo(filtersData)
        this.setState({ updateState: true })
    }

    //Will call when reached last cell
    private loadMoreData = () => {

        console.log('Func Clledv >>>', this.state.prevPage, this.state.page)
        if (this.state.prevPage == this.state.page) {
            console.log('Func  return Clled')
            return
        }
        this.setState({ loadMore: true, isCalled: true })
        console.log('Func Clled2')
        setTimeout(() => {
            console.log('Func Clled3')
            this.apiGetLastAcceptedRides()
        }, 200)
    }

    render() {

        console.log('searchInfo >>>>', this.props.searchInfo)
        let origin = ''
        let destination = ''

        if (this.props.searchInfo.origin != '') {
            origin = this.props.searchInfo.origin.address
        }

        if (this.props.searchInfo.destination != '') {
            destination = this.props.searchInfo.destination.address
        }

        console.log('Search results >>>>', this.state.searchResults)
        console.log('Last rides Array >>>>', this.state.lastRidesArr)
        console.log('Last rides Array Length>>>>', this.state.lastRidesArr.length)
        console.log('is API CALLED >>>>', this.state.isApiCalled)

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showFilters}>
                    <Filters removeFilters={() => this.removeFilters()} filtersData={this.props.searchInfo} updateState={(data: any) => this.updateState(data)} />
                </Modal>
                <View style={styles.container}>
                    <TextInputWithBtn disabled={true} placeholder='Origin' value={origin} icon={require('../../assets/originIcon.png')} backgroundColor='#F6F6F6' func={() => { }} marginTop={40} />
                    <TextInputWithBtn disabled={true} placeholder='Destination' value={destination} icon={require('../../assets/destinationIcon.png')} backgroundColor='#F6F6F6' func={() => { }} />
                    {(this.state.searchResults.length > 0 || this.state.lastRidesArr.length > 0) ?
                        <View style={styles.resultsVw}>
                            {this.state.lastRidesArr.length > 0 ? <TextGrey fontSize={16} text='Choose from your last rides' textAlign='left' width='90%' /> : null}
                            <ScrollView>
                                <FlatList
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                    style={{ marginTop: 24, width: '100%', marginBottom: 8 }}
                                    data={this.state.lastRidesArr}
                                    keyExtractor={(item: any, index: any) => index.toString()}
                                    renderItem={({ item, index }: any) => <ChooseLastRideCell
                                        onClickEvent={(index: any) => this.lastRidecellSelected(index)}
                                        item={item} index={index} />}
                                    onEndReachedThreshold={0.5}
                                    onScroll={({ nativeEvent }) => {
                                        if (this.state.isCalled == false && General.isCloseToBottom(nativeEvent)) {
                                            this.loadMoreData()
                                        }
                                    }}
                                />
                                <FlatList
                                    showsVerticalScrollIndicator={false}
                                    style={{ marginTop: 24, width: '100%', marginBottom: 8 }}
                                    data={this.state.searchResults}
                                    keyExtractor={(item: any, index: any) => index.toString()}
                                    renderItem={({ item, index }: any) => <SearchResultCell
                                        onClickEvent={(index: any) => this.cellSelected(index)}
                                        item={item} index={index} />}
                                />
                            </ScrollView>
                        </View> :
                        (this.state.isApiCalled ? (this.state.isInternetError ? <NoInternetFoundView func={() => this.refreshData()} /> : (this.state.isDataNotFoundError ? <View style={styles.noRideContainer}>
                            <TextGreyBold fontSize={14} text='No ride available' color='rgba(75, 75, 77, 0.36)' />
                            <Image resizeMode='contain' source={require('../../assets/noRide.png')} style={styles.noRideImg} />
                        </View> : null)) : null)}
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    searchInfo: state.saveSearchInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
    saveFilterInfo: (info: string) => dispatch({ type: FILTER_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchResult);