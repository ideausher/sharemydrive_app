//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_REGULAR, APP_FONT_BOLD } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    noRideContainer: {
        marginTop: 60
    },
    noRideImg: {
        width: 135,
        height: 125,
        marginTop: 16
    },
    resultsVw: {
        flex: 1,
        width: '90%',
        marginTop: 16,
    }
});