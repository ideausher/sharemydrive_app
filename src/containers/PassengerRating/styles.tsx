//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        padding: 20
    },
    crossBtn:
    {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginRight: 16,
        marginTop: 16,
    },
    crossImg: {
        height: 16,
        width: 16,
        resizeMode: 'contain',
    },
    profielImg: {
        marginTop: 16,
        height: 70,
    },
    optionsVw: {
        width: '100%',
        //backgroundColor: 'red',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 8,
        marginBottom: 8,
    },
    list: {
        marginTop: 24,
        marginBottom: 46,
        width: '100%',
        //backgroundColor: 'red'
    },
    bottomBtnsVw: {
        width: '100%',
        justifyContent:'space-between',
        position: 'absolute',
        flexDirection: 'row',
        bottom: 16,
        //backgroundColor:'pink'
    }
});