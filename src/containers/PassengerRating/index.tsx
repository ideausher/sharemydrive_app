import React, { Component } from 'react';
import { View, Text, SafeAreaView, FlatList } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import { BorderBtn, GreenTextBtn } from '../../custom/CustomButton';
import { TextInputMultiLine } from '../../custom/CustomTextInput';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import PassengerRatingCell from '../../custom cells/PassengerRatingCell';
import { APP_SKY_BLUE_COLOR } from '../../constants/colors';
import { API_REVIEW, AVATAR_URL } from '../../constants/apis';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class PassengerRating extends Component<props, object> {

    state = {
        passengers: [] as any[]
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: null,
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });
    }

    backBtnHandler() {
        this.props.navigation.goBack()
    }

    optionsBtnAction(index: any) {

    }

    rateLaterBtnAction() {
        this.props.navigation.navigate('Home')
    }

    submitBtnAction() {
        this.setState({ showLoader: true })
        this.callReviewAPI()
    }

    // paarmeters for API
    private getParams() {

        let rideInfo = this.props.navigation.getParam('rideInfo')
        let ratings = []

        for (let i = 0; i < rideInfo.ride_accepted.length; i++) {

            let passengerInfo = rideInfo.ride_accepted[i]
            let user_idOuterArr = passengerInfo.user_id


            let isFound = false
            for (let j = 0; j < this.state.passengers.length; j++) {

                let passengerRatingInfo = this.state.passengers[j]
                let user_idInnerArr = passengerRatingInfo.user_id

                if (user_idOuterArr == user_idInnerArr) {

                    isFound = true
                    let rating = {
                        "ride_id": passengerInfo.ride_id,
                        "rating": passengerRatingInfo.rating,
                        "rating_select": '',
                        "info": passengerRatingInfo.review,
                        "user_id_to": user_idOuterArr,
                    }

                    ratings.push(rating)
                    break
                }
            }

            if(!isFound)
            {
                let rating = {
                    "ride_id": passengerInfo.ride_id,
                    "rating": 3,
                    "rating_select": '',
                    "info": '',
                    "user_id_to": user_idOuterArr,
                }

                ratings.push(rating)
            }
        }

        console.log('ratings Arr >>>>', ratings)

        var params: any = {
            "type": "2",
            "ratings":ratings 
        }

        return params
    }

    // Call API to update User Data
    private callReviewAPI() {
        console.log('callReviewAPI API called with params >>>', this.getParams())
        let weakSelf = this

        RequestManager.postRequestWithHeaders(API_REVIEW, this.getParams()).then((response: any) => {
            this.setState({ showLoader: false })
            General.showMsgWithDelay(response.message)
            this.props.navigation.navigate('Home')
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    private ratingChanged(rating: any, index: any) {

        console.log('Rating Changed func called')

        let rideInfo = this.props.navigation.getParam('rideInfo')

        let passengerInfo = rideInfo.ride_accepted[index]
        let user_id = passengerInfo.user_id

        let passengers = this.state.passengers
        let isFound = false
        for (let i = 0; i < passengers.length; i++) {

            let passengerRatingInfo = passengers[i]
            let user_idInnerArr = passengerRatingInfo.user_id

            if (user_id == user_idInnerArr) {
                passengerRatingInfo.rating = rating
                passengers[i] = passengerRatingInfo
                isFound = true
                break
            }
        }

        console.log('is Found >', isFound)
        if (isFound == false) {
            passengers.push({
                "rating": rating,
                "review": '',
                "user_id": user_id
            })
        }

        console.log('Passengers Arr after Change >', passengers)
        this.setState({ passengers: passengers })
    }

    private reviewChanged(review: any, index: any) {

        console.log('Rating Changed func called')

        let rideInfo = this.props.navigation.getParam('rideInfo')

        let passengerInfo = rideInfo.ride_accepted[index]
        let user_id = passengerInfo.user_id

        let passengers = this.state.passengers
        let isFound = false
        for (let i = 0; i < passengers.length; i++) {

            let passengerRatingInfo = passengers[i]
            let user_idInnerArr = passengerRatingInfo.user_id

            if (user_id == user_idInnerArr) {
                passengerRatingInfo.review = review
                passengers[i] = passengerRatingInfo
                isFound = true
                break
            }
        }

        console.log('is Found >', isFound)
        if (isFound == false) {
            passengers.push({
                "rating": '',
                "review": review,
                "user_id": user_id
            })
        }

        console.log('Passengers Arr after Change >', passengers)
        this.setState({ passengers: passengers })
    }

    render() {

        let rideInfo = this.props.navigation.getParam('rideInfo')

        console.log('RIDE INFO >>>>>>', rideInfo)

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.container}>
                    <TextGreyBold marginTop={16} fontSize={17} text='How was your ride?' width='70%' />
                    <TextGrey marginTop={16} marginLeft={20} marginRight={20} fontSize={12} text='Please provide your valuable feedback to us as it will helps us to improve the experience' textAlign='center' color='#4E4E50' />
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        style={styles.list}
                        data={rideInfo.ride_accepted}
                        keyExtractor={(item: any, index: any) => index.toString()}
                        renderItem={({ item, index }: any) => <PassengerRatingCell item={item} index={index} ratingChanged={(rating: any, index: any) => this.ratingChanged(rating, index)} reviewChanged={(review: any, index: any) => this.reviewChanged(review, index)} />} />
                    <View style={styles.bottomBtnsVw}>
                        <GreenTextBtn title='Rate later' func={() => this.rateLaterBtnAction()} fontSize={16} marginTop={0} />
                        <GreenTextBtn title='Submit' func={() => this.submitBtnAction()} fontSize={16} marginTop={0} />
                    </View>
                </View>
            </SafeAreaView >
        );
    }
}