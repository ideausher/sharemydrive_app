import React, { Component } from 'react';
import { View, Text, SafeAreaView, Alert, Modal, FlatList } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { BackHeaderBtn, NoDataFoundView, NoInternetFoundView } from '../../custom/CustomComponents';
import { Loader } from '../../Utils/Loader';
import PassengerCell from '../../custom cells/PassengerCell';
import RequestManager from '../../Utils/RequestManager';
import { API_GET_RIDE_BY_ID, API_GET_PASSENGERS, AVATAR_URL } from '../../constants/apis';
import General from '../../Utils/General';
import { checkAndAddUser } from '../../Firebase/FirestoreHandler';
//@ts-ignore
import { connect } from 'react-redux';

export interface props {
    navigation: NavigationScreenProp<any, any>
    loginInfo: any
};

class Passengers extends Component<props, object> {

    state = {
        passengers: [] as any[],
        showLoader: false,
        isInternetError: false,
        isDataNotFoundError: false,
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Your Passengers</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        this.apiGetPassengers()
    }

    backBtnHandler() {
        this.props.navigation.goBack()
    }

    cellSelected(index: any) {

        let passengerInfo = this.state.passengers[index]

        let chatId = this.props.loginInfo.id.toString() + passengerInfo.id.toString()

        // First Add CHat for Sender means Login User
        let receiverDict = {
            firstName: passengerInfo.firstname,
            lastName: passengerInfo.lastname,
            image: AVATAR_URL + passengerInfo.image,
            // email: this.props.loginInfo.email,
            createdOn: Date.now(),
            id: passengerInfo.id,
            chatId: chatId
        }

        console.log('receiverDict Sent >>>>>>', receiverDict)

        checkAndAddUser(this.props.loginInfo.id, passengerInfo.id, receiverDict).then((data: any) => {
            if (data.exists) {
                receiverDict["chatId"] = data.chatId
                this.props.navigation.navigate('Messages', { chatData: receiverDict })
            } else {
                // Now AddChat for Receiver means Driver

                let senderDict = {
                    firstName: this.props.loginInfo.firstName,
                    lastName: this.props.loginInfo.lastName,
                    image: this.props.loginInfo.image,
                    // email: this.props.loginInfo.email,
                    createdOn: Date.now(),
                    id: this.props.loginInfo.id,
                    chatId: chatId
                }

                console.log('senderDict Sent >>>>>>', senderDict)

                checkAndAddUser(passengerInfo.id, this.props.loginInfo.id, senderDict)

                this.props.navigation.navigate('Messages', { chatData: receiverDict })
            }
        })
    }

    private refreshData() {
        this.apiGetPassengers()
    }

    // Get paarmeters for Login API
    private getParams() {
        return {
            ride_id: this.props.navigation.getParam('rideId')
        }
    }

    async apiGetPassengers() {
        console.log('apiGetPassengers called with params >>>', this.getParams())
        this.setState({ showLoader: true, isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_PASSENGERS, this.getParams())
            .then((response: any) => {
                console.log('REsponse of apiGetPassengers API >>>>', response);
                weakSelf.setState({ showLoader: false, passengers: response.data })

                if (response.data.length == 0) {
                    weakSelf.setState({ isInternetError: false, isDataNotFoundError: true })
                }
            }).catch((error: any) => {
                General.showErroMsgInternetNoData(this, error)
            })
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    {this.state.passengers.length > 0 ? <FlatList
                        showsVerticalScrollIndicator={false}
                        style={{ marginTop: 24, width: '100%', marginBottom: 8 }}
                        data={this.state.passengers}
                        keyExtractor={(item: any, index: any) => index.toString()}
                        renderItem={({ item, index }: any) => <PassengerCell
                            onClickEvent={(index: any) => this.cellSelected(index)}
                            item={item} index={index} />}
                    /> : (this.state.showLoader ? null :
                        (this.state.isDataNotFoundError ?
                            <NoDataFoundView func={() => this.refreshData()} message='You have no pssengers' /> :
                            (this.state.isInternetError ?
                                <NoInternetFoundView func={() => this.refreshData()} /> : null)))}
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    loginInfo: state.saveLoginInfo,
});

export default connect(mapStateToProps)(Passengers);