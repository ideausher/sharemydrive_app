//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_REGULAR, APP_FONT_BOLD } from '../../constants/fonts';
import { APP_GREY_TEXT_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: 'green',
        padding: 16
    },
    greyTxt: {
        flex: 1,
        fontSize: 20,
        fontFamily: APP_FONT_BOLD,
        color: '#A2A2A2',
        textAlign: 'center',
    },
    imgVw: {
        margin: 16,
        overflow: 'hidden',
        height: 60,
        width: 60,
        borderRadius: 40,
        //backgroundColor: 'green',
        borderColor: 'black'
    },
    img: {
        height: 60,
        width: 60,
    },
    revealOptionImage: {
        marginLeft: 16,
        height: 14,
        width: 14,
        resizeMode: 'contain',
    },
    revealOptionText: {
        marginLeft: 16,
        fontWeight: '500',
        color: APP_GREY_TEXT_COLOR,
        fontFamily: APP_FONT_REGULAR,
        // backgroundColor:'red',
    },
    borderLine: {
        backgroundColor: '#E8E6F1',
        height: 1,
        marginLeft: 46,
        flex: 1
    },
    logoutBtn:{
        marginTop: 50,
        alignSelf: 'center'
    },
    logoutImg:{
        width: 80,
        height: 24,
    }
});