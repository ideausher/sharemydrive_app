
import { SafeAreaView, ScrollView, View, Text, Image, TouchableOpacity, Modal, Alert } from 'react-native';
import React, { Component } from 'react';
import { NavigationScreenProp } from "react-navigation";
import { DrawerItems } from 'react-navigation-drawer';
import { DrawerNavigatorItems, DrawerView } from 'react-navigation-drawer';
//@ts-ignore
import { connect } from 'react-redux';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { styles } from './styles';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { APP_GREY_TEXT_COLOR } from '../../constants/colors';
import { Popup } from '../../custom/Popup';
import RequestManager from '../../Utils/RequestManager';
import { API_LOGOUT } from '../../constants/apis';
import LocalDataManager from '../../Utils/LocalDataManager';
import General from '../../Utils/General';
import { internetConnectionError } from '../../constants/messages';
import { INITIAL_PICK_UP_INFO, INITIAL_SEARCH_INFO } from '../../Redux/Constants';

export interface Props {
  navigation: NavigationScreenProp<any, any>
  name: '',
  avatarSource: '',
  saveInitialPickupInfo: any,
  saveInitialSearchInfo: any
};

class Drawer extends Component<Props> {

  state = {
    showConfirmLogoutPopup: false,
    selectedItem: ''
  }

  constructor(props) {
    super(props);
  }

  backBtnHandler() {
    console.log('Back Button Pressed ')
    this.props.navigation.navigate('Home')
    this.props.navigation.closeDrawer()
  }

  logout() {
    this.props.navigation.closeDrawer()
    this.setState({ showConfirmLogoutPopup: true })
  }

  // myProfileAction() {
  //   this.props.navigation.closeDrawer()

  //   setTimeout(() => {
  //     this.props.navigation.navigate('Profile')
  //   }, 50);
  // }

  revealOptionSelected(item: any) {

    this.setState({selectedItem: item.routeName})
    this.props.navigation.closeDrawer()

    // console.log('This props >>>', this.props)
     console.log('Item in revealOptionSelected  >>>', item)

    setTimeout(() => {
      if (item.routeName == 'History') {
        this.props.navigation.navigate('History')
      }
      else if (item.routeName == 'TermsAndPP') {
        this.props.navigation.navigate('TermsAndPP')
      }
      else if (item.routeName == 'InviteFriends') {
        this.props.navigation.navigate('InviteFriends')
      }
      else if (item.routeName == 'ChangePswd') {
        this.props.navigation.navigate('ChangePswd')
      }
      else if (item.routeName == 'HelpSupport') {
        this.props.navigation.navigate('HelpSupport')
      }
      else if (item.routeName == 'Signout') {
        this.props.navigation.navigate('Signout')
      }
    }, 200);

    setTimeout(() => {
      this.setState({selectedItem: ''})
    }, 1000);
  }

  confirmLogout() {
    this.setState({ showLoader: true, showConfirmLogoutPopup: false })
    let weakSelf = this
    RequestManager.getRequest(API_LOGOUT, {}).then((response: any) => {
      console.log('REsponse of API_LOGOUT API >>>>', response);
      weakSelf.setState({ showLoader: false })
      setTimeout(function () {
        LocalDataManager.removeAllLocalData()
        weakSelf.props.navigation.navigate('Login')
        weakSelf.props.saveInitialPickupInfo()
        weakSelf.props.saveInitialSearchInfo()
      }, 200);

    }).catch((error: any) => {
      if (error == internetConnectionError) {
        this.setState({ isInternetError: true })
      }
      else {
        General.showErroMsg(this, error)
      }
    })
  }

  cancelLogout() {
    this.setState({ showConfirmLogoutPopup: false })
    this.props.navigation.navigate('Home')
  }

  render() {

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Modal
          animated={true}
          transparent={true}
          visible={this.state.showConfirmLogoutPopup}>
          <Popup img={require('../../assets/cartoon.png')} title='Are you sure you want to logout?' btnText='Confirm' greenBtnText='No, thanks' func={() => this.confirmLogout()} greenBtnAction={() => this.cancelLogout()} />
        </Modal>
        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 50 }}>
          <BackHeaderBtn func={() => this.backBtnHandler()} />
          <Text style={styles.greyTxt}>Hi, {this.props.name}</Text>
          <View style={{ height: 40, width: 40 }} />
        </View>
        <View style={styles.imgVw}>
          {this.props.avatarSource != '' ?
            <Image
              source={{ uri: this.props.avatarSource }}
              style={styles.img}
              resizeMode='cover' /> :
            <Image
              source={require('../../assets/avatar.png')}
              style={[styles.img, { backgroundColor: 'transparent' }]}
              resizeMode='contain' />}
        </View>
        <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>

          {this.props.items.map((item: any) => {
            return item.routeName != 'Home' ?
              <View>
                <TouchableOpacity onPress={() => this.revealOptionSelected(item)} style={{ height: 60, justifyContent:'center' }}>
                  <View style={[{ flexDirection: 'row', alignItems: 'center', height: 40, borderRadius: 20 }, (item.routeName == this.state.selectedItem ? {backgroundColor: '#2785FE', borderRadius: 10} : null)]}>
                    <Image
                      source={item.params.image}
                      style={[styles.revealOptionImage, (item.routeName == this.state.selectedItem ? { tintColor: 'white' } : { tintColor: '#2785FE' })]} />
                    <Text style={[styles.revealOptionText, (item.routeName == this.state.selectedItem ? { color: 'white' } : { color: APP_GREY_TEXT_COLOR })]}>{item.params.name}</Text>
                  </View>
                </TouchableOpacity>
                <View style={styles.borderLine} />
              </View> : null
          }
          )}
          {/* <DrawerItems {...this.props} /> */}
          <TouchableOpacity onPress={() => this.logout()} style={styles.logoutBtn}>
            <Image
              source={require('../../assets/logout.png')}
              style={styles.logoutImg} />
            {/* <Text style={[styles.revealOptionText, { color: 'red' }]}>Logout</Text> */}
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = (state: any) => ({
  name: state.saveLoginInfo.firstName,
  avatarSource: state.saveLoginInfo.image,
});

const mapDispatchToProps = (dispatch: any) => ({
  saveInitialPickupInfo: (info: string) => dispatch({ type: INITIAL_PICK_UP_INFO, payload: info }),
  saveInitialSearchInfo: (info: string) => dispatch({ type: INITIAL_SEARCH_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Drawer);

