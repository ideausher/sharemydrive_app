import React, { Component } from 'react';
import { View, Text, SafeAreaView, Alert, Modal, FlatList, KeyboardAvoidingView,Platform ,NativeModules,StatusBarIOS} from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { OpenDrawerHeaderBtn, NotificationsHeaderBtn, NoDataFoundView, NoInternetFoundView, NotificationsNoDataFoundView } from '../../custom/CustomComponents';
import { Loader } from '../../Utils/Loader';
import { TextGrey } from '../../custom/CustomText';
import ChatCell from '../../custom cells/ChatCell';
import { firestore } from 'react-native-firebase';
//@ts-ignore
import { connect } from 'react-redux';
import LocalDataManager from '../../Utils/LocalDataManager';
import { EventRegister } from 'react-native-event-listeners'
import { API_USER_DETAILS_BY_ID } from '../../constants/apis';
import RequestManager from '../../Utils/RequestManager';
import { updateReadUnreadStatus } from '../../Firebase/FirestoreHandler';

const { StatusBarManager } = NativeModules;


export interface props {
    navigation: NavigationScreenProp<any, any>
    loginInfo: any,
    notificationsInfo: any
};

class Chats extends Component<props, object> {

    notificationListener: any = undefined

    state = {
        chatsArr: [] as any[],
        showLoader: false,
        isInternetError: false,
        isDataNotFoundError: false,
        isHighlighted: false,
        isCalled: false,
        statusBarHeight: 0,
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Messages</Text>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerRight: (
                <NotificationsHeaderBtn func={navigation.getParam('notificationsHandler')} isHighlighted={navigation.getParam('isHighlighted')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle,

        }
    }

    componentDidMount() {

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }


        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': this.state.isHighlighted });

        this.props.navigation.addListener('willFocus', (route) => this.refreshData());

        // this.notificationListener = EventRegister.addEventListener('notificationListener', (data) => {
        //     this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': data });
        // })
    }

    componentWillUnmount() {
        //EventRegister.removeEventListener(this.notificationListener)
    }

    componentWillReceiveProps(newProps: any) {
        console.log('Props Notifications Info chats >>>', newProps.notificationsInfo)
        console.log('is Highlighted chats >>>', newProps.notificationsInfo.highlight)
        console.log('this.state.ishighlighted  chats>>>', this.state.isHighlighted)

        if (this.state.isHighlighted != newProps.notificationsInfo.highlight) {
            this.props.navigation.setParams({ 'isHighlighted': newProps.notificationsInfo.highlight });
            this.setState({ isHighlighted: newProps.notificationsInfo.highlight })
        }
    }

    private refreshData() {

        console.log("Refresh data chats called")

        // this.checkForNewNotifications()
        this.getUsers()
    }

    checkForNewNotifications() {

        // Check if notificatiosj icon needs to be highlighted
        LocalDataManager.getDataAsyncStorage('newNotifications').then((data: any) => {
            console.log('Data Retrived from Storage >>>>', data)
            if (data != null) {
                this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': data });
                this.setState({ isHighlighted: data })
            } else {
                this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': false });
                this.setState({ isHighlighted: false })
            }
        })
    }

    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    notificationsHandler() {
        this.props.navigation.navigate('Notifications')
    }

    getUsers() {

        let loginUserId = this.props.loginInfo.id.toString()
        console.log('Login ID >>>>>', loginUserId)
        firestore().collection('users').doc(loginUserId).collection('chats').onSnapshot((response) => {
            console.log('getUsers Response >>>>>', response)
            const listData: any[] = [];
            var chatData: any = {};
            response.docs.forEach((doc: any) => {
                chatData = doc.data();
                listData.push(chatData);
            });

            console.log('Users >>>>>', listData)
            this.setState({ isCalled: true, chatsArr: listData })
            //Get Last msg of Each User 
            this.getLastMessageforChatArr(listData)
            // this.getImageforChatArr(listData)
        }, (error) => {
            Alert.alert(error.message);
        });
    }

    getLastMessageforChatArr(chatsArr: any) {

        console.log("chatsArr >>>>>", chatsArr)

        chatsArr.forEach((chat: any, index: any) => {

            firestore().collection('messages').doc(chat.chatId).collection('lastMsg').doc('lastMsg').onSnapshot((response) => {
                console.log('getLastMessageforChatId Response >>>>>', response.data())

                let lastMsgData: any = response.data()

                console.log("lastMsgData >>>>>", lastMsgData)

                if (lastMsgData != undefined) {
                    chat['lastMsg'] = lastMsgData.message
                    chat['sentDate'] = lastMsgData.sentDate
                    chat['lastImage'] = lastMsgData.image
                    chat['senderID'] = lastMsgData.senderID
                    chat['status'] = lastMsgData.status
                    chat['chatID'] = chat.chatId

                    let chatsArr = this.state.chatsArr

                    chatsArr[index] = chat

                    this.setState({ chatsArr: chatsArr })
                    this.apiGetUserDetailsById(chat.id, index)
                }
                // else {
                //     let chatsArray = this.state.chatsArr
                //     console.log("Chats Arr count before >>>>>", chatsArray)
                //     chatsArray.splice(index, 1)
                //     this.setState({ chatsArr: chatsArray.slice() })
                //     console.log("Chats Arr count After >>>>>", chatsArray)
                // }
            }, (error) => {
                Alert.alert(error.message);
            });
        });
    }

    getImageforChatArr(chatsArr: any) {

        console.log("chatsArr >>>>>", chatsArr)

        chatsArr.forEach((chat: any, index: any) => {


        });
    }


    // Get paarmeters for Login API
    private getParams(userId: any) {
        return {
            id: userId
        }
    }

    async apiGetUserDetailsById(userId: any, index: any) {
        console.log('GET apiGetUserDetailsById called with params Mesages>>>', this.getParams(userId))
        // this.setState({ showLoader: true })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_USER_DETAILS_BY_ID, this.getParams(userId))
            .then((response: any) => {
                console.log('REsponse of apiGetUserDetailsById API Messages>>>>', response);

                let chatsArr = this.state.chatsArr

                let chat = chatsArr[index]
                chat.image = response.data.image
                chatsArr[index] = chat

                this.setState({ chatsArr: chatsArr })

            }).catch((error: any) => {
                console.log("Erro While erecieving user details >>>>>", error)
                //General.showErroMsg(this, error)
            })
    }

    cellSelected(index: any) {
        console.log("Chat data >>>>", this.state.chatsArr[index])
        if (this.props.loginInfo.id != this.state.chatsArr[index].senderID) {
            updateReadUnreadStatus(this.state.chatsArr[index].chatID)
        }
        this.props.navigation.navigate('Messages', { chatData: this.state.chatsArr[index] })
    }

    render() {

        console.log("Chat Arr in render >>>>>", this.state.chatsArr)
        return (
            <SafeAreaView style={{ flex: 1 }}>
                 <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : undefined}
                    keyboardVerticalOffset={(Platform.OS === 'android') ? this.state.statusBarHeight + 60 : this.state.statusBarHeight + 44}
                    style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }}
                    enabled
                >
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    {this.state.chatsArr.length > 0 ? <TextGrey marginTop={10} fontSize={18} textAlign='left' text='Recent chats' color='#707070' width='90%' /> : null}
                    {this.state.chatsArr.length > 0 ? <FlatList
                        showsVerticalScrollIndicator={false}
                        style={{ marginTop: 24, width: '100%', marginBottom: 8 }}
                        data={[...this.state.chatsArr]}
                        keyExtractor={(item: any, index: any) => index.toString()}
                        renderItem={({ item, index }: any) => <ChatCell
                            onClickEvent={(index: any) => this.cellSelected(index)}
                            myId={this.props.loginInfo.id}

                            item={item} index={index} />}
                    /> : (this.state.isCalled ? <NotificationsNoDataFoundView title='NO MESSAGES YET !' message='' img={require('../../assets/noMessages.png')} marginTop={80} /> : null)}
                </View>
                </KeyboardAvoidingView>
            </SafeAreaView>


        );
    }
}

const mapStateToProps = (state: any) => ({
    loginInfo: state.saveLoginInfo,
    notificationsInfo: state.saveNotificationsInfo
});

export default connect(mapStateToProps)(Chats);
