//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding:16
    },
});