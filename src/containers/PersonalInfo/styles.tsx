//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD, APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        //backgroundColor: 'white',
        padding: 16
    },    
    genderDOBContainer:{
        marginTop: 16,
        flexDirection: 'row', 
      //  backgroundColor:'red'
    },
    genderBtn:{
        backgroundColor: '#D3D3D3',
        borderRadius: 5, 
        width: 30,
        height: 30, 
        alignItems: 'center', 
        justifyContent: 'center', 
        marginRight: 32
    },
    otherBtn:{
        width: 60,
    },
    genderImg:{
        width: 12, 
        height: 18
    },
    otherImg:{
        width: 32, 
        height: 18
    },
    dialogText:{
        fontSize:13,
        width:250
    }
});
