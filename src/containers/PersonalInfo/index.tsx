import React, { Component } from 'react';
import { View, Text, Image, StatusBar, Platform, StatusBarIOS, NativeModules, KeyboardAvoidingView, Keyboard, TouchableOpacity, SafeAreaView, Modal } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { TextInputMultiLine, TextInputWithoutImage, TextInputWithoutImageFunc } from '../../custom/CustomTextInput';
import { TextGrey } from '../../custom/CustomText';
import { LOGIN } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { APP_GREY_BOLD_TEXT_COLOR } from '../../constants/colors';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
import { API_SIGNUP } from '../../constants/apis';
import { ForGotScreenType } from '../../Utils/Enums';
import LocalDataManager from '../../Utils/LocalDataManager';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import SimpleToast from 'react-native-simple-toast';
import moment from "moment";
import { MaterialDialog } from 'react-native-material-dialog';
import { APP_GREEN_COLOR } from '../../constants/colors';




export interface props {
    navigation: NavigationScreenProp<any, any>
    loginInfo: any,
    saveLoginInfo: any,
}

const { StatusBarManager } = NativeModules;



class PersonalInfo extends Component<props, object> {

    state = {
        showPopup: false,
        updationMessage: '',
        statusBarHeight: 0,
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
        homeAddress: {} as any,
        officeAddress: {} as any,
        otherAddress: {} as any,
        countryCode: '',
        gender: 1,
        bio: '',
        otp: '',
        year: '',
        month: '',
        date: '',
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Personal Information</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle,

        }
    }


    componentDidMount() {
        // console.log('Props >>>>', this.props)
        // StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }
    }

    componentWillReceiveProps(newProps: any) {

        console.log('Old Props personal info: ' + JSON.stringify(newProps))

        console.log(' Login props info   >>>', newProps.loginInfo)
        let dob = newProps.loginInfo.dob

        console.log('DOB >>>', dob)
        console.log('date >>>', dob.split('-')[2])
        console.log('month >>>', dob.split('-')[1])
        console.log('year >>>', dob.split('-')[0])
        console.log('Props Login Phone number >>>', newProps.loginInfo.phoneNumber)
        console.log('Bio >>>', newProps.loginInfo.bio)

        this.setState({
            firstName: newProps.loginInfo.firstName,
            lastName: newProps.loginInfo.lastName,
            email: newProps.loginInfo.email,
            phoneNumber: newProps.loginInfo.phoneNumber == null ? "" : newProps.loginInfo.phoneNumber,
            countryCode: newProps.loginInfo.countryCode == null ? "" : newProps.loginInfo.countryCode,
            gender: newProps.loginInfo.gender,
            bio: newProps.loginInfo.bio,
            year: (dob != '' ? dob.split('-')[0] : ''),
            month: (dob != '' ? dob.split('-')[1] : ''),
            date: (dob != '' ? dob.split('-')[2] : ''),
            homeAddress: this.getAddressFromType("1", newProps.loginInfo.address),
            officeAddress: this.getAddressFromType("2", newProps.loginInfo.address),
            otherAddress: this.getAddressFromType("3", newProps.loginInfo.address),
        })
        console.log('Bio 2 >>>', newProps.loginInfo.bio)
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    genderBtnAction(selectedOption: Number) {
        this.setState({ gender: selectedOption })
    }

    // Called on Save  btn Click
    private saveBtnAction() {


        this.setState({ showLoader: true })
        if (this.isDataValidate())
            this.callProfileUpdateAPI()
    }




    /**
     * @function isDateValidate validate data of all fields
     */
    isDataValidate() {
        //  this.state.year + '-' + this.state.month + '-' + this.state.date,
        if (this.state.date.length < 1) {
            SimpleToast.show("Please enter birth date")
            return false
        } else if (this.state.month.length < 1) {
            SimpleToast.show("Please enter birth month")
            return false
        } else if (this.state.year.length < 4) {
            SimpleToast.show("Please enter birth year")
            return false
        }
        //  else if ((this.state.bio.trim()).length < 50) {
        //     SimpleToast.show("Please enter atleast 50 words in Bio")
        //     return false
        // }
        var markedDate = moment(new Date()).format("YYYY-MM-DD")
        var birthDate = this.state.year.concat("-").concat(this.state.month).concat("-").concat(this.state.date)
        console.log("Dates >>", markedDate, "  ------   ", birthDate, (markedDate < birthDate));
        if (markedDate <= birthDate) {
            SimpleToast.show("Please enter valid birth date")
            return false;
        }
        return true;
    }

    getAddressFromType(type: String, addressArr: Array<any>) {
        let temp
        if (addressArr == undefined){
            return {}
        }
            
        // for (let address of addressArr) {
        //     console.log("check adresses loop >>>>>", address)
        //     if (address != undefined && address["address_type"] == type) {
        //         return address
        //     }
        //     else {
        //         return {}
        //     }
        // }
        
else{
    temp = addressArr.filter(data => data.address_type == type)
    if(temp.length > 0){
        return temp[0]
    }else{
        return {}
    }
}
        
    }

    getAddressparams(address: any, addressType: String) {

        return {
            latitude: address.lat,
            longitude: address.long,
            country: address.country,
            place_id: address.placeId,
            city: address.city,
            state: address.state,
            full_address: address.address,
            address_type: addressType
        }
    }

    private getAddress() {

        let address: any = {
            homeAddress: {},
            workAddress: {},
            otherAddress: {}
        }

        if (this.state.homeAddress != undefined && this.state.homeAddress.latitude != 0) {
            address['homeAddress'] = this.state.homeAddress
        }
        if (this.state.officeAddress != undefined && this.state.officeAddress.latitude != 0) {
            address['workAddress'] = this.state.officeAddress
        }
        if (this.state.otherAddress != undefined && this.state.otherAddress.latitude != 0) {
            address['otherAddress'] = this.state.otherAddress
        }

        console.log("Address Param to send in APi >>>>", address)

        return address
    }

    // paarmeters for API
    private getParams() {

        var params: any = {
            "first_name": this.state.firstName,
            "last_name": this.state.lastName,
            "gender": this.state.gender,
            "phone_country_code": this.state.countryCode,
            "phone_number": this.state.phoneNumber,
            "bio": this.state.bio,
            "date_of_birth": this.state.year + '-' + (this.state.month.length == 1 ? "0".concat(this.state.month) : this.state.month) + '-' + (this.state.date.length == 1 ? "0".concat(this.state.date) : this.state.date),
            "address": this.getAddress()
        }

        if (this.state.otp != '') {
            params['otp'] = this.state.otp
        }

        return params
    }

    // Call API to update User Data
    private callProfileUpdateAPI() {
        console.log('updateprofile API called with params >>>', this.state.date + " : " + this.state.month + " : " + this.state.year)
        console.log('updateprofile API called with params >>>', this.getParams())
        RequestManager.putRequestWithHeaders(API_SIGNUP, this.getParams()).then((response: any) => {
            this.setState({ showLoader: false, otp: '', showPopup: true, updationMessage: response.message })
            //General.showMsgWithDelay(response.message)

            this.updateLoginInfo()
            // this.props.navigation.goBack()
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    goBack = () => {
        this.setState({ ...this.state, showPopup: false })
        this.props.navigation.goBack()
    }

    private updateLoginInfo() {

        let loginDict = this.getLoginInfoDict()
        console.log('loginDict >>>>', loginDict)
        LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(loginDict))
        this.props.saveLoginInfo(loginDict)

    }

    private getLoginInfoDict() {

        return {
            email: this.state.email,
            firstname: this.state.firstName,
            lastname: this.state.lastName,
            id: this.props.loginInfo.id,
            phone_country_code: this.state.countryCode,
            phone_number: this.state.phoneNumber,
            token: this.props.loginInfo.token,
            gender: this.state.gender,
            image: this.props.loginInfo.image,
            date_of_birth: this.state.year + '-' + this.state.month + '-' + this.state.date,
            refferal_code: this.props.loginInfo.referralCode,
            referral_code_text: this.props.loginInfo.referralText,
            is_notification: this.props.loginInfo.isNotification,
            bio: this.state.bio,
            suggestedPrice: this.props.loginInfo.suggestedPrice,
            is_vehicle_added: this.props.loginInfo.isVehicleInfoAdded,
            is_bank_detail_added: this.props.loginInfo.isBankDetailsAdded,
            address: [this.state.homeAddress, this.state.officeAddress, this.state.otherAddress]
        }
    }

    // Moved on next screen to update Phone Number
    private EditPhoneNumber() {
        console.log('EditPhoneNumber Fun called')
        this.props.navigation.navigate('ForgotPswd', { forgotScreenType: ForGotScreenType.editPhoneNumber, params: { phone: this.state.phoneNumber, countryCode: this.state.countryCode }, func: this.updatePhoneNumber.bind(this) });
    }

    // This Func is passed to upadte Phone number
    private updatePhoneNumber(phoneNum: string, countryCod: string, otp: string) {
        console.log('Update Phone Num func called?>>>>', phoneNum, countryCod, otp);

        this.setState({ phoneNumber: phoneNum, countryCode: countryCod, otp: otp })
    }

    // Moved on next screen to update Phone Number
    private addHomeAddress() {
        console.log('addHomeAddress Fun called')

        this.props.navigation.navigate('SelectLocation', { isAddress: true, func: this.updateHomeAddress.bind(this) })
    }

    updateHomeAddress(address: any) {
        console.log('Home Address >>>>', address)
        this.setState({ homeAddress: this.getAddressparams(address, "1") })
    }

    // Moved on next screen to update Phone Number
    private addOfficeAddress() {
        console.log('addHomeAddress Fun called')

        this.props.navigation.navigate('SelectLocation', { isAddress: true, func: this.updateOfficeAddress.bind(this) })
    }

    updateOfficeAddress(address: any) {
        console.log('Office Address >>>>', address)
        this.setState({ officeAddress: this.getAddressparams(address, "2") })
    }

    // Moved on next screen to update Phone Number
    private addOtherAddress() {
        console.log('addHomeAddress Fun called')

        this.props.navigation.navigate('SelectLocation', { isAddress: true, func: this.updateOtherAddress.bind(this) })
    }

    updateOtherAddress(address: any) {
        console.log('Other Address >>>>', address)
        this.setState({ otherAddress: this.getAddressparams(address, "3") })
    }



    render() {

        console.log("Adresses CHECK >>>>>>", this.state.officeAddress)
        return (
            <SafeAreaView style={{ flex: 1 }}>
                
                    {this.state.showPopup ?    
                    <MaterialDialog
                        title="Profile Updation!"
                        titleColor={APP_GREEN_COLOR}
                        visible={this.state.showPopup}
                        cancelLabel=""
                        onOk={() => this.goBack()}
                        colorAccent={APP_GREEN_COLOR}
                        onCancel={() => { }}>
                        <Text style={styles.dialogText}>
                            {this.state.updationMessage}
                        </Text>
                    </MaterialDialog>
                    :null}

                    {/* <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'padding'}
                    keyboardVerticalOffset={(Platform.OS === 'android') ? this.state.statusBarHeight + 64 : this.state.statusBarHeight + 44}
                    style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled={true}>
                 */}
                    <View style={styles.container}>
                        {/* <ScrollView>
                        contentContainerStyle={{ justifyContent: "flex-end", flex: 1 }}
                            showsVerticalScrollIndicator={false} style={{ flex: 1 }}> */}

                        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
                            <TextGrey marginTop={30} fontSize={12} text='Your first name' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImage placeholder='Enter first name' onChange={(text: any) => this.setState({ firstName: text })} value={this.state.firstName} fontSize={14} width='100%' />
                            <TextGrey marginTop={16} fontSize={12} text='Your last name' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImage placeholder='Enter last name' onChange={(text: any) => this.setState({ lastName: text })} value={this.state.lastName} fontSize={14} width='100%' />
                            <TextGrey marginTop={16} fontSize={12} text='Your Email' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImage placeholder='Enter email' editable={false} onChange={(text: any) => this.setState({ email: text })} value={this.state.email} fontSize={14} width='100%' />
                            <TextGrey marginTop={16} fontSize={12} text='Phone No' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImageFunc placeholder='Enter phone number here' onChange={(text: any) => this.setState({ phoneNumber: text })} value={this.state.phoneNumber} fontSize={14} width='100%' func={() => this.EditPhoneNumber()} />

                            <TextGrey marginTop={16} fontSize={12} text='Home address' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImageFunc placeholder='Home address' value={this.state.homeAddress != undefined && this.state.homeAddress.full_address != undefined ? this.state.homeAddress.full_address : ''} fontSize={14} width='100%' func={() => this.addHomeAddress()} />

                            <TextGrey marginTop={16} fontSize={12} text='Office address' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImageFunc placeholder='Office address' value={this.state.officeAddress != undefined && this.state.officeAddress.full_address != undefined ? this.state.officeAddress.full_address : ''} fontSize={14} width='100%' func={() => this.addOfficeAddress()} />

                            <TextGrey marginTop={16} fontSize={12} text='Other address' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImageFunc placeholder='Other address' value={this.state.otherAddress != undefined && this.state.otherAddress.full_address != undefined ? this.state.otherAddress.full_address : ''} fontSize={14} width='100%' func={() => this.addOtherAddress()} />

                            <TextGrey marginTop={16} fontSize={12} text='Gender' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <View style={styles.genderDOBContainer}>
                                <TouchableOpacity onPress={() => this.genderBtnAction(0)} style={[styles.genderBtn, (this.state.gender == 0 ? { backgroundColor: '#487CFD', } : { backgroundColor: '#D3D3D3' })]}>
                                    <Image
                                        source={require('../../assets/male.png')}
                                        style={styles.genderImg}
                                        resizeMode='contain' />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.genderBtnAction(1)} style={[styles.genderBtn, (this.state.gender == 1 ? { backgroundColor: '#487CFD', } : { backgroundColor: '#D3D3D3' })]}>
                                    <Image
                                        source={require('../../assets/female.png')}
                                        style={styles.genderImg}
                                        resizeMode='contain' />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.genderBtnAction(2)} style={[styles.genderBtn, styles.otherBtn, (this.state.gender == 2 ? { backgroundColor: '#487CFD', } : { backgroundColor: '#D3D3D3' })]}>
                                    <Image
                                        source={require('../../assets/other.png')}
                                        style={styles.otherImg}
                                        resizeMode='contain' />
                                </TouchableOpacity>
                            </View>
                            <TextGrey marginTop={16} fontSize={12} text='Date of birth' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <View style={styles.genderDOBContainer}>
                                <TextInputWithoutImage placeholder='mm' onChange={(text: any) => this.setState({ month: text })} value={this.state.month} fontSize={14} width={60} textAlign='center' marginRight={16} marginTop={0} keyboardType='numeric' maxLength={2} returnKeyType="done" />
                                <TextInputWithoutImage placeholder='dd' onChange={(text: any) => this.setState({ date: text })} value={this.state.date} fontSize={14} width={60} textAlign='center' marginRight={16} marginTop={0} keyboardType='numeric' maxLength={2} returnKeyType="done" />
                                <TextInputWithoutImage placeholder='yyyy' onChange={(text: any) => this.setState({ year: text })} value={this.state.year} fontSize={14} width={60} textAlign='center' marginRight={16} marginTop={0} keyboardType='numeric' maxLength={4} returnKeyType="done" />
                            </View>
                            <TextGrey marginTop={16} fontSize={12} text='Bio' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputMultiLine placeholder='Write a short bio here' onChange={(text: any) => this.setState({ bio: text })} value={this.state.bio} fontSize={14} width='100%' height={100} />
                            <TextGrey marginTop={16} fontSize={12} text='Min 50 words' textAlign='right' color='rgba(128, 128, 128, 0.5)' />
                            <CommonBtn title='Save' func={() => this.saveBtnAction()} marginTop={50} marginBottom={60} />

                            {/* </ScrollView> */}
                        </KeyboardAwareScrollView>
                    </View>
                    {/* </KeyboardAvoidingView> */}
               
            </SafeAreaView>




            // <View style={{ flexDirection: 'column', flex: 1, backgroundColor: 'red', }}>
            //     <View style={[{ height: 80, zIndex: 1, backgroundColor: 'yellow', position: 'absolute', top: 0, left: 0, right: 0 }]}>

            //     </View>
            //     {/* <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'padding'}
            //        keyboardVerticalOffset={(Platform.OS === 'android') ? 80 + 64 : 80 + 44}
            //          style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled={true}>
            //    */}
            //     <KeyboardAwareScrollView>
            //         <TextInput placeholder={"placeholder"} style={{ marginTop: 20, width: '100%', height: 100, backgroundColor: 'white' }} />
            //         <TextInput placeholder={"placeholder"} style={{ marginTop: 20, width: '100%', height: 100, backgroundColor: 'white' }} />
            //         <TextInput placeholder={"placeholder"} style={{ marginTop: 20, width: '100%', height: 100, backgroundColor: 'white' }} />
            //         <TextInput placeholder={"placeholder"} style={{ marginTop: 20, width: '100%', height: 100, backgroundColor: 'white' }} />
            //         <TextInput placeholder={"placeholder"} style={{ marginTop: 20, width: '100%', height: 100, backgroundColor: 'white' }} />
            //         <TextInput placeholder={"placeholder"} style={{ marginTop: 20, width: '100%', height: 100, backgroundColor: 'white' }} />
            //     </KeyboardAwareScrollView>
            //     {/* </KeyboardAvoidingView> */}
            // </View>
        )
    }
}

const mapStateToProps = (state: any) => ({
    loginInfo: state.saveLoginInfo,
});

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveLoginInfo: (info: string) => dispatch({ type: LOGIN, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfo);
