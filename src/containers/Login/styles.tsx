//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { WINDOW_WIDTH } from '../../constants';
import { APP_NAVY_BLUE_COLOR, APP_BORDER_GREY_COLOR } from '../../constants/colors';
import { APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'white'
    },
    linearGradient: {
        width: '100%',
        height: '40%',
    },
    image: {
        marginTop: 40
    },
    topView: {
        flex: 1,
        position: 'absolute',
        // backgroundColor: 'green',
        left: 0,
        top: 0,
        bottom: 0,
        right: 0,
        alignItems: 'center'
    },
    scrollVw: {
        // flex: 1,
        // width: (WINDOW_WIDTH - 20),
        width: '100%',
        // backgroundColor:'purple'
    },
    whiteVw: {
        flex: 1,
        width: '95%',
        backgroundColor: 'white',
        marginTop: 50,
        // marginLeft: 10,
        // marginRight: 10,
        marginBottom: 0,
        borderRadius: 30,
        //width: (WINDOW_WIDTH - 50),
        alignItems: 'center'
    },
    signBtnVw: {
        width: '50%',
        //height: 100,
        alignItems: 'center',
       // backgroundColor:'red',
        padding: 8
    },
    activeBlueLine: {
        marginTop: 8,
        width: 100,
        height: 2,
        backgroundColor: APP_NAVY_BLUE_COLOR,
    },
    inactiveBlueLine: {
        marginTop: 8,
        width: 100,
        height: 2,
        backgroundColor: 'white',
    },
    signActiveTxt: {
        fontFamily: APP_FONT_REGULAR,
        fontSize: 22,
        textAlign: 'center',
        color: '#717173'
    },
    signInActiveTxt: {
        fontFamily: APP_FONT_REGULAR,
        fontSize: 22,
        textAlign: 'center',
        color: '#C8C7CC'
    },
    socialBtnImg: {
        width: 25,
        height: 25,
        
    },
    socialGoogleImg: {
        width: 40,
        height: 40,
    },
    orTxt: {
        color: '#707070',
        fontSize: 14,
        fontFamily: APP_FONT_REGULAR,
    },
    pswdVw:{
        flexDirection: 'row',
        marginTop: 16,
        alignItems: 'center',
        height: 40,
        padding: 8,
        borderWidth: 0.2,
        borderColor: APP_BORDER_GREY_COLOR,
        width: '90%',
        borderRadius: 10,
        backgroundColor: 'white',
      },
    pswdTextInput:{
        textAlign: 'left',
        flex: 1,
        height: 40,
        fontSize: 12,
        fontFamily: APP_FONT_REGULAR,
        color: '#717173',
      },
});