import React, { Component } from 'react';
import { View, Text, SafeAreaView, Image, ScrollView, TouchableOpacity, Alert, Modal, KeyboardAvoidingView, Platform, StatusBar, NativeModules, StatusBarIOS, BackHandler, Keyboard, TextInput } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import LinearGradient from 'react-native-linear-gradient';
import { TextInputPhoneNum, TextInputWithoutImage } from '../../custom/CustomTextInput';
import { BlueTextBtn, CommonBtn } from '../../custom/CustomButton';
import { VerificationFor, ForGotScreenType } from '../../Utils/Enums';
import Toast from 'react-native-simple-toast';
//@ts-ignore
import validator from 'validator';
import RequestManager from '../../Utils/RequestManager';
import { API_SENDOTP, API_SIGNIN, API_SOCIALLOGIN } from '../../constants/apis';
import LocalDataManager from '../../Utils/LocalDataManager';
import General from '../../Utils/General';
import { APP_GREY_BOLD_TEXT_COLOR, APP_GREY_PLACEHOLDER_COLOR } from '../../constants/colors';
//@ts-ignore
import { CountrySelection } from 'react-native-country-list';
import { Loader } from '../../Utils/Loader';
import { LOGIN } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { TextGrey } from '../../custom/CustomText';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from "react-native-fbsdk";
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin'
// 'react-native-google-signin';


const { StatusBarManager } = NativeModules;

export interface props {
    navigation: NavigationScreenProp<any, any>
    saveLoginInfo: any,
};

class Login extends Component<props, object> {

    state = {
        isSignIn: true,
        loginCountryCode: '61',
        signupCountryCode: '61',
        loginFlag: undefined,
        signupFlag: undefined,
        loginPhoneNumber: '',
        loginPswd: '',
        signupPhoneNumber: '',
        signupPswd: '',
        cnfrmPswd: '',
        showCodeList: false,
        firstName: '',
        lastName: '',
        email: '',
        referralCode: '',
        showLoader: false,
        statusBarHeight: 0,
        showLogin: false
    }

    scrollvw: ScrollView;

    static navigationOptions = () => {
        return {
            header: null
        }
    }

    async componentDidMount() {

        StatusBar.setHidden(true);

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }
        else {
            // Will Prevent to move back on hardware back btn clicked
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        }

        this.checkIfUserSignupAndVerificationPending()


        //////////// INITIALIZE SOCIAL LOGIN ///////////
        try {
            await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
            // google services are available
        } catch (err) {
            console.error('play services are not available');
        }

        //initial configuration
        GoogleSignin.configure({
            //It is mandatory to call this method before attempting to call signIn()
            // scopes: ['https://www.googleapis.com/auth/drive.readonly'],
            // Repleace with your webClientId generated from Firebase console
            webClientId: "168835854999-09b2n0au81bu388c3j21m65i3e6st3au.apps.googleusercontent.com",
        });
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        return true
    }

    // // Check if Signup form data exists Means user Signup but verification pending thenmove directly to Verification Screen
    private checkIfUserSignupAndVerificationPending() {

        let weakSelf = this
        LocalDataManager.getDataAsyncStorage('signupInfo').then((data: any) => {
            if (data != null && data != {}) {
                this.props.navigation.navigate('Verification', { verificationFor: VerificationFor.signup, phoneNumber: data['phone_country_code'] + ' ' + data['phone_number'] })
                setTimeout(function () {
                    weakSelf.setState({ showLogin: true })
                }, 400);

            } else {
                console.log("Show login set to true called")
                this.setState({ showLogin: true })
            }
        })
    }

    /// LOGIN PROCESS /////

    // Validate Login Credentials eneterd
    private validateLoginDetails() {

        if (this.state.loginCountryCode.length <= 0) {
            // Alert.alert('Please enter Country Code.');
            Toast.show('Please enter country code', Toast.SHORT);
            return false;
        }
        else if (this.state.loginPhoneNumber.length <= 0) {
            Toast.show('Please enter phone number', Toast.SHORT);
            return false;
        }
        // else if (!validator.isMobilePhone(this.state.loginPhoneNumber, 'any', { strictMode: false })) {
        //     Toast.show('Please enter valid phone number', Toast.SHORT);
        //     return false;
        // }
        else if (this.state.loginPhoneNumber.length < 8) {
            Toast.show('Please enter valid phone number', Toast.SHORT);
            return false;
        }
        else if (this.state.loginPswd.length <= 0) {
            Toast.show('Please enter Password', Toast.SHORT);
            return false;
        }

        return true;
    }

    // Caled on Login Btn Clicked
    private signInBtnAction() {

        Keyboard.dismiss()
        if (this.validateLoginDetails()) {

            this.setState({ showLoader: true })
            this.apiSignin()
        }
    }

    // Get paarmeters for Login API
    private getLoginAPIParams() {
        return {
            phone_number: this.state.loginPhoneNumber,
            phone_country_code: this.state.loginCountryCode,
            password: this.state.loginPswd,
        }
    }

    // Call Login API
    private async apiSignin() {
        console.log('PArams Sign In >>>>', this.getLoginAPIParams())
        let weakSelf = this
        RequestManager.postRequest(API_SIGNIN, this.getLoginAPIParams(), true).then((response: any) => {

            console.log('REsponse of Login API >>>>', response);
            setTimeout(function () {
                weakSelf.setState({ showLoader: false })
            }, 200);
            this.saveLoginUserDetailsAndMove(response.data)
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    // After Login Save Login data in redux and Local Storage(for use after App kill) and Move to Home Screen
    private saveLoginUserDetailsAndMove(data: any) {

        LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(data))
        console.log("Payment pending >>>>", data.payment_pending_ride)
        //Check if payment is Pending
        if (data.payment_pending_ride != undefined && data.payment_pending_ride.ride_id != undefined) {
            console.log("Payment pending >>>>", data.payment_pending_ride)
            LocalDataManager.saveDataAsyncStorage('shouldShowPaymentPopup', JSON.stringify(true))
            LocalDataManager.saveDataAsyncStorage('paymentInfo', JSON.stringify(data.payment_pending_ride))
        }
        this.props.saveLoginInfo(data);
        this.props.navigation.navigate('LoginSuccess');
    }

    /// SIGNUP PROCESS /////

    // Called to show Country Code picker
    showCountryCodeList() {
        this.setState({ showCodeList: true })
    }

    // Caleld when Country Code is selected and save selected country code in state
    countryCodeSelcted(item: any) {

        if (this.state.isSignIn) {
            this.setState({ loginCountryCode: item.callingCode, showCodeList: false, loginFlag: item.flag })
        }
        else {
            this.setState({ signupCountryCode: item.callingCode, showCodeList: false, signupFlag: item.flag })
        }
    }

    // Called on forgot Paswd Btn click -> Moves to forgot Pswd Screen
    private forgotPswd() {
        this.setState({ loginPhoneNumber: '', loginPswd: '', loginCountryCode: '', loginFlag: undefined })
        this.props.navigation.navigate('ForgotPswd', { forgotScreenType: ForGotScreenType.forgotPswd });
    }

    signInOrSignUpBtnAction(isSignIn: Boolean) {
        this.scrollvw.scrollTo()
        this.setState({ isSignIn: isSignIn })
    }

    // Validation for all fields
    validateSignupDetail() {
        if (this.state.firstName.length <= 0) {
            //Alert.alert('Please enter name.');
            Toast.show('Please enter first name', Toast.SHORT);
            return false;
        }
        // else if (this.state.lastName.length <= 0) {
        //     Toast.show('Please enter last name', Toast.SHORT);
        //     return false;
        // }
        else if (this.state.email.length <= 0) {
            //Alert.alert('Please enter email.');
            Toast.show('Please enter email', Toast.SHORT);
            return false;
        }
        else if (!validator.isEmail(this.state.email)) {
            // Alert.alert('Please enter valid email.');
            Toast.show('Please enter valid email', Toast.SHORT);
            return false;
        }
        else if (this.state.signupCountryCode.length <= 0) {
            // Alert.alert('Please enter Country Code.');
            Toast.show('Please enter country code', Toast.SHORT);
            return false;
        }
        else if (this.state.signupPhoneNumber.length <= 0) {
            // Alert.alert('Please enter phone number.');
            Toast.show('Please enter phone number', Toast.SHORT);
            return false;
        }
        else if (!validator.isMobilePhone(this.state.signupPhoneNumber, 'any', { strictMode: false })) {
            Toast.show('Please enter valid phone number', Toast.SHORT);
            return false;
        }
        else if (this.state.signupPswd.length <= 0) {
            // Alert.alert('Please enter Password.');
            Toast.show('Please enter Password', Toast.SHORT);
            return false;
        }
        else if (this.state.signupPswd.length < 6) {
            //Alert.alert('Please enter Password of atleast 6 digits');
            Toast.show('Please enter Password of atleast 6 digits', Toast.SHORT);
            return false;
        }
        else if (this.state.cnfrmPswd.length <= 0) {
            //    Alert.alert('Please enter confirm Password.');
            Toast.show('Please enter confirm Password', Toast.SHORT);
            return false;
        }
        else if (this.state.cnfrmPswd != this.state.signupPswd) {
            //  Alert.alert('Confirm password doesn\'t matched');
            Toast.show('Confirm password doesn\'t matched', Toast.SHORT);
            return false;
        }

        return true;
    }

    // called in Signup Btn Click
    signUpBtnAction() {
       
        if (this.validateSignupDetail()) {
            this.setState({ showLoader: true })
            this.apiSendOTP()
        }
    }

    // Gete parameters of send OTp Api
    getOTPParams() {
        return {
            phone_country_code: this.state.signupCountryCode,
            phone_number: this.state.signupPhoneNumber,
            //email: this.state.email
        }
    }

    // API tp send OTp on added Phone Number for verification
    apiSendOTP() {
        RequestManager.postRequest(API_SENDOTP, this.getOTPParams(), false).then((response: any) => {
            console.log('REsponse of Send OTP API >>>>', response);

            this.saveSignupDataOnLocal()
            this.setState({ showLoader: false })

            setTimeout(() => {
              //  Alert.alert(response.message)
                Toast.show(response.message, Toast.SHORT);
                this.props.navigation.navigate('Verification', { verificationFor: VerificationFor.signup, phoneNumber: this.state.signupCountryCode + ' ' + this.state.signupPhoneNumber })
            }, 200);
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    // Ge all values to Save in Local Storage 
    private getSignupData() {
        let params: any = {
            firstname: this.state.firstName,
            lastname: this.state.lastName,
            email: this.state.email,
            password: this.state.signupPswd,
            phone_country_code: this.state.signupCountryCode,
            phone_number: this.state.signupPhoneNumber,
        }

        if (this.state.referralCode.length > 0) {
            params['referral_code'] = this.state.referralCode
        }

        console.log('Params >>>>', params)

        return params
    }

    /////////////////    SOCIAL LOGIN    ////////////////////////////

    getGoogleSocialLoginParam = (result: any) => {
        return {
            email: result.email ? result.email : "",
            type: "google",
            social_id: result.id,
            firstname: result.name ? result.name : "",
            social_image: result.photo
        }
    }

    googleLogin = async () => {
        try {
            console.log("start >> play service >> ");
            await GoogleSignin.hasPlayServices();
            console.log("start >> play service >> 111111111");
            const userInfo = await GoogleSignin.signIn();
            console.log("start >> play service >> 22222222222");
            console.log("start >> userInfo >> ", userInfo);
            this.socialLogin(this.getGoogleSocialLoginParam(userInfo.user))
            console.log("start >> play service >> 3333333333");
        } catch (error) {
            console.log(error, error.code);

            //    this.printToast(JSON.stringify(error.message))
            if (error.code == statusCodes.SIGN_IN_CANCELLED) {
                Toast.show("Gmail Login Cancelled")

                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (f.e. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
    }


    facebookLogin = async () => {

        const ref = this

        if (Platform.OS === "android") {
            LoginManager.setLoginBehavior("web_only")
        }
        console.log("start >>", LoginManager);

        LoginManager.logInWithPermissions(["public_profile", 'email']).then(
            function (result: any) {
                if (result.isCancelled) {
                    console.log("facebook nLogin cancelled");
                } else {
                    console.log(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString(), result
                    );

                    const token = AccessToken.getCurrentAccessToken()
                    console.log("facebookAccessTOKEN>>>>>>>>", token);

                    AccessToken.getCurrentAccessToken().then((data: any) => {
                        const infoRequest = new GraphRequest(
                            '/me?fields=id,name,email,picture',
                            null,
                            (error: any, result: any) => {
                                if (error) {
                                    Alert.alert('Error fetching data: ' + error.toString());
                                } else {
                                    console.log("start >>> facebook payload >>", result);

                                    ref.facebookSocialLoginAPI(result)
                                    return true
                                }
                            }
                        );

                        new GraphRequestManager().addRequest(infoRequest).start();

                        //   FB.api(
                        //     '/me',
                        //     'GET',
                        //     {"fields":"id,name,email,picture"},
                        //     function(response) {
                        //         // Insert your code here
                        //     }
                        //   );

                    }).catch((error: any) => {
                        console.log("START >>> FACEBOOK CATCH", error);

                    })



                    // console.log("facebook result", result);
                }
            },
            function (error: any) {
                console.log("facebook Login fail with error: " + error);
            }
        );
    }

    getFBSocialLoginParam = (result: any) => {
        return {
            email: result.email ? result.email : "",
            type: "facebook",
            social_id: result.id,
            firstname: result.name ? result.name : "",
            social_image: result.picture ? result.picture.data.url : "",

        }
    }

    facebookSocialLoginAPI = (result: any) => {
        this.socialLogin(this.getFBSocialLoginParam(result))
    }

    socialLogin(payload: any) {
        console.log("start>>login payload", payload);
        this.setState({ showLoader: true })

        let weakSelf = this
        RequestManager.postRequest(API_SOCIALLOGIN, payload, true).then((response: any) => {

            console.log('REsponse of Login API >>>>', response);
            setTimeout(function () {
                weakSelf.setState({ showLoader: false })
            }, 200);
            this.saveLoginUserDetailsAndMove(response.data)
        }).catch((error: any) => {
            console.log('Error of Login API >>>>', error);
            General.showErroMsg(this, error)
        })
    }


    ////////////////////////////////////////////////////////

    // Save Signup data on local storage so that if user Fills Signup form, move to next screen and kills app without verify phone Number with OTp then next time we will show him/her Direct verification Screen
    private saveSignupDataOnLocal() {
        LocalDataManager.saveDataAsyncStorage('signupInfo', JSON.stringify(this.getSignupData()))
    }

    render() {
        if (!this.state.showLogin) {
            return null
        }

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showLoader}>
                        <Loader />
                    </Modal>
                    <Modal
                        animated={true}
                        transparent={true}
                        visible={this.state.showCodeList}>
                        <View style={{ flex: 1, marginTop: 20 }}>
                            <TouchableOpacity
                                style={{ backgroundColor: 'white' }}
                                onPress={() => this.setState({ showCodeList: false })}>
                                <Text style={{ alignSelf: 'flex-end', lineHeight: 30, width: 60, textAlign: 'center', fontSize: 14, color: APP_GREY_BOLD_TEXT_COLOR, marginRight: 8 }}>Cancel</Text>
                            </TouchableOpacity>
                            <CountrySelection action={(item: any) => this.countryCodeSelcted(item)} selected={true} />
                        </View>
                    </Modal>
                    <View style={styles.container}>
                        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} colors={['#4CE5B1', '#0CB8CB']} style={styles.linearGradient}>
                        </LinearGradient>
                        <View style={styles.topView}>
                            <Image source={require('../../assets/car.png')} style={styles.image} />
                            <View style={styles.whiteVw}>
                                <View style={{ flexDirection: 'row', marginTop: 22 }}>
                                    <View style={styles.signBtnVw}>
                                        <TouchableOpacity onPress={() => this.signInOrSignUpBtnAction(true)}>
                                            <Text style={styles.signActiveTxt}>Sign In</Text>
                                            <View style={this.state.isSignIn ? styles.activeBlueLine : styles.inactiveBlueLine} />
                                        </TouchableOpacity>
                                    </View>
                                    <View style={styles.signBtnVw}>
                                        <TouchableOpacity onPress={() => this.signInOrSignUpBtnAction(false)}>
                                            <Text style={this.state.isSignIn ? styles.signInActiveTxt : styles.signActiveTxt}>Sign Up</Text>
                                            <View style={this.state.isSignIn ? styles.inactiveBlueLine : styles.activeBlueLine} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <ScrollView ref={(input: any) => { this.scrollvw = input as any }} showsVerticalScrollIndicator={false} style={styles.scrollVw}>
                                    {this.state.isSignIn ? <View style={{ width: '100%', alignItems: 'center' }}>
                                        <TextInputPhoneNum func={(text: any) => this.showCountryCodeList()} value={this.state.loginPhoneNumber} placeholder='Mobile Number' img={this.state.loginFlag == undefined ? require('../../assets/flag.png') : { uri: this.state.loginFlag }} countryPlaceholder='+61' countryCode={this.state.loginCountryCode} fontSize={12} onChange={(text: any) => this.setState({ loginPhoneNumber: text })} />
                                        {/* <TextInputWithoutImage placeholder='Password' onChange={(text: any) => this.setState({ loginPswd: text })} value={this.state.loginPswd} autoCapitalize='none' marginTop={16} secureTextEntry={true} /> */}

                                        <View style={styles.pswdVw}>
                                            <TextInput style={styles.pswdTextInput} placeholder='Password' placeholderTextColor={APP_GREY_PLACEHOLDER_COLOR} value={this.state.loginPswd} onChangeText={(text: any) => this.setState({ loginPswd: text })} secureTextEntry={true} autoCapitalize='none' />
                                        </View>

                                        <BlueTextBtn title='Forgot password?' func={() => this.forgotPswd()} marginRight='5%' textAlign='right' alignSelf='flex-end' />
                                        <CommonBtn title='Sign In' func={() => this.signInBtnAction()} marginTop={80} />
                                    </View> :
                                        <View style={{ width: '100%', alignItems: 'center' }}>
                                            <TextInputWithoutImage placeholder='Enter First Name' onChange={(text: any) => this.setState({ firstName: text })} value={this.state.firstName} marginTop={16} />
                                            <TextInputWithoutImage placeholder='Enter Last Name' onChange={(text: any) => this.setState({ lastName: text })} value={this.state.lastName} marginTop={16} />
                                            <TextInputWithoutImage placeholder='name@exmple.com' onChange={(text: any) => this.setState({ email: text })} value={this.state.email} autoCapitalize='none' keyboardType='email-address' marginTop={16} />
                                            <TextInputPhoneNum func={(text: any) => this.showCountryCodeList()} value={this.state.signupPhoneNumber} placeholder='Mobile Number' img={this.state.signupFlag == undefined ? require('../../assets/flag.png') : { uri: this.state.signupFlag }} countryPlaceholder='+61' countryCode={this.state.signupCountryCode} fontSize={12} onChange={(text: any) => this.setState({ signupPhoneNumber: text })} />
                                            <TextInputWithoutImage placeholder='Password' onChange={(text: any) => this.setState({ signupPswd: text })} value={this.state.signupPswd} autoCapitalize='none' marginTop={16} secureTextEntry={true} />
                                            <TextInputWithoutImage placeholder='Confirm Password' onChange={(text: any) => this.setState({ cnfrmPswd: text })} value={this.state.cnfrmPswd} autoCapitalize='none' marginTop={16} secureTextEntry={true} />
                                            <TextInputWithoutImage placeholder='Enter referral code (if any)' onChange={(text: any) => this.setState({ referralCode: text })} value={this.state.referralCode} autoCapitalize='none' marginTop={16} />
                                            {/* <TextGrey title='By creating an account with Share My Drive you are accepting our terms and conditions.' color='#487CFD'  fontSize={10}/> */}
                                            <TextGrey marginTop={10} fontSize={10} text='By creating an account with Share My Drive you are accepting our terms and conditions.' color='#487CFD' width='90%' />

                                            <CommonBtn title='Sign Up' func={() => this.signUpBtnAction()} marginTop={20} />
                                        </View>}
                                    <View style={[{ width: '100%', alignItems: 'center' }, (this.state.isSignIn ? { marginTop: 60 } : { marginTop: 20 })]}>
                                        <Text style={styles.orTxt}>OR</Text>
                                        <View style={{ flexDirection: 'row', width: 200, marginTop: 16, marginBottom: 40, alignItems: 'center' }}>
                                            <TouchableOpacity style={{ width: 100, alignItems: 'center' }} onPress={() => this.facebookLogin()}>
                                                <Image resizeMode='contain' source={require('../../assets/facebook-logo.png')} style={styles.socialBtnImg} />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ width: 100, alignItems: 'center' }} onPress={() => this.googleLogin()}>
                                                <Image resizeMode='contain' source={require('../../assets/google.png')} style={styles.socialBtnImg} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveLoginInfo: (info: string) => dispatch({ type: LOGIN, payload: info }),
});

export default connect(null, mapDispatchToProps)(Login);
