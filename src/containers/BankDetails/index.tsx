import React, { Component } from 'react';
import { View, Text, Image, StatusBar, Platform, StatusBarIOS, NativeModules, KeyboardAvoidingView, Keyboard, TouchableOpacity, SafeAreaView, ScrollView, Modal, Alert } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { TextInputWithBtn, TextInputWithoutImage, TextInputWithoutImageFunc } from '../../custom/CustomTextInput';
import { TextGrey } from '../../custom/CustomText';
import { LOGIN, BANK } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { APP_GREY_BOLD_TEXT_COLOR } from '../../constants/colors';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
import { API_SIGNUP, API_BANK } from '../../constants/apis';
import { ForGotScreenType } from '../../Utils/Enums';
import LocalDataManager from '../../Utils/LocalDataManager';
import { DropdownList } from '../../custom/DropdownList';
import Toast from 'react-native-simple-toast';
import { MaterialDialog } from 'react-native-material-dialog';
import { APP_GREEN_COLOR } from '../../constants/colors';

export interface props {
    navigation: NavigationScreenProp<any, any>
    bankInfo: any,
    saveBankInfo: any,
    loginInfo: any,
    saveLoginInfo: any,
}

const { StatusBarManager } = NativeModules;

const list = ['a', 'b', 'c', 'd', 'e']

class BankDetails extends Component<props, object> {

    state = {
        showPopup: false,
        updationMessage: '',
        accountHolderName: '',
        accountNum: '',
        // bankName: '',
        bsb_no: '',
        showLoader: false,
        statusBarHeight: 0,
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Bank Details</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        console.log('Props >>>>', this.props)
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        this.apiGetBankDetails()
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    async apiGetBankDetails() {

        this.setState({ showLoader: true })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_BANK, {})
            .then((response: any) => {
                console.log('REsponse of apiGetBankDetails API >>>>', response);
                weakSelf.setState({ showLoader: false })

                if (response.data.length > 0) {
                    weakSelf.props.saveBankInfo(response.data[response.data.length - 1])
                }
            }).catch((error: any) => {
                console.log('Error Mesage Shown From here 1')
                General.showErroMsg(this, error)
            })
    }

    componentWillReceiveProps(newProps: any) {

        // console.log('Old Props: ' + JSON.stringify(newProps))

        this.setState({
            accountHolderName: newProps.bankInfo.accountHolderName,
            accountNum: newProps.bankInfo.accountNum,
            //bankName: newProps.bankInfo.bankName,
            bsb_no: newProps.bankInfo.bsb_no,
        })
    }

    // Called on Save  btn Click
    private saveBtnAction() {

        if (this.validateDetails()) {
            this.setState({ showLoader: true })
            this.callBankUpdateAPI()
        }
    }

    // Validate Login Credentials eneterd
    private validateDetails() {

        if (this.state.accountHolderName.length <= 0) {
            // Alert.alert('Please enter Country Code.');
            Toast.show('Please enter account holder name', Toast.SHORT);
            return false;
        }
        else if (this.state.accountNum.length <= 0) {
            Toast.show('Please enter account number', Toast.SHORT);
            return false;
        }
        // else if (this.state.bankName.length <= 0) {
        //     Toast.show('Please enter car bank name', Toast.SHORT);
        //     return false;
        // }
        else if (this.state.bsb_no.length <= 0) {
            Toast.show('Please enter BSB number', Toast.SHORT);
            return false;
        }


        return true;
    }


    // paarmeters for API
    private getParams() {

        var params: any = {
            "account_holder_name": this.state.accountHolderName,
            "account_number": this.state.accountNum,
            //"bank_name": this.state.bankName,
            "bsb_no": this.state.bsb_no
        }

        return params
    }

    // Call API to update User Data
    private callBankUpdateAPI() {
        console.log('callBankUpdateAPI API called with params >>>', this.getParams())
        let weakSelf = this

        RequestManager.postRequestWithHeaders(API_BANK, this.getParams()).then((response: any) => {
            this.setState({ showLoader: false, showPopup: true, updationMessage: response.message })
            // General.showMsgWithDelay(response.message)

            weakSelf.props.saveBankInfo(response.data)
            console.log('bank data updated >>>', response.data)
            this.updateLoginInfo()
            // weakSelf.props.navigation.goBack()
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    goBack = () => {
        this.setState({ ...this.state, showPopup: false })
        this.props.navigation.goBack()
    }

    private updateLoginInfo() {

        let loginDict = this.getLoginInfoDict()
        console.log('loginDict >>>>', loginDict)
        LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(loginDict))
        this.props.saveLoginInfo(loginDict)

    }
    private getLoginInfoDict() {

        return {
            email: this.props.loginInfo.email,
            firstname: this.props.loginInfo.firstName,
            lastname: this.props.loginInfo.lastName,
            id: this.props.loginInfo.id,
            phone_country_code: this.props.loginInfo.countryCode,
            phone_number: this.props.loginInfo.phoneNumber,
            token: this.props.loginInfo.token,
            gender: this.props.loginInfo.gender,
            image: this.props.loginInfo.image,
            date_of_birth: this.props.loginInfo.dob,
            refferal_code: this.props.loginInfo.referralCode,
            referral_code_text: this.props.loginInfo.referralText,
            is_notification: this.props.loginInfo.isNotification,
            bio: this.props.loginInfo.bio,
            suggestedPrice: this.props.loginInfo.suggestedPrice,
            is_vehicle_added: this.props.loginInfo.isVehicleInfoAdded,
            is_bank_detail_added: 1,
        }
    }

    render() {

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                    <MaterialDialog
                        title="Bank Info Updation!"
                        titleColor={APP_GREEN_COLOR}
                        visible={this.state.showPopup}
                        cancelLabel=""
                        onOk={() => this.goBack()}
                        colorAccent={APP_GREEN_COLOR}
                        onCancel={() => { }}>
                        <Text style={styles.dialogText}>
                            {this.state.updationMessage}
                        </Text>
                    </MaterialDialog>
                    <View style={styles.container}>
                        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>

                            <TextGrey marginTop={30} fontSize={12} text='Account holder name' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImage placeholder='Enter account holder name' onChange={(text: any) => this.setState({ accountHolderName: text })} value={this.state.accountHolderName} fontSize={14} width='100%' maxLength={30} />

                            <TextGrey marginTop={16} fontSize={12} text='Account number' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImage placeholder='Enter account number' onChange={(text: any) => this.setState({ accountNum: text })} value={this.state.accountNum} fontSize={14} width='100%' keyboardType='numeric' returnKeyType='done' maxLength={18} />

                            {/* <TextGrey marginTop={16} fontSize={12} text='Bank name' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImage placeholder='Enter bank name' onChange={(text: any) => this.setState({ bankName: text })} value={this.state.bankName} fontSize={14} width='100%' maxLength={30}/> */}

                            <TextGrey marginTop={16} fontSize={12} text='BSB Number' textAlign='left' color={APP_GREY_BOLD_TEXT_COLOR} />
                            <TextInputWithoutImage placeholder='Enter BSB number' onChange={(text: any) => this.setState({ bsb_no: text })} value={this.state.bsb_no} fontSize={14} width='100%' maxLength={7} keyboardType='numeric' />

                            <CommonBtn title='Save' func={() => this.saveBtnAction()} marginTop={60} marginBottom={60} />
                        </ScrollView>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state: any) => ({
    bankInfo: state.saveBankInfo,
    loginInfo: state.saveLoginInfo,
});

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveBankInfo: (info: string) => dispatch({ type: BANK, payload: info }),
    saveLoginInfo: (info: string) => dispatch({ type: LOGIN, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(BankDetails);
