//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    linearGradient: {
        width: '100%',
        height: '42%',
        alignItems: 'center'
    },
    headerVw: {
        width: '100%',
        marginTop: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        //backgroundColor:'red'
    },
    img: {
        width: 80,
        height: 80,
        borderRadius: 40,
    },
    imgBackGround: {
        width: 80,
        height: 80,
        borderRadius: 40,
        backgroundColor: 'rgba(255,255,255, 0.5)',
        justifyContent: 'flex-end'
    },
    plusImg: {
        width: 20,
        height: 20,
        alignSelf: 'flex-end',
        position: 'absolute'

    },
    whiteTxt: {
        fontSize: 16,
        fontFamily: APP_FONT_REGULAR,
        color: 'white',
        textAlign: 'center',
        marginTop: 8,
        // backgroundColor:'red'
    },
    whiteVw: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: -24,
        borderRadius: 30,
        padding: 24
    },
    optionsVw: {
        marginTop: 40,
        flexDirection: 'row',
        alignItems: 'center',
        // backgroundColor:'red'
    },
    optionImg: {
        width: 20,
        height: 20,
        marginRight: 24
    },
});