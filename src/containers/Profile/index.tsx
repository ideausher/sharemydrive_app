import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image, Platform, Alert, Modal, ImageStore } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import LinearGradient from 'react-native-linear-gradient';
import { OpenDrawerHeaderBtn, NotificationsHeaderBtn } from '../../custom/CustomComponents';
import ImageResizer from 'react-native-image-resizer';
import CustomImagePicker from '../../Utils/CustomImagePicker';
import General from '../../Utils/General';
import RequestManager from '../../Utils/RequestManager';
import { API_UPLOAD_IMAGE } from '../../constants/apis';
//@ts-ignore
import { connect } from 'react-redux';
import { LOGIN } from '../../Redux/Constants';
import { TextGrey } from '../../custom/CustomText';
import { Loader } from '../../Utils/Loader';
import LocalDataManager from '../../Utils/LocalDataManager';
import { EventRegister } from 'react-native-event-listeners'
import FastImage from 'react-native-fast-image';


export interface props {
    navigation: NavigationScreenProp<any, any>
    loginInfo: any,
    saveLoginInfo: any,
    notificationsInfo: any
};

const OptionsView = (props: any) => {
    return (
        <TouchableOpacity style={styles.optionsVw} onPress={props.func}>
            <Image
                source={props.img}
                style={styles.optionImg}
                resizeMode='contain'
            />
            <TextGrey marginTop={0} fontSize={18} text={props.title} textAlign='left' color='#4E4E50' />
        </TouchableOpacity>
    );
};

class Profile extends Component<props, object> {

    notificationListener: any = undefined

    state = {
        avatarSource: undefined as any,
        uploadImageType: '',
        showLoader: false,
        isHighlighted: false,
    };

    static navigationOptions = () => {
        return {
            header: null
        }
    }

    componentDidMount() {

        this.props.navigation.addListener('willFocus', (route) => this.refreshData());

        // this.notificationListener = EventRegister.addEventListener('notificationListener', (data) => {
        //     this.setState({ isHighlighted: data})
        // })
    }

    componentWillUnmount() {
        // EventRegister.removeEventListener(this.notificationListener)
    }

    componentWillReceiveProps(newProps: any) {
        console.log('Props Notifications Info profile>>>', newProps.notificationsInfo)
        console.log('is Highlighted profile>>>', newProps.notificationsInfo.highlight)
        console.log('this.state.ishigh;ighted  profile>>>', this.state.isHighlighted)

        if (this.state.isHighlighted != newProps.notificationsInfo.highlight) {
            this.setState({ isHighlighted: newProps.notificationsInfo.highlight })
        }
    }

    private refreshData() {

        console.log("Refresh data profile called")

        // Check if notificatiosj icon needs to be highlighted
        LocalDataManager.getDataAsyncStorage('newNotifications').then((data: any) => {
            console.log('Data Retrived from Storage newNotifications >>>>', data)
            if (data != null) {
                this.setState({ isHighlighted: data })
            }
        })
    }

    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    notificationsHandler() {
        this.props.navigation.navigate('Notifications')
    }

    private async updateImg() {
        CustomImagePicker.showImgPicker().then((response: any) => {

            console.log('Image Selced >>>', response)
            this.setState({ uploadImageType: response.type, showLoader: true })
            this.resizeImageandUpload(response)

        }).catch((error: any) => {
            console.log('Error While CApture image >>>', error)
            if (error != undefined) {
                General.showErroMsg(this, error)
            }
        })
    }

    private resizeImageandUpload(image: any) {
        let newWidth = 800;
        let newHeight = 800 / (image.width / image.height);

        let rotation = 0

        if (image.originalRotation === 90) {
            rotation = 90
        } else if (image.originalRotation === 270) {
            rotation = -90
        }

        ImageResizer.createResizedImage(image.uri, newWidth, newHeight, 'JPEG', 100, rotation).then((response) => {
            this.setState({ avatarSource: response })
            this.apiUploadImage(response)

        }).catch((err) => {
            console.log('Image Resize error  >>>', err)
        });
    }

    private uploadImageParams(image: any) {

        const formData = new FormData();
        // formData.append('profile_image', this.state.avatarSource.data); 

        formData.append("profile_image", {
            name: this.state.avatarSource.name,
            type: this.state.uploadImageType,
            uri:
                Platform.OS === "android" ? image.uri : image.uri.replace("file://", "")
        });

        return formData
    }

    // API tp send OTp on added Phone Number for verification
    apiUploadImage(image: any) {
        RequestManager.uploadImage(API_UPLOAD_IMAGE, this.uploadImageParams(image)).then((response: any) => {
            console.log('REsponse of apiUploadImage >>>>', response);
            this.updateLoginInfo(response.data.profile_image)

            this.setState({ showLoader: false })

            setTimeout(() => {
                Alert.alert(response.message)
            }, 200);

        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    private updateLoginInfo(imageStr: String) {

        let loginDict = this.getLoginInfoDict(imageStr)
        console.log('loginDict >>>>', loginDict)
        LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(loginDict))
        this.props.saveLoginInfo(loginDict)

    }
    private getLoginInfoDict(imageStr: String) {

        return {
            email: this.props.loginInfo.email,
            firstname: this.props.loginInfo.firstName,
            lastname: this.props.loginInfo.lastName,
            id: this.props.loginInfo.id,
            phone_country_code: this.props.loginInfo.countryCode,
            phone_number: this.props.loginInfo.phoneNumber,
            token: this.props.loginInfo.token,
            gender: this.props.loginInfo.gender,
            image: imageStr,
            date_of_birth: this.props.loginInfo.dob,
            refferal_code: this.props.loginInfo.referralCode,
            referral_code_text: this.props.loginInfo.referralText,
            is_notification: this.props.loginInfo.isNotification,
            bio: this.props.loginInfo.bio,
            suggestedPrice: this.props.loginInfo.suggestedPrice,
            is_vehicle_added: this.props.loginInfo.isVehicleInfoAdded,
            is_bank_detail_added: this.props.loginInfo.isBankDetailsAdded,
            address: this.props.loginInfo.address
        }
    }

    peronalInfo() {
        this.props.navigation.navigate('PersonalInfo')
    }

    vehicleInfo() {
        this.props.navigation.navigate('VehicleInfo')
        //Alert.alert('In Progress')
    }

    bankDetails() {
        this.props.navigation.navigate('BankDetails')
        //Alert.alert('In Progress')
    }

    render() {

        if (this.state.avatarSource != undefined) {
            console.log("Avatar Source >>>>>>", this.state.avatarSource)
            console.log("Avatar Source path >>>>>>", this.state.avatarSource.path)
            console.log("Avatar Source Uri >>>>>>", this.state.avatarSource.uri)
        }


        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} colors={['#4CE5B1', '#0CB8CB']} style={styles.linearGradient}>
                        <View style={styles.headerVw}>
                            <OpenDrawerHeaderBtn func={() => this.revealBtnHandler()} isWhite={true} />
                            <NotificationsHeaderBtn func={() => this.notificationsHandler()} isWhite={true} isHighlighted={this.state.isHighlighted} />
                        </View>
                        <TouchableOpacity style={styles.imgBackGround} onPress={() => this.updateImg()}>
                            {this.state.avatarSource != undefined ?
                                <FastImage
                                source={{ uri: this.state.avatarSource.uri }}
                                style={[styles.img, { backgroundColor: 'transparent' }]}
                                resizeMode={FastImage.resizeMode.cover} /> :
                                this.props.loginInfo.image != '' ?
                                    <FastImage
                                        source={{ uri: this.props.loginInfo.image }}
                                        style={[styles.img, { backgroundColor: 'transparent' }]}
                                        resizeMode={FastImage.resizeMode.cover} /> :
                                    null
                            }
                            <Image
                                source={require('../../assets/addImage.png')}
                                style={styles.plusImg}
                                resizeMode='cover' />
                        </TouchableOpacity>
                        <Text style={styles.whiteTxt}>Add your profile picture</Text>
                    </LinearGradient>
                    <View style={styles.whiteVw}>
                        <OptionsView img={require('../../assets/personalInfo.png')} title='Personal Information' func={() => this.peronalInfo()} />
                        <OptionsView img={require('../../assets/vehicleInfo.png')} title='Vehicle Information' func={() => this.vehicleInfo()} />
                        <OptionsView img={require('../../assets/bankDetails.png')} title='Bank Details' func={() => this.bankDetails()} />
                    </View>
                </View>
            </SafeAreaView>
            
            
        );
    }
}

const mapStateToProps = (state: any) => ({
    loginInfo: state.saveLoginInfo,
    notificationsInfo: state.saveNotificationsInfo
});

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveLoginInfo: (info: string) => dispatch({ type: LOGIN, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
