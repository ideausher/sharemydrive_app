import React, { Component } from 'react';
import { View, Text, SafeAreaView, FlatList, Alert, Modal, TouchableOpacity, Image } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { OpenDrawerHeaderBtn, NotificationsHeaderBtn } from '../../custom/CustomComponents';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { TextInputWithBtn } from '../../custom/CustomTextInput';
import LastRideCell from '../../custom cells/LastRideCell';
//@ts-ignore
import { connect } from 'react-redux';
import { CommonBtn } from '../../custom/CustomButton';
import { TextGrey } from '../../custom/CustomText';
import RequestManager from '../../Utils/RequestManager';
import { API_GET_PUBLISHED_RIDES } from '../../constants/apis';
import General from '../../Utils/General';
import { Loader } from '../../Utils/Loader';
import { internetConnectionError } from '../../constants/messages';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import { ORIGIN_INFO, DESTINATION_INFO, DATE_TIME_INFO } from '../../Redux/Constants';
import { Popup } from '../../custom/Popup';
import { APP_SKY_BLUE_COLOR } from '../../constants/colors';
import LocalDataManager from '../../Utils/LocalDataManager';
import { EventRegister } from 'react-native-event-listeners'

export interface props {
    navigation: NavigationScreenProp<any, any>
    pickupInfo: any,
    saveOriginInfo: any,
    saveDestinationInfo: any,
    saveBookingDateTimeInfo: any,
    loginInfo: any,
    notificationsInfo: any
};

class PickUp extends Component<props, object> {

    notificationListener: any = undefined

    state = {
        lastRidesArr: [] as any[],
        page: 0,
        isRefreshing: false,
        loadMore: false,
        isCalled: false,
        prevPage: -1,
        showLoader: false,
        showPopup: false,
        multipleDaysEnabled: false,
        isHighlighted: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Pick up</Text>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerRight: (
                <NotificationsHeaderBtn func={navigation.getParam('notificationsHandler')} isHighlighted={navigation.getParam('isHighlighted')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {

        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': this.state.isHighlighted });

        this.props.navigation.addListener('willFocus', (route) => this.refreshData());

        // this.notificationListener = EventRegister.addEventListener('notificationListener', (data) => {
        //     this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': data });
        // })
    }

    componentWillUnmount() {
        //EventRegister.removeEventListener(this.notificationListener)
    }

    componentWillReceiveProps(newProps: any) {
        console.log('Props Notifications Info pickup >>>', newProps.notificationsInfo)
        console.log('is Highlighted  pickup >>>', newProps.notificationsInfo.highlight)
        console.log('this.state.ishigh;ighted pickup>>>', this.state.isHighlighted)

        if (this.state.isHighlighted != newProps.notificationsInfo.highlight) {
            this.props.navigation.setParams({ 'isHighlighted': newProps.notificationsInfo.highlight });
            this.setState({ isHighlighted: newProps.notificationsInfo.highlight })
        }
    }

    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    notificationsHandler() {
        this.props.navigation.navigate('Notifications')
    }

    private refreshData() {

        console.log("Refresh data pickup called")
        // this.checkForNewNotifications()

        if (this.props.loginInfo.firstName == '' || this.props.loginInfo.isVehicleInfoAdded == 0 || this.props.loginInfo.isBankDetailsAdded == 0) {
            this.setState({ showPopup: true })
        }
        else {
            this.setState({ page: 0, prevPage: -1, lastRidesArr: [] })
            setTimeout(() => {
                this.apiGetLastPublishedRides(true)
            }, 200)
        }
    }

    checkForNewNotifications() {

        // Check if notificatiosj icon needs to be highlighted
        LocalDataManager.getDataAsyncStorage('newNotifications').then((data: any) => {
            console.log('Data Retrived from Storage >>>>', data)
            if (data != null) {
                this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': data });
                this.setState({ isHighlighted: data })
            } else {
                this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': false });
                this.setState({ isHighlighted: false })
            }
        })
    }

    private refreshList() {
        this.setState({ isRefreshing: true })
        setTimeout(() => {
            this.apiGetLastPublishedRides(true)
        }, 200)
    }

    // Get paarmeters for Login API
    private getParams() {
        return {
            type: "3",
            fetch: 'o',
            page: this.state.isRefreshing ? 0 : this.state.page,
            limit: "5",
        }
    }

    async apiGetLastPublishedRides(refresh: boolean) {
        console.log('GET Lats published Rides API called with page index >>>', this.state.page)
        this.setState({ showLoader: true })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_PUBLISHED_RIDES, this.getParams())
            .then((response: any) => {
                console.log('REsponse of apiGetLastPublishedRides API >>>>', response);
                weakSelf.setState({ showLoader: false })
                let ridesArrData: any[] = []

                if (refresh) {
                    console.log('data refershed')
                    weakSelf.setState({ page: 0, prevPage: -1, lastRidesArr: [] })
                    ridesArrData = response.data

                    console.log('Rides data in refresh >>>>', ridesArrData)
                }
                else {
                    ridesArrData = this.state.lastRidesArr.concat(response.data)
                }

                setTimeout(() => {

                    console.log('Prev and next page >>>', this.state.prevPage, this.state.page)

                    weakSelf.setState({ showLoader: false, lastRidesArr: ridesArrData, loadMore: false, prevPage: this.state.page, isRefreshing: false })

                    setTimeout(() => {

                        console.log('Again Prev and next page >>>', this.state.prevPage, this.state.page)
                        weakSelf.setState({ isCalled: false })
                    }, 200)

                    if (response.data.length > 0) {
                        let nextPage = this.state.page + 1
                        weakSelf.setState({ page: nextPage })
                    }
                }, 200)
            }).catch((error: any) => {
                this.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })
                console.log('Error >>>>', error)
                if (error == internetConnectionError) {

                }
                else {
                    if (error.message == null || error.message == undefined) {
                        Toast.show((error), Toast.SHORT);
                    }
                    else if (error.message != "No Record Found.") {
                        Toast.show((error.message), Toast.SHORT);
                    }
                }
            })
    }

    selectOrigin() {
        console.log('Seelct origin Func Called', this.props.pickupInfo.origin)
        this.props.navigation.navigate('SelectLocation', { isOrigin: true, isSearch: false, location: this.props.pickupInfo.origin })
    }

    selectDestination() {
        console.log('Seelct desination Func Called', this.props.pickupInfo.destination)
        this.props.navigation.navigate('SelectLocation', { isOrigin: false, isSearch: false, location: this.props.pickupInfo.destination })
    }

    selectDateTime() {
        this.props.navigation.navigate('SelectDateTime', { isSearch: false, isMultipleDays: this.state.multipleDaysEnabled })
    }

    multipleDaysBtnAction() {
        if (this.state.multipleDaysEnabled == false) {
            this.props.navigation.navigate('SelectDateTime', { isSearch: false, isMultipleDays: true })
        }

        this.setState({ multipleDaysEnabled: !this.state.multipleDaysEnabled })
    }

    cellSelected(index: any) {
        let originDict = this.getLocationDict(this.state.lastRidesArr[index].start_location)
        console.log('originDict >>>>', originDict)
        this.props.saveOriginInfo(originDict)

        let destinationDict = this.getLocationDict(this.state.lastRidesArr[index].end_location)
        console.log('destinationDict >>>>', destinationDict)
        this.props.saveDestinationInfo(destinationDict)

        console.log('ride_date_time >>>', this.state.lastRidesArr[index].ride_date_time)

        let lastDateTimeMoment = moment(this.state.lastRidesArr[index].ride_date_time, "yyyy-MM-dd HH:mm:ss")

        if (lastDateTimeMoment > moment().add(10, 'minutes')) {
            let date = this.state.lastRidesArr[index].ride_date_time.split(' ')[0]
            let time = this.state.lastRidesArr[index].ride_date_time.split(' ')[1]

            console.log('Date time >>>', date, time)

            let formattedTime = moment(time, 'HH:mm:ss').format("hh:mm A")
            console.log('formattedTime >>>', formattedTime)

            this.props.saveBookingDateTimeInfo({ date: [date], time: formattedTime })
        }
    }

    goToProfile() {
        this.setState({ showPopup: false })
        this.props.navigation.navigate('Profile')
    }

    private getLocationDict(location: any) {

        return {
            address: location.full_address,
            country: location.country,
            lat: location.latitude,
            long: location.longitude,
            state: location.state,
            city: location.city,
            placeId: location.place_id
        }
    }

    nextBtnAction() {
        this.props.navigation.navigate('VacantSeats')
    }

    //Will call when reached last cell
    private loadMoreData = () => {

        console.log('Func Clledv >>>', this.state.prevPage, this.state.page)
        if (this.state.prevPage == this.state.page) {
            console.log('Func  return Clled')
            return
        }
        this.setState({ loadMore: true, isCalled: true })
        console.log('Func Clled2')
        setTimeout(() => {
            console.log('Func Clled3')
            this.apiGetLastPublishedRides(false)
        }, 200)
    }

    render() {

        console.log('pickupInfo >>>>', this.props.pickupInfo)
        let origin = ''
        let destination = ''
        let date = ''
        let time = ''

        if (this.props.pickupInfo.origin != '') {
            origin = this.props.pickupInfo.origin.address
        }

        if (this.props.pickupInfo.destination != '') {
            destination = this.props.pickupInfo.destination.address
        }

        if (this.props.pickupInfo.date != '') {
            date = moment(this.props.pickupInfo.date, 'YYYY-MM-DD').format('DD-MM-YYYY')
        }

        if (this.props.pickupInfo.time != '') {
            time = this.props.pickupInfo.time
        }

        let popupMsg = ''

        console.log('LoginInfo >>.', this.props.loginInfo)

        if (this.props.loginInfo.firstName == '') {
            popupMsg = 'Please update your profile before publishing a ride'
        }
        else if (this.props.loginInfo.isVehicleInfoAdded == 0) {
            popupMsg = 'Please add your vehicle information before publishing a ride'
        }
        else if (this.props.loginInfo.isBankDetailsAdded == 0) {
            popupMsg = 'Please add your bank information before publishing a ride'
        }

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showPopup}>
                    <Popup title={popupMsg} btnText='Go to Profile' func={() => this.goToProfile()} />
                </Modal>
                <View style={styles.container}>
                    <TextInputWithBtn placeholder='Origin' value={origin} icon={require('../../assets/originIcon.png')} backgroundColor='#F6F6F6' func={() => this.selectOrigin()} marginTop={40} />
                    <TextInputWithBtn placeholder='Destination' value={destination} icon={require('../../assets/destinationIcon.png')} backgroundColor='#F6F6F6' func={() => this.selectDestination()} />
                    <TextInputWithBtn placeholder='Date & Time' value='Date & Time' backgroundColor='#F6F6F6' func={() => this.selectDateTime()} textAlign='center' height={35} color={APP_SKY_BLUE_COLOR} />


                    {/* <TextInputWithBtn placeholder='Date & Time' value={date == '' ? '' : (date + ', ' + time)} icon={require('../../assets/calendar.png')} backgroundColor='#F6F6F6' func={() => this.selectDateTime()} /> */}

                    {date != '' ? <View style={styles.enableFeatureContainer}>
                        <TouchableOpacity style={styles.imgBtn} onPress={() => this.multipleDaysBtnAction()}>
                            <Image resizeMode='contain' source={this.state.multipleDaysEnabled ? require('../../assets/checkBox.png') : require('../../assets/checkBoxBlank.png')} style={styles.checkImg} />
                        </TouchableOpacity>
                        <TextGrey fontSize={12} text='Continue for multiple days' textAlign='left' marginLeft={-8} marginRight={8} marginTop={0} />
                    </View> : null}
                    {origin == '' || destination == '' || date == '' ?
                        (this.state.lastRidesArr.length > 0 ?
                            < View style={styles.listContainer}>
                                {/* // <View style={styles.line} /> */}
                                <TextGrey fontSize={16} text='Republish your last rides' textAlign='left' width='90%' />
                                <FlatList
                                    onRefresh={() => this.refreshList()}
                                    refreshing={this.state.isRefreshing}
                                    style={{ marginTop: 24, width: '100%', marginBottom: 200 }}
                                    data={this.state.lastRidesArr}
                                    keyExtractor={(item: any, index: any) => index.toString()}
                                    renderItem={({ item, index }: any) => <LastRideCell
                                        onClickEvent={(index: any) => this.cellSelected(index)}
                                        item={item} pickup={true} index={index} />}
                                    onEndReachedThreshold={0.5}
                                    onScroll={({ nativeEvent }) => {
                                        if (this.state.isCalled == false && General.isCloseToBottom(nativeEvent) && this.state.isRefreshing == false) {
                                            this.loadMoreData()
                                        }
                                    }}
                                />
                            </View>
                            : null
                            // (this.state.showLoader || this.state.isRefreshing ? null :
                            //     (this.state.isDataNotFoundError ?
                            //         <NoDataFoundView func={() => this.refreshData()} showFilterBtn={this.state.doctorsList.length == 0} /> :
                            //         (this.state.isInternetError ?
                            //             <NoInternetFoundView func={() => this.refreshData()} showFilterBtn={this.state.doctorsList.length == 0} /> : null)))
                        )
                        :
                        <CommonBtn title='Next' func={() => this.nextBtnAction()} marginTop={180} />}
                </View>
            </SafeAreaView >
        );
    }
}

const mapStateToProps = (state: any) => ({
    pickupInfo: state.savePickUpInfo,
    loginInfo: state.saveLoginInfo,
    notificationsInfo: state.saveNotificationsInfo
});

const mapDispatchToProps = (dispatch: any) => ({
    saveOriginInfo: (info: string) => dispatch({ type: ORIGIN_INFO, payload: info }),
    saveDestinationInfo: (info: string) => dispatch({ type: DESTINATION_INFO, payload: info }),
    saveBookingDateTimeInfo: (info: string) => dispatch({ type: DATE_TIME_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(PickUp);
