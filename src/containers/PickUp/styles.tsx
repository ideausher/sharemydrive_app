//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    listContainer:{
       // backgroundColor:'red',
        width:'90%',
        marginTop: 80,
    },
    line:{
        backgroundColor: 'rgba(232, 230, 241, 0.24)',
        height: 2,
        width: '100%',
        alignSelf: 'center'
    },
    enableFeatureContainer: {
        marginTop: 20,
        flexDirection: 'row',
        height: 60,
        alignItems: 'center',
        alignSelf:'flex-start',
        marginLeft:'10%'
       // backgroundColor: 'green',
    },
    imgBtn: {
        width: 40,
        height: 60,
        justifyContent: 'center',
    },
});