import React, { Component } from 'react';
import { View, Text, Modal, FlatList, TouchableOpacity ,BackHandler} from 'react-native';
import { headerStyle, headerTitleStyle, navHdrTxtStyle } from '../../common/styles';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager';
import { API_GET_CARDS, } from '../../constants/apis';
import { Loader } from '../../Utils/Loader';
import General from '../../Utils/General';
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextGrey } from '../../custom/CustomText';
import CardCell from '../../custom cells/CardCell';

export interface props {
    navigation: NavigationScreenProp<any, any>,
};

export default class SavedCards extends Component<props, object> {

    state = {
        cardsArr: [] as any[],
        showLoader: false,
        selectedCardIndex: -1,
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Saved cards</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });
       // BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        this.apiGetCards()
    }
    handleBackButton() {
        return true;
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    async apiGetCards() {
        // this.setState({ showLoader: true })
        RequestManager.getRequest(API_GET_CARDS, {}).then((response: any) => {
            console.log('REsponse of API_GET_CARDS API >>>>', response);
            this.setState({ showLoader: false, cardsArr: response.data })
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    addPaymentBtnAction() {
        this.props.navigation.navigate('PaymentMode');
    }

    cardSelected(index: any) {
        console.log('Cele Slected Func Called >>>', index)
        this.setState({ selectedCardIndex: index })
        setTimeout(() => {

            this.props.navigation.navigate('CardDetails', {
                selectedCard: this.state.cardsArr[index]
            });
        }, 200)
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    <TextGrey fontSize={18} text='Would you like to pay using saved cards or you want to add a new card ?' textAlign='left' width='90%' color='#888892' marginTop={50} marginLeft={22} />
                    <FlatList
                        style={{ flex: 1, marginTop: 50 }}
                        data={this.state.cardsArr}
                        keyExtractor={(item: any, index: any) => index.toString()}
                        renderItem={({ item, index }: any) => <CardCell
                            onClickEvent={(indx: any) => this.cardSelected(indx)} item={item} index={index} selectedCardIndx={this.state.selectedCardIndex} />
                        }
                    />
                    <TouchableOpacity
                        onPress={() => this.addPaymentBtnAction()}>
                        <TextGrey fontSize={18} text='+ Add Payment Method' textAlign='left' color='#2785FE' marginTop={50} marginLeft={22} marginBottom={50} />
                        {/* <TextGrey style={styles.text}></Text> */}
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}