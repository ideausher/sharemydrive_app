import React, { Component } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image, FlatList, Modal } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { OpenDrawerHeaderBtn, NotificationsHeaderBtn, NoDataFoundView, NoInternetFoundView } from '../../custom/CustomComponents';
import { GreenTextBtn } from '../../custom/CustomButton';
import { CommonBtn } from '../../custom/CustomButton';
import HistoryRidesCell from '../../custom cells/HistoryRidesCell';
//@ts-ignore
import { connect } from 'react-redux';
import { Popup } from '../../custom/Popup';
import RequestManager from '../../Utils/RequestManager';
import { API_GET_PUBLISHED_RIDES, API_DRIVER_CHANGE_RIDE_STATUS } from '../../constants/apis';
import { internetConnectionError } from '../../constants/messages';
import Toast from 'react-native-simple-toast';
import { Loader } from '../../Utils/Loader';
import General from '../../Utils/General';
import RNGooglePlaces from 'react-native-google-places';
import { createRoute } from '../../Firebase/FirestoreHandler';
import { cancel } from 'redux-saga/effects';
import moment from 'moment';
import { PopupScreenType, NotificationsType } from '../../Utils/Enums';
import { RIDE_INFO } from '../../Redux/Constants';
import LocalDataManager from '../../Utils/LocalDataManager';
import { EventRegister } from 'react-native-event-listeners'

export interface props {
    navigation: NavigationScreenProp<any, any>,
    loginInfo: any,
    saveRidePaymentInfo: any
    notificationsInfo: any
};

class Home extends Component<props, object> {

    notificationListener: any = undefined

    state = {
        isUpcoming: true,
        showHome: true,
        historyRidesArr: [] as any[],
        upcomingRidesArr: [] as any[],
        showPopup: false,
        historyPage: 0,
        isRefreshing: false,
        loadMore: false,
        isCalled: false,
        historyPrevPage: -1,
        upcomingPage: 0,
        upcomingPrevPage: -1,
        showLoader: false,
        isLocationUpdateStarted: false,
        timerId: 0,
        lat: 0,
        long: 0,
        isInternetError: false,
        isDataNotFoundError: false,
        showCancelPopup: false,
        cancelIndex: -1,
        isAnyRideStarted: false,
        isHighlighted: false,
        isFocused: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>{navigation.getParam('title')}</Text>
            ),
            headerLeft: (
                <OpenDrawerHeaderBtn func={navigation.getParam('revealBtnHandler')} />
            ),
            headerRight: (
                <NotificationsHeaderBtn func={navigation.getParam('notificationsHandler')} isHighlighted={navigation.getParam('isHighlighted')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {


        console.log("Ride request received >>>>>", NotificationsType.rideCancelledByPassenger)
        console.log("Compoennet Did Mount func home called")
        this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'title': '', 'isHighlighted': this.state.isHighlighted });

        this.props.navigation.addListener('willFocus', (route) => this.refreshData());
        this.props.navigation.addListener('willBlur', (route) => this.screenBlurred());

        // this.notificationListener = EventRegister.addEventListener('notificationListener', (data) => {
        //   // this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'title': '', 'isHighlighted': data });
        // })
    }

    componentWillUnmount() {
        // EventRegister.removeEventListener(this.notificationListener)
    }

    componentWillReceiveProps(newProps: any) {
        console.log('Props Notifications Info Home >>>', newProps.notificationsInfo)
        console.log('is Highlighted Home>>>', newProps.notificationsInfo.highlight)
        console.log('this.state.ishigh;ighted Home>>>', this.state.isHighlighted)

        if (this.state.isHighlighted != newProps.notificationsInfo.highlight) {
            this.setState({ isHighlighted: newProps.notificationsInfo.highlight })
            this.props.navigation.setParams({ 'isHighlighted': newProps.notificationsInfo.highlight });
        }
    }

    revealBtnHandler() {
        this.props.navigation.openDrawer();
    }

    notificationsHandler() {
        this.props.navigation.navigate('Notifications')
    }

    private refreshData() {

        console.log("screen focused home")
        this.setState({ isFocused: true })
        this.checkForPaymentPopup()
        // this.checkForNewNotifications()
        console.log("Refresh data Home called")
        console.log('Show Home >>>>', this.state.showHome)
        if (this.state.showHome == false) {
            if (this.state.isUpcoming) {
                this.setState({ upcomingPage: 0, upcomingPrevPage: -1, upcomingRidesArr: [] })
            }
            else {
                this.setState({ historyPage: 0, historyPrevPage: -1, historyRidesArr: [] })
            }

            setTimeout(() => {
                this.apiGetUpcomingHistoryRides(true)
            }, 200)
        }
    }

    protected screenBlurred() {
        console.log("ScreenBlurred home")
        this.setState({ isFocused: false })
    }

    checkForPaymentPopup() {
        console.log('checkForPaymentPopup func called')
        LocalDataManager.getDataAsyncStorage('shouldShowPaymentPopup').then((data: any) => {
            console.log('Data Retrived from Storage checkForPaymentPopup>>>>', data)
            if (data != null && data == true) {
                // this.props.saveRidePaymentInfo({rideId: selectedRide.id})
                this.props.navigation.navigate('GreyPopup', { screenType: PopupScreenType.rideStarted })
            }
            else {
                // this.checkForNewNotifications()
            }
        })
    }

    checkForNewNotifications() {
        // Check if notificatiosj icon needs to be highlighted
        LocalDataManager.getDataAsyncStorage('newNotifications').then((data: any) => {
            console.log('Data Retrived from Storage componentDidMount Home>>>>', data)
            if (data != null) {

                this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'title': '', 'isHighlighted': data });
                this.setState({ isHighlighted: data })

            } else {
                this.props.navigation.setParams({ 'revealBtnHandler': () => this.revealBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'title': '', 'isHighlighted': false });
                this.setState({ isHighlighted: false })
            }
        })
    }

    upcomingHistoryBtnAction(upcoming: boolean) {
        console.log('ISupcoming >>>>', upcoming)
        if (this.state.isCalled) {
            return
        }

        if (upcoming) {
            this.setState({ isUpcoming: true, showHome: false })
        }
        else {
            this.setState({ isUpcoming: false, showHome: false })
        }

        setTimeout(() => {
            this.props.navigation.setParams({ 'title': 'Your rides' });
            this.apiGetUpcomingHistoryRides(false)
        }, 200)
    }

    offerRide() {
        console.log('Login Info in Home', this.props.loginInfo)

        if (this.props.loginInfo.firstName == '' || this.props.loginInfo.isVehicleInfoAdded == 0 || this.props.loginInfo.isBankDetailsAdded == 0) {
            this.setState({ showPopup: true })
        }
        else {
            this.props.navigation.navigate('PickUp')
        }
    }

    findRide() {

        console.log('Login Info in Home', this.props.loginInfo)

        if (this.props.loginInfo.firstName == '' || this.props.loginInfo.isVehicleInfoAdded == 0 || this.props.loginInfo.isBankDetailsAdded == 0) {
            this.setState({ showPopup: true })
        }
        else {
            this.props.navigation.navigate('Search')
        }
    }

    cellSelected(index: any) {
        if (this.state.isUpcoming) {
            console.log('upcomingRidesArr >>>', this.state.upcomingRidesArr, index)
            let selectedRide = this.state.upcomingRidesArr[index]
            console.log('Selected Ride >>>', selectedRide)

            // Passenger
            if (selectedRide.AccOrOff == 'Ride Requested') {
                this.props.navigation.navigate('BookingDetails', { rideId: selectedRide.id, afterBooking: true })
            }
            // Driver
            else {
                this.props.navigation.navigate('Passengers', { rideId: selectedRide.id })
            }
        }
        else {

        }
    }

    goToProfile() {
        this.setState({ showPopup: false })
        this.props.navigation.navigate('Profile')
    }

    noThanksBtnAction() {
        this.setState({ showPopup: false, showCancelPopup: false })
    }

    cancelRideBtnAction() {
        this.setState({ showCancelPopup: false })
        // Cal Cancel Ride API here 
        this.apiChangeRideSttaus(this.state.cancelIndex, true)
    }

    // Get paarmeters for Login API
    private getParams() {
        return {
            type: this.state.isUpcoming ? "1" : "2",
            fetch: 'b',
            page: this.state.isRefreshing ? 0 : (this.state.isUpcoming ? this.state.upcomingPage : this.state.historyPage),
            limit: "5",
        }
    }

    async apiGetUpcomingHistoryRides(refresh: boolean) {
        console.log('apiGetUpcomingHistoryRides called with params >>>', this.getParams())
        this.setState({ showLoader: true, isCalled: true, isInternetError: false, isDataNotFoundError: false })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_GET_PUBLISHED_RIDES, this.getParams())
            .then((response: any) => {
                console.log('REsponse of apiGetUpcomingHistoryRides API >>>>', response);
                weakSelf.setState({ showLoader: false })
                let ridesArrData: any[] = []

                if (this.state.isUpcoming) {

                    if (refresh) {
                        console.log('data refershed')
                        weakSelf.setState({ upcomingPage: 0, upcomingPrevPage: -1, upcomingRidesArr: [] })
                        ridesArrData = response.data

                        console.log('Rides data in refresh >>>>', ridesArrData)
                    }
                    else {
                        ridesArrData = this.state.upcomingRidesArr.concat(response.data)
                    }

                    setTimeout(() => {

                        console.log('Prev and next page >>>', this.state.upcomingPrevPage, this.state.upcomingPage)

                        weakSelf.setState({ showLoader: false, upcomingRidesArr: ridesArrData, loadMore: false, upcomingPrevPage: this.state.upcomingPage, isRefreshing: false })

                        setTimeout(() => {

                            console.log('Again Prev and next page >>>', this.state.upcomingPrevPage, this.state.upcomingPage)
                            if (weakSelf.state.upcomingRidesArr.length == 0) {
                                weakSelf.setState({ isInternetError: false, isDataNotFoundError: true })
                            }
                            else {
                                // Update current location
                                this.checkAndUpdateCurrentLocationOfDriver(-1)
                            }
                            weakSelf.setState({ isCalled: false })
                        }, 200)

                        if (response.data.length > 0) {
                            let nextPage = this.state.upcomingPage + 1
                            weakSelf.setState({ upcomingPage: nextPage })
                        }
                    }, 200)
                }
                else {
                    if (refresh) {
                        weakSelf.setState({ historyPage: 0, historyPrevPage: -1, historyRidesArr: [] })
                        ridesArrData = response.data
                    }
                    else {
                        ridesArrData = this.state.historyRidesArr.concat(response.data)
                    }

                    setTimeout(() => {

                        console.log('Prev and next page >>>', this.state.historyPrevPage, this.state.historyPage)

                        weakSelf.setState({ showLoader: false, historyRidesArr: ridesArrData, loadMore: false, historyPrevPage: this.state.historyPage, isRefreshing: false })

                        setTimeout(() => {

                            console.log('Again Prev and next page >>>', this.state.historyPrevPage, this.state.historyPage)
                            if (weakSelf.state.historyRidesArr.length == 0) {
                                weakSelf.setState({ isInternetError: false, isDataNotFoundError: true })
                            }
                            weakSelf.setState({ isCalled: false })
                        }, 200)

                        if (response.data.length > 0) {
                            let nextPage = this.state.historyPage + 1
                            weakSelf.setState({ historyPage: nextPage })
                        }
                    }, 200)
                }
            }).catch((error: any) => {
                this.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })

                if (error == internetConnectionError) {
                    this.setState({ isInternetError: true, isDataNotFoundError: false })
                }
                else if (error.message != null && error.message != undefined && error.message == "No Record Found.") {
                    this.setState({ isInternetError: false, isDataNotFoundError: true })
                }
                else {
                    General.showErroMsg(this, error)
                }
            })
    }

    leftBtnAction(index: any) {

        console.log('Left BtNctin Called')
        let selectedRide = this.state.upcomingRidesArr[index]

        // As passenger
        if (selectedRide.AccOrOff == 'Ride Requested') {
            // Nothing to do for passender on Pending btn Click

            // remove this -> Added temporary for payemnt Testing
            // Ride started
            if (selectedRide.status == 0) {

            }
        }
        // As Driver
        else {
            console.log('In Ride Requested Func')
            // status = 0 > ride started -> Number of seat booked will be shown here
            if (selectedRide.status == 0) {
                // Nothing on seat booked
            }
            // status = 1 > ride published but not started yet -> Cancel ride btn will be available
            else if (selectedRide.status == 1) {
                // Show Cancel Ride pop up
                this.setState({ showCancelPopup: true, cancelIndex: index })
            }
        }
    }

    rightBtnAction(index: any) {
        let selectedRide = this.state.upcomingRidesArr[index]
        console.log('selectedRide rightBtnAction >>>>>', selectedRide)

        // As passenger
        if (selectedRide.AccOrOff == 'Ride Requested') {

            // status =0 > ride started -> Track ride btn wil be available
            if (selectedRide.status == 0) {
                this.props.navigation.navigate('TrackRide', { rideInfo: this.state.upcomingRidesArr[index] })
            }
            // status = 1 > ride published but not started yet -> Tack Ride ride btn wil be disabled
            else if (selectedRide.status == 1) {
                // Nothing on when ride is published but not started
            }
        }
        // As Driver
        else {
            // status =0 > ride started -> Track ride btn wil be available
            if (selectedRide.status == 0) {
                this.props.navigation.navigate('TrackRide', { rideInfo: this.state.upcomingRidesArr[index], instance: this })
            }
            // status = 1 -> ride published but not started yet -> Start ride btn wil be available
            else if (selectedRide.status == 1) {

                if (this.state.isAnyRideStarted) {
                    Toast.show('One ride can bestarted at a time', Toast.SHORT);

                    return
                }

                // Check if curremt time passed
                let rideTime = selectedRide.ride_date_time
                let dateTimeMoment = moment(rideTime, 'YYYY-MM-DD HH:mm:ss')
                let isCurrentTimePassed = dateTimeMoment.subtract(10, 'minutes').isBefore()

                // If passed then driver can start ride
                if (isCurrentTimePassed) {
                    // Call Start Ride API here
                    this.apiChangeRideSttaus(index, false)
                }
            }
        }
    }

    updateCurrentLocation = (index: any) => {
        RNGooglePlaces.getCurrentPlace()
            .then((results) => {
                const { latitude, longitude } = results[0].location;
                this.setState({ lat: latitude, long: longitude })

                this.updateTracking(index)
            })
            .catch((error) => console.log(error.message));
    }

    updateTracking = (index: any) => {
        if (this.state.isLocationUpdateStarted == false) {
            this.setState({
                isLocationUpdateStarted: true
            })
            let timerId = setInterval(() => this.updateCurrentLocation(index), 2000);
            this.setState({ timerId: timerId })
        }

        let dictToSent = {
            accuracy: 14.33,
            bearing: 0,
            driverId: this.state.upcomingRidesArr[index].user_id,
            lat: this.state.lat,
            long: this.state.long,
            mkey: '',
            provider: 'fused',
            speed: 0,
            timeStamp: new Date().getTime()
        }

        console.log('DictTo Sent >>>>>>', dictToSent)

        createRoute(this.state.upcomingRidesArr[index].user_id, this.state.upcomingRidesArr[index].id, dictToSent)

        // .then((data: any) => {


        // }, (error: any) => {

        // })
    }

    //Will call when scroll view pulled down to refresh
    private refreshList() {
        this.setState({ isRefreshing: true })
        setTimeout(() => {
            this.apiGetUpcomingHistoryRides(true)
        }, 200)
    }

    //Will call when reached last cell
    private loadMoreData = () => {

        if (this.state.isUpcoming && this.state.upcomingPrevPage == this.state.upcomingPage) {
            console.log('Func Clledv >>>', this.state.upcomingPrevPage, this.state.upcomingPage)
            console.log('Func  return Clled')
            return

        }
        else if (this.state.isUpcoming == false && this.state.historyPrevPage == this.state.historyPage) {
            console.log('Func Clledv >>>', this.state.historyPrevPage, this.state.historyPage)
            console.log('Func  return Clled')
            return
        }

        this.setState({ loadMore: true, isCalled: true })
        console.log('Func Clled2')
        setTimeout(() => {
            console.log('Func Clled3')
            this.apiGetUpcomingHistoryRides(false)
        }, 200)
    }

    // Get parameters of Change Status of Ride Api (Cancel/Start Ride)
    getChangeRideStatusAPIParams(index: any, cancel: boolean) {

        let selectedRide = this.state.upcomingRidesArr[index]
        return {
            "rideid": selectedRide.id,
            "ridestatus": cancel ? 3 : 0 // 0 for start and 3 for Cancel
        }
    }

    // API tp send OTp on added Phone Number for verification
    apiChangeRideSttaus(index: any, cancel: boolean) {

        console.log('apiChangeRideStatus called with params >>>>', this.getChangeRideStatusAPIParams(index, cancel))
        RequestManager.postRequestWithHeaders(API_DRIVER_CHANGE_RIDE_STATUS, this.getChangeRideStatusAPIParams(index, cancel)).then((response: any) => {
            console.log('REsponse of apiChangeRideSttaus >>>>', response);
            this.setState({ showLoader: false })

            this.refreshList()
            this.checkAndUpdateCurrentLocationOfDriver(index)

            if (response.message == 'Ride Status Changed Successfully') {
                Toast.show(cancel ? 'Ride cancelled' : 'Ride started', Toast.SHORT);

                // If Ride started then set parameter to true
                if (cancel == false) {
                    this.setState({ isAnyRideStarted: true })
                }
            }
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    checkAndUpdateCurrentLocationOfDriver(index: any) {

        console.log('Index before >>>', index)
        // Check if any ride is started then start updating current location on firestore for Driver
        if (index == -1) {
            for (let i = 0; i < this.state.upcomingRidesArr.length; i++) {
                let ride = this.state.upcomingRidesArr[i]

                // As passenger
                if (ride.AccOrOff == 'Ride Requested') {
                    // Nothing to do for passender
                }
                // As Driver
                else {
                    console.log('In Ride Requested Func')
                    // status = 0 -> ride started
                    if (ride.status == 0) {
                        // Nothing on seat booked
                        index = i
                        break;
                    }
                }
            }
        }

        console.log('Index After >>>', index)

        if (this.state.timerId == 0) {
            if (index != -1) {
                console.log('Start Timer Called', this.state.timerId)
                this.updateCurrentLocation(index)
            }
        }
        else {
            console.log('Stop Timer Called', this.state.timerId)
            clearInterval(this.state.timerId)
            this.setState({ timerId: 0 })
        }
    }

    render() {

        let popupMsg = ''

        console.log('LoginInfo isVehicleInfoAdded>> ', this.props.loginInfo.isVehicleInfoAdded, popupMsg)

        if (this.props.loginInfo.firstName == '') {
            popupMsg = 'Please update your profile before publishing a ride'
        }
        else if (this.props.loginInfo.isVehicleInfoAdded == 0) {
            console.log(' isVehicleInfoAdded condition true>> ', this.props.loginInfo.isVehicleInfoAdded)
            popupMsg = 'Please add your vehicle information before publishing a ride'
        }
        else if (this.props.loginInfo.isBankDetailsAdded == 0) {
            popupMsg = 'Please add your bank information before publishing a ride'
        }

        console.log('LoginInfo showPOpup, pop up msg home>> ', this.state.showPopup, popupMsg)

        console.log('historyRidesArr >>.', this.state.historyRidesArr)
        console.log('upcomingRidesArr >>.', this.state.upcomingRidesArr)

        let ridesArr: any[] = []
        if (this.state.isUpcoming) {
            ridesArr = this.state.upcomingRidesArr
        }
        else {
            ridesArr = this.state.historyRidesArr
        }

        console.log('ridesArr >>.', ridesArr)

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showPopup}>
                    <Popup title={popupMsg} btnText='Go to Profile' greenBtnText='No, thanks' func={() => this.goToProfile()} greenBtnAction={() => this.noThanksBtnAction()} />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showCancelPopup}>
                    <Popup img={require('../../assets/cartoon.png')} title='Are you sure you want to cancel the ride?' btnText='Cancel ride' greenBtnText='No, thanks' func={() => this.cancelRideBtnAction()} greenBtnAction={() => this.noThanksBtnAction()} />
                </Modal>
                <View style={styles.container}>
                    <View style={{ flexDirection: 'row', marginTop: 24 }}>
                        <View style={styles.signBtnVw}>
                            <TouchableOpacity onPress={() => this.upcomingHistoryBtnAction(true)}>
                                <Text style={styles.upcomingHistoryTxt}>Upcoming</Text>
                                <View style={this.state.isUpcoming ? styles.activeBlueLine : styles.inactiveBlueLine} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.signBtnVw}>
                            <TouchableOpacity onPress={() => this.upcomingHistoryBtnAction(false)}>
                                <Text style={styles.upcomingHistoryTxt}>History</Text>
                                <View style={this.state.isUpcoming ? styles.inactiveBlueLine : styles.activeBlueLine} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {this.state.showHome ? <View style={{ alignItems: 'center' }}>
                        <Text style={styles.greyBoldText}>Going to a place?</Text>
                        <Text style={styles.greyText}>"Share the journey, and the cost"</Text>
                        <GreenTextBtn title='Offer a ride' func={() => this.offerRide()} fontSize={17} marginTop={50} />
                        <CommonBtn title='Find a ride' func={() => this.findRide()} marginTop={16} />
                    </View> :
                        (ridesArr.length > 0 ? <FlatList
                            showsVerticalScrollIndicator={false}
                            style={{ marginTop: 24, width: '100%', marginBottom: 8 }}
                            onRefresh={() => this.refreshList()}
                            refreshing={this.state.isRefreshing}
                            data={ridesArr}
                            // keyExtractor={(item: any, index: any) => index.toString()}
                            renderItem={({ item, index }: any) => <HistoryRidesCell
                                onClickEvent={(index: any) => this.cellSelected(index)} leftBtnAction={(index: any) => this.leftBtnAction(index)} rightBtnAction={(index: any) => this.rightBtnAction(index)}
                                item={item} index={index} isUpcoming={this.state.isUpcoming} loginUserId={this.props.loginInfo.id} />}
                            onEndReachedThreshold={0.5}
                            onScroll={({ nativeEvent }) => {
                                if (this.state.isCalled == false && General.isCloseToBottom(nativeEvent) && this.state.isRefreshing == false) {
                                    this.loadMoreData()
                                }
                            }}
                        /> : (this.state.showLoader ? null :
                            (this.state.isDataNotFoundError ?
                                <NoDataFoundView func={() => this.refreshData()} message='You have no recent ride offers.' /> :
                                (this.state.isInternetError ?
                                    <NoInternetFoundView func={() => this.refreshData()} /> : null))))}
                    {this.state.showHome ? <Image
                        source={require('../../assets/homeImg.png')}
                        style={styles.homeImg}
                        resizeMode='contain' /> : null}

                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    loginInfo: state.saveLoginInfo,
    notificationsInfo: state.saveNotificationsInfo
});

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveRidePaymentInfo: (info: string) => dispatch({ type: RIDE_INFO, payload: info }),
});


export default connect(mapStateToProps, mapDispatchToProps)(Home);
