import React, { Component } from 'react';
import { View, Text, Image, StatusBar, Platform, StatusBarIOS, NativeModules, KeyboardAvoidingView, Keyboard } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { TextInputWithImageFoucsEdit, TextInputWithBtn } from '../../custom/CustomTextInput';
import { TextGreyBold } from '../../custom/CustomText';
import Map from '../../Utils/Map';
import RNGooglePlaces from 'react-native-google-places';
import { ORIGIN_INFO, DESTINATION_INFO } from '../../Redux/Constants';
//@ts-ignore
import { connect } from 'react-redux';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';

export interface props {
    navigation: NavigationScreenProp<any, any>
    pickupInfo: any,
    saveOriginInfo: any,
    saveDestinationInfo: any
}

const { StatusBarManager } = NativeModules;

class Demo extends Component<props, object> {

    state = {
        statusBarHeight: 0,
        isOrigin: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Select Date & Time</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        console.log('Props >>>>', this.props)
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        console.log('iOrigin >>>',  this.props.navigation.getParam('isOrigin'))
        this.setState({ isOrigin: this.props.navigation.getParam('isOrigin')})
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <View style={styles.container}>
                    
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const mapStateToProps = (state: any) => ({
    pickupInfo: state.savePickUpInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
    saveOriginInfo: (info: string) => dispatch({ type: ORIGIN_INFO, payload: info }),
    saveDestinationInfo: (info: string) => dispatch({ type: DESTINATION_INFO, payload: info }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Demo);