import React, { Component } from 'react';
import { View, Text, SafeAreaView, Alert, Image, TouchableOpacity, ScrollView, KeyboardAvoidingView, Platform, StatusBar, NativeModules, StatusBarIOS, } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import { AirbnbRating } from 'react-native-ratings';
import { BorderBtn, GreenTextBtn } from '../../custom/CustomButton';
import { TextInputMultiLine } from '../../custom/CustomTextInput';
import { APP_SKY_BLUE_COLOR } from '../../constants/colors';
import { API_REVIEW, AVATAR_URL } from '../../constants/apis';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';

const { StatusBarManager } = NativeModules;

export interface props {
    navigation: NavigationScreenProp<any, any>
};

export default class DriverRating extends Component<props, object> {

    state = {
        carNeat: false,
        smoothDriving: false,
        driverBehaviour: false,
        fairPrice: false,
        rating: '3',
        review: '',
        statusBarHeight: 0
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            header: null
        }
    }

    componentDidMount() {
        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        console.log('This in componentDidMount >>', this)
    }

    crossBtnAction() {
        this.props.navigation.goBack()
    }

    optionsBtnAction(index: any) {

        if (index == 1) {
            this.setState({ carNeat: !this.state.carNeat })
        }
        else if (index == 2) {
            this.setState({ smoothDriving: !this.state.smoothDriving })
        }
        else if (index == 3) {
            this.setState({ driverBehaviour: !this.state.driverBehaviour })
        }
        else if (index == 4) {
            this.setState({ fairPrice: !this.state.fairPrice })
        }
    }

    submitBtnAction() {
        this.setState({ showLoader: true })
        this.callReviewAPI()
    }

    // paarmeters for API
    private getParams() {

        let ratingSelectedValues = []

        if (this.state.carNeat) {
            ratingSelectedValues.push('Car Neatness')
        }
        if (this.state.smoothDriving) {
            ratingSelectedValues.push('Smooth Driving')
        }
        if (this.state.driverBehaviour) {
            ratingSelectedValues.push('Driver\'s Behaviour')
        }
        if (this.state.fairPrice) {
            ratingSelectedValues.push('Fair Price')
        }

        let ratingSelectedValuesStr = ratingSelectedValues.toString()

        var params: any = {
            "type": "1",
            "ratings":
                [{
                    "ride_id": this.props.navigation.getParam('rideInfo').id,
                    "rating": this.state.rating,
                    "rating_select": ratingSelectedValuesStr,
                    "info": this.state.review,
                    "user_id_to": this.props.navigation.getParam('rideInfo').user_id,
                }]
        }

        return params
    }

    // Call API to update User Data
    private callReviewAPI() {
        console.log('callReviewAPI API called with params >>>', this.getParams())
        let weakSelf = this

        RequestManager.postRequestWithHeaders(API_REVIEW, this.getParams()).then((response: any) => {
            this.setState({ showLoader: false })
            General.showMsgWithDelay(response.message)
            this.props.navigation.navigate('Home')
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    private ratingChanged(_rating: any) {

        console.log('This >>', this)
        this.setState({ rating: _rating })
    }

    render() {
        let rideInfo = this.props.navigation.getParam('rideInfo')


        console.log('rideInfo in DRIVER RATING >>>>>>>', rideInfo)
        let image = rideInfo.user_details.image
        let url = AVATAR_URL + image

        console.log('URL IN DRIVER RATING >>>>', url)

        return (
            <SafeAreaView style={{ flex: 1 }}>
                <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                    <View style={styles.container}>
                        <View style={styles.whitePopupVw}>
                            <TouchableOpacity style={styles.crossBtn} activeOpacity={0.9} onPress={() => this.crossBtnAction()}>
                                <Image style={styles.crossImg} source={require('../../assets/cross.png')} />
                            </TouchableOpacity>
                            <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, width: '100%', marginTop: -8 }}>
                                <View style={styles.innerVw}>
                                    <TextGreyBold marginTop={8} fontSize={17} text='How was your ride?' width='70%' />
                                    <TextGrey marginTop={24} fontSize={12} text='Please provide your valuable feedback to us as it will helps us to improve the experience' textAlign='center' color='#4E4E50' />
                                    {rideInfo.user_details.image == '' ?
                                        <Image
                                            source={require('../../assets/avatar.png')}
                                            style={styles.profileImg}
                                            resizeMode='contain'
                                        /> :
                                        <Image style={styles.profileImg} source={{ uri: url }} />
                                    }
                                    <TextGrey marginTop={12} marginBottom={16} fontSize={16} text={rideInfo.user_details.firstname + rideInfo.user_details.lastname} textAlign='center' color='#494949' />
                                    <AirbnbRating
                                        showRating={false}
                                        onFinishRating={(rating: any) => this.ratingChanged(rating)}
                                        size={24} />
                                    <View style={[styles.optionsVw, { marginTop: 16 }]}>
                                        <BorderBtn title='Car Neatness' titleColor={this.state.carNeat ? 'white' : '#B6B6B6'} borderColor='#F6F5F6' func={(index: any) => this.optionsBtnAction(index)} width={120} height={30} marginTop={0} index={1} backgroundColor={this.state.carNeat ? APP_SKY_BLUE_COLOR : 'white'} />
                                        <BorderBtn title='Smooth Driving' titleColor={this.state.smoothDriving ? 'white' : '#B6B6B6'} borderColor='#F6F5F6' func={(index: any) => this.optionsBtnAction(index)} width={120} height={30} marginTop={0} index={2} backgroundColor={this.state.smoothDriving ? APP_SKY_BLUE_COLOR : 'white'} />
                                    </View>
                                    <View style={styles.optionsVw}>
                                        <BorderBtn title={'Driver\'s Behaviour'} titleColor={this.state.driverBehaviour ? 'white' : '#B6B6B6'} borderColor='#F6F5F6' func={(index: any) => this.optionsBtnAction(index)} width={120} height={30} marginTop={0} index={3} backgroundColor={this.state.driverBehaviour ? APP_SKY_BLUE_COLOR : 'white'} />
                                        <BorderBtn title='Fair Price' titleColor={this.state.fairPrice ? 'white' : '#B6B6B6'} borderColor='#F6F5F6' func={(index: any) => this.optionsBtnAction(index)} width={120} height={30} marginTop={0} index={4} backgroundColor={this.state.fairPrice ? APP_SKY_BLUE_COLOR : 'white'} />
                                    </View>
                                    <TextInputMultiLine placeholder='Write a review....' onChange={(text: any) => this.setState({ review: text })} value={this.state.review} fontSize={12} width='100%' height={100} />
                                    <GreenTextBtn title='Submit' func={() => this.submitBtnAction()} fontSize={16} marginTop={16} marginBottom={16} />
                                </View>
                            </ScrollView>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        );
    }
}