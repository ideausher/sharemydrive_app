//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#140F2693',
    },
    whitePopupVw: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: 'white',
        marginTop: 100,
        alignItems: 'center',
    },
    innerVw:{
        flex: 1,
       // backgroundColor: 'green',
        width: '70%',
        alignItems: 'center',
        marginTop: -8, 
        alignSelf: 'center'
    },
    crossBtn:
    {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginRight: 16,
        marginTop: 16,
    },
    crossImg: {
        height: 16,
        width: 16,
        resizeMode: 'contain',
    },
    profileImg: {
        marginTop: 16,
        height: 70,
        width: 70,
    },
    optionsVw:{
        width: '100%',
        //backgroundColor: 'red',
        flexDirection:'row',
        justifyContent: 'space-between',
        marginTop: 8,
        marginBottom: 8,
    },
});