//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD, APP_FONT_REGULAR } from '../../constants/fonts';
import { APP_GREY_TEXT_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#140F2693', 
        alignItems:'center',
        justifyContent:'center'
    },  
    popupVw: {
        margin: 16,
        borderRadius: 10,
        backgroundColor: 'white',
        width: '80%',
        alignItems:'center'
    },
    image: {
        width: 120,
        height:50,
        marginTop: 60,
    },  
});