import React, { Component } from 'react';
import { View, Image, StatusBar } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { GreenTextBtn } from '../../custom/CustomButton';

import { TextGreyBold, TextGrey } from '../../custom/CustomText';


export interface props {
    navigation: NavigationScreenProp<any, any>
}

export interface props {
    navigation: NavigationScreenProp<any, any>,
};

export default class PaymentCompleted extends Component<props, object> {

    state = {
        statusBarHeight: 0,
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            header: null
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
    }

    okayBtnHandler() {

        this.props.navigation.navigate('Home')
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.popupVw]}>
                    <Image source={require('../../assets/paymentCompleted.png')} style={styles.image} />
                    <TextGreyBold marginTop={36} fontSize={20} text='Payment completed' width='70%' />
                    <TextGrey marginTop={16} text='You have successfully paid for your ride' width='70%' />
                    <GreenTextBtn title='Okay' func={() => this.okayBtnHandler()} fontSize={15} marginTop={8} marginBottom={16} />
                </View>
            </View>
        )
    }
}
