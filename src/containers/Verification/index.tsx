import React, { Component } from 'react';
import { View, Text, KeyboardAvoidingView, StatusBarIOS, Platform, NativeModules, TextInput, Keyboard, Modal, Alert, ScrollView, StatusBar } from 'react-native';
import { } from '../../constants';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { CommonBtn, BlueTextBtn } from '../../custom/CustomButton';
import { Loader } from '../../Utils/Loader';
import { API_SIGNUP, API_SENDOTP, API_CHECK_OTP, API_CHECK_PHONE_OTP, API_FORGOT_PSWD } from '../../constants/apis';
//@ts-ignore
import { connect } from 'react-redux';
import { VerificationFor, ChangePswdScreenType } from '../../Utils/Enums';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { headerStyle, headerTitleStyle } from '../../common/styles';
import LocalDataManager from '../../Utils/LocalDataManager';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
import { LOGIN } from '../../Redux/Constants';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import Toast from 'react-native-simple-toast';


export interface props {
    navigation: NavigationScreenProp<any, any>
    saveLoginInfo: any,
};

const { StatusBarManager } = NativeModules;

class Verification extends Component<props, object> {

    state = {
        firstDigit: '',
        secondDigit: '',
        thirdDigit: '',
        fourthDigit: '',
        statusBarHeight: 0,
        showLoader: false,
        verificationFor: VerificationFor.signup,
        phoneNumber: ''
    }

    // Instances of Textinput  to use for next TextInput focus and dismiss Keyboard
    txtFirst: TextInput;
    txtSecond: TextInput;
    txtThird: TextInput;
    txtFourth: TextInput;

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: null,
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        console.log('Props >>>>', this.props)
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        this.setState({ verificationFor: this.props.navigation.getParam('verificationFor'), phoneNumber: this.props.navigation.getParam('phoneNumber') })
    }

    backBtnHandler() {
        this.props.navigation.goBack();
        //this.props.navigation.navigate('Login')
    }

    /// RESEND CODE APIS ////////

    // Called this func on resend Btn Click
    resendOTP() {
        console.log('Resend OTP func called');

        this.setState({ showLoader: true })

        if (this.state.verificationFor === VerificationFor.forgotPswd) {
            this.apiResendOTPForgotPswd()
        }
        else {
            this.apiResendOTPSignupEditPhone()
        }
    }

    // Get paramsters for Reesnd OTP SignUp API
    getResendOTPSignupEditPhoneParams() {

        var phone = this.props.navigation.getParam('phoneNumber')

        return {
            phone_country_code: phone.split(' ')[0],
            phone_number: phone.split(' ')[1]
        }
    }

    // Call API to Resend OTP SignUp and Edit Phone (Same APi for both)
    async apiResendOTPSignupEditPhone() {
        console.log('Send OTP params >>>>', this.getResendOTPSignupEditPhoneParams())
        RequestManager.postRequest(API_SENDOTP, this.getResendOTPSignupEditPhoneParams(), false).then((response: any) => {
            console.log('REsponse of Send OTP API >>>>', response);

            this.setState({ showLoader: false })
            //General.showMsgWithDelay(response.message)
            Toast.show(response.message, Toast.SHORT);
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    getResendOTPForgotPswdParams() {
        var phone = this.props.navigation.getParam('phoneNumber')
        return {
            phone_no: phone.split(' ')[1],
            phone_country_code: phone.split(' ')[0],
        }
    }

    async apiResendOTPForgotPswd() {

        RequestManager.postRequest(API_FORGOT_PSWD, this.getResendOTPForgotPswdParams(), false).then((response: any) => {

            console.log('REsponse of apiForgotPassword >>>>', response);

            this.setState({ showLoader: false })
            Toast.show(response.message, Toast.SHORT);
            //General.showMsgWithDelay(response.message)
        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    verifyNowBtnAction() {
        // this.props.navigation.navigate('ChangePswd')
        console.log('Sign up func called', this.state.verificationFor)

        let otp = this.state.firstDigit + this.state.secondDigit + this.state.thirdDigit + this.state.fourthDigit;
        if (otp.length < 4) {
            Alert.alert("Please Enter a valid OTP")
            return;
        }
        if (this.state.verificationFor == VerificationFor.payment) {
            this.props.navigation.navigate('BookingSuccess');
        }
        else if (this.state.verificationFor === VerificationFor.signup) {
            console.log('in Sign up condition')
            this.setState({ showLoader: true })
            this.apiSignup()
        }
        else if (this.state.verificationFor === VerificationFor.forgotPswd) {
            this.setState({ showLoader: true })
            this.apiVerifiyForgotPswdOTP()
        }
        else if (this.state.verificationFor === VerificationFor.editPhoneNumber) {
            this.setState({ showLoader: true })
            this.apiVerifiyEditPhoneOTP()
        }
    }

    // Call signup API with OTp verification
    async apiSignup() {
        LocalDataManager.getDataAsyncStorage('signupInfo').then((data: any) => {
            console.log('Data Retrived from Storage >>>>', data)
            if (data != null && data != {}) {

                let otp = this.state.firstDigit + this.state.secondDigit + this.state.thirdDigit + this.state.fourthDigit;
                data['otp'] = otp
            //    if(data['lastname']== undefined || data['lastname']== ""){
            //     data['lastname']=" "
            //    }

                RequestManager.postRequest(API_SIGNUP, data, true).then((response: any) => {
                    console.log('REsponse of Signup API >>>>', response);

                    this.setState({ showLoader: false })
                    this.saveLoginUserDetailsAndMove(response.data)
                }).catch((error: any) => {
                    General.showErroMsg(this, error)
                })
            }
        })
    }

    // After Login Save Login data in redux and Local Storage(for use after App kill) and Move to Home Screen
    saveLoginUserDetailsAndMove(data: any) {
        LocalDataManager.removeLocalData('signupInfo')
        LocalDataManager.saveDataAsyncStorage('loginInfo', JSON.stringify(data))
        this.props.saveLoginInfo(data);
        this.props.navigation.navigate('Home');
    }

    // Same parameters used for OTp verification for both Forgot pswd and Edit phone number case.
    getVerifyForgotPswdEditPhoneOTPParams() {
        var param = this.props.navigation.getParam('params');
        const id = param['id'];
        return {
            otp: this.state.firstDigit + this.state.secondDigit + this.state.thirdDigit + this.state.fourthDigit,
            id: id,
        }
    }

    // Parameters to send in next class in case of Update Password after forgot pswd
    getPassingParamsForUpdatePswd() {
        var param = this.props.navigation.getParam('params');
        const id = param['id'];
        return {
            changePswdScreenType: ChangePswdScreenType.updatePswd,
            id: id,
            otp: this.state.firstDigit + this.state.secondDigit + this.state.thirdDigit + this.state.fourthDigit,
        }
    }

    // Call API to Verify OTP in Forgot Pswd Case
    async apiVerifiyForgotPswdOTP() {
        RequestManager.postRequest(API_CHECK_OTP, this.getVerifyForgotPswdEditPhoneOTPParams(), false).then((response: any) => {

            this.setState({ showLoader: false })
            this.props.navigation.navigate('ChangePswd', { params: this.getPassingParamsForUpdatePswd() })

        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    // Call API to Verify OTP in Edit Phone Number Case
    async apiVerifiyEditPhoneOTP() {

        console.log('getVerifyForgotPswdEditPhoneOTPParams >>>>', this.getVerifyForgotPswdEditPhoneOTPParams())
        RequestManager.postRequestWithHeaders(API_CHECK_PHONE_OTP, this.getVerifyForgotPswdEditPhoneOTPParams()).then((response: any) => {

            console.log('Success Response of apiVerifiyEditPhoneOTP API >>>>', response);
            this.setState({ showLoader: false })

            let otp = this.state.firstDigit + this.state.secondDigit + this.state.thirdDigit + this.state.fourthDigit
            var phone = this.props.navigation.getParam('phoneNumber')
            this.props.navigation.state.params.func(phone.split(' ')[1], phone.split(' ')[0], otp)
            this.props.navigation.navigate('PersonalInfo');
        }).catch((error: any) => {
            console.log('Error Response of apiVerifiyEditPhoneOTP API >>>>', error);
            General.showErroMsg(this, error)
        })
    }

    render() {

        let title = 'Phone Verification'
        let enterCodeDesc = 'Please enter the OTP code sent'
        if (this.state.verificationFor == VerificationFor.forgotPswd) {
            title = 'Enter Code'
            enterCodeDesc = 'Enter the 4-digit code sent to'
        }

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <View style={styles.container}>
                    <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                        <View style={styles.scrollContainer}>
                            <TextGreyBold text={title} fontSize={23} marginTop={30} />
                            <TextGrey marginTop={16} fontSize={14} text={enterCodeDesc} />
                            {this.state.verificationFor == VerificationFor.forgotPswd ? <TextGreyBold text={this.state.phoneNumber} fontSize={12} marginTop={16} /> : null}
                            {/* <Text style={{ marginTop: 16, width: '' }}>
                                    <Text style={styles.greyText}>Enter sms code sent to your registered mobile number</Text>
                                    <Text style={[styles.greyBoldText, { fontSize: 12 }]}> {this.state.phoneNumber}</Text>
                                </Text> */}
                            <View style={styles.codeInputContainer}>
                                <TextInput ref={(input: any) => { this.txtFirst = input as any }} style={[styles.inputContainer]} placeholder='0' placeholderTextColor='#d9d9d9' keyboardType='numeric' value={this.state.firstDigit} onChangeText={(text: any) => { this.setState({ firstDigit: text }), ((text.length == 1) ? this.txtSecond.focus() : null) }} maxLength={1} returnKeyType="done" />

                                <TextInput ref={(input: any) => { this.txtSecond = input as any }} style={[styles.inputContainer]} placeholder='0' placeholderTextColor='#d9d9d9' keyboardType='numeric' value={this.state.secondDigit} onChangeText={(text: any) => { this.setState({ secondDigit: text }), ((text.length == 1) ? this.txtThird.focus() : null) }} maxLength={1} returnKeyType="done" />

                                <TextInput ref={(input: any) => { this.txtThird = input as any }} style={[styles.inputContainer]} placeholder='0' placeholderTextColor='#d9d9d9' keyboardType='numeric' value={this.state.thirdDigit} onChangeText={(text: any) => { this.setState({ thirdDigit: text }), ((text.length == 1) ? this.txtFourth.focus() : null) }} maxLength={1} returnKeyType="done" />

                                <TextInput ref={(input: any) => { this.txtFourth = input as any }} style={[styles.inputContainer]} placeholder='0' placeholderTextColor='#d9d9d9' keyboardType='numeric' value={this.state.fourthDigit} onChangeText={(text: any) => { this.setState({ fourthDigit: text }), ((text.length == 1) ? Keyboard.dismiss() : null) }} maxLength={1} returnKeyType="done" />
                            </View>
                            <View style={styles.verifyNow}>
                                <CommonBtn title='Verify now' func={() => this.verifyNowBtnAction()} />
                            </View>
                            <Text style={[styles.greyText, styles.didntReceiveCodeTxt]}>I didn't receive a code yet</Text>
                            <BlueTextBtn title='Resend Code' func={() => this.resendOTP()} marginTop={0} fontSize={14} bold={true} />

                        </View>
                    </ScrollView>
                </View>
            </KeyboardAvoidingView>
        );
    }
}

// Redux Func to Save Login Info in Redux
const mapDispatchToProps = (dispatch: any) => ({
    saveLoginInfo: (info: string) => dispatch({ type: LOGIN, payload: info }),
});

export default connect(null, mapDispatchToProps)(Verification);
