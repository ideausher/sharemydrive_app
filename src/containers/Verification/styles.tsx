//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { WINDOW_WIDTH } from '../../constants';
import { APP_NAVY_BLUE_COLOR, APP_GREY_TEXT_COLOR } from '../../constants/colors';
import { APP_FONT_REGULAR, APP_FONT_BOLD } from '../../constants/fonts';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    scrollContainer: {
        alignItems: 'center',
    },
    codeInputContainer: {
        marginTop: 50,
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
      inputContainer: {
        alignItems: 'center',
        height: 70,
        width: '18%',
        padding: 10,
        borderBottomColor: '#D8D8D8',
        borderBottomWidth: 1,
        textAlign: 'center',
        fontSize: 21,
        fontFamily: APP_FONT_REGULAR,
        color: '#4E4E50',
        
    },
    didntReceiveCodeTxt: {
        marginTop: 20
    },
    verifyNow:{
         marginTop: 30, width: '100%' 
    }
});