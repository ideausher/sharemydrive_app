import React, { Component } from 'react';
import { View, Text, Image, StatusBar, Platform, StatusBarIOS, NativeModules, KeyboardAvoidingView, Keyboard, ScrollView } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn, NotificationsHeaderBtn, NoDataFoundView } from '../../custom/CustomComponents';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
//@ts-ignore
import { connect } from 'react-redux';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';
import { Rating } from 'react-native-ratings';
import { API_USER_DETAILS_BY_ID, AVATAR_URL } from '../../constants/apis';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
import { APP_SKY_BLUE_COLOR } from '../../constants/colors';
import FastImage from 'react-native-fast-image';
import LocalDataManager from '../../Utils/LocalDataManager';
import { EventRegister } from 'react-native-event-listeners';

export interface props {
    navigation: NavigationScreenProp<any, any>
    notificationsInfo: any
}

const { StatusBarManager } = NativeModules;

const AllowedOptionsVw = (props: any) => {

    return (
        <View style={styles.allowedOptionsVw}>
            <Image
                source={props.img}
                style={styles.allowedOptionsImg}
                resizeMode='contain'
            />
            <TextGrey marginTop={0} text={props.title} textAlign='left' marginLeft={24} />
        </View>
    );
};

class ViewProfile extends Component<props, object> {

    notificationListener: any = undefined

    state = {
        statusBarHeight: 0,
        isOrigin: false,
        petAllowed: true,
        wificonnection: true,
        smoking: true,
        userDetails: {} as any,
        isAPICalledOnce: false,
        showLoader: false,
        isHighlighted: false
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>View Profile</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerRight: (
                <NotificationsHeaderBtn func={navigation.getParam('notificationsHandler')} isHighlighted={navigation.getParam('isHighlighted')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        console.log('Props >>>>', this.props)
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': this.state.isHighlighted });

        this.props.navigation.addListener('willFocus', (route) => this.refreshData());

        // this.notificationListener = EventRegister.addEventListener('notificationListener', (data) => {
        //     this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': data });
        // })

        this.apiGetUserDetailsById()
    }

    componentWillUnmount() {
        //EventRegister.removeEventListener(this.notificationListener)
    }

    componentWillReceiveProps(newProps: any) {
        console.log('Props Notifications Info  view profile>>>', newProps.notificationsInfo)
        console.log('is Highlighted  View profile>>>', newProps.notificationsInfo.highlight)
        console.log('this.state.ishigh;ighted View profile>>>', this.state.isHighlighted)

        if (this.state.isHighlighted != newProps.notificationsInfo.highlight) {
            this.setState({ isHighlighted: newProps.notificationsInfo.highlight })
            this.props.navigation.setParams({ 'isHighlighted': newProps.notificationsInfo.highlight });
        }
    }

    private refreshData() {

        console.log("Refresh data view profile called")

        // Check if notificatiosj icon needs to be highlighted
        LocalDataManager.getDataAsyncStorage('newNotifications').then((data: any) => {
            console.log('Data Retrived from Storage >>>>', data)
            if (data != null) {
                this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': data });
                this.setState({ isHighlighted: data })
            } else {
                this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler(), 'notificationsHandler': () => this.notificationsHandler(), 'isHighlighted': false });
                this.setState({ isHighlighted: false })
            }
        })
    }

    backBtnHandler() {
        this.props.navigation.goBack();
    }

    notificationsHandler() {
        this.props.navigation.navigate('Notifications')
    }

    // Get paarmeters for Login API
    private getParams() {
        return {
            id: this.props.navigation.getParam('rideInfo').user_id
        }
    }

    async apiGetUserDetailsById() {
        console.log('GET apiGetUserDetailsById called with params >>>', this.getParams())
        this.setState({ showLoader: true })
        let weakSelf = this

        //Get Login User's Detail
        RequestManager.getRequest(API_USER_DETAILS_BY_ID, this.getParams())
            .then((response: any) => {
                console.log('REsponse of apiGetUserDetailsById API >>>>', response);
                weakSelf.setState({ showLoader: false, userDetails: response.data, isAPICalledOnce: true })

            }).catch((error: any) => {
                General.showErroMsg(this, error)
            })
    }

    bookRide() {
        this.props.navigation.navigate('BookingDetails', { rideId: this.props.navigation.getParam('rideInfo').id, afterBooking: false })
    }

    render() {

        let userData: any = this.state.userDetails
        console.log('User data >>>', userData)

        let rideInfo = this.props.navigation.getParam('rideInfo')
        console.log('Ride Info >>>', rideInfo)

        let url = AVATAR_URL + userData.image

        return (
            userData.firstname != undefined ? <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, }} contentContainerStyle={{flexGrow: 1}}>
                <View style={styles.container}>
                    <View style={styles.imgNameContainer}>
                        {/* <Image
                            source={require('../../assets/avatar.png')}
                            style={styles.profileImg}
                            resizeMode='contain'
                        /> */}
                        {userData.image == '' ?
                            <Image
                                source={require('../../assets/avatar.png')}
                                style={styles.profileImg}
                                resizeMode='contain'
                            /> :
                            <FastImage style={styles.profileImg} source={{ uri: url }} />}
                        <View style={styles.nameContainer}>
                            <TextGrey marginTop={0} text={userData.firstname + ' ' + userData.lastname + ' ,' + userData.age} textAlign='left' color='#717173' />
                            <TextGrey marginTop={4} text={'Rides offered  ' + userData.No_of_rides_offered} textAlign='left' color={APP_SKY_BLUE_COLOR} fontSize={12} />
                            <TextGrey marginTop={0} text={'Rides taken  ' + userData.No_of_rides_taken} textAlign='left' color={APP_SKY_BLUE_COLOR} fontSize={12} />
                            {/* <TextGrey text='Location: Alice Spring' textAlign='left' color='#717173' /> */}
                        </View>
                    </View>
                    <View style={styles.locationContainer}>
                        <Image
                            source={require('../../assets/pickup.png')}
                            style={styles.image}
                            resizeMode='contain'
                        />
                        <View style={styles.originDestinationVw}>
                            <TextGreyBold marginTop={0} fontSize={14} text={rideInfo.start_location.city} textAlign='left' />
                            <TextGreyBold marginTop={0} fontSize={14} text={rideInfo.end_location.city} textAlign='left' />
                        </View>

                        <View style={styles.priceLocationContainer}>
                            {/* <Image source={require('../../assets/location.png')} style={styles.locImg} /> */}
                            <Text style={styles.priceTxt}>${rideInfo.price_per_seat}</Text>
                        </View>
                    </View>
                    <View style={styles.ratingVw}>
                        <TextGrey marginTop={0} text={userData.rating} textAlign='left' marginRight={8} />
                        <Rating
                            startingValue={userData.rating}
                            imageSize={16}
                            ratingCount={5}
                            isDisabled={true}
                            readonly={true}
                        />
                    </View>
                    <TextGreyBold marginTop={24} fontSize={14} text='About' textAlign='left' />
                    <TextGrey marginTop={8} text={userData.bio} textAlign='left' />

                    {/* <View style={styles.vehicleInfoVw}>
                        <TextGreyBold marginTop={0} fontSize={18} text={userData.car_model} textAlign='left' />
                        <Image
                            source={require('../../assets/greyCar.png')}
                            style={styles.carImg}
                            resizeMode='contain'
                        />
                    </View>
                    <View style={styles.vehicleInfoVw}>
                        <TextGreyBold marginTop={0} fontSize={18} text='Registered Year' textAlign='left' />
                        <TextGreyBold marginTop={0} fontSize={18} text={userData.register_year} textAlign='left' />
                    </View>
                    <View style={styles.vehicleInfoVw}>
                        <TextGreyBold marginTop={0} fontSize={18} text='Color' textAlign='left' />
                        <TextGreyBold marginTop={0} fontSize={18} text={userData.car_color} textAlign='left' />
                    </View> */}
                    {this.props.navigation.getParam('showBookRideBtn') == true ? <CommonBtn title='Book Ride' func={() => this.bookRide()} marginTop={75} /> : null}

                </View>
            </ScrollView> :
                (this.state.showLoader || this.state.isAPICalledOnce == false) ?
                    null :
                    <NoDataFoundView func={() => this.apiGetUserDetailsById()} showFilterBtn={true} />
        )
    }
}

const mapStateToProps = (state: any) => ({
    notificationsInfo: state.saveNotificationsInfo
});

export default connect(mapStateToProps)(ViewProfile);
