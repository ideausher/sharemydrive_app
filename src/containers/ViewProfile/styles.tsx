//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_FONT_BOLD, APP_FONT_REGULAR } from '../../constants/fonts';
import { APP_BORDER_GREY_COLOR } from '../../constants/colors';

export const styles = ScaleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        padding: 16
    },
    imgNameContainer: {
        flexDirection: 'row',
        marginTop: 8,
        alignItems: 'center',
        // backgroundColor:'red'
    },
    nameContainer: {
        marginLeft: 8
    },
    profileImg: {
        height:50,
        width: 50,
        borderRadius: 25,
        backgroudColor: '#717173'
    },
    locationContainer: {
        flexDirection: 'row',
        marginTop: 24,
        //backgroundColor:'red',
        height: 80
    },
    image: {
        height: 80,
    },
    originDestinationVw: {
        flex: 1,
        justifyContent: 'space-between',
        marginLeft: 8,
        marginRight: 8,
        // backgroundColor:'orange'
    },
    priceLocationContainer: {
        justifyContent: 'flex-end',
        width: 80,
        // backgroundColor:'green'
    },
    locImg: {
        width: 30,
        height: 30,
        //backgroundColor:'green'
    },
    priceTxt: {
        fontSize: 19,
        fontFamily: APP_FONT_BOLD,
        color: '#2785FE',
        textAlign: 'right',
    },
    ratingVw: {
        marginTop: 16,
        flexDirection: 'row'
    },
    allowedVw: {
        marginTop: 16,
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderColor: APP_BORDER_GREY_COLOR,
        // backgroundColor:'green'
    },
    allowedOptionsVw: {
        marginTop: 16,
        marginBottom: 16,
        flexDirection: 'row',
        alignItems: 'center',
        //backgroundColor:'red'
    },
    allowedOptionsImg: {
        width: 24,
        height: 24,
    },
    vehicleInfoVw: {
        marginTop: 24,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        //backgroundColor:'red'
    },
    carImg: {
        width: 36,
        height: 12,
    }
});
