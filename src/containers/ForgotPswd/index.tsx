import React, { Component } from 'react';
import { View, StatusBar, Platform, StatusBarIOS, NativeModules, KeyboardAvoidingView, Modal, TouchableOpacity, Text } from 'react-native'
import { NavigationScreenProp } from "react-navigation";
import { styles } from './styles';
import { CommonBtn } from '../../custom/CustomButton';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { TextInputPhoneNum } from '../../custom/CustomTextInput';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import { navHdrTxtStyle, headerStyle, headerTitleStyle } from '../../common/styles';

import { VerificationFor, ForGotScreenType } from '../../Utils/Enums';
import { Loader } from '../../Utils/Loader';
import General from '../../Utils/General';
import { APP_GREY_BOLD_TEXT_COLOR } from '../../constants/colors';
//@ts-ignore
import { CountrySelection } from 'react-native-country-list';
import Toast from 'react-native-simple-toast';
//@ts-ignore
import validator from 'validator';
import RequestManager from '../../Utils/RequestManager';
import { API_FORGOT_PSWD, API_SENDOTP } from '../../constants/apis';

export interface props {
    navigation: NavigationScreenProp<any, any>
    pickupInfo: any,
    saveOriginInfo: any,
    saveDestinationInfo: any
}

const { StatusBarManager } = NativeModules;

export default class ForgotPswd extends Component<props, object> {

    state = {
        statusBarHeight: 0,
        countryCode: '61',
        flag: undefined,
        phoneNumber: '',
        showCodeList: false,
        showLoader: false,
        forgotScreenType: ForGotScreenType.forgotPswd,
        oldPhoneNumber: '',
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
           
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Forgot Password</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        console.log('Props >>>>', this.props)
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }

        this.setState({ forgotScreenType: this.props.navigation.getParam('forgotScreenType') })

        if (this.props.navigation.getParam('forgotScreenType') == ForGotScreenType.editPhoneNumber) {
            //this.setState({ oldPhoneNumber: this.props.navigation.getParam('params').phone, countryCode: this.props.navigation.getParam('params').countryCode })
            this.setState({ oldPhoneNumber: this.props.navigation.getParam('params').phone })
        }
    }

    backBtnHandler() {
        console.log('backBtnHandler executed')
        this.props.navigation.goBack();
    }

    // Called to show Country Code picker
    showCountryCodeList() {
        this.setState({ showCodeList: true })
    }

    // Caleld when Country Code is selected and save selected country code in state
    countryCodeSelcted(item: any) {

        this.setState({ countryCode: item.callingCode, showCodeList: false, flag: item.flag })
    }

    validateDetail() {
        if (this.state.countryCode.length <= 0) {
            Toast.show('Please enter country code', Toast.SHORT);
            return false;
        }
        else if (this.state.phoneNumber.length <= 0) {
            Toast.show('Please enter phone number', Toast.SHORT);
            return false;
        }
        else if (this.state.phoneNumber.length < 8) {
            Toast.show('Please enter valid phone number', Toast.SHORT);
            return false;
        }
        // else if (!validator.isMobilePhone(this.state.phoneNumber, 'any', { strictMode: false })) {
        //     Toast.show('Please enter valid phone number', Toast.SHORT);
        //     return false;
        // }
        else if (this.state.phoneNumber == this.state.oldPhoneNumber) {
            Toast.show('New number is same as previous one', Toast.SHORT);
            return false;
        }
        return true;
    }

    continueBtnAction() {
        if (this.validateDetail()) {
            this.setState({ showLoader: true })

            if (this.state.forgotScreenType == ForGotScreenType.editPhoneNumber) {
                this.apiSendOTP()
            }
            else {
                this.apiForgotPassword()
            }
        }
    }

    getForgotPswdParams() {
        return {
            phone_no: this.state.phoneNumber,
            phone_country_code: this.state.countryCode
        }
    }

    async apiForgotPassword() {

        console.log('Api forgot pswd func called with params >>>>', this.getForgotPswdParams())

        RequestManager.postRequest(API_FORGOT_PSWD, this.getForgotPswdParams(), false).then((response: any) => {

            console.log('REsponse of apiForgotPassword >>>>', response);

            this.setState({ showLoader: false })
            this.props.navigation.navigate('Verification', { params: { id: response.data.id }, verificationFor: VerificationFor.forgotPswd, phoneNumber: this.state.countryCode + ' ' + this.state.phoneNumber })


        }).catch((error: any) => {
            General.showErroMsg(this, error)
        })
    }

    getOTPParams() {
        return {
            phone_country_code: this.state.countryCode,
            phone_number: this.state.phoneNumber,
        }
    }

    apiSendOTP() {

        RequestManager.postRequest(API_SENDOTP, this.getOTPParams(), false).then((response: any) => {
            console.log('REsponse of Send OTP API >>>>', response);

            this.setState({ showLoader: false })

            setTimeout(() => {
                this.props.navigation.navigate('Verification', { params: { id: response.data.id }, verificationFor: VerificationFor.editPhoneNumber, phoneNumber: this.state.countryCode + ' ' + this.state.phoneNumber, func: this.props.navigation.getParam('func') })
            }, 200);
        }).catch((error) => {
            General.showErroMsg(this, error)
        })
    }

    render() {

        let title = 'Forgot password?'
        let msg = 'Please enter your registered mobile number for password recovery.'

        if (this.state.forgotScreenType == ForGotScreenType.editPhoneNumber) {
            title = 'Phone number'
            msg = 'Please enter your new mobile number.'
        }

        return (
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={(Platform.OS === 'android') ? -200 : this.state.statusBarHeight + 44} style={{ height: '100%', width: '100%', flexDirection: 'column', justifyContent: 'flex-start' }} enabled>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showLoader}>
                    <Loader />
                </Modal>
                <Modal
                    animated={true}
                    transparent={true}
                    visible={this.state.showCodeList}>
                    <View style={{ flex: 1, marginTop: 20 }}>
                        <TouchableOpacity
                            style={{ backgroundColor: 'white' }}
                            onPress={() => this.setState({ showCodeList: false })}>
                            <Text style={{ alignSelf: 'flex-end', lineHeight: 30, width: 60, textAlign: 'center', fontSize: 14, color: APP_GREY_BOLD_TEXT_COLOR, marginRight: 8 }}>Cancel</Text>
                        </TouchableOpacity>
                        <CountrySelection action={(item: any) => this.countryCodeSelcted(item)} selected={true} />
                    </View>

                </Modal>
                <View style={styles.container}>
                    <TextGreyBold marginTop={36} fontSize={20} text={title} width='70%' />
                    <TextGrey marginTop={16} fontSize={14} text={msg} textAlign='center' width='70%' />
                    <TextInputPhoneNum func={(text: any) => this.showCountryCodeList()} value={this.state.phoneNumber} placeholder='Mobile Number' img={this.state.flag == undefined ? require('../../assets/flag.png') : { uri: this.state.flag }} countryPlaceholder='+61' countryCode={this.state.countryCode} marginTop={70} fontSize={12} onChange={(text: any) => this.setState({ phoneNumber: text })} />
                    <CommonBtn title='Continue' func={() => this.continueBtnAction()} marginTop={70} />
                </View>
            </KeyboardAvoidingView>
        )
    }
}


