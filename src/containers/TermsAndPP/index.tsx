import React, { Component } from 'react';
import { View, Text, SafeAreaView ,StatusBar,Platform,NativeModules,StatusBarIOS} from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { headerStyle, headerTitleStyle,navHdrTxtStyle } from '../../common/styles';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import RequestManager from '../../Utils/RequestManager';
import General from '../../Utils/General';
import { API_TERMS_COND } from '../../constants/apis';


export interface props {
    navigation: NavigationScreenProp<any, any>
};


const { StatusBarManager } = NativeModules;

export default class TermsAndPP extends Component<props, object> {


    state = {
        statusBarHeight: 0,
        showLoader: false,
        message :''
    }


    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle:  (
                <View >
                  <Text style={navHdrTxtStyle.style}>Terms &amp; Conditions</Text>
                </View>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight((statusBarFrameData: any) => {
                this.setState({ statusBarHeight: statusBarFrameData.height });
            });
            StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData: any) => {
                this.setState({ statusBarHeight: statusBarData.frame.height });
            });
        }
        this.getTermsAndCondition()
    }

    backBtnHandler() {
        this.props.navigation.navigate('Home');
    }


    private getTermsAndCondition() {
        let weakSelf = this
        this.setState({ showLoader: true })
        RequestManager.getRequest(API_TERMS_COND,{'language':'en'}).then((response: any) => {
            this.setState({ showLoader: false })
           // General.showMsgWithDelay(response.message)
           this.setState({...this.state,message:response.data})
           console.log("start >> response terms >>",response);
           
        }).catch((error) => {
          //  General.showErroMsg(this, error)
          console.log("start >> Error terms >>",error);
        })
    }
    // notificationsHandler() {
    //     this.props.navigation.navigate('Notifications')
    // }

    // cellSelected(index: any) {
    //     //this.props.navigation.navigate('Feedback');
    //     this.props.navigation.navigate('ChatScreen');
    // }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.blueConatiner}>
                    <View style={styles.container}>
                            <Text style={{padding:10}}>{this.state.message}</Text>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}