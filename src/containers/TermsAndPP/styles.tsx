//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';

export const styles = ScaleSheet.create({
    blueConatiner: {
        flex: 1,
        backgroundColor: 'white'
    },
    container: {
        flex: 1,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,        
        backgroundColor: 'white',
    },
});