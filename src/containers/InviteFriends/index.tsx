import React, { Component } from 'react';
import { View, Text, SafeAreaView, StatusBar, Image } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { styles } from './styles';
import { BackHeaderBtn } from '../../custom/CustomComponents';
import { headerStyle, headerTitleStyle, navHdrTxtStyle, commonShadowStyle } from '../../common/styles';
import { TextGreyBold, TextGrey } from '../../custom/CustomText';
import { APP_GREY_BOLD_TEXT_COLOR } from '../../constants/colors';
import { CommonBtn } from '../../custom/CustomButton';
import Share from 'react-native-share';
//@ts-ignore
import { connect } from 'react-redux';

export interface props {
    navigation: NavigationScreenProp<any, any>
    loginInfo: any
};

class InviteFriends extends Component<props, object> {

    state = {
    }

    static navigationOptions = ({ navigation }: any) => {
        return {
            headerTitle: (
                <Text style={navHdrTxtStyle.style}>Invite friends</Text>
            ),
            headerLeft: (
                <BackHeaderBtn func={navigation.getParam('backBtnHandler')} />
            ),
            headerStyle: headerStyle.style,
            headerTitleStyle: headerTitleStyle
        }
    }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.props.navigation.setParams({ 'backBtnHandler': () => this.backBtnHandler() });

    }

    backBtnHandler() {
        this.props.navigation.navigate('Home');
    }

    private generateCode() {
        let chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
        var randomNum = '';
        for (var i = 6; i > 0; --i)
        {
            randomNum += chars[Math.floor(Math.random() * chars.length)];
        } 

        console.log('Random Num >>>', randomNum)

        return randomNum
    }

    inviteFriends() {
        console.log("start >>> props",this.props.loginInfo);
        
        // const url = 'https://awesome.contents.com/';
        const title = 'Invite friends';
        const shareOptions = {
            title,
            subject: title,
            message: (this.props.loginInfo.referralText.replace("[]",this.props.loginInfo.referralCode ))+"Please download the app Android :- https://play.google.com/store/apps/details?id=com.facebook.katana, IOS:- https://play.google.com/store/apps/details?id=com.facebook.katana",
        }

        Share.open(shareOptions);
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.container}>
                    <Image
                        source={require('../../assets/gift.png')}
                        style={styles.giftImg}
                        resizeMode='contain' />
                    <TextGreyBold marginTop={0} fontSize={30} text='Invite friends' />
                    <TextGrey marginTop={16} fontSize={12} text={'When your friend sign up with your referral code you\'ll both get reward points'} color={APP_GREY_BOLD_TEXT_COLOR} marginLeft={30} marginRight={30} />
                    <TextGrey marginTop={50} fontSize={16} text='Share Your Invite Code' color={APP_GREY_BOLD_TEXT_COLOR} textAlign='left' width='90%' />
                    <TextGreyBold marginTop={16} fontSize={24} text={this.props.loginInfo.referralCode} textAlign='left' width='90%' />
                    <View style={[styles.line, commonShadowStyle]} />
                    <CommonBtn title='Invite friends' func={() => this.inviteFriends()} marginTop={60} />
                </View>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = (state: any) => ({
    loginInfo: state.saveLoginInfo,
});

export default connect(mapStateToProps)(InviteFriends);