//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';

export const styles = ScaleSheet.create({
       container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems:'center'
    },
    giftImg:{
        width: 230,
        height: 200,
        marginTop: 40
    },
    line:{
        marginTop: 10,
        backgroundColor: 'rgba(232, 230, 241, 0.56)',
        height: 2,
        width: '90%',
        alignSelf: 'center'
    }
});