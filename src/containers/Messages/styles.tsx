//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';
import { APP_BORDER_GREY_COLOR, APP_GREY_BOLD_TEXT_COLOR, App_MIDIUM_GREY_COLOR, APP_LIGHT_GREY_COLOR, APP_GREEN_COLOR } from '../../constants/colors';
import { Platform } from 'react-native';
import { APP_FONT_REGULAR } from '../../constants/fonts';

export const styles = ScaleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  flatListStyle:{
    flex: 1, width: '100%',marginBottom:  (Platform.OS === "ios" ? 58 : 68), 
  },
  textVwCntnr: {
    width: '100%',
    height: (Platform.OS === "ios" ? 50 : 60),
    padding: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    position:'absolute',
    bottom :0,
    zIndex:1
    //backgroundColor: 'pink',
  },
  // //Text Styles
  // msgTxtInpt: {
  //   width: '75%',
  //   height: '100%',
  //   backgroundColor: "red",//'#fff',
  //   borderRadius: 20,
  //   paddingLeft: 8,
  //   alignSelf:'center',
  //   //textAlignVertical: 'top'
  // },
  // msgTxtInpt: {
  //   paddingHorizontal: 20,
  //   paddingVertical: 10,
  //   fontSize: 18,
  //   flex: 1,
  // },
  msgTxtInpt: {
    flex: 1,
    paddingHorizontal: 10,
    paddingVertical: 10,
    textAlign: 'left',
    height: '100%',
    fontSize: 12,
    fontFamily: APP_FONT_REGULAR,
    color: APP_GREY_BOLD_TEXT_COLOR,
    // Android only Property
    textAlignVertical: 'top',
    backgroundColor: '#fff',
    borderRadius: 10,
    borderColor: APP_LIGHT_GREY_COLOR,
    borderWidth: 1
  },
  //Button Touch Styles
  sendBtn: {
    width: 35,
    height: '80%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  //Image Styles
  sendImg: {
    height: '80%',
    width: '80%',
    resizeMode: 'contain',
  },
  fullImageContainer: {
    flex: 1,
    //justifyContent: 'center',
    alignItems: 'center',
    padding: 32,
    backgroundColor: "#140F2693",
  },
  crossBtn: {
    backgroundColor: 'white',
    marginTop: 16,
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
   // alignSelf: 'flex-end'
  },
  crossText: {
    fontSize: 20,
    alignText: 'center',
    fontFamily: APP_FONT_REGULAR,
    color: APP_GREEN_COLOR,
  },
  fullImage: {
    flex: 1,
   // marginTop: 16,
    width: "100%",
    backgroundColor: '#717173'
  },
});
