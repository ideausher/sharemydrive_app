import React from 'react';
import { Dimensions, StyleSheet, Image } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker, } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

const CustomMap = (props: any) => {

    { props.currentLocation != null ? console.log(props.currentLocation) : null }
    return (
        <MapView
            provider={PROVIDER_GOOGLE}
            style={[styles.map, (props.height != null ? { height: props.height } : null), (props.marginTop != null ? { marginTop: props.marginTop } : null)]}
            showsUserLocation={true}
            region={{
                latitude: ((props.startPoint.latitude != null) ? props.startPoint.latitude : 0),
                longitude: ((props.startPoint.longitude != null) ? props.startPoint.longitude : 0),
                latitudeDelta: 0.015 * 30,
                longitudeDelta: 0.0121 * 30,

            }}
            onPress={event => props.func(event.nativeEvent.coordinate)} >
            <Marker
                coordinate={{
                    latitude: props.startPoint.latitude, longitude: props.startPoint.longitude
                }}
                title={props.startPoint.city}
                description='Start Point'
            >
                <Image source={require('../assets/carWhite.png')} style={{ height: 35, width: 35 }} />
            </Marker>

            <Marker
                coordinate={{
                    latitude: props.endPoint.latitude, longitude: props.endPoint.longitude
                }}
                title={props.endPoint.city}
                description='End Point'
            >
                <Image source={require('../assets/endPoint.png')} style={{ height: 35, width: 35 }} />
            </Marker>

            {/* {props.markers.map((marker: any) => (
                <Marker
                    coordinate={{
                        latitude: marker.lat, longitude: marker.long
                    }}
                    title={marker.state}
                >
                    <Image source={require('../assets/midPoint.png')} style={{ height: 35, width: 35 }} />
                </Marker>
            )
            )} */}

            <MapViewDirections
                origin={{ latitude: props.startPoint.latitude, longitude: props.startPoint.longitude }}
                destination={{ latitude: props.endPoint.latitude, longitude: props.endPoint.longitude }}
                apikey={'AIzaSyAYs5C1yyuhTqPrUftQLi15cOlUAOc16d4'}
                strokeWidth={6}
                strokeColor='#03E1EA'
            />

            {/* {props.trailArr.map((marker: any) => (
                <Marker
                    coordinate={{
                        latitude: marker.lat, longitude: marker.long
                    }}
                    >
                    <Image source={require('../../assets/rec.png')} style={{ height: 20, width: 20 }} />
                </Marker>
                )
            )}   */}
        </MapView>
    )
}

export default CustomMap;

const styles = StyleSheet.create({
    map: {
        //flex: 1,
        width: '100%',
        // marginBottom: 0,
        marginTop: 20
    }
})
