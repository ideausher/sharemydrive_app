import React from 'react';
import { View, Text, Dimensions } from 'react-native';
import { BackHeaderBtn } from '../custom/CustomComponents';
//@ts-ignore
import ScaleSheet from 'react-native-scalesheet';

export const NavigationWithBack = (props: any) => {

  console.log('Props in NavigationWithBack>>>>', props);
  
  return ({
    headerTitle: (
        <Text style={navHdrTxtStyle.style}>{props.title}</Text>
    ),
    headerLeft: (
      <BackHeaderBtn func={props.func} />
    ),
    headerStyle: headerStyle.style,
    headerTitleStyle: headerTitleStyle
  })
}

const navHdrTxtStyle = {
  textAlign: 'center',
  color: 'grey',
  fontWeight: 'bold',
  fontSize: 18,
  width: Dimensions.get('screen').width - 120,
  backgroundColor:'red'
}

const headerTitleStyle = {
  alignSelf: 'center',
  textAlign: "center",
  justifyContent: 'center',
  flex: 1,
  fontWeight: 'bold',
  textAlignVertical: 'center'
}

const headerStyle = ScaleSheet.create({
  style: {
    height: 110,
    elevation: 0,
    borderBottomWidth: 0,
  },
})