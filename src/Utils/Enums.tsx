
export enum VerificationFor {
    signup,
    payment,
    forgotPswd,
    editPhoneNumber,
}

export enum ChangePswdScreenType {
    updatePswd,
    changePswd,
}

export enum ForGotScreenType {
    forgotPswd,
    editPhoneNumber,
}

export enum PopupScreenType {
    ridePublished,
    rideStarted,
}

export enum NotificationsType {
    rideRequestReceived = "0",
    rideStatusPending = "1",
    rideAccepted = "2",
    rideRejected = "3",
    rideCompleted = "4",
    rideCancelledByDriver = "7",
    rideRequestForRide = "8",
    rideCancelledByPassenger = "9",
    offerNotification = "12",
}