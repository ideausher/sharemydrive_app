import React, { Component } from 'react';
import { Alert, PermissionsAndroid, NativeModules, Platform} from 'react-native';
import Toast from 'react-native-simple-toast';
//import { IOS } from 'react-native-permissions/lib/typescript/constants';
import { getDistance, getPreciseDistance } from 'geolib';
import moment from 'moment';
import { internetConnectionError } from '../constants/messages';
import LocalDataManager from './LocalDataManager';

;



export default class General {

    static getLocale() {
        console.log('Before getting Locale >>>')

        if (Platform.OS === 'ios') {
            let locale = NativeModules.SettingsManager.settings.AppleLocale
            console.log('Locale >>>', locale)
            if (locale === undefined) {
                // iOS 13 workaround, take first of AppleLanguages array  ["en", "en-NZ"]
                locale = NativeModules.SettingsManager.settings.AppleLanguages[0]
                console.log('Locale >>>', locale)
                if (locale == undefined) {
                    locale = "en" // default language
                }
            }

            if (locale.includes('_')) {
                let splits = locale.split('_')
                let firstObject = splits[0]
                locale = firstObject
            }

            console.log('Locale >>>', locale)

            return locale
        }
        else {
            let locale = NativeModules.I18nManager.localeIdentifier

            let splits = locale.split('_')
            let firstObject = splits[0]
            console.log('Locale >>>', locale)
            console.log('splits >>>', splits)
            console.log('firstObject >>>', firstObject)

            return firstObject
        }
    }

    static showErroMsgInternetNoData(instance: any, error: any) {

        console.log('Error REsponse in showErroMsgInternetNoData >>>>', error);

        instance.setState({ showLoader: false, loadMore: false, isCalled: false, isRefreshing: false })
        setTimeout(() => {

            if (error == internetConnectionError) {
                console.log('internetConnectionError conditions set true >>>>');

                instance.setState({ isInternetError: true, isDataNotFoundError: false })
            }
            else if (error.message != null && error.message != undefined && error.message == "No Record Found.") {

                console.log('No Record Found conditions set true >>>>');
                instance.setState({ isInternetError: false, isDataNotFoundError: true })
            }
            else if (error.message == null || error.message == undefined) {

                if (error == 'Unauthorized.') {
                    console.log('In Unauthorized condition >>>', instance)
                    LocalDataManager.removeAllLocalData()
                    instance.props.navigation.navigate('Login')
                    // instance.props.saveInitialPickupInfo()
                    // instance.props.saveInitialSearchInfo()
                }

                Toast.show((error), Toast.SHORT);
            }
            else {
                Toast.show((error.message), Toast.SHORT);
            }
        }, 400);
    }

    static showErroMsg(instance: any, error: any) {

        console.log('Error REsponse in showErroMsg >>>>', error);

        instance.setState({ showLoader: false })
        setTimeout(() => {

            if (error.message == null || error.message == undefined) {

                if (error == 'Unauthorized.') {
                    console.log('In Unauthorized condition >>>', instance)
                    LocalDataManager.removeAllLocalData()
                    instance.props.navigation.navigate('Login')
                    //instance.props.saveInitialPickupInfo()
                    //instance.props.saveInitialSearchInfo()
                }

                Toast.show((error), Toast.SHORT);
            }
            else {
                Toast.show((error.message), Toast.SHORT);
            }
        }, 400);
    }

    static showMsgWithDelay(msg: any) {
        setTimeout(() => {
             Alert.alert(msg)
        }, 200);
    }

    static isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }: any) => {
        const paddingToBottom = 50; // how far from the bottom

        // console.log('Layout Measurement >>', layoutMeasurement.height)
        // console.log('contentOffset >>', contentOffset)
        // console.log('contentSize >>', contentSize)
        let value1 = layoutMeasurement.height + contentOffset.y
        let value2 = contentSize.height - paddingToBottom
        let isVAlue2GreaterThanLayout = value2 > layoutMeasurement.height
        let value1GreterthanValue2 = value1 >= value2
        let isClose = (isVAlue2GreaterThanLayout && value1GreterthanValue2)

        // console.log('Value1 >>> ', value1)
        // console.log('Value2 >>> ', value2)
        // console.log('isVAlue2GreaterThanLayout >>', isVAlue2GreaterThanLayout)
        // console.log('value1GreterthanValue2 >>', value1GreterthanValue2)
        // console.log('IS Close >>>', isClose)
        return isClose
    };

    static getEndTime(originLat: any, originLong: any, destinationLat: any, destinationLong: any, dateTime: any) {
        let distance = getPreciseDistance(
            { latitude: originLat, longitude: originLong },
            { latitude: destinationLat, longitude: destinationLong },
            0.01
        );

        // console.log('distance >>>', distance)
        let timeInMinutes = ((distance / 1000)) / 1  //1 Km/min

        //console.log('dateTime >>>', dateTime)
        let dateTimeMoment = moment(dateTime, 'YYYY-MM-DD HH:mm:ss')
        let endTime = dateTimeMoment.add(timeInMinutes, 'minutes')
        let formattedEndTime = endTime.format('YYYY-MM-DD, hh:mm A')

        //console.log('dateTimeMoment >>>', dateTimeMoment)
        // console.log('endTime >>>', endTime)
        //console.log('formattedEndTime >>>', formattedEndTime)
        return formattedEndTime
    }

    // static async requestAndroidLocationPermission() {

    //     return new Promise(function (resolve, reject) {

    //         try {
    //             const granted = await PermissionsAndroid.request(
    //                 PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    //                 {
    //                     title: 'Mediport',
    //                     message: String(localize.mediportNeedsAccessToLocation),
    //                     buttonNeutral: localize.askMeLater,
    //                     buttonNegative: localize.cancel,
    //                     buttonPositive: localize.ok,
    //                 }
    //             );
    //             if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //                 console.log('You can use the location');
    //                 resolve()
    //             } else {
    //                 console.log('location permission denied');
    //                 reject()
    //             }
    //         } catch (err) {
    //             console.warn(err);
    //         }
    //     })
    // }
}

