import React from 'react';
import { Dimensions, StyleSheet } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker, } from 'react-native-maps';

const Map = (props: any) => {

    console.log('Props Markers >>>', props.markers)

    return (
        <MapView
            provider={PROVIDER_GOOGLE}
            style={[styles.map, (props.height != null ? {height: props.height} : null), (props.marginTop != null ? {marginTop: props.marginTop} : null)]}
            showsUserLocation={true}
            region={{
                latitude: ((props.currentLocation != null) ? props.currentLocation.location.latitude : (props.markers[0] != null ? props.markers[0].lat : 0)),
                longitude: ((props.currentLocation != null) ? props.currentLocation.location.longitude : (props.markers[0] != null ? props.markers[0].long : 0)),
                latitudeDelta: 0.015 * 5, //0.0922 * 1.5,
                longitudeDelta: 0.0121 * 5, //0.0421 * 1.5,
            }}

            onPress={event => props.func(event.nativeEvent.coordinate)}
        >
            {props.markers.map((marker: any, index: any) => (
                <Marker
                    coordinate={{
                        latitude: marker.lat, longitude: marker.long
                    }}
                    title={marker.state}
                    image={require('../assets/locMarker.png')}
                    key={index}
                    onPress={(e) => { e.stopPropagation(); props.markerClick(index) }}
                />))}
        </MapView>
    )
}

export default Map;

const styles = StyleSheet.create({
    map: {
        //flex: 1,
        width: '100%',
       // marginBottom: 0,
        marginTop: 20
    }
})
