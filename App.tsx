/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import 'react-native-gesture-handler';
import React, { Component } from 'react';
import AppContainer from './src/navigationConfig/AppContainer';
import { Provider } from 'react-redux';
import store from './src/Redux/store';
import firebase from 'react-native-firebase';
import NetInfo from "@react-native-community/netinfo";
import General from './src/Utils/General';
import { internetConnectionError } from './src/constants/messages';
import { updateUserToken } from './src/Firebase/FirestoreHandler';
//import { NavigationContainer } from '@react-navigation/native';



declare var global: { HermesInternal: null | {} };

export let token: string = '';

class App extends Component {

  state = {
    tokenStatus: false,
  }

  componentDidMount() {
    console.disableYellowBox = true;

    console.log('componentDidMount func called')

    const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max).setDescription('My apps test channel');
    firebase.notifications().android.createChannel(channel);

    console.log('Channel craeted >>>', channel)

    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.checkPermission();
      }
      else {
        General.showErroMsg(this, internetConnectionError)
      }
    });
  }

  checkPermission = async () => {
    const enabled = await firebase.messaging().hasPermission();

    console.log('checkPermission called is enabled>>>', enabled)
    if (enabled) {
      this.getFcmToken();
    } else {
      this.requestPermission();
    }
  }

  getFcmToken = async () => {
    
    const fcmToken = await firebase.messaging().getToken();

    console.log('FCM token >>>>>', fcmToken)
    if (fcmToken) {
      token = fcmToken
     // updateUserToken(fcmToken)
    } else {
    }
    this.setState({ tokenStatus: true })
  }

  requestPermission = async () => {

    console.log('requestPermission called')
    try {
      await firebase.messaging().requestPermission();
      this.getFcmToken()
    } catch (error) {
      this.setState({ tokenStatus: true })
    }
  }

  render() {
    return (
     
      <Provider store={store}>
        {
          this.state.tokenStatus ?
            <AppContainer />
            : null
        }
      </Provider>
      
    );
  }
};

export default App;
